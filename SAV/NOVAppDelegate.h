//
//  NOVAppDelegate.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/23/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController_ipad.h"

@interface NOVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController * navController;

@property (strong, nonatomic) LoginViewController * loginViewController;

@end
