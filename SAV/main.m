//
//  main.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/23/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NOVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NOVAppDelegate class]));
        
    }
}
