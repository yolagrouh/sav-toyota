//
//  NOVInstanceViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVInstanceViewController.h"
#import "NOVGridCell.h"

@interface NOVInstanceViewController_ipad : NOVInstanceViewController<TMQuiltViewDataSource, TMQuiltViewDelegate, NOVGridCellDelegate>


@property (weak, nonatomic) IBOutlet NOVButton *validerBtn;


- (IBAction)annuler:(id)sender;

- (IBAction)afficherFavoris:(id)sender;



@end
