#import "NOVInstanceViewController_ipad.h"
#import "TMQuiltViewController.h"
#import "JBCountdownLabel.h"


@interface NOVInstanceViewController_ipad (){
    
    
    TMQuiltViewController * gridController;
    
}

@property(nonatomic, strong)NSArray * instanceListe;

@end

@implementation NOVInstanceViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.instanceListe = [Utils getSavedMutableObject:@"SAVED_DATA"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideFromFav) name:SHOW_TADDITIONNELS_NOTIFICATION object:nil];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    gridController = [[TMQuiltViewController alloc]init];
    
    gridController.view.frame = CGRectMake(0, 80, self.view.frame.size.width, self.view.frame.size.height - 114);
    
    gridController.quiltView.delegate = self;
    
    gridController.quiltView.dataSource = self;
    
    gridController.quiltView.backgroundColor =[UIColor clearColor];
    
    [self.view addSubview:gridController.view];
    
    [gridController.quiltView reloadData];
    
    [self.view bringSubviewToFront:self.validerBtn];
    
}


#pragma mark - QuiltViewControllerDataSource

- (NSInteger)quiltViewNumberOfCells:(TMQuiltView *)TMQuiltView {
    
    return [self.instanceListe count];
    
}

- (TMQuiltViewCell *)quiltView:(TMQuiltView *)quiltView cellAtIndexPath:(NSIndexPath *)indexPath {
    
    NOVGridCell *cell = (NOVGridCell *)[quiltView dequeueReusableCellWithReuseIdentifier:@"NOVGridCell"];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle]loadNibNamed:@"NOVGridCell" owner:nil options:nil]objectAtIndex:0];
    }
    
    cell.tag = indexPath.row;
    
    cell.delegate= self;
    
    cell.historiqueBtn.hidden = NO;
    
    NSDictionary * rdv = [self.instanceListe objectAtIndex:indexPath.row];
    
    if (![[rdv objectForKey:@"NOM_CLIENT"] isKindOfClass:[NSNull class]]) {
        
        cell.nomLabel.text          = [rdv objectForKey:@"NOM_CLIENT"];
        
    }else{
        
        cell.nomLabel.text          = @"";
    }
    
    if (![[rdv objectForKey:@"IMMATRICULE"] isKindOfClass:[NSNull class]]) {
        
        cell.immatriculeLabel.text  = [rdv objectForKey:@"IMMATRICULE"];
        
    }else{
        
        cell.immatriculeLabel.text  = @"";
        
    }
    
    
    if ([rdv objectForKey:@"TEL"]) {
        
        cell.telProprietaire.text =[rdv objectForKey:@"TEL"];
        
    }else{
        
        cell.telProprietaire.text = @"";
    }
    
    cell.contactName.text = [rdv objectForKey:@"CONTACT_NOM"];
    cell.contactTel.text = [rdv objectForKey:@"CONTACT_TEL"];
    
    NSString * travaux = @"";
    
    for (NSDictionary * travail in [rdv objectForKey:@"ALL_WORKS"]) {
        
        travaux = [NSString stringWithFormat:@"%@, %@",travaux, [travail objectForKey:@"TRAVAIL"]];
    }
    
    travaux = [travaux substringFromIndex:2];
    
    cell.travauxLabel.text  = travaux;
    
    cell.dateLabel.text = [NSString stringWithFormat:@"%@ %@", [rdv objectForKey:@"DATE_LIVRAISON"],[rdv objectForKey:@"HEURE_LIVRAISON"]];
    
    
    NSString * dateLivraisonString = [NSString stringWithFormat:@"%@ %@", [rdv objectForKey:@"DATE_LIVRAISON"], [rdv objectForKey:@"HEURE_LIVRAISON"]];
    
    NSDate * dateLivraison = [Utils dateFromString:dateLivraisonString dateFormat:@"yyy-MM-dd HH:mm"];
    
    NSInteger seconds = [dateLivraison timeIntervalSinceDate:[NSDate date]];
    
    if (seconds > 0) {
        
        
        JBCountdownLabel * countdownLabel = [[JBCountdownLabel alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 36, cell.frame.size.width - 30, 46) format:@"%@" time:seconds delegate:nil];
        countdownLabel.textColor = [UIColor colorWithRed:0.48 green:0.63 blue:0.07 alpha:1];
        countdownLabel.textAlignment = NSTextAlignmentRight;
        [cell addSubview:countdownLabel];
        
    }else{
        
        UILabel * timeExeeded = [[UILabel alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 36, cell.frame.size.width - 30, 46) ];
        
        timeExeeded.text = @"Date de livraison dépassée";
        
        timeExeeded.textAlignment = NSTextAlignmentRight;
        
        timeExeeded.font = [UIFont fontWithName:@"Arial" size:14];
        
        timeExeeded.textColor = [UIColor redColor];
        
        [cell addSubview:timeExeeded];
        
    }
    
    return cell;
}

-(void)quiltView:(TMQuiltView *)quiltView didSelectCellAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary * rdv = [self.instanceListe objectAtIndex:indexPath.row];
    
    [self.delegate nOVInstanceViewController:self selectedRDV:rdv];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
    }];
    
}

#pragma mark - TMQuiltViewDelegate

- (NSInteger)quiltViewNumberOfColumns:(TMQuiltView *)quiltView {
    
    
    return 3;
}

- (CGFloat)quiltView:(TMQuiltView *)quiltView heightForCellAtIndexPath:(NSIndexPath *)indexPath {
    
    return 344;
}
- (IBAction)annuler:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = CGRectMake(0, - self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
    }];
}

- (IBAction)afficherFavoris:(id)sender {
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_FAVORIS object:nil];
    
    //    [UIView animateWithDuration:0.3 animations:^{
    //
    //        self.view.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    //
    //    } completion:^(BOOL finished) {
    //
    //        [self.view removeFromSuperview];
    //
    //    }];
}

#pragma mark --
#pragma mark HistoriqueDelegate
-(void)nOVGridCell:(NOVGridCell *)nOVGridCell showHistorique:(NSInteger)index{
    
    NSDictionary * rdv = [self.instanceListe objectAtIndex:index];
    
    [self.delegate nOVInstanceViewController:self showHistorique:rdv];
    
    //    [UIView animateWithDuration:0.3 animations:^{
    //
    //        self.view.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    //
    //    } completion:^(BOOL finished) {
    //
    //        [self.view removeFromSuperview];
    //
    //    }];
}


-(void)hideFromFav{
    
    [self annuler:self];
    
}

@end
