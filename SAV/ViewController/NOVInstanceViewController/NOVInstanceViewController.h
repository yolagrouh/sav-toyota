//
//  NOVInstanceViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVGradientViewController.h"

@class NOVInstanceViewController;

@protocol NOVInstanceViewControllerDelegate <NSObject>

@required

-(void)nOVInstanceViewController:(NOVInstanceViewController *)nOVInstanceViewController selectedRDV:(NSDictionary *)rdv;

-(void)nOVInstanceViewController:(NOVInstanceViewController *)nOVInstanceViewController showHistorique:(NSDictionary *)rdv;

@end

@interface NOVInstanceViewController : NOVGradientViewController

@property(nonatomic, weak)id<NOVInstanceViewControllerDelegate>delegate;

@end
