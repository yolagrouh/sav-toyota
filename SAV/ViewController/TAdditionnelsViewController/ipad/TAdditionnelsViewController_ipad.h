//
//  TAdditionnelsViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAdditionnelsViewController.h"
#import "NOVTAdditionnelView.h"
#import "NOVCAccordViewController_ipad.h"
#import "NOVRelanceOptionsViewController_ipad.h"

@interface TAdditionnelsViewController_ipad : TAdditionnelsViewController<NOVTAdditionnelViewDelegate, NOVCAccordViewControllerDelegate, NOVRelanceOptionsViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *clientLabel;

@property (weak, nonatomic) IBOutlet NOVBlockView *viewContainer;

@property (weak, nonatomic) IBOutlet UIView *recommendationsContainer;

@property (weak, nonatomic) IBOutlet UIView *tAdditionnelsView;


@property (weak, nonatomic) IBOutlet UIButton *addTravauxBtn;

@property (weak, nonatomic) IBOutlet NOVButton *validerBtn;


- (IBAction)valider:(id)sender;

- (IBAction)addTravaux:(id)sender;
@end
