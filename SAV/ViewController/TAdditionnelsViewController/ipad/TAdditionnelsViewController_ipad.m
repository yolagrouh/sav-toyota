#import "TAdditionnelsViewController_ipad.h"
#import "LivraisonViewController_ipad.h"

@interface TAdditionnelsViewController_ipad (){
    
    NSInteger numberOfRequests;
    
    NSInteger requestsCount;
}

@property(nonatomic, strong) LivraisonViewController_ipad * livraisonViewController;
@property(nonatomic, strong) NOVCAccordViewController_ipad * accordClientViewController ;
@property(nonatomic, strong)  NOVCAccordViewController_ipad * nOVCAccordViewController;
@property(nonatomic, strong)  NOVRelanceOptionsViewController_ipad * nOVRelanceOptionsViewController;



@property(nonatomic, strong)NSMutableArray * suggestionsResponse;
@property(nonatomic, strong)NSMutableArray * suggestions;

@end

@implementation TAdditionnelsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        numberOfRequests = 0;
        
        requestsCount = 0;
        
        self.suggestions = [NSMutableArray array];
        
        self.suggestionsResponse = [NSMutableArray array];
        
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getSavedTravaux];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    [self doGet:[NSString stringWithFormat:@"%@%@",TRAVAUX_ADDITIONELS_WS, sharedData.katashiki] requestID:3];

}

-(void)setUpView{
    
    [super setUpView];
    
    NOVSharedData * sharedDate = [NOVSharedData sharedNOVData];
    
    self.clientLabel.text = sharedDate.nomClient;
    
    NOVTAdditionnelView * view = [[[NSBundle mainBundle]loadNibNamed:@"NOVTAdditionnelView" owner:nil options:nil] objectAtIndex:0];
    
    view.tag = 0;
    
    view.suggestions = self.suggestions;
    
    view.selectSwitch.layer.cornerRadius = view.selectSwitch.frame.size.height/2;
    
    //view.travailLabel.suggestions = self.suggestions;
    
    [Utils addBorder:view.raisonsTV borderColor:[UIColor colorWithRed:178.0/255 green:191.0/255 blue:210.0/255 alpha:1] width:1];
    
    view.delegate = self;
    
    [self.tAdditionnelsView addSubview:view];
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.viewContainer.frame.size.height);
}

- (IBAction)valider:(id)sender {
    
    if (!self.isOnline) {
        
        [Utils showAlertView:@"Veuillez vous assurer que vous disposez d'une connexion internet avant de continuer."];
        
        return;
        
    }
    
    for (NOVTAdditionnelView * view in [self.tAdditionnelsView subviews]) {
        
        [view.travailLabel resignFirstResponder];
        [view.coutLabel resignFirstResponder];
        
    }

    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    if([sharedData.travauxAdditionnels count] > 0){
        
        self.nOVCAccordViewController = [[NOVCAccordViewController_ipad alloc]initWithNibName:@"NOVCAccordViewController" bundle:nil];
        
        self.nOVCAccordViewController.delegate = self;
        
        
        [self.view addSubview:self.nOVCAccordViewController.view];
        
        [self.nOVCAccordViewController show];
        
    }else{
        
        [self showValidation:@"Ecran livraison" cancelBtn:@"Dossiers en cours" title:@"Voulez-vous retourner à"];
    }
    
   
   /* NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]] ;
    
    if (!savedRDV) {
        
        savedRDV = [NSMutableArray array];
    }
    
    [savedRDV addObject:[[NOVSharedData sharedNOVData] createArchivableObject]];
    
    [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];*/
}

- (IBAction)addTravaux:(id)sender {
    
    UIView * lastFiels = [[self.tAdditionnelsView subviews] lastObject];
    
    CGRect  frame = lastFiels.frame;
    
    NOVTAdditionnelView * view = [[[NSBundle mainBundle]loadNibNamed:@"NOVTAdditionnelView" owner:nil options:nil] objectAtIndex:0];
    
    view.isCollappsed = NO;

    view.suggestions = self.suggestions;
    
    view.suggestionsObjects = self.suggestionsResponse;
    
    view.tag = [[self.tAdditionnelsView subviews] count] ;
    
    view.delegate = self;
    
    view.selectSwitch.layer.cornerRadius = view.selectSwitch.frame.size.height/2;
    
    view.frame = CGRectMake(self.tAdditionnelsView.frame.size.width, frame.origin.y + frame.size.height , frame.size.width, frame.size.height);

   // [view.travailLabel becomeFirstResponder];
    
    [self.tAdditionnelsView addSubview:view];
    
    [Utils addBorder:view.raisonsTV borderColor:[UIColor colorWithRed:178.0/255 green:191.0/255 blue:210.0/255 alpha:1] width:1];
    
    [UIView animateWithDuration:0.3 animations:^{
        
         self.tAdditionnelsView.frame = CGRectMake(self.tAdditionnelsView.frame.origin.x, self.tAdditionnelsView.frame.origin.y, self.tAdditionnelsView.frame.size.width, self.tAdditionnelsView.frame.size.height + COLLAPSED_HEIGHT);
        
        self.recommendationsContainer.frame = CGRectMake(self.recommendationsContainer.frame.origin.x, self.recommendationsContainer.frame.origin.y + COLLAPSED_HEIGHT, self.recommendationsContainer.frame.size.width, self.recommendationsContainer.frame.size.height);
        
        self.validerBtn.frame = CGRectMake(self.validerBtn.frame.origin.x, self.validerBtn.frame.origin.y + COLLAPSED_HEIGHT, self.validerBtn.frame.size.width, self.validerBtn.frame.size.height );
        
        self.viewContainer.frame = CGRectMake(self.viewContainer.frame.origin.x, self.viewContainer.frame.origin.y, self.viewContainer.frame.size.width, self.viewContainer.frame.size.height + COLLAPSED_HEIGHT);
    
        view.frame =  CGRectMake(0, view.frame.origin.y, frame.size.width, frame.size.height);
        
        //Translate Btn
        
        /*NOVTAdditionnelView * view  = [[self.tAdditionnelsView subviews] objectAtIndex:[[self.tAdditionnelsView subviews] count] -1];
        self.addTravauxBtn.frame = CGRectMake(self.addTravauxBtn.frame.origin.x, self.addTravauxBtn.frame.origin.y + 58, self.addTravauxBtn.frame.size.width, self.addTravauxBtn.frame.size.height );*/
        
        
    } completion:^(BOOL finished) {
        
        
    }];
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.viewContainer.frame.size.height + 20);
}


#pragma mark --
#pragma mark Data

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    if (requestID == 3){
        
        NSLog(@"Response %@", [response description]);
        
        [self.suggestionsResponse removeAllObjects];
        
        [self.suggestionsResponse addObjectsFromArray:[response objectForKey:@"result"]];
        
        for (id prix in self.suggestionsResponse) {
            
            [self.suggestions addObject:[prix objectForKey:@"OP_DESCRIPTION"]];
            
        }
        
        NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
        
        sharedData.requestsList = [NSMutableArray arrayWithArray:[response objectForKey:@"result"]];
        
        NOVTAdditionnelView * view = [[self.tAdditionnelsView subviews] objectAtIndex:0];
        
       // view.travailLabel.suggestions = self.suggestions;
        
        for (NOVTAdditionnelView * view in [self.tAdditionnelsView subviews]) {
            
            view.suggestionsObjects = self.suggestionsResponse;
            
    
            
        }
     
        return;
    }
    
//    requestsCount ++;
//    
//    if (requestsCount == numberOfRequests) {
//        
//        [self showValidation:@"Ecran livraison" cancelBtn:@"Dossiers en cours" title:@"Voulez-vous retourner à"];
//        
//    }

        [self showValidation:@"Ecran livraison" cancelBtn:@"Dossiers en cours" title:@"Voulez-vous retourner à"];
    
}

-(void)sendData{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSString * timeStamp = sharedData.timeStamp;
    
    
    numberOfRequests = [[self.tAdditionnelsView subviews] count];
    
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    NSInteger index = 0;
    
    for (NOVTAdditionnelView * view in [self.tAdditionnelsView subviews]) {
        
        if (view.opCode) {
            
            NSString * taskName = [view.opCode  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            
            NSString * taskDescription = [view.travailLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSString * statut ;
            
            view.selectSwitch.isOn ? (statut = @"No" ): (statut = @"Yes");
            
            NSString * requete = [NSString stringWithFormat:@"%@field_IPAD_NUMBER=%@&field_TASK_NAME=%@&field_TASK_DESCRIPTION=%@&field_TASK_TYPE=%@&field_STATUS=New&field_CONFIRMATION=%@&field_MESSAGE=message",TASK_SAVE_WS,timeStamp,taskName, taskDescription,taskName, statut];
            
            
            
            [params setObject:timeStamp             forKey:[NSString stringWithFormat:@"tasks[%d][IPAD_NUMBER]",index]];
            [params setObject:taskName              forKey:[NSString stringWithFormat:@"tasks[%d][TASK_NAME]",index]];
            [params setObject:taskDescription       forKey:[NSString stringWithFormat:@"tasks[%d][TASK_DESCRIPTION]",index]];
            [params setObject:taskName              forKey:[NSString stringWithFormat:@"tasks[%d][TASK_TYPE]",index]];
            [params setObject:statut                forKey:[NSString stringWithFormat:@"tasks[%d][CONFIMATION]",index]];
            [params setObject:@"New"                forKey:[NSString stringWithFormat:@"tasks[%d][STATUS]",index]];
            [params setObject:@""                   forKey:[NSString stringWithFormat:@"tasks[%d][MESSAGE]",index]];
            
            
            //[self doGet:requete requestID:[[self.tAdditionnelsView subviews] indexOfObject:view]];
            index++;
        }
        
        
    }
    
    if (index> 0) {
        
        [self doPost:TASK_INSERT_WS params:params requestID:999];
        
    }else{
        
        [self showValidation:@"Ecran livraison" cancelBtn:@"Dossiers en cours" title:@"Voulez-vous retourner à"];
    }
    
    
}

#pragma mark --
#pragma mark ViewDelegate 

-(void)nOVTAdditionnelView:(NOVTAdditionnelView *)nOVTAdditionnelView choose:(BOOL)chooses{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSMutableDictionary * tAdd;
    
    NSInteger index = 0;
    
    for (NSDictionary * tAdditionel in sharedData.travauxAdditionnels ) {
        
        if ([[tAdditionel objectForKey:@"TRAVAIL"] isEqualToString:nOVTAdditionnelView.travail]) {
            
            tAdd = [NSMutableDictionary dictionaryWithDictionary:tAdditionel];
            
            index = [sharedData.travauxAdditionnels indexOfObject:tAdditionel];
            
            break;
            
        }
        
    }

    
    if (chooses) {
        
        [tAdd setObject:@"OUI" forKey:@"STATUS"];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            for (int i= nOVTAdditionnelView.tag + 1 ; i < [[self.tAdditionnelsView subviews] count]; i++) {
                
                UIView * view = [[self.tAdditionnelsView subviews] objectAtIndex:i];
                
                view.frame = CGRectMake(0, view.frame.origin.y - EXPANDED_HEIGHT +  COLLAPSED_HEIGHT  , view.frame.size.width, view.frame.size.height);
                
            }
            
            
            self.tAdditionnelsView.frame = CGRectMake(self.tAdditionnelsView.frame.origin.x, self.tAdditionnelsView.frame.origin.y, self.tAdditionnelsView.frame.size.width, self.tAdditionnelsView.frame.size.height - EXPANDED_HEIGHT +  COLLAPSED_HEIGHT);
            
            
            self.viewContainer.frame = CGRectMake(self.viewContainer.frame.origin.x, self.viewContainer.frame.origin.y, self.viewContainer.frame.size.width, self.viewContainer.frame.size.height - EXPANDED_HEIGHT +  COLLAPSED_HEIGHT);
            
            
            self.validerBtn.frame = CGRectMake(self.validerBtn.frame.origin.x, self.validerBtn.frame.origin.y - EXPANDED_HEIGHT +  COLLAPSED_HEIGHT, self.validerBtn.frame.size.width, self.validerBtn.frame.size.height );
            
        }];
        
    }else{
        
        [tAdd setObject:@"NON" forKey:@"STATUS"];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            for (int i= nOVTAdditionnelView.tag + 1 ; i < [[self.tAdditionnelsView subviews] count]; i++) {
                
                UIView * view = [[self.tAdditionnelsView subviews] objectAtIndex:i];
                
                view.frame = CGRectMake(0, view.frame.origin.y + EXPANDED_HEIGHT - COLLAPSED_HEIGHT  , view.frame.size.width, view.frame.size.height);
                
            }
            
            self.tAdditionnelsView.frame = CGRectMake(self.tAdditionnelsView.frame.origin.x, self.tAdditionnelsView.frame.origin.y, self.tAdditionnelsView.frame.size.width, self.tAdditionnelsView.frame.size.height + EXPANDED_HEIGHT -  COLLAPSED_HEIGHT);
            
            
            self.viewContainer.frame = CGRectMake(self.viewContainer.frame.origin.x, self.viewContainer.frame.origin.y, self.viewContainer.frame.size.width, self.viewContainer.frame.size.height + EXPANDED_HEIGHT -  COLLAPSED_HEIGHT);
            
            
            self.validerBtn.frame = CGRectMake(self.validerBtn.frame.origin.x, self.validerBtn.frame.origin.y + EXPANDED_HEIGHT -  COLLAPSED_HEIGHT, self.validerBtn.frame.size.width, self.validerBtn.frame.size.height );
            
        }];
        
        
    }
    
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.viewContainer.frame.size.height + 20);

    
    [sharedData.travauxAdditionnels replaceObjectAtIndex:index withObject:tAdd];
}

-(void)nOVTAdditionnelView:(NOVTAdditionnelView *)nOVTAdditionnelView travail:(NSString *)travail prix:(NSString *)prix{
    
    BOOL found = NO;
    
    NSInteger indice = 0;
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    for (NSDictionary * tAdditionel in sharedData.travauxAdditionnels ) {
        
        if ([[tAdditionel objectForKey:@"TRAVAIL"] isEqualToString:travail]) {
            
            indice = [sharedData.travauxAdditionnels indexOfObject:tAdditionel];
            
            found = YES;
            
            break;
            
        }
        
    }
    
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:travail,@"TRAVAIL", prix, @"PRIX",@"OUI",@"STATUS", nil];
    
    if (found) {
        
        [sharedData.travauxAdditionnels replaceObjectAtIndex:indice withObject:dict];

    }else{
        
        [sharedData.travauxAdditionnels addObject:dict];
        
    }
    
}

-(void)nOVTAdditionnelView:(NOVTAdditionnelView *)nOVTAdditionnelView showRaison:(UIButton *)sender{
    
    self.nOVRelanceOptionsViewController = [[NOVRelanceOptionsViewController_ipad alloc]initWithNibName:@"NOVRelanceOptionsViewController" bundle:nil];
    
    
    self.nOVRelanceOptionsViewController.raisons = [NSMutableArray arrayWithObjects:@"Raison 1",@"Raison 2",@"Raison 3",@"Raison 4", @"Raison 5",@"Autre", nil];
    
    
    self.nOVRelanceOptionsViewController.selectionMode = PopUpModeMultiple;
    
    self.nOVRelanceOptionsViewController.delegate = self;
    
    self.nOVRelanceOptionsViewController.view.tag = sender.tag;
    
    self.nOVRelanceOptionsViewController.owner   = nOVTAdditionnelView.tag;
    
    [self.view addSubview:self.nOVRelanceOptionsViewController.view];
    
    [self.nOVRelanceOptionsViewController show];
   
}


#pragma mark --
#pragma mark Raison Options Delegate

-(void)nOVRelanceOptionsViewController:(NOVRelanceOptionsViewController *)nOVRelanceOptionsViewController selected:(NSString *)raison{
    
    /***GET DATA**/
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSMutableDictionary * tAdditionel = [NSMutableDictionary dictionaryWithDictionary: [sharedData.travauxAdditionnels objectAtIndex:nOVRelanceOptionsViewController.owner]];
    
    [tAdditionel setObject:@"NON" forKey:@"STATUS"];
    
    
    /***GET DATA**/
    
    NSLog(@"TAG  %d",nOVRelanceOptionsViewController.owner);
    
    NOVTAdditionnelView * view = [[self.tAdditionnelsView subviews] objectAtIndex:nOVRelanceOptionsViewController.owner];
    
    
    switch (nOVRelanceOptionsViewController.view.tag ) {
        case 0:
            
            view.raisonsTV.text = raison;
            
            [tAdditionel setObject:raison forKey:@"RAISON1"];
            
            break;
            
        case 1:
            
            view.raisonsTV.text = raison;
            
            [tAdditionel setObject:raison forKey:@"RAISON2"];
            
            break;
            
        default:
            break;
    }
    
    
    [sharedData.travauxAdditionnels replaceObjectAtIndex:nOVRelanceOptionsViewController.owner withObject:tAdditionel];
    
}

#pragma mark --
#pragma mark NOVAccord client 

-(void)nOVCAccordViewController:(NOVCAccordViewController *)nOVCAccordViewController accord:(BOOL)accord{
    
    if (accord) {
        
        [self saveTravauxAdditionnels];
        [self sendData];
        
    }
    
}


#pragma mark --
#pragma mark Validation

-(void)showCancel{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_RELANCES_VIEW object:nil];
}


-(void)showNext{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    [self saveTravauxAdditionnels];
    
    
    self.livraisonViewController = [[LivraisonViewController_ipad alloc]initWithNibName:@"LivraisonViewController" bundle:nil];
    
//    self.livraisonViewController.view.frame = CGRectMake(0, - self.livraisonViewController.view.frame.size.height,  self.livraisonViewController.view.frame.size.width,  self.livraisonViewController.view.frame.size.height);
    
   //[self.view addSubview: self.livraisonViewController.view];
    
//    [UIView animateWithDuration:0.3 animations:^{
//        
//        
//        self.livraisonViewController.view.frame = CGRectMake(0, 0 , self.livraisonViewController.view.frame.size.width,  self.livraisonViewController.view.frame.size.height);
//    }];
    
    [self.navigationController pushViewController:self.livraisonViewController animated:YES];
    
}

-(NSInteger )getCode:(NSString *)request{
    
    NSInteger index = -1;
    
    for (id prix in self.suggestionsResponse) {
        
        if ([[prix objectForKey:@"OP_CODE"] isEqualToString:request]) {
            
            index = [self.suggestionsResponse indexOfObject:prix];
            
            break;
            
        }
        
    }
    
    return index;
}


#pragma mark --
#pragma mark get Saved Travaux 

-(void)saveTravauxAdditionnels{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]] ;
    
    NSDictionary * dataToSave = [sharedData createArchivableObject];
    
    NSInteger index = -1;
    
    for (id data in savedRDV) {
        
        if ([[data objectForKey:@"TIME_STAMP"] isEqualToString:sharedData.timeStamp]) {
            
            index = [savedRDV indexOfObject:data];
            
            break;
            
        }
        
    }
    if (!savedRDV) {
        
        savedRDV = [NSMutableArray array];
    }

    
    if (index >= 0) {
        
        [savedRDV replaceObjectAtIndex:index withObject:dataToSave];
        
    }
    
    [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];
    
}

-(void)getSavedTravaux{
    
        NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
        NSMutableArray * relances = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]];
    
        for (id data in relances) {
        
            if ([[data objectForKey:@"TIME_STAMP"] isEqualToString:sharedData.timeStamp]) {
            
                sharedData.travauxAdditionnels = [NSMutableArray arrayWithArray:[data objectForKey:@"TRAVAUX_ADDITIONNELS"]];
            
            }
        
        }
    
}

@end
