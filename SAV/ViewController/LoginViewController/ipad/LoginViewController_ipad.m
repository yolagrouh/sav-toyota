#import "LoginViewController_ipad.h"
#import "VinViewController_ipad.h"
#import "NOVMainViewController_ipad.h"
#import <Crashlytics/Crashlytics.h>
#import "NOVSharedUSer.h"
#import "NSString+MD5.h"

@interface LoginViewController_ipad ()

@property(nonatomic, strong)NSMutableArray * users;

@end

@implementation LoginViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    
}

#pragma mark --
#pragma mark Setup

-(void)setUpView{
    
    [super setUpView];
    
    ///
    
    
    UIColor * startColor = [UIColor colorWithRed:33.0/255 green:43.0/255 blue:50.0/255 alpha:1];
    
    UIColor * endColor = [UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    
    gradient.frame = self.view.bounds;
    
    gradient.colors = [NSArray arrayWithObjects:(id)[startColor CGColor], (id)[endColor CGColor], nil];
    
    [self.forgotPWDView
     .layer insertSublayer:gradient atIndex:0];
    
    ///
    
    self.users = [NSMutableArray array];
    
    NSDictionary * user1 = [NSDictionary dictionaryWithObjectsAndKeys:@"BMEROUAN",@"field_login_commercial",@"1234",@"field_password_commercial",@"Mr. BRAHIM MEROUAN",@"nom", nil];
    NSDictionary * user2 = [NSDictionary dictionaryWithObjectsAndKeys:@"HGANNOUNE",@"field_login_commercial",@"e10adc3949ba59abbe56e057f20f883e",@"field_password_commercial",@"HAMID GANNOUNE",@"nom", nil];
    NSDictionary * user3 = [NSDictionary dictionaryWithObjectsAndKeys:@"MHARTI",@"field_login_commercial",@"1234",@"field_password_commercial",@"MUSTAPHA HARTI",@"nom", nil];
    NSDictionary * user4 = [NSDictionary dictionaryWithObjectsAndKeys:@"ALOUARDI",@"field_login_commercial",@"1234",@"field_password_commercial",@"Mr. ADIL LOUARDI",@"nom", nil];
    
    NSDictionary * user5 = [NSDictionary dictionaryWithObjectsAndKeys:@"admin",@"field_login_commercial",@"admin",@"field_password_commercial",@"Admin",@"nom", nil];
//    
//    [self.users addObject:user1];
//    [self.users addObject:user2];
//    [self.users addObject:user3];
//    [self.users addObject:user4];
//    [self.users addObject:user5];
    
    [self doGet:SA_LIST requestID:0];
    
}

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    if (requestID == 0) {
        
        
        [self.users addObjectsFromArray:response];
        
    }
    
    
}

#pragma mark --
#pragma mark Actions

- (IBAction)login:(id)sender {
    
   //[[Crashlytics sharedInstance] crash];
    
    //[NSDictionary dictionaryWithObject:nil forKey:@"NULL"];

    if ([self.loginTF.text length] == 0 || [self.passwordTF.text length] == 0) {
        
        [Utils showAlertView:@"Veuillez rensigner votre login et mot de passe"];
        
        return;
        
    }
    
    BOOL found = NO;
    
    for (NSDictionary * user in self.users) {
        
        if ([[user objectForKey:@"field_login_commercial"] isEqualToString:self.loginTF.text] &&[[user objectForKey:@"field_password_commercial"] isEqualToString:[self.passwordTF.text MD5]]   ) {
            
            NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
            
            sharedData.sa = [user objectForKey:@"field_nom_commercial"];
            
            //sharedData.workshop = @"MEGA SERVICE CENTER";//Change me later
            
            NOVSharedUSer * sharedUSer = [NOVSharedUSer sharedNOVUser];
            
            sharedUSer.name = [user objectForKey:@"field_nom_commercial"];
            
            sharedUSer.login = [user objectForKey:@"field_login_commercial"];
            
            sharedUSer.workshop = [user objectForKey:@"field_succursale_commercial"];//Change me later
            
            found = YES;
            
            break;
        }
        
    }
    
    if (!found) {
        
        [Utils showAlertView:@"Votre login ou mot de passe est incorrect"];
        
        return;
        
    }
    
//    VinViewController_ipad * vinViewController_ipad = [[VinViewController_ipad alloc]initWithNibName:@"VinViewController" bundle:nil];
//    
//    [self.navigationController pushViewController:vinViewController_ipad animated:YES];
    
    NOVMainViewController_ipad * mainViewController = [[NOVMainViewController_ipad alloc]initWithNibName:@"NOVMainViewController_sav" bundle:nil];
    
    [self.navigationController pushViewController:mainViewController animated:YES];

}

- (IBAction)resetPWD:(id)sender {
    
    if ([self.fLoginTF.text  length]==0) {
        
        [self showAlerte:@"Veuillez renseigner votre login"];
        
        return;
        
    }
    
    if (![self.fPWDTF.text isEqualToString:self.fCPWDTF.text]) {
        
        [self showAlerte:@"Les champs mot de passe et confirmation du mot de passe doivent correspondre"];
        
        return;
        
    }
    
    self.forgotPWDView.hidden = YES;
    
    
    NSString * request = [NSString stringWithFormat:@"%@&password=%@",self.fLoginTF.text , self.fPWDTF.text];
    
    [self doGet:[PWD_RESET_WS stringByAppendingString:request] requestID:99];
    
    
    [[[UIAlertView alloc]initWithTitle:@"TOYOTA SAV" message:@"Votre mot de passe a été modifié avec succès.\n Vous allez recevoir un lien de confirmation dans votre boite mail.\n Vous devez confirmer votre nouveau mot de passe et relancer l'application pour pouvoir l'utiliser." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

- (IBAction)cancel:(id)sender {
    
    self.forgotPWDView.hidden = YES;
}

- (IBAction)showPWDView:(id)sender {
    
    self.forgotPWDView.hidden =  NO;
}

#pragma mark --
#pragma mark Memory

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
    
}
@end
