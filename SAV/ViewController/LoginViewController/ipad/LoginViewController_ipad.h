//
//  LoginViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

@interface LoginViewController_ipad : LoginViewController


@property (weak, nonatomic) IBOutlet NOVButton *loginBtn;

@property (weak, nonatomic) IBOutlet NOVTextField *loginTF;

@property (weak, nonatomic) IBOutlet NOVTextField *passwordTF;

@property (weak, nonatomic) IBOutlet UIView *forgotPWDView;
@property (weak, nonatomic) IBOutlet UITextField *fLoginTF;
@property (weak, nonatomic) IBOutlet UITextField *fPWDTF;
@property (weak, nonatomic) IBOutlet UITextField *fCPWDTF;

- (IBAction)login:(id)sender;

- (IBAction)resetPWD:(id)sender;

- (IBAction)cancel:(id)sender;
- (IBAction)showPWDView:(id)sender;

@end
