//
//  NOVCRappelViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVCRappelViewController.h"

@interface NOVCRappelViewController_ipad : NOVCRappelViewController

@property (strong, nonatomic) NSString * rappel;

@property (weak, nonatomic) IBOutlet UILabel *rappelInfoLabel;

@property (weak, nonatomic) IBOutlet UILabel *rappelLabel;
@property (weak, nonatomic) IBOutlet UITextView *rappelTV;

@end
