#import "NOVCRappelViewController_ipad.h"

@interface NOVCRappelViewController_ipad ()

@end

@implementation NOVCRappelViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        

        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    self.rappelTV.editable = YES;
    
    [self.rappelTV setFont:[UIFont boldSystemFontOfSize:20]];
    
    self.rappelTV.editable = NO;
    
    if ([self.rappel length] == 0) {
        
        self.rappelInfoLabel.text = @"Aucune campagne de rappel n’a été enregistrée pour ce véhicule sur notre système" ;
        
        self.rappelTV.hidden = YES;
        
        return;
    }
    
    self.rappelLabel.text = self.rappel;
    self.rappelLabel.hidden = YES;
    
    self.rappelTV.text = self.rappel;
}


@end
