#import "NOVFavViewController_ipad.h"
#import "TMQuiltViewController.h"

@interface NOVFavViewController_ipad (){
    
    
    TMQuiltViewController * gridController;
    
}

@property(nonatomic, strong)NSMutableArray * bookmark;

@end

@implementation NOVFavViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	
        self.bookmark = [NSMutableArray arrayWithArray: [Utils getSavedMutableObject:BOOKMARK]];
        
    }
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];

    gridController = [[TMQuiltViewController alloc]init];
    
    gridController.view.frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 54);
    
    gridController.quiltView.delegate = self;
    
    gridController.quiltView.dataSource = self;
    
    gridController.quiltView.backgroundColor =[UIColor clearColor];
    
    [self.view addSubview:gridController.view];
    
    [gridController.quiltView reloadData];
}


#pragma mark - QuiltViewControllerDataSource

- (NSInteger)quiltViewNumberOfCells:(TMQuiltView *)TMQuiltView {
    
    return [self.bookmark count];
    
}

- (TMQuiltViewCell *)quiltView:(TMQuiltView *)quiltView cellAtIndexPath:(NSIndexPath *)indexPath {
    
    NOVGridCell *cell = (NOVGridCell *)[quiltView dequeueReusableCellWithReuseIdentifier:@"NOVGridCell"];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle]loadNibNamed:@"NOVGridCell" owner:nil options:nil]objectAtIndex:0];
    }
    
    cell.tag = indexPath.row;
    
    cell.delegate= self;
    
    cell.historiqueBtn.hidden = NO;
    
    NSDictionary * rdv = [self.bookmark objectAtIndex:indexPath.row];
    
    if (![[rdv objectForKey:@"NOM_CLIENT"] isKindOfClass:[NSNull class]]) {
        
        cell.nomLabel.text          = [rdv objectForKey:@"NOM_CLIENT"];
        
    }else{
        
        cell.nomLabel.text          = @"";
    }
    
    if (![[rdv objectForKey:@"IMMATRICULE"] isKindOfClass:[NSNull class]]) {
        
        cell.immatriculeLabel.text  = [rdv objectForKey:@"IMMATRICULE"];
        
    }else{
        
        cell.immatriculeLabel.text  = @"";
        
    }
    
    NSString * travaux = @"";
    
    for (NSDictionary * travail in [rdv objectForKey:@"ALL_WORKS"]) {
        
        travaux = [NSString stringWithFormat:@"%@, %@",travaux, [travail objectForKey:@"TRAVAIL"]];
    }
    
    travaux = [travaux substringFromIndex:2];
    
    cell.travauxLabel.text  = travaux;
    
    cell.dateLabel.text = [NSString stringWithFormat:@"%@ %@", [rdv objectForKey:@"DATE_LIVRAISON"],[rdv objectForKey:@"HEURE_LIVRAISON"]];
    
    NSString * dateLivraisonString = [NSString stringWithFormat:@"%@ %@", [rdv objectForKey:@"DATE_LIVRAISON"], [rdv objectForKey:@"HEURE_LIVRAISON"]];
    
    NSDate * dateLivraison = [Utils dateFromString:dateLivraisonString dateFormat:@"yyy-MM-dd HH:mm"];
    
    NSInteger seconds = [dateLivraison timeIntervalSinceDate:[NSDate date]];

    return cell;
}

-(void)quiltView:(TMQuiltView *)quiltView didSelectCellAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary * rdv = [self.bookmark objectAtIndex:indexPath.row];
    
   
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
         [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_TADDITIONNELS_NOTIFICATION object:rdv];
        
        [self.view removeFromSuperview];
        
    }];
    
}

#pragma mark - TMQuiltViewDelegate

- (NSInteger)quiltViewNumberOfColumns:(TMQuiltView *)quiltView {
    
    
    return 3;
}

- (CGFloat)quiltView:(TMQuiltView *)quiltView heightForCellAtIndexPath:(NSIndexPath *)indexPath {
    
    return 344;
}
- (IBAction)annuler:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = CGRectMake(0, - self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
    }];
    
   // [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TMQuiltViewDelegate


-(void)nOVGridCell:(NOVGridCell *)nOVGridCell showHistorique:(NSInteger)index{
    
    NSDictionary * rdv = [self.bookmark objectAtIndex:index];
    
    [self.delegate nOVFavViewController:self showHistorique:rdv];
    
   // [self hide];
    
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
