//
//  NOVFavViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVFavViewController.h"
#import "NOVGridCell.h"

@interface NOVFavViewController_ipad : NOVFavViewController<TMQuiltViewDataSource, TMQuiltViewDelegate, NOVGridCellDelegate>


@end
