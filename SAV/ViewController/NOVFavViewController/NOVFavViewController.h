//
//  NOVFavViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class NOVFavViewController;

@protocol NOVFavViewControllerDelegate <NSObject>

@required


-(void)nOVFavViewController:(NOVFavViewController *)nOVFavViewController showHistorique:(NSDictionary *)rdv;

@end


@interface NOVFavViewController : NOVParentPopUpViewController

@property(nonatomic, weak)id<NOVFavViewControllerDelegate>delegate;

@end
