#import "NOVCAccordViewController_ipad.h"

@interface NOVCAccordViewController_ipad ()

@end

@implementation NOVCAccordViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        self.shouldAddGradient = NO;
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    //[NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(valider:) userInfo:nil repeats:NO];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)valider:(id)sender {
    
    [super valider:sender];
    
    [self.delegate nOVCAccordViewController:self accord:YES];
    
}

- (IBAction)annuler:(id)sender {
    
    [super annuler:sender];
    
    
    [self.delegate nOVCAccordViewController:self accord:NO];

}
@end
