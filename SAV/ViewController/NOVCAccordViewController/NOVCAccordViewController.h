//
//  NOVCAccordViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class NOVCAccordViewController;

@protocol NOVCAccordViewControllerDelegate <NSObject>

@required

-(void)nOVCAccordViewController:(NOVCAccordViewController *)nOVCAccordViewController accord:(BOOL)accord;

@end

@interface NOVCAccordViewController : NOVParentPopUpViewController

@property(nonatomic, weak)id<NOVCAccordViewControllerDelegate>delegate;

@end
