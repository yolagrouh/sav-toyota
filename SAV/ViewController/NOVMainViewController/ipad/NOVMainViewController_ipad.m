#import "NOVMainViewController_ipad.h"
#import "ClientViewController_ipad.h"
#import "NOVRdvViewController_ipad.h"
#import "ExterieurViewController_ipad.h"
#import "TSuggeresViewController_ipad.h"
#import "InterieurViewController_ipad.h"
#import "TAdditionnelsViewController_ipad.h"
#import "NOVSharedUSer.h"
#import "AIDatePickerController.h"
#import "NOVConventionViewController_ipad.h"
#import "LivraisonViewController_ipad.h"


#define STEP_LABEL_DEFAULT_START_COLOR          [UIColor colorWithRed:172.0/255 green:172.0/255 blue:172.0/255 alpha:1]
#define STEP_LABEL_DEFAULT_END_COLOR            [UIColor colorWithRed:114.0/255 green:114.0/255 blue:114.0/255 alpha:1]

#define STEP_LABEL_SELECTED_START_COLOR         [UIColor colorWithRed:240.0/255 green:31.0/255 blue:76.0/255 alpha:1]
#define STEP_LABEL_SELECTED_END_COLOR           [UIColor colorWithRed:221.0/255 green:0.0/255 blue:43.0/255 alpha:1]


#define CLIENT_VIEW_CONTROLLER_INDEX            0
#define INTERIEUR_VIEW_CONTROLLER_INDEX         1
#define EXTERIEUR_VIEW_CONTROLLER_INDEX         2
#define TSUGGERES_VIEW_CONTROLLER_INDEX         3
#define TADDITIONNELS_VIEW_CONTROLLER_INDEX     4
#define LIVRAISON_VIEW_CONTROLLER_INDEX         5


@interface NOVMainViewController_ipad ()

@property(nonatomic, assign)CGRect exterieurViewFrame;
@property(nonatomic, strong)UISwipeGestureRecognizer *swipeRecognizer;
@property(nonatomic, strong)UISwipeGestureRecognizer *swipeRecognizer2;
@property(nonatomic, strong)AIDatePickerController *datePickerViewController;

//ViewControllers
//
//
//@property(nonatomic, weak)ClientViewController_ipad * clientViewController;
//@property(nonatomic, weak)InterieurViewController_ipad * interieurViewController;
//@property(nonatomic, weak)ExterieurViewController_ipad * exterieurViewController;
//@property(nonatomic, weak)TSuggeresViewController_ipad * tSuggeresViewController;

//Other

@property(nonatomic, strong)NSMutableArray * viewConttollers;

@property(nonatomic, strong)NOVInstanceViewController_ipad * nOVInstanceViewController;
@property(nonatomic, strong)NOVConventionViewController_ipad * novConventionViewController;
@property(nonatomic, strong)VinViewController_ipad * vinViewController;
@property(nonatomic, strong)NOVConventionViewController_ipad *  nOVConventionViewController;

@property(nonatomic, strong)NOVFavViewController_ipad * nOVFavViewController;
@end

@implementation NOVMainViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {

        self.shouldHandleKeyBoard = NO;
        
        self.exterieurViewFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        self.viewConttollers = [NSMutableArray array];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finish:) name:FINISH_VISIT_NOTIFICATION object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRelancesView:) name:SHOW_RELANCES_VIEW object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showVinPopUp:) name:SHOW_VIN_POP_UP_WITH_OBJECT object:nil];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imprimerConvention:) name:IMPRESSION_NOTIFICATION object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDatePicker:) name:SHOW_DATE_PICKER_NOTIFICATION object:nil];
        
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFav:) name:SHOW_FAVORIS object:nil];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTAdditionnels:) name:SHOW_TADDITIONNELS_NOTIFICATION object:nil];
        
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    
    NOVSharedUSer * user = [NOVSharedUSer sharedNOVUser];
    
    self.welcomeLabel.text =[ NSString stringWithFormat:@"Bonjour : %@", user.name ];
    
 //
   // ClientViewController_ipad * clientViewController = [[ClientViewController_ipad alloc]initWithNibName:@"ClientViewController" bundle:nil];
    
    //clientViewController.rendezVous = [NSMutableDictionary dictionaryWithDictionary:self.rendezVous];
    
    
    NOVRdvViewController_ipad * nOVRdvViewController = [[NOVRdvViewController_ipad alloc]initWithNibName:@"NOVRdvViewController" bundle:nil];

    
    self.mainNavController = [[UINavigationController alloc]initWithRootViewController:nOVRdvViewController];
    
    self.mainNavController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight;
    
    self.mainNavController.view.backgroundColor = [UIColor clearColor];
    
    self.mainNavController.delegate = self;
    
    self.mainNavController.navigationBarHidden = YES;
    
    self.mainNavController.view.frame = self.contentView.bounds;

    [self.contentView addSubview:self.mainNavController.view];
    
}


-(void)setUpView{
    
    //Header
    self.welcomeLabel.gradientStartColor    =  [UIColor colorWithRed:190.0/255 green:207.0/255 blue:213.0/255 alpha:1];
    self.welcomeLabel.gradientEndColor      =  [UIColor colorWithRed:148.0/255 green:163.0/255 blue:171.0/255 alpha:1];
    
    self.animator = [[CBStoreHouseTransitionAnimator alloc] init];
    
    self.interactiveTransition = [[CBStoreHouseTransitionInteractiveTransition alloc] init];
    [self.interactiveTransition attachToViewController:self];

    //Gesture
    
    self.swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected)];
    self.swipeRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    
    self.swipeRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected)];
    self.swipeRecognizer2.direction = UISwipeGestureRecognizerDirectionUp;
    
    [self.swipeView addGestureRecognizer:self.swipeRecognizer];
    [self.swipeView2 addGestureRecognizer:self.swipeRecognizer2];
    

}

-(void)swipeDetected{
    
    self.nOVInstanceViewController = [[NOVInstanceViewController_ipad alloc]initWithNibName:@"NOVInstanceViewController" bundle:nil];
    
    self.nOVInstanceViewController.delegate = self;
    
    self.nOVInstanceViewController.view.frame = CGRectMake(0, self.view.frame.size.height, self.nOVInstanceViewController.view.frame.size.width, self.nOVInstanceViewController.view.frame.size.height);
    
    [self.view addSubview:self.nOVInstanceViewController.view];
    
    [self.view bringSubviewToFront:self.nOVInstanceViewController.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.nOVInstanceViewController.view.frame = CGRectMake(0, 0, self.nOVInstanceViewController.view.frame.size.width, self.nOVInstanceViewController.view.frame.size.height);
        
    }];

    
}

#pragma mark --
#pragma mark NavController 

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    [self addViewControllerToPopStack:viewController];
    
    self.addFolderBtn.enabled = YES;
    
    self.stepOneBtn.selected = self.stepTwoBtn.selected = self.stepThreeBtn.selected = self.stepFourBtn.selected = self.stepFourBtn.selected = self.stepFiveBtn.selected = NO;
    
    self.stepOneLabel.gradientStartColor    = STEP_LABEL_DEFAULT_START_COLOR;
    self.stepOneLabel.gradientEndColor      = STEP_LABEL_DEFAULT_END_COLOR;
    
    self.stepTwoLabel.gradientStartColor    = STEP_LABEL_DEFAULT_START_COLOR;
    self.stepTwoLabel.gradientEndColor      = STEP_LABEL_DEFAULT_END_COLOR;
    
    self.stepThreeLabel.gradientStartColor  = STEP_LABEL_DEFAULT_START_COLOR;
    self.stepThreeLabel.gradientEndColor    = STEP_LABEL_DEFAULT_END_COLOR;
    
    self.stepFourLabel.gradientStartColor   = STEP_LABEL_DEFAULT_START_COLOR;
    self.stepFourLabel.gradientEndColor     = STEP_LABEL_DEFAULT_END_COLOR;
    
    self.stepFiveLabel.gradientStartColor   = STEP_LABEL_DEFAULT_START_COLOR;
    self.stepFiveLabel.gradientEndColor     = STEP_LABEL_DEFAULT_END_COLOR;
    
    [UIView animateWithDuration:0.3 delay:0.5 options:1   animations:^{
        
        self.stepsView.frame = CGRectMake(0,  self.view.frame.size.height - self.stepsView.frame.size.height , self.stepsView.frame.size.width, self.stepsView.frame.size.height);
        
        self.headerView.frame = CGRectMake(0, 0, self.headerView.frame.size.width, self.headerView.frame.size.height);
        
        self.contentView.frame = CGRectMake(0, self.headerView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.headerView.frame.size.height - 82);
        
        self.swipeView.frame = CGRectMake(self.swipeView.frame.origin.x, self.view.frame.size.height - self.swipeView.frame.size.height, self.swipeView.frame.size.width, self.swipeView.frame.size.height);
        
        self.swipeView2.frame = CGRectMake(self.swipeView2.frame.origin.x, self.view.frame.size.height - self.swipeView2.frame.size.height, self.swipeView2.frame.size.width, self.swipeView2.frame.size.height);
        
        self.infoClientBtn.frame = CGRectMake(self.infoClientBtn.frame.origin.x, self.view.frame.size.height - self.infoClientBtn.frame.size.height, self.infoClientBtn.frame.size.width, self.infoClientBtn.frame.size.height);
        
        self.interieurBtn.frame = CGRectMake(self.interieurBtn.frame.origin.x, self.view.frame.size.height - self.interieurBtn.frame.size.height, self.interieurBtn.frame.size.width, self.interieurBtn.frame.size.height);
        
        self.exterieurBtn.frame = CGRectMake(self.exterieurBtn.frame.origin.x, self.view.frame.size.height - self.exterieurBtn.frame.size.height, self.exterieurBtn.frame.size.width, self.exterieurBtn.frame.size.height);
        
        self.tssugeresBtn.frame = CGRectMake(self.tssugeresBtn.frame.origin.x, self.view.frame.size.height - self.tssugeresBtn.frame.size.height, self.tssugeresBtn.frame.size.width, self.tssugeresBtn.frame.size.height);
        
        self.tAdditionnelBtn.frame = CGRectMake(self.tAdditionnelBtn.frame.origin.x, self.view.frame.size.height - self.tAdditionnelBtn.frame.size.height, self.tAdditionnelBtn.frame.size.width, self.tAdditionnelBtn.frame.size.height);
        
        
    } completion:^(BOOL finished) {
        
        self.interventionsBtn.enabled = YES;
        
    } ];
    
    if ([viewController isKindOfClass:[ClientViewController_ipad class]]) {
        
        self.infoClientBtn.enabled = YES;
        
        self.stepOneBtn.selected = YES;
        
         self.addFolderBtn.enabled = NO;
        
        self.stepOneLabel.gradientStartColor    = STEP_LABEL_SELECTED_START_COLOR;
        self.stepOneLabel.gradientEndColor      = STEP_LABEL_SELECTED_END_COLOR;
        
    }else if ([viewController isKindOfClass:[InterieurViewController_ipad class]]) {
        
        self.interieurBtn.enabled = YES;
        
        self.stepTwoBtn.selected = YES;
        
        self.addFolderBtn.enabled = NO;
        
        self.stepTwoLabel.gradientStartColor    = STEP_LABEL_SELECTED_START_COLOR;
        self.stepTwoLabel.gradientEndColor      = STEP_LABEL_SELECTED_END_COLOR;
        
    }else if ([viewController isKindOfClass:[ExterieurViewController_ipad class]]) {
        
        self.exterieurBtn.enabled = YES;
        
        [UIView animateWithDuration:0.3 delay:0.5 options:1   animations:^{
            
            self.stepsView.frame = CGRectMake(0, self.view.frame.size.height, self.stepsView.frame.size.width, self.stepsView.frame.size.height);
            
            self.headerView.frame = CGRectMake(0, -self.headerView.frame.size.height, self.headerView.frame.size.width, self.headerView.frame.size.height);
            
            self.contentView.frame = CGRectMake(0 , 0 , self.view.frame.size.width, self.view.frame.size.height );
            
            viewController.view.frame = CGRectMake(0 , 0 , self.view.frame.size.width, self.view.frame.size.height );
            
            self.swipeView.frame = CGRectMake(self.swipeView.frame.origin.x, self.view.frame.size.height, self.swipeView.frame.size.width, self.swipeView.frame.size.height);
            
            self.swipeView2.frame = CGRectMake(self.swipeView2.frame.origin.x, self.view.frame.size.height, self.swipeView2.frame.size.width, self.swipeView2.frame.size.height);
            
            self.infoClientBtn.frame = CGRectMake(self.infoClientBtn.frame.origin.x, self.view.frame.size.height, self.infoClientBtn.frame.size.width, self.infoClientBtn.frame.size.height);
            
            self.interieurBtn.frame = CGRectMake(self.interieurBtn.frame.origin.x, self.view.frame.size.height, self.interieurBtn.frame.size.width, self.interieurBtn.frame.size.height);
            
            self.exterieurBtn.frame = CGRectMake(self.exterieurBtn.frame.origin.x, self.view.frame.size.height, self.exterieurBtn.frame.size.width, self.exterieurBtn.frame.size.height);
            
            self.tssugeresBtn.frame = CGRectMake(self.tssugeresBtn.frame.origin.x, self.view.frame.size.height, self.tssugeresBtn.frame.size.width, self.tssugeresBtn.frame.size.height);
            
            self.tAdditionnelBtn.frame = CGRectMake(self.tAdditionnelBtn.frame.origin.x, self.view.frame.size.height, self.tAdditionnelBtn.frame.size.width, self.tAdditionnelBtn.frame.size.height);
        
            
        } completion:^(BOOL finished) {
            
            self.interventionsBtn.enabled = NO;
            
        } ];
        
        self.stepThreeBtn.selected = YES;
        
        self.addFolderBtn.enabled = NO;
        
        self.stepThreeLabel.gradientStartColor    = STEP_LABEL_SELECTED_START_COLOR;
        self.stepThreeLabel.gradientEndColor      = STEP_LABEL_SELECTED_END_COLOR;
        
    }else if ([viewController isKindOfClass:[TSuggeresViewController_ipad class]]) {
        
        self.tssugeresBtn.enabled = YES;
        
        self.stepFourBtn.selected = YES;
        
        self.addFolderBtn.enabled = NO;
        
        self.stepFourLabel.gradientStartColor    = STEP_LABEL_SELECTED_START_COLOR;
        self.stepFourLabel.gradientEndColor      = STEP_LABEL_SELECTED_END_COLOR;
        
    }else if ([viewController isKindOfClass:[TAdditionnelsViewController_ipad class]]) {
        
        self.infoClientBtn.enabled      = NO;
        self.interieurBtn.enabled       = NO;
        self.exterieurBtn.enabled       = NO;
        self.tssugeresBtn.enabled       = NO;
        self.tAdditionnelBtn.enabled    = NO;
        
        self.stepFiveBtn.selected = YES;
        
        self.addFolderBtn.enabled = YES;
        
        self.stepFiveLabel.gradientStartColor    = STEP_LABEL_SELECTED_START_COLOR;
        self.stepFiveLabel.gradientEndColor      = STEP_LABEL_SELECTED_END_COLOR;
        
    }else if ([viewController isKindOfClass:[LivraisonViewController_ipad class]]) {
        
        self.infoClientBtn.enabled      = NO;
        self.interieurBtn.enabled       = NO;
        self.exterieurBtn.enabled       = NO;
        self.tssugeresBtn.enabled       = NO;
        self.tAdditionnelBtn.enabled    = NO;
        
        self.stepFiveBtn.selected = YES;
        
        self.addFolderBtn.enabled = YES;
        
        self.stepFiveLabel.gradientStartColor    = STEP_LABEL_SELECTED_START_COLOR;
        self.stepFiveLabel.gradientEndColor      = STEP_LABEL_SELECTED_END_COLOR;
        
    }
    
 
}

#pragma mark - Navigation Controller Delegate

-(id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                 animationControllerForOperation:(UINavigationControllerOperation)operation
                                              fromViewController:(UIViewController *)fromVC
                                                toViewController:(UIViewController *)toVC
{
    switch (operation) {
        case UINavigationControllerOperationPush:
            
            self.interactiveTransition = nil;
            self.animator.type = AnimationTypePush;
            return self.animator;
        case UINavigationControllerOperationPop:
            self.animator.type = AnimationTypePop;
            return self.animator;
        default:
            return nil;
    }
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                         interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController
{
    return self.interactiveTransition;
}

- (IBAction)showInstanceViewController:(id)sender {
    
//    self.nOVInstanceViewController = [[NOVInstanceViewController_ipad alloc]initWithNibName:@"NOVInstanceViewController" bundle:nil];
//    
//    self.nOVInstanceViewController.delegate = self;
//    
//    self.nOVInstanceViewController.view.frame = CGRectMake(0, self.view.frame.size.height, self.nOVInstanceViewController.view.frame.size.width, self.nOVInstanceViewController.view.frame.size.height);
//    
//    [self.view addSubview:self.nOVInstanceViewController.view];
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        
//         self.nOVInstanceViewController.view.frame = CGRectMake(0, 0, self.nOVInstanceViewController.view.frame.size.width, self.nOVInstanceViewController.view.frame.size.height);
//        
//    }];
    
    
}

#pragma mark --
#pragma mark NOVInstanceViewController delegate 


-(void)nOVInstanceViewController:(NOVInstanceViewController *)nOVInstanceViewController selectedRDV:(NSDictionary *)rdv{
    
    UIViewController * viewController ;
    
    NOVSharedData * sharedData  = [NOVSharedData sharedNOVData];
    
    [sharedData setData:rdv];
    
    viewController = [[TAdditionnelsViewController_ipad alloc]initWithNibName:@"TAdditionnelsViewController" bundle:nil];
    
//    if ([[sharedData travauxAdditionnels] count] == 0) {
//    
//        viewController = [[TAdditionnelsViewController_ipad alloc]initWithNibName:@"TAdditionnelsViewController" bundle:nil];
//        
//        
//    }else{
//        
//        viewController = [[LivraisonViewController_ipad alloc]initWithNibName:@"LivraisonViewController" bundle:nil];
//        
//    }
    
    [self.mainNavController pushViewController:viewController animated:YES];
    
}

-(void)nOVInstanceViewController:(NOVInstanceViewController *)nOVInstanceViewController showHistorique:(NSDictionary *)rdv{
    
    NOVSharedData * sharedData  = [NOVSharedData sharedNOVData];
    
    [sharedData setData:rdv];
    
    self.nOVConventionViewController = [[NOVConventionViewController_ipad alloc]initWithNibName:@"NOVConventionViewController" bundle:nil];
    
    self.nOVConventionViewController.conventionMode = ConventionModeRead;
    
    // TAdditionnelsViewController_ipad * tAdditionnelsViewController = [[TAdditionnelsViewController_ipad alloc]initWithNibName:@"TAdditionnelsViewController" bundle:nil];
    
    // [self.mainNavController pushViewController:nOVConventionViewController animated:YES];
    
    self.nOVConventionViewController.view.frame = CGRectMake(0, -self.view.frame.size.height, self.nOVConventionViewController.view.frame.size.width,self.nOVConventionViewController.view.frame.size.height);
    
    [self.view addSubview:self.nOVConventionViewController.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.nOVConventionViewController.view.frame = CGRectMake(0, 0, self.nOVConventionViewController.view.frame.size.width, self.nOVConventionViewController.view.frame.size.height);
        
    }];

    
}

#pragma mark --
#pragma mark Bookmaks Delegate

-(void)nOVFavViewController:(NOVFavViewController *)nOVFavViewController showHistorique:(NSDictionary *)rdv{
    
    NOVSharedData * sharedData  = [NOVSharedData sharedNOVData];
    
    [sharedData setData:rdv];
    
    self.nOVConventionViewController = [[NOVConventionViewController_ipad alloc]initWithNibName:@"NOVConventionViewController" bundle:nil];
    
    self.nOVConventionViewController.conventionMode = ConventionModeRead;
    
    // TAdditionnelsViewController_ipad * tAdditionnelsViewController = [[TAdditionnelsViewController_ipad alloc]initWithNibName:@"TAdditionnelsViewController" bundle:nil];
    
    // [self.mainNavController pushViewController:nOVConventionViewController animated:YES];
    
    self.nOVConventionViewController.view.frame = CGRectMake(0, -self.view.frame.size.height, self.nOVConventionViewController.view.frame.size.width,self.nOVConventionViewController.view.frame.size.height);
    
    [self.view addSubview:self.nOVConventionViewController.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.nOVConventionViewController.view.frame = CGRectMake(0, 0, self.nOVConventionViewController.view.frame.size.width, self.nOVConventionViewController.view.frame.size.height);
        
    }];
    
}

#pragma mark --
#pragma mark Finish viste

-(void)finish:(NSNotification *)notifications{
    
    [self.mainNavController popToRootViewControllerAnimated:YES];
    
    self.infoClientBtn.enabled      = NO;
    self.interieurBtn.enabled       = NO;
    self.exterieurBtn.enabled       = NO;
    self.tssugeresBtn.enabled       = NO;
    self.tAdditionnelBtn.enabled    = NO;
    
}


#pragma mark --
#pragma mark Impression

-(void)imprimerConvention:(NSNotification *)notification{
    
    self.novConventionViewController = [[NOVConventionViewController_ipad alloc]initWithNibName:@"NOVConventionViewController" bundle:nil];
    
    [self.view addSubview:self.novConventionViewController.view];
    
    [self.nOVConventionViewController show];

}

#pragma mark --
#pragma mark Previous History 

-(IBAction)showPrevious:(id)sender{
    
//    switch ([sender tag]) {
//        case 0:
//            
//            if (self.clientViewController) {
//                
//                [self.mainNavController popToViewController:self.clientViewController animated:YES];
//                
//            }
//            
//            break;
//            
//        case 1:
//            
//            if (self.interieurViewController) {
//                
//                [self.mainNavController popToViewController:self.interieurViewController animated:YES];
//                
//            }
//            
//            break;
//        case 2:
//            
//            if (self.exterieurViewController) {
//                
//                [self.mainNavController popToViewController:self.exterieurViewController animated:YES];
//                
//            }
//            
//            break;
//        case 3:
//            
//            if (self.tSuggeresViewController) {
//                
//                [self.mainNavController popToViewController:self.tSuggeresViewController animated:YES];
//                
//            }
//            
//            break;
//        default:
//            break;
//    }
    
}

- (IBAction)rechercherClient:(id)sender {
    
    [self.mainNavController popToRootViewControllerAnimated:YES];

    self.vinViewController = [[VinViewController_ipad alloc]initWithNibName:@"VinViewController" bundle:nil];
    
    self.vinViewController.view.frame = CGRectMake(0, 0, self.vinViewController.view.frame.size.width, self.vinViewController.view.frame.size.height);
    
    self.vinViewController.delegate = self;
    
    //isPopUpVisible = YES;
    
    
    [self.view addSubview:self.vinViewController.view];
    
    [self.vinViewController show];
    
}

- (IBAction)switchRDVREL:(id)sender {
    
    self.rdvBtn.selected = self.relanceBtn.selected = NO;
    
    switch ([sender tag]) {
        case 0://RDV
            
            self.rdvBtn.selected = YES;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RELOAD_RDV_NOTIFICATION object:nil];
            
            break;
            
        case 1://Relances
            
            self.relanceBtn.selected = YES;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RELOAD_RELANCES_NOTIFICATION object:nil];
            
            break;
        default:
            break;
    }
    
}

- (IBAction)showFav:(id)sender {
    
//    self.swipeRecognizer2.enabled = NO;
//    self.swipeRecognizer.enabled = NO;


    self.nOVFavViewController = [[NOVFavViewController_ipad alloc]initWithNibName:@"NOVFavViewController" bundle:nil];
    
    self.nOVFavViewController.delegate = self;
    
    self.nOVFavViewController.view.frame = CGRectMake(0, self.view.frame.size.height, self.nOVFavViewController.view.frame.size.width, self.nOVFavViewController.view.frame.size.height);
    
    [self.view addSubview:self.nOVFavViewController.view];
    
    [self.view bringSubviewToFront:self.nOVFavViewController.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.nOVFavViewController.view.frame = CGRectMake(0, 0, self.nOVFavViewController.view.frame.size.width, self.nOVFavViewController.view.frame.size.height);
        
    }];
    
}

- (IBAction)showViewController:(id)sender {

    NSInteger tag = [sender tag];
    
    if ([self.viewConttollers count] <= tag) {
        return;
    }
    
    
    NSInteger oldViewControllerTag = [self.mainNavController.viewControllers indexOfObject:self.mainNavController.visibleViewController];
    
    UIViewController * viewController = [self.viewConttollers objectAtIndex:tag];
    
    NSLog(@"View Frame %f %f %f %f", viewController.view.frame.origin.x, viewController.view.frame.origin.y, viewController.view.frame.size.width, viewController.view.frame.size.height ) ;
    
    
    if ([self.mainNavController.viewControllers containsObject:viewController]) {
        
         [self.mainNavController popToViewController:viewController animated:YES];
        
    }else{
        
         [self.mainNavController pushViewController:viewController  animated:YES];
        
    }
   
    
//    if (oldViewControllerTag < tag) {
//        
//        
//        [self.mainNavController pushViewController:viewController  animated:YES];
//        
//        
//    }else if (oldViewControllerTag > tag){
//        
//        [self.mainNavController popToViewController:viewController animated:YES];
//        
//    }
//    
//    
//    
}

-(void)showDatePicker:(NSNotification * )notification{
    
    UIDatePickerMode pickerMode;
    
    NSDictionary * dict = [notification userInfo];
    
    if ([[dict objectForKey:@"SENDER"] intValue] == 0) {
        
        pickerMode = UIDatePickerModeDate;
        
    }else{
        
        pickerMode = UIDatePickerModeTime;
    }
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    NSDate *date = [dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]];
    
    self.datePickerViewController = [AIDatePickerController pickerWithDate:date selectedBlock:^(NSDate *selectedDate) {
        
        NSLog(@"Selected Date : %@", selectedDate);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SET_DATE_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:selectedDate, @"selected_date", [dict objectForKey:@"SENDER"], @"SENDER"  , nil]];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } cancelBlock:^{
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    
    self.datePickerViewController.datePicker.datePickerMode = pickerMode;
    
    [self presentViewController:self.datePickerViewController animated:YES completion:nil];
    
  //  [self.view addSubview:self.datePickerViewController.view];
    
}

#pragma mark --
#pragma mark VIN Delegate 

-(void)vinViewController:(VinViewController *)vinViewController client:(NSDictionary *)client{
    
    ClientViewController_ipad * clientViewController = [[ClientViewController_ipad alloc]initWithNibName:@"ClientViewController" bundle:nil];
    
    clientViewController.rendezVous = [NSMutableDictionary dictionaryWithDictionary:client];
    
    
    [self.mainNavController pushViewController:clientViewController animated:YES];
    
}

#pragma mark --
#pragma mark TextField Delegate

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [super textFieldDidEndEditing:textField];
    
    
    if ([textField.text length]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:RECHERCHE_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObject:textField.text forKey:@"RECHERCHE"]];
        
    }
    
}

#pragma mark --
#pragma mark VIN PopUp Notification

-(void)showVinPopUp:(NSNotification *)notification{
    
    NSString * vin = [[notification userInfo] objectForKey:@"SN"];
    
    self.vinViewController = [[VinViewController_ipad alloc]initWithNibName:@"VinViewController" bundle:nil];
    
    self.vinViewController.vin = vin;
    
    self.vinViewController.delegate = self;
    
    [self.view addSubview:self.vinViewController.view];
   
    [self.vinViewController show];
    
}

#pragma mark --
#pragma mark Memory

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)addViewControllerToPopStack:(id)viewController{
    
    BOOL found = NO;
    
    NSInteger index = -1;
    
    for (id controller in self.viewConttollers) {
        
        if ([controller isKindOfClass:[viewController class]]) {
            
            found = YES;
            
            index = [self.viewConttollers indexOfObject:controller];
            
        }
        
    }
    
    if (!found) {
        
        [self.viewConttollers addObject:viewController];
        
    }else{
        
        [self.viewConttollers replaceObjectAtIndex:index withObject:viewController];
        
    }
    
    NSLog(@"ViewController Count %d", [self.viewConttollers count]);
    
}

-(void)showTAdditionnels:(NSNotification *)notification{
    
    
    NSDictionary * rdv = [notification object];
    
    UIViewController * viewController ;
    
    NOVSharedData * sharedData  = [NOVSharedData sharedNOVData];
    
    [sharedData setData:rdv];
    
    viewController = [[TAdditionnelsViewController_ipad alloc]initWithNibName:@"TAdditionnelsViewController" bundle:nil];
    
    //    if ([[sharedData travauxAdditionnels] count] == 0) {
    //
    //        viewController = [[TAdditionnelsViewController_ipad alloc]initWithNibName:@"TAdditionnelsViewController" bundle:nil];
    //
    //
    //    }else{
    //
    //        viewController = [[LivraisonViewController_ipad alloc]initWithNibName:@"LivraisonViewController" bundle:nil];
    //
    //    }
    
    [self.mainNavController pushViewController:viewController animated:YES];

    
}

#pragma mark --
#pragma mark ShowRelances 

-(void)showRelancesView:(NSNotification *)notification{
    
    [self swipeDetected];
    
}
@end
