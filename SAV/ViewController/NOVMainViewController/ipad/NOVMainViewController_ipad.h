//
//  NOVMainViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVMainViewController.h"
#import "CBStoreHouseTransition.h"
#import "NOVInstanceViewController_ipad.h"
#import "NOVFavViewController_ipad.h"
#import "VinViewController_ipad.h"

@interface NOVMainViewController_ipad : NOVMainViewController<UINavigationControllerDelegate, NOVInstanceViewControllerDelegate, VinViewControllerDelegate, UITextFieldDelegate, NOVFavViewControllerDelegate>


//Data

@property(nonatomic, strong) NSDictionary * rendezVous;
//Outlets
@property(nonatomic, strong) UINavigationController * mainNavController;
@property (nonatomic, strong) CBStoreHouseTransitionAnimator *animator;
@property (nonatomic, strong) CBStoreHouseTransitionInteractiveTransition *interactiveTransition;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *stepsView;


@property (weak, nonatomic) IBOutlet UIView *swipeView;
@property (weak, nonatomic) IBOutlet UIView *swipeView2;

//RDV Relance
@property (weak, nonatomic) IBOutlet UIButton *rdvBtn;

@property (weak, nonatomic) IBOutlet UIButton *relanceBtn;


//Buttons

@property (weak, nonatomic) IBOutlet UIButton *addFolderBtn;


@property (weak, nonatomic) IBOutlet UIButton *interventionsBtn;


@property (weak, nonatomic) IBOutlet UIButton *stepOneBtn;

@property (weak, nonatomic) IBOutlet UIButton *stepTwoBtn;

@property (weak, nonatomic) IBOutlet UIButton *stepThreeBtn;

@property (weak, nonatomic) IBOutlet UIButton *stepFourBtn;

@property (weak, nonatomic) IBOutlet UIButton *stepFiveBtn;

//

@property (weak, nonatomic) IBOutlet UIButton *infoClientBtn;

@property (weak, nonatomic) IBOutlet UIButton *interieurBtn;

@property (weak, nonatomic) IBOutlet UIButton *exterieurBtn;

@property (weak, nonatomic) IBOutlet UIButton *tssugeresBtn;

@property (weak, nonatomic) IBOutlet UIButton *tAdditionnelBtn;



//Labels

@property (weak, nonatomic) IBOutlet FXLabel *stepOneLabel;

@property (weak, nonatomic) IBOutlet FXLabel *stepTwoLabel;

@property (weak, nonatomic) IBOutlet FXLabel *stepThreeLabel;

@property (weak, nonatomic) IBOutlet FXLabel *stepFourLabel;

@property (weak, nonatomic) IBOutlet FXLabel *stepFiveLabel;

//Header
@property (weak, nonatomic) IBOutlet FXLabel *welcomeLabel;


- (IBAction)showInstanceViewController:(id)sender;

-(void)finish:(NSNotification *)notifications;

-(void)imprimerConvention:(NSNotification *)notification;

-(void)showDatePicker:(NSNotification * )notification;

-(IBAction)showPrevious:(id)sender;

- (IBAction)rechercherClient:(id)sender;


- (IBAction)switchRDVREL:(id)sender;

- (IBAction)showFav:(id)sender;


- (IBAction)showViewController:(id)sender;

@end
