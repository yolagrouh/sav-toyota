//
//  NOVFavViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class NOVOptionsViewController;

@protocol NOVOptionsViewControllerDelegate <NSObject>

@required


-(void)nOVFavViewController:(NOVOptionsViewController *)nOVFavViewController showHistorique:(NSDictionary *)rdv;

@end


@interface NOVOptionsViewController : NOVParentPopUpViewController

@property(nonatomic, weak)id<NOVOptionsViewControllerDelegate>delegate;

@end
