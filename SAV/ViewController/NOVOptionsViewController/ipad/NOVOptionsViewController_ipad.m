#import "NOVOptionsViewController_ipad.h"

#import "NOVOptionsCellTableViewCell.h"

@interface NOVOptionsViewController_ipad (){
    
    
}

@property(nonatomic, strong)NSMutableArray * bookmark;

@end

@implementation NOVOptionsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        self.filteredSuggestions        = [NSMutableArray array];
        
        self.filteredSuggestionObjects  = [NSMutableArray array];
        
    }
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    [self.filteredSuggestions addObjectsFromArray:self.suggestions];
    
    [self.filteredSuggestionObjects addObjectsFromArray:self.suggestionObjects];

    [self.tableView reloadData];
}

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.filteredSuggestionObjects count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 94;
    
}

-(UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"NOVOptionsCellTableViewCell";
    
    NOVOptionsCellTableViewCell *cell = (NOVOptionsCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        
        cell = (NOVOptionsCellTableViewCell *)[self getCellWithIdentifier:cellIdentifier];
        
    }
    
    NSDictionary * option = [self.filteredSuggestionObjects objectAtIndex:indexPath.row];
    
    cell.optionLabel.text = [option objectForKey:@"OP_DESCRIPTION"];
    
    cell.codeLabel.text =[option objectForKey:@"NAME"];;
//    
//    cell.indicator.hidden = ![[intervention objectForKey:@"SELECTED"]boolValue];
//    
//    cell.interventionLabel.text = [intervention objectForKey:@"OP_CODE"];
//    
//    cell.descriptionLabel.text = [intervention objectForKey:@"OP_DESCRIPTION"];
//    
//    cell.prixLabel.text = [intervention objectForKey:@"MONTANT"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSString * selection = [[self.filteredSuggestionObjects objectAtIndex:indexPath.row] objectForKey:@"OP_DESCRIPTION"];
    
    [self.delegate nOVOptionsViewController:self selected:selection];
    
    
    [self hide];
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    [self.filteredSuggestionObjects removeAllObjects];
    
    for (id suggestion in self.suggestionObjects) {
        
        if ([[suggestion objectForKey:@"OP_DESCRIPTION" ] rangeOfString:[textField.text stringByAppendingString:string] options:NSCaseInsensitiveSearch].location != NSNotFound) {
            
            [self.filteredSuggestionObjects addObject:suggestion];
            
            [self.tableView reloadData];
            
        }
        
    }
    
    return YES;
    
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
