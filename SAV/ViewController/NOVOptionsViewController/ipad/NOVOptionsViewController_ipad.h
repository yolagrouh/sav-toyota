//
//  NOVFavViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVOptionsViewController.h"

@class NOVOptionsViewController;

@protocol NOVOptViewControllerDelegate <NSObject>

@required
-(void)nOVOptionsViewController:(NOVOptionsViewController *)nOVOptionsViewController selected:(NSString *)option;

@end

@interface NOVOptionsViewController_ipad : NOVOptionsViewController<UITextFieldDelegate>

@property(nonatomic, weak)id<NOVOptViewControllerDelegate>delegate;

@property(nonatomic, strong)NSMutableArray * suggestionObjects;

@property(nonatomic, strong)NSMutableArray * filteredSuggestionObjects;

@property(nonatomic, strong)NSMutableArray * suggestions;

@property(nonatomic, strong)NSMutableArray * filteredSuggestions;

@property (weak, nonatomic) IBOutlet UITextField *rechercheTF;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
