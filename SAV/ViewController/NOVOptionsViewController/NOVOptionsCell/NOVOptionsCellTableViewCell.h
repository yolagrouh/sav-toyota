//
//  NOVOptionsCellTableViewCell.h
//  SAV
//
//  Created by MacBook Pro on 05/03/2015.
//  Copyright (c) 2015 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NOVOptionsCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *optionLabel;

@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@end
