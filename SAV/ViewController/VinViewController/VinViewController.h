//
//  VinViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class VinViewController;

@protocol VinViewControllerDelegate <NSObject>

@required

-(void)vinViewController:(VinViewController *)vinViewController client:(NSDictionary *)client;

@end

@interface VinViewController : NOVParentPopUpViewController

@end
