#import "VinViewController_ipad.h"
#import "NOVMainViewController_ipad.h"
#import "AFNetworking.h"

@interface VinViewController_ipad ()

@end

@implementation VinViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    if (self.vin) {
        
        self.vinTF.text = self.vin;
        
         [self doGet:[NSString stringWithFormat:@"%@%@",VIN_WS, self.vinTF.text] requestID:0];
        
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [[NOVSharedData sharedNOVData] reset];
    
}

#pragma mark --
#pragma mark Setup

-(void)setUpView{
    
   // [super setUpView];
    
}

#pragma mark --
#pragma mark WS

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    self.validerBtn.enabled =YES;
    
    NSArray * results = [response objectForKey:@"results"];
    
    if ([results count] > 0) {
        
        NSDictionary * client = [results objectAtIndex:0];
        
        NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
        
        sharedData.adresseClient                =[client objectForKey:@"ADRESSE"];
        sharedData.cinRCClient                  =[client objectForKey:@"CIN_RC"];
        sharedData.codeClient                   =[client objectForKey:@"CODE_CLIENT"];
        sharedData.dateMiseEnCirculation        =[client objectForKey:@"DATE_MISE_EN_CIRCULATION"];
        sharedData.emailClient                  =[client objectForKey:@"EMAIL"];
        sharedData.immatriculation              =[client objectForKey:@"IMMATRICULE"];
        sharedData.katashiki                    =[client objectForKey:@"KATASHIKI"];
        sharedData.modelVehicule                =[client objectForKey:@"MODÈLE_VEH"];
        sharedData.nomClient                    =[client objectForKey:@"NOM_CLIENT"];
        sharedData.sn                           =[client objectForKey:@"SN"];
        sharedData.telClient                    =[client objectForKey:@"TEL"];
        sharedData.typeClient                   =[client objectForKey:@"TYPE_CLIENT"];
        //sharedData.operationCode                =[client objectForKey:@"OPERATION_CODE"];
        //sharedData.dateRDV                      =[client objectForKey:@"DATE_RDV"];
        
//        [[NOVSharedData sharedNOVData] setDateRDV:[client objectForKey:@"DATE_RDV"]];
//        [[NOVSharedData sharedNOVData] setOperationCode:[client objectForKey:@"OPERATION_CODE"]];
//        [[NOVSharedData sharedNOVData] setSa:[client objectForKey:@"SA"]];
//        [[NOVSharedData sharedNOVData] setWorkshop:[client objectForKey:@"WORKSHOP"]];
        
//        NOVMainViewController_ipad * mainViewController = [[NOVMainViewController_ipad alloc]initWithNibName:@"NOVMainViewController" bundle:nil];
//        
//        mainViewController.rendezVous = [NSDictionary dictionaryWithDictionary:client];
//        
//        [self.navigationController pushViewController:mainViewController animated:YES];
        
        [self hide];
        
        [self.delegate vinViewController:self client:[NSDictionary dictionaryWithDictionary:client]];
        
        return;
        
    }

    [Utils showAlertView:@"Aucun VIN trouvé."];
   
    
}

#pragma mark --
#pragma mark Actions

- (IBAction)valider:(id)sender {
    
    if ([self.vinTF.text length] == 0) {
        
        [Utils showAlertView:@"Veuillez saisir un VIN avant de procéder."];
        
        return;
        
    }else if ([self.vinTF.text length] < 8) {
        
        [Utils showAlertView:@"Vous devez saisir 8 caractère pour pouvoir effectuer une recherche."];
        
        return;
        
    }
    
    
    self.validerBtn.enabled = NO;
    [self doGet:[NSString stringWithFormat:@"%@%@",VIN_WS, self.vinTF.text] requestID:0];
}

- (IBAction)annuler:(id)sender {
    
    [super annuler:sender];
    
}

#pragma mark --
#pragma mark Memory

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
