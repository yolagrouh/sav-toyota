//
//  VinViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VinViewController.h"

@interface VinViewController_ipad : VinViewController

@property(nonatomic, weak) id<VinViewControllerDelegate>delegate;

@property(nonatomic, strong)NSString * vin;

@property (weak, nonatomic) IBOutlet NOVTextField *vinTF;

@property (weak, nonatomic) IBOutlet UIButton *validerBtn;

-(IBAction)valider:(id)sender;

- (IBAction)annuler:(id)sender;


@end
