//
//  ClientViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClientViewController.h"
#import "InterventionsViewController_ipad.h"
#import "NOVValidationViewController_ipad.h"
#import "NOVPopUpViewController_ipad.h"
#import "NOVSuggestionsTF.h"

@interface ClientViewController_ipad : ClientViewController<UITextFieldDelegate, UITextViewDelegate, InterventionsViewControllerDelegate,NOVValidationViewControllerDelegate, NOVPopUpViewControllerDelegate>

//Data

@property (strong, nonatomic) NSMutableDictionary * rendezVous;
@property (strong, nonatomic) NSString * selectedRequetesClient;

//Selected Options

@property(nonatomic, strong) NSMutableArray * selectedRequests;
@property(nonatomic, strong) NSMutableArray * selectedOtherRequests;

//Outlets

@property (weak, nonatomic) IBOutlet UITableView *historiqueTableView;

@property (weak, nonatomic) IBOutlet UITextView *recommendationTV;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *requetesClient;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *infosClient;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *infosVehicule;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray * viewBlocks;

@property (weak, nonatomic) IBOutlet NOVBlockView *contactView;
@property (weak, nonatomic) IBOutlet UIButton *autreRequetBtn;

@property (weak, nonatomic) IBOutlet UIView *rendezVousView;


@property (weak, nonatomic) IBOutlet UIView *otherRequetesContainer;

@property (weak, nonatomic) IBOutlet NOVButton *validerBtn;

@property (weak, nonatomic) IBOutlet UIButton *addOtherRequeteBtn;


@property (weak, nonatomic) IBOutlet NOVTextField *nomTF;

@property (weak, nonatomic) IBOutlet NOVTextField *telTF;

@property (weak, nonatomic) IBOutlet UITextView *adresseTF;

@property (weak, nonatomic) IBOutlet NOVTextField *cinTF;

@property (weak, nonatomic) IBOutlet NOVTextField *emailTF;

@property (weak, nonatomic) IBOutlet NOVSuggestionsTF *firstAutreTF;

@property (weak, nonatomic) IBOutlet NOVTextField *contactNomTF;

@property (weak, nonatomic) IBOutlet NOVTextField *contactTelTF;

@property (weak, nonatomic) IBOutlet UILabel *dateRDVLabel;

@property (weak, nonatomic) IBOutlet NOVBlockView *historiqueBView;

@property (weak, nonatomic) IBOutlet NOVBlockView *recommendationBView;

@property (weak, nonatomic) IBOutlet NOVBlockView *requetsClientBView;

@property (weak, nonatomic) IBOutlet NOVTextField *matricumeTF;
- (IBAction)valider:(id)sender;

- (IBAction)addRequetClient:(id)sender;

- (IBAction)addNewRequetClient:(id)sender;

- (IBAction)editField:(id)sender;

-(IBAction)setUpAutresRequestesClient:(id)sender;

-(void)filterArray:(NSArray *)options option:(NSString *)option;

@end
