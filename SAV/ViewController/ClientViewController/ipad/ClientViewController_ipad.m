#import "ClientViewController_ipad.h"
#import "InterieurViewController_ipad.h"
#import "NOVCRappelViewController_ipad.h"
#import "NOVHistoriqueCell.h"

#define INFO_CLIENT_BLOCK_INDEX                 0
#define INFO_VEHICULE_BLOCK_INDEX               1
#define REQUETES_CLIENT_BLOCK_INDEX             2
#define HISTORIQUE_BLOCK_INDEX                  3
#define RECOMMENDATION_BLOCK_INDEX              4


//Tags

#define NOM_CLIENT_TF_TAG                       0
#define TEL_CLIENT_TF_TAG                       1
#define CIN_CLIENT_TF_TAG                       2
#define EMAIL_CLIENT_TF_TAG                     3
#define MODEL_TF_TAG                            4
#define MATRICULE_TF_TAG                        5
#define DATE_TF_TAG                             6
#define VIN_TF_TAG                              7
#define NOM_CONTACT_TF_TAG                      8
#define TEL_CONTACT_TF_TAG                      9

//

#define NOM_CLIENT_EDIT_BTN_TAG                 0
#define TEL_CLIENT_EDIT_BTN_TAG                 1
#define ADRESSE_CLIENT_EDIT_BTN_TAG             2
#define CIN_CLIENT_EDIT_BTN_TAG                 3
#define EMAIL_CLIENT_EDIT_BTN_TAG               4
#define NOM_CONTACT_EDIT_BTN_TAG                5
#define TEL_CONTACT_EDIT_BTN_TAG                6
#define MATRICULE_EDIT_BTN_TAG                  7

@interface ClientViewController_ipad (){
    
    
    CGRect initialRequetesClientFrame;
    CGRect initialOtherRequetesClientFrame;
    CGRect initialHistoriqueFrame;
    CGRect initialRecommendationsFrame;
    
    CGRect initialaddRequestBtnFrame;
    CGRect initialValiderBtnFrame;
    
    NSInteger selectedCell;
    
    NSInteger otherRequestCount;
    
}

@property(nonatomic, strong)NSMutableArray * historique;

@property(nonatomic, strong) InterventionsViewController_ipad * interventionsViewController;
@property(nonatomic, strong) NOVValidationViewController_ipad * validationViewController;

@property(nonatomic, strong) NOVCRappelViewController_ipad * nOVCRappelViewController;
@property(nonatomic, strong) NOVPopUpViewController_ipad * nOVPopUpViewController;

@property(nonatomic, strong)NSMutableArray * suggestionsResponse;
@property(nonatomic, strong)NSMutableArray * suggestions;
@end

@implementation ClientViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        selectedCell = -1;
        
        otherRequestCount = 0;
        
        self.suggestions = [NSMutableArray array];
        
        self.suggestionsResponse = [NSMutableArray array];
        
        self.selectedRequetesClient     = [NSString string];
        
        self.selectedRequests           = [NSMutableArray array];
        
        self.selectedOtherRequests      = [NSMutableArray array];
        
        self.historique                 = [NSMutableArray array];
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

-(void)setUpView{
    
    [super setUpView];
    
    
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    initialRequetesClientFrame          = [[self.viewBlocks objectAtIndex:REQUETES_CLIENT_BLOCK_INDEX] frame];
    initialOtherRequetesClientFrame     = [self.otherRequetesContainer frame];
    initialHistoriqueFrame              = [[self.viewBlocks objectAtIndex:HISTORIQUE_BLOCK_INDEX] frame];
    initialRecommendationsFrame         = [[self.viewBlocks objectAtIndex:RECOMMENDATION_BLOCK_INDEX] frame];
    
    initialaddRequestBtnFrame           = [self.addOtherRequeteBtn frame];
    initialValiderBtnFrame              = [self.validerBtn frame];
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.contentSV.frame.size.height + 550);
    
    //UITextFields
    
    self.adresseTF.layer.cornerRadius = 10;
    
    self.adresseTF.layer.borderColor = [UIColor colorWithRed:155.0/255 green:160.0/255 blue:165.0/255 alpha:1.0].CGColor;
    
    self.adresseTF.layer.borderWidth = 1;
    
    //Infos Client
    
    UITextField * nom               = [self.infosClient objectAtIndex:0];
    UITextField * tel               = [self.infosClient objectAtIndex:1];
    UITextField * cin               = [self.infosClient objectAtIndex:2];
    UITextField * email             = [self.infosClient objectAtIndex:3];
    
    nom.text                        = [self.rendezVous objectForKey:@"NOM_CLIENT"];
    
    if (![[self.rendezVous objectForKey:@"TEL"] isKindOfClass:[NSNull class]]) {
        
        tel.text =[self.rendezVous objectForKey:@"TEL"];
        
    }
    
    
    if (![[self.rendezVous objectForKey:@"ADRESSE"] isKindOfClass:[NSNull class]]) {
        
        self.adresseTF.text =[ NSString stringWithFormat:@"%@ %@ ",[self.rendezVous objectForKey:@"ADRESSE"],[self.rendezVous objectForKey:@"VILLE"]  ];
        
    }
    
    if (![[self.rendezVous objectForKey:@"CIN_RC"] isKindOfClass:[NSNull class]]) {
        
        cin.text =[self.rendezVous objectForKey:@"CIN_RC"];
        
    }
    
    if (![[self.rendezVous objectForKey:@"EMAIL"] isKindOfClass:[NSNull class]]) {
        
        email.text =[self.rendezVous objectForKey:@"EMAIL"];
        
    }
    
    
    //Rendez vous
    
    BOOL hasRDV = NO;
    
    NSString * dateRDV = @"";
    
    for (id rdv in sharedData.rendezVous) {
        
        
        if ([[rdv objectForKey:@"SN"] isEqualToString:[self.rendezVous objectForKey:@"SN"]]) {
            
            hasRDV = YES;
            
            dateRDV = [rdv objectForKey:@"DATE_RDV"];
            
            break;
            
        }
        
    }
    
    if (hasRDV) {
        
        self.dateRDVLabel.text  = dateRDV;
        
    }else{
        
        
        self.rendezVousView.hidden = YES;
    }
    
    //Infos Vehicule
    
    UITextField * model             = [self.infosVehicule objectAtIndex:0];
    UITextField * matricule         = [self.infosVehicule objectAtIndex:1];
    UITextField * annee             = [self.infosVehicule objectAtIndex:2];
    UITextField * vin               = [self.infosVehicule objectAtIndex:3];
    
   if (![[self.rendezVous objectForKey:@"MODÈLE_VEH"] isKindOfClass:[NSNull class]]) {
        
        model.text =[self.rendezVous objectForKey:@"MODÈLE_VEH"];
        
    }
    
    if (![[self.rendezVous objectForKey:@"IMMATRICULE"] isKindOfClass:[NSNull class]]) {
        
        matricule.text =[self.rendezVous objectForKey:@"IMMATRICULE"];
        
    }
    
    if (![[self.rendezVous objectForKey:@"DATE_MISE_EN_CIRCULATION"] isKindOfClass:[NSNull class]]) {
        
        annee.text =[self.rendezVous objectForKey:@"DATE_MISE_EN_CIRCULATION"];
        
    }
    
    if (![[self.rendezVous objectForKey:@"SN"] isKindOfClass:[NSNull class]]) {
        
        vin.text =[self.rendezVous objectForKey:@"SN"];
        
    }
    
    //Get Compagne Rappel
    
    
    [self doGet:[NSString stringWithFormat:@"%@%@",RAPPEL_WS,sharedData.sn] requestID:1];
    
        [self doGet:[NSString stringWithFormat:@"%@%@",HISTORIQUE_WS,sharedData.sn] requestID:2];
    
     //[self doGet:HISTORIQUE_WS requestID:2];
    
     [self doGet:[NSString stringWithFormat:@"%@%@",TRAVAUX_AUTRE_WS, sharedData.katashiki] requestID:3];
    [self doGet:[NSString stringWithFormat:@"%@%@",TOUS_TRAVAUX_WS, sharedData.katashiki] requestID:4];
}


-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    if (requestID == 1) {
        
        id rappel ;
        
        self.nOVCRappelViewController = [[NOVCRappelViewController_ipad alloc]initWithNibName:@"NOVCRappelViewController" bundle:nil];
        
        if ([[response objectForKey:@"result"] count] > 0) {
            
            rappel = [[response objectForKey:@"result"] objectAtIndex:0];
            
            
           NSString * rappelString = @"";
            
            for (id rappel in [response objectForKey:@"result"]) {
                
                rappelString = [NSString stringWithFormat:@"%@\n%@",rappelString, [rappel objectForKey:@"CAMPAIGN_NAME"]];
                
            }
            
            self.nOVCRappelViewController.rappel = rappelString;
            
            
        }
        
        [self.view addSubview:self.nOVCRappelViewController.view];
        
        [self.nOVCRappelViewController show];
        
        
    }else if (requestID == 2) {
        
        [self.historique addObjectsFromArray:[response objectForKey:@"result"]];
        
       
        
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"CREATION_DATE"  ascending:YES];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MMM-yy"];
        
//        NSArray * histo =  [self.historique sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
//            
//            NSDate *d1 = [formatter dateFromString:obj1[@"CREATION_DATE"]];
//            NSDate *d2 = [formatter dateFromString:obj2[@"CREATION_DATE"]];
//            
//            //return [d1 compare:d2]; // ascending order
//            return [d2 compare:d1]; // descending order
//        }];
        
        NSArray * histo =  [self.historique sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
            
            id KM1 = [obj1 objectForKey:@"INCIDENT_ID"];
            id KM2 = [obj2 objectForKey:@"INCIDENT_ID"];
            
            
            //return [d1 compare:d2]; // ascending order
            return [KM2 floatValue] > [KM1 floatValue]; // descending order
        }];

       
        self.historique = [NSMutableArray arrayWithArray:histo];
    
        [self.historiqueTableView reloadData];
        
        if([self.historique count] > 0){
            
            NOVSharedData * sharedNovData = [NOVSharedData sharedNOVData];
            
            if([[[self.historique objectAtIndex:0] objectForKey:@"KM"] isKindOfClass:[NSNull class]]){
                
                sharedNovData.previousKM = @"0";
            }else{
                
                sharedNovData.previousKM = [[self.historique objectAtIndex:0] objectForKey:@"KM"];
                
            }
            
            
            
        }
    }else if (requestID == 3){
        
        NSLog(@"Response %@", [response description]);
        
        [self.suggestionsResponse removeAllObjects];
        
        [self.suggestionsResponse addObjectsFromArray:[response objectForKey:@"result"]];
        
        for (id prix in self.suggestionsResponse) {
            
            [self.suggestions addObject:[prix objectForKey:@"OP_DESCRIPTION"]];
            
        }
        
        self.firstAutreTF.suggestions = self.suggestions;
        
        self.firstAutreTF.suggestionsObjects = self.suggestionsResponse;
        
    }else if (requestID == 4){
        
        
        NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
        
        sharedData.requestsList = [NSMutableArray arrayWithArray:[response objectForKey:@"result"]];
        
    }else if (requestID == 888){
        
        NSArray * recommendations = [response objectForKey:@"results" ];
        
        if ([recommendations count] == 0) {
            
             self.recommendationTV.text = @"Aucune recommendation n'est enregistrée";
            
            return;
        }
        
        self.recommendationTV.text = [[[response objectForKey:@"results" ] objectAtIndex:0] objectForKey:@"NOTES"];
        
    }else if (requestID == 9999){//UU
        
        [self showValidation];
        
    }
    
}

-(void)gotError:(NSError *)error requestId:(NSInteger)requestID{
    
    [super gotError:error requestId:requestID];
    
    if (requestID == 9999) {
        [self showValidation];
    }
    
    
}

#pragma mark --
#pragma mark Actions
- (IBAction)valider:(id)sender {
 
    
    NOVSharedData *sharedData = [NOVSharedData sharedNOVData];
    
    for (NOVSuggestionsTF * tf in [self.otherRequetesContainer subviews]) {
        
        
        if ([tf.text length ]> 0 && ![sharedData.selectedOtherRequestsClient containsObject:tf.text]) {
            
            [sharedData.selectedOtherRequestsClient addObject:tf.text];
            
        }
        
    }
    
    
    if ([self.selectedRequests count] == 0&&[sharedData.selectedOtherRequestsClient count] == 0 ) {
        
        [self showError:@"Veuillez spécifier les requêtes client avant de procéder."];
        
        return;
        
    }
    
    NSString * userUpdate = [NSString stringWithFormat:@"field_Party_id=%@&field_Vehicle_matricule=54&field_Party_nom=%@&field_Party_prenom=%@&field_Contact_nom=%@&field_Contact_prenom=%@&field_Contact_Tel=%@&field_Party_tel=%@&field_Party_adresse=%@&field_Party_cin=%@&field_Party_email=%@",[self.rendezVous objectForKey:@"CODE_CLIENT"], self.nomTF.text,self.nomTF.text, self.contactNomTF.text, self.contactNomTF.text,self.contactTelTF.text,self.telTF.text,self.adresseTF.text, self.cinTF.text, self.emailTF.text ];
    
    
    
    userUpdate = [userUpdate stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    userUpdate = [USER_SAVE_WS stringByAppendingString:userUpdate];
    
    
    [self doGet:userUpdate requestID:9999];
    
    
}

- (IBAction)addRequetClient:(id)sender {
    
    NOVTaggedButton * button = (NOVTaggedButton *)sender;
    
    button.selected = !button.selected;
    
    if (sender != self.autreRequetBtn) {
        
       // NSArray * requests = [NSArray arrayWithObjects:@"PAV",@"PAR",@"CD",@"CA",@"FC",@"SMI",@"SM II",@"SP",@"SI", nil];
        
        NSMutableArray * selectedRequetes;
        
        if ([self.selectedRequetesClient length]>0) {
            
            selectedRequetes  = [[self.selectedRequetesClient componentsSeparatedByString:@","] mutableCopy];
            
        }else{
            
            selectedRequetes =[NSMutableArray array];
        }
        
        
        
        NSString * selectesString = button.content;
        
        if ([selectedRequetes containsObject: selectesString]){
            
            [selectedRequetes removeObject:selectesString];
            
        }else{
            
            [selectedRequetes addObject:selectesString];
            
        }
        
        self.selectedRequetesClient = [selectedRequetes componentsJoinedByString:@","];
        
        if (button.isSelected) {
            
            self.interventionsViewController = [[InterventionsViewController_ipad alloc] initWithNibName:@"InterventionsViewController" bundle:nil];
            
            self.interventionsViewController.sender = sender;
            
            self.interventionsViewController.delegate = self;
            
            NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
            
            UITextField * vin               = [self.infosVehicule objectAtIndex:3];
            
            self.interventionsViewController.vin = vin.text;
            
            self.interventionsViewController.katachiki = sharedData.katashiki;
            
            self.interventionsViewController.typeTravail = selectesString;
            
            [self.view addSubview: self.interventionsViewController.view];
            
            [self.interventionsViewController show];
            
        }else{
            //Remove Selected Object
            
            if ([selectesString isEqualToString:@"PSM"]) {
                
                for (UIButton * requetBtn in self.requetesClient) {
                    
                    
                    requetBtn.enabled = YES;
                }
                
            }
            
            NSMutableArray * selectedRequetes=  [[self.selectedRequetesClient componentsSeparatedByString:@","] mutableCopy];
            
             NSArray * tempArray=  [NSArray arrayWithArray:selectedRequetes];
            
            for (NSString * requete in tempArray) {
                
                if (requete.length >= button.content.length) {
                    
                    if ([[requete substringFromIndex:button.content.length] isEqualToString:button.content]) {
                        
                        [selectedRequetes removeObjectAtIndex: [tempArray indexOfObject:requete]];
                        
                    }
                    
                }
                
            }
            
            self.selectedRequetesClient = [selectedRequetes componentsJoinedByString:@","];
            
            //
            
            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"OP_CODE BEGINSWITH[cd] $option"];
            
            NSArray *filteredArray = [self.selectedRequests filteredArrayUsingPredicate:[predicate predicateWithSubstitutionVariables:@{@"option":button.content}]];
            
            for (id option in filteredArray) {
                
                [self.selectedRequests removeObject:option];
                
            }
        }
        NOVSharedData * sharedData =  [NOVSharedData sharedNOVData];
        
        sharedData.selectedRequestsClient  = self.selectedRequests;
        
    }else{
        
        self.otherRequetesContainer.hidden = !self.otherRequetesContainer.hidden;
        
        self.addOtherRequeteBtn.hidden = !self.addOtherRequeteBtn.hidden;
        
        if (!((UIButton *)sender).isSelected) {
            
            self.otherRequetesContainer.frame   = initialOtherRequetesClientFrame;
            self.addOtherRequeteBtn.frame       = initialaddRequestBtnFrame;
            self.validerBtn.frame               = initialValiderBtnFrame;
            [[self.viewBlocks objectAtIndex:REQUETES_CLIENT_BLOCK_INDEX] setFrame:initialRequetesClientFrame];
            [[self.viewBlocks objectAtIndex:HISTORIQUE_BLOCK_INDEX] setFrame:initialHistoriqueFrame];
            [[self.viewBlocks objectAtIndex:RECOMMENDATION_BLOCK_INDEX] setFrame:initialRecommendationsFrame];
            
            for (UITextField * textField in [self.otherRequetesContainer subviews]) {
                
                NSMutableArray * tempsArray = [[self.selectedRequetesClient componentsSeparatedByString:@","] mutableCopy];
                
                NSLog(@"Text : %@",textField.text);
                
                if ([tempsArray containsObject:textField.text]) {
                    
                    [tempsArray removeObject:textField.text];
                }
                
                [tempsArray count] == 0 ? self.selectedRequetesClient = @"": ( self.selectedRequetesClient = [tempsArray componentsJoinedByString:@","]);
                
                if (textField != self.firstAutreTF) {
                    
                    [textField removeFromSuperview];
                }
                
            }
            
            self.firstAutreTF.text = nil;
            
        }
        
    }
    
}

- (IBAction)addNewRequetClient:(id)sender {
    
    UIButton * autreBtn = [self.requetesClient lastObject];
    
    if (!self.autreRequetBtn.isSelected) {
        
        return;
    }
    
    otherRequestCount  ++ ;
    
    UIView * requetesBlock          = self.requetsClientBView;
    
    UIView * historiqueBlock        = self.historiqueBView;
    
    UIView * recommendationsBlock   = self.recommendationBView;
    
    UIView * lastTF = [[self.otherRequetesContainer subviews] lastObject];
    
    CGRect frame = lastTF.frame;
    
    NOVSuggestionsTF * textField = [[NOVSuggestionsTF alloc]initWithFrame:CGRectMake(0, frame.origin.y + frame.size.height + 10, frame.size.width, frame.size.height)];
    
    
    textField.tag = 99 + otherRequestCount;
    
    textField.font = [UIFont fontWithName:@"Arial" size:12];
    
   // [textField becomeFirstResponder];
    
    textField.delegate = self;
    
    textField.placeholder = @"SAISIR LE TEXTE";
    
    textField.font = [UIFont fontWithName:@"Arial" size:12];
    
    [self.otherRequetesContainer addSubview:textField];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        textField.frame = CGRectMake(0, frame.origin.y + frame.size.height + 10, frame.size.width, frame.size.height);
        
        requetesBlock.frame = CGRectMake(requetesBlock.frame.origin.x, requetesBlock.frame.origin.y, requetesBlock.frame.size.width, requetesBlock.frame.size.height + 50);
        
        historiqueBlock.frame = CGRectMake(historiqueBlock.frame.origin.x, historiqueBlock.frame.origin.y + 50, historiqueBlock.frame.size.width, historiqueBlock.frame.size.height );
        
        recommendationsBlock.frame = CGRectMake(recommendationsBlock.frame.origin.x, recommendationsBlock.frame.origin.y + 50, recommendationsBlock.frame.size.width, recommendationsBlock.frame.size.height);
        
        self.validerBtn.frame = CGRectMake(self.validerBtn.frame.origin.x, self.validerBtn.frame.origin.y + 50, self.validerBtn.frame.size.width, self.validerBtn.frame.size.height);
        
        self.addOtherRequeteBtn.frame = CGRectMake(self.addOtherRequeteBtn.frame.origin.x, self.addOtherRequeteBtn.frame.origin.y + 50, self.addOtherRequeteBtn.frame.size.width, self.addOtherRequeteBtn.frame.size.height);
        
        self.otherRequetesContainer.frame = CGRectMake(self.otherRequetesContainer.frame.origin.x, self.otherRequetesContainer.frame.origin.y, self.otherRequetesContainer.frame.size.width, self.otherRequetesContainer.frame.size.height + 50);
        
    } completion:^(BOOL finished) {
        
        textField.suggestions = self.suggestions;
        
        textField.suggestionsObjects = self.suggestionsResponse;
        
    }] ;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.contentSV.contentSize.height + 50);
    
}

- (IBAction)editField:(id)sender {
    
    switch ([sender tag]) {
            
        case TEL_CLIENT_EDIT_BTN_TAG:
            
            self.telTF.enabled = YES;
            
            [self.telTF becomeFirstResponder];
            
            break;
        case ADRESSE_CLIENT_EDIT_BTN_TAG:
            
            self.adresseTF.editable = YES;
            
            [self.adresseTF becomeFirstResponder];
            
            break;
        case EMAIL_CLIENT_EDIT_BTN_TAG:
            
            self.emailTF.enabled = YES;
            
            [self.emailTF becomeFirstResponder];
            
            break;
        case NOM_CONTACT_EDIT_BTN_TAG:
            
            self.contactNomTF.enabled = YES;
            
            [self.contactNomTF becomeFirstResponder];
            
            break;
        case TEL_CONTACT_EDIT_BTN_TAG:
            
            self.contactTelTF.enabled = YES;
            
            [self.contactTelTF becomeFirstResponder];
            
            break;
        case NOM_CLIENT_EDIT_BTN_TAG:
            
            self.nomTF.enabled = YES;
            
            [self.nomTF becomeFirstResponder];
            
            break;
        case CIN_CLIENT_EDIT_BTN_TAG:
            
            self.cinTF.enabled = YES;
            
            [self.cinTF becomeFirstResponder];
            
            break;
        case MATRICULE_EDIT_BTN_TAG:
            
            self.matricumeTF.enabled = YES;
            
            [self.matricumeTF becomeFirstResponder];
            
            break;
        default:
            break;
    }
    
}

#pragma mark --
#pragma mark TextField Delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    switch (textField.tag) {
        case TEL_CLIENT_TF_TAG: case TEL_CONTACT_TF_TAG:
            
            if (![Utils isNumber:string]) {
                
                return NO;
                
            }
            
            if ([textField.text length] == 10 && [string length]!=0) {
                
                return NO;
            }
            
            break;
            
        case NOM_CONTACT_TF_TAG:
            
            if (![Utils isAlpha:string]) {
                
                return NO;
                
            }

            
            break;
            
        default:
            break;
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [super textFieldDidBeginEditing:textField];
    
    if ([textField.text length] == 0) {
        
        return;
    }
    
    if ([self.selectedRequetesClient length] == 0) {
        
        return;
        
    }
    
    NSMutableArray * tempsArray = [[self.selectedRequetesClient componentsSeparatedByString:@","] mutableCopy];
    
    if ([tempsArray containsObject:textField.text]) {
        
        [tempsArray removeObject:textField.text];
    }
    
    [tempsArray count] == 0 ? self.selectedRequetesClient = @"": ( self.selectedRequetesClient = [tempsArray componentsJoinedByString:@","]);
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [super textFieldDidEndEditing:textField];
    
    textField.enabled = NO;
    
    if (textField.tag >= 99) {
     
        textField.enabled= YES;
        
    }
    
//    if (textField.tag >= 99) {//Autre TF
//        
//        textField.enabled = YES;
//        
//        if ([textField.text length] > 0) {
//            
//            NOVSharedData * sharedData =  [NOVSharedData sharedNOVData];
//            
//            if ([self.selectedRequetesClient length] == 0) {
//                
//                self.selectedRequetesClient = textField.text;
//               
//                
//                if (![sharedData.selectedOtherRequestsClient containsObject:textField.text]) {
//                    
//                     [sharedData.selectedOtherRequestsClient insertObject:textField.text atIndex:0];
//                }
//                
//                return;
//            }
//            
//            self.selectedRequetesClient = [NSString stringWithFormat:@"%@,%@",self.selectedRequetesClient, textField.text];
//            
//             [sharedData.selectedOtherRequestsClient addObject:textField.text];
//            
//                
//            NSLog(@"Requests %@", [sharedData.selectedOtherRequestsClient description]);
//            
//        }
//        
//    }
    
    if ([textField.text length] >0) {
        
        switch (textField.tag) {
                
            case NOM_CLIENT_TF_TAG:
                
                if (![Utils isAlpha:textField.text]) {
                    
                    [Utils showAlertView:@"Veuillez renseigner nom et prénom valides."];
                    
                    //textField.text = nil;
                    
                    return;
                    
                }
                
                break;
            case TEL_CLIENT_TF_TAG:
                
                if (![Utils isValidePhone:textField.text]) {
                    
                    [Utils showAlertView:@"Veuillez renseigner un numéro de téléphone valide."];
                    
                    //textField.text = nil;
                    
                    return;
                    
                }
                
                break;
                
            case CIN_CLIENT_TF_TAG:
                
                if (![Utils isAlphaNumerique:textField.text]) {
                    
                    [Utils showAlertView:@"Veuillez renseigner un numéro CIN valide."];
                    
                    //textField.text = nil;
                    
                    return;
                    
                }
                
                break;
            case EMAIL_CLIENT_TF_TAG:
                
                if (![Utils isValideEmail:textField.text]) {
                    
                    [Utils showAlertView:@"Veuillez renseigner un email valide."];
                    
                    //textField.text = nil;
                    
                    return;
                    
                }
                
                break;
            case NOM_CONTACT_TF_TAG:
                
                if (![Utils isAlpha:textField.text]) {
                    
                    [Utils showAlertView:@"Veuillez renseigner nom et prénom valides."];
                    
                    //textField.text = nil;
                    
                    return;
                    
                }
                
                break;
            case TEL_CONTACT_TF_TAG:
                
                if (![Utils isValidePhone:textField.text]) {
                    
                    [Utils showAlertView:@"Veuillez renseigner un numéro de téléphone valide."];
                    
                    //textField.text = nil;
                    
                    return;
                    
                }
                
                break;
            default:
                break;
        }

        
    }
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    textView.editable = NO;
    
}

#pragma mark --
#pragma mark Interventions Delegate

-(void)interventionsViewController:(InterventionsViewController *)interventionsViewController selected:(NSArray *)options{
    
    NSArray * definedOptions =  [NSArray arrayWithObjects:@"FRE",@"MOT", @"PMA", @"PSU",@"PSU", @"FCLIM" , @"PSM",nil];
    
    NSString * selectedDefinedOption ;
    
    BOOL found = NO;
    
    for (NSDictionary * option in options) {
        
        for (NSString * defined in definedOptions) {
            
            if ([Utils checkString:[option objectForKey:@"OP_CODE"] contains:defined]) {
                
                selectedDefinedOption = defined;
                
                found = YES;
                
                break;
                
            }
            
        }
        
        if (found) {        
            break;
        }
        
    }
    
    if ([selectedDefinedOption isEqualToString:@"PSM"]) {
        
        [self.selectedRequests removeAllObjects];
        
        for (UIButton * requetBtn in self.requetesClient) {
            
            
            requetBtn.enabled = NO;
        }
        
    }
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"OP_CODE BEGINSWITH[cd] $option"];
    
    NSArray *filteredArray = [self.selectedRequests filteredArrayUsingPredicate:[predicate predicateWithSubstitutionVariables:@{@"option":selectedDefinedOption}]];
    
    for (id option in filteredArray) {
        
        [self.selectedRequests removeObject:option];
        
    }
    
    for (id option in options) {
        
        if (![self.selectedRequests containsObject:option]) {
            
            [self.selectedRequests addObject:option];
            
        }
        
    }
    
    
    NOVSharedData * sharedData =  [NOVSharedData sharedNOVData];
    
    sharedData.selectedRequestsClient  = self.selectedRequests;
}

#pragma mark --
#pragma mark StepValidation

-(void)showNext{
    
    [super showNext];
    
    self.nOVPopUpViewController = [[NOVPopUpViewController_ipad alloc]initWithNibName:@"NOVPopUpViewController" bundle:nil];
    
    self.nOVPopUpViewController.delegate = self;
    
    self.nOVPopUpViewController.time = 2;
    
    self.nOVPopUpViewController.showImage  = YES;
    
    self.nOVPopUpViewController.message = @"Merci de mettre la housse du siège et le tapis de sol";
    
    [self.view addSubview:self.nOVPopUpViewController.view];
    
    [self.nOVPopUpViewController show];
    
}

#pragma mark--
#pragma mark Housse PopUp

-(void)nOVPopUpViewController:(NOVPopUpViewController *)nOVPopUpViewController showNext:(BOOL)showNext{
    
    NOVSharedData * shareData = [NOVSharedData sharedNOVData];
    
    shareData.requetesClient = self.selectedRequetesClient;
    
    shareData.contactNom        = [self.contactNomTF.text length] == 0 ? @"" :self.contactNomTF.text;
    shareData.contactTel        = [self.contactTelTF.text length] == 0 ? @"" :self.contactTelTF.text;
    shareData.adresseClient     = [self.adresseTF.text length] == 0 ? @"" :self.adresseTF.text;
    shareData.telClient         = [self.telTF.text length] == 0 ? @"" :self.telTF.text;
    shareData.emailClient       = [self.emailTF.text length] == 0 ? @"" :self.emailTF.text;
    
    
    InterieurViewController_ipad * interieurViewController = [[InterieurViewController_ipad alloc]initWithNibName:@"InterieurViewController" bundle:nil];
    
    interieurViewController.rendezVous =self.rendezVous;
    
    [self.navigationController pushViewController:interieurViewController animated:YES];
    
}
    
#pragma mark --
#pragma mark Historique 

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.historique count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 140;
    
}

-(UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"NOVHistoriqueCell";
    
    NOVHistoriqueCell *cell = (NOVHistoriqueCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        
        cell = (NOVHistoriqueCell *)[self getCellWithIdentifier:cellIdentifier];
        
    }
    
//    UIColor * startColor = [UIColor colorWithRed:33.0/255 green:43.0/255 blue:50.0/255 alpha:1];
//    
//    UIColor * endColor = [UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:1];
    
   
    if (selectedCell == indexPath.row) {
        
            cell.dateLabel.textColor            = [UIColor whiteColor];
            cell.kilometrageLabel.textColor     = [UIColor whiteColor];
            cell.interventionLabel.textColor    = [UIColor whiteColor];
        
        
          [Utils addGradient:cell.contentView startColor:[UIColor colorWithRed:254.0/255 green:33.0/255 blue:64.0/255 alpha:1] endColor:[UIColor colorWithRed:251.0/255 green:40.0/255 blue:68.0/255 alpha:1]];
        
        
    }else{
        
        
        cell.dateLabel.textColor            = [UIColor colorWithRed:114.0/255 green:114.0/255 blue:114.0/255 alpha:1.0];
        cell.kilometrageLabel.textColor     = [UIColor colorWithRed:114.0/255 green:114.0/255 blue:114.0/255 alpha:1.0];
        cell.interventionLabel.textColor    = [UIColor colorWithRed:114.0/255 green:114.0/255 blue:114.0/255 alpha:1.0];
        
        UIColor * endColor = [UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1];
        
        [Utils addGradient:cell.contentView startColor:[UIColor whiteColor] endColor:endColor];
    }
        
        
  
    
    NSDictionary * historique = [self.historique objectAtIndex:indexPath.row];
    
    if (![[historique objectForKey:@""] isKindOfClass:[NSNull class]]) {
        
         cell.dateLabel.text                 = [historique objectForKey:@"CREATION_DATE"];
        
    }else{
        
        cell.dateLabel.text = @"";
    }
    
    if (![[historique objectForKey:@"KM"] isKindOfClass:[NSNull class]]) {
        
        cell.kilometrageLabel.text                 = [NSString stringWithFormat:@"%@ KM", [historique objectForKey:@"KM"] ];
        
    }else{
        
        cell.kilometrageLabel.text = @"";
    }
    
    if (![[historique objectForKey:@"OP_DESCRIPTION"] isKindOfClass:[NSNull class]]) {
        
        cell.interventionLabel.text                 = [historique objectForKey:@"OP_DESCRIPTION"];
        cell.interventionTV.text                    = [NSString stringWithFormat:@"%@ / %@", [historique objectForKey:@"SUMMARY"],[historique objectForKey:@"OP_CODE"] ];
        
    }else{
        
        cell.interventionLabel.text = @"";
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedCell = indexPath.row;
    
    NSDictionary * historique = [self.historique objectAtIndex:indexPath.row];

    //self.recommendationTV.text = [NSString stringWithFormat:@"Recommendation : %@\nDétails : %@ ",[historique objectForKey:@"UPSELLING"],[historique objectForKey:@"SUMMARY"] ];
    
    [self.historiqueTableView reloadData];
    
    
    [self getRecommendation:[historique objectForKey:@"INCIDENT_ID"]];
}

-(NSInteger )getCode:(NSString *)request{
    
    NSInteger index = -1;
    
    for (id prix in self.suggestionsResponse) {
        
        if ([[prix objectForKey:@"OP_CODE"] isEqualToString:request]) {
            
            index = [self.suggestionsResponse indexOfObject:prix];
            
            break;
            
        }
        
    }
    
    return index;
}


#pragma mark -- Recommendation


-(void)getRecommendation:(NSString *)incidentID{
    
    

    NSString * url = [NSString stringWithFormat:@"%@%@",RECOMMENDATION_WS, incidentID];
    
    [self doGet:url requestID:888];
    
}



@end
