//
//  InterventionsViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class InterventionsViewController;

@protocol InterventionsViewControllerDelegate <NSObject>

@required
-(void)interventionsViewController:(InterventionsViewController *)interventionsViewController selected:(NSArray *)options;

@end

@interface InterventionsViewController : NOVParentPopUpViewController

@property(nonatomic, weak)id<InterventionsViewControllerDelegate>delegate;

@end
