//
//  InterventionsViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterventionsViewController.h"
#import "NOVTaggedButton.h"



@interface InterventionsViewController_ipad : InterventionsViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak)NOVTaggedButton * sender;

@property (strong, nonatomic) NSString * typeTravail;

@property (strong, nonatomic) NSString * vin;

@property (strong, nonatomic) NSString * katachiki;

@property(nonatomic, strong) NSMutableArray * interventions;

@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end
