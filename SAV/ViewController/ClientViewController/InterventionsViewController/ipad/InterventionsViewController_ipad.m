#import "InterventionsViewController_ipad.h"
#import "NOVInterventionCell.h"

@interface InterventionsViewController_ipad ()

@property(nonatomic, assign)BOOL isEmpty;

@end

@implementation InterventionsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        self.interventions = [NSMutableArray array];
        
        self.shouldAddGradient = NO;
        
        self.isEmpty = NO;
        
    }
	return self;
}


- (void)viewDidLoad
{
	[super viewDidLoad];

    if ([self.vin isEqualToString:@"SB1KC58E40E002466"]) {
        
        [self doGet:[NSString stringWithFormat:@"%@%@",TOUS_TRAVAUX_WS, @"NDE150L-DHFNYW"] requestID:0];
        
    }else if([self.vin isEqualToString:@"NMTDD26RX0R047202"]){
        
          [self doGet:[NSString stringWithFormat:@"%@%@",TOUS_TRAVAUX_WS, @"AUR20L-GWFEYW"] requestID:0];
        
    }else {
        
        [self doGet:[NSString stringWithFormat:@"%@%@",TOUS_TRAVAUX_WS, self.katachiki] requestID:0];
        
    }
}

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    
    NSMutableArray * tempInterventions = [NSMutableArray arrayWithArray:self.interventions];
    
    for (NSMutableDictionary * intervention in [response objectForKey:@"result"]) {
        
        if([[intervention objectForKey:@"OP_CODE"]  length] >= [self.typeTravail length]){
            
            if ([[[intervention objectForKey:@"OP_CODE"] substringToIndex:[self.typeTravail length]] isEqualToString:self.typeTravail]) {
                
                NSMutableDictionary * tempsDict = [NSMutableDictionary dictionaryWithDictionary:intervention];
                
                [tempsDict setObject:[NSNumber numberWithBool:NO ] forKey:@"SELECTED"] ;
                
                [tempInterventions addObject:tempsDict];
                
            }

        }
        
    }
    
    self.interventions = [NSMutableArray arrayWithArray:tempInterventions];
    
    if ([self.interventions count] == 0) {
        
         [self showEmpty];
       
         self.isEmpty = YES;
    }
    
    [self.tableView reloadData];
    
}



#pragma mark --
#pragma mark TableView 

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.interventions count];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 79;
    
}

-(UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"NOVInterventionCell";
    
    NOVInterventionCell *cell = (NOVInterventionCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        
        cell = (NOVInterventionCell *)[self getCellWithIdentifier:cellIdentifier];
        
    }
    
    NSMutableDictionary * intervention = [NSMutableDictionary dictionaryWithDictionary:[self.interventions objectAtIndex:indexPath.row]];
    
    cell.indicator.hidden = ![[intervention objectForKey:@"SELECTED"]boolValue];
    
    cell.interventionLabel.text = [intervention objectForKey:@"OP_CODE"];
    
    cell.descriptionLabel.text = [intervention objectForKey:@"OP_DESCRIPTION"];
    
    cell.prixLabel.text = [intervention objectForKey:@"PRIX"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary * intervention = [NSMutableDictionary dictionaryWithDictionary:[self.interventions objectAtIndex:indexPath.row]];
    
    NSString * type = [intervention objectForKey:@"OP_CODE"];
    
    if ([type length] > 3 && [[type substringToIndex:3] isEqualToString:@"PMA"]) {
        
        
        for (id inter in self.interventions) {
            
            [inter setObject:[NSNumber numberWithBool:NO ] forKey:@"SELECTED"] ;
            
        }
        
        [intervention setObject:[NSNumber numberWithBool:YES ] forKey:@"SELECTED"] ;
    }else{
        
        BOOL selected = ![[intervention objectForKey:@"SELECTED"]boolValue] ;
        
        [intervention setObject:[NSNumber numberWithBool:selected ] forKey:@"SELECTED"] ;
        
        
    }
    
    
    [self.interventions replaceObjectAtIndex:indexPath.row withObject:intervention];
    
    [tableView reloadData];
    
}

- (IBAction)valider:(id)sender {
    
    NSMutableArray * selectedItems = [NSMutableArray array];
    
    for (NSMutableDictionary * intervention in self.interventions) {
        
        if ([[intervention objectForKey:@"SELECTED"] boolValue]) {
            
            [selectedItems addObject:intervention];
            
        }
        
    }
    
    if ([selectedItems count] == 0 && !self.isEmpty) {
        
         [self showError:@"Veuillez seléctionner au moin une intervention"];
        
        return;
        
    }else if ([selectedItems count] != 0) {
        
        [self.delegate interventionsViewController:self selected:selectedItems];
        
    }else{
        
        
        self.sender.selected = NO;
    }
    
    
    
    [super valider:sender];
    
}

- (IBAction)annuler:(id)sender {
    
    self.sender.selected = NO;
   
    [super annuler:sender];
   
}
@end
