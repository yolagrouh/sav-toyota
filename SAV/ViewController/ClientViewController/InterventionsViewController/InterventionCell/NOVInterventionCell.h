//
//  NOVInterventionCell.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/5/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NOVInterventionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *interventionLabel;

@property (weak, nonatomic) IBOutlet UILabel *prixLabel;

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UIImageView *indicator;




@end
