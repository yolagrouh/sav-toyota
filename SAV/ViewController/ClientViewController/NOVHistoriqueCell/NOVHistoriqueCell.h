//
//  NOVHistoriqueCell.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/16/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "BaseCell.h"

@interface NOVHistoriqueCell : BaseCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *kilometrageLabel;
@property (weak, nonatomic) IBOutlet UILabel *interventionLabel;

@property (weak, nonatomic) IBOutlet UITextView *interventionTV;


@end
