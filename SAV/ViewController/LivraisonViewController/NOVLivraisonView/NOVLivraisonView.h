//
//  NOVLivraisonView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/7/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVTextField.h"
#import "NOVTaggedButton.h"

@class NOVLivraisonView;

@protocol NOVLivraisonViewDelegate <NSObject>

@required
-(void)nOVLivraisonView:(NOVLivraisonView *)nOVLivraisonView showArgumentaire:(NSString *)argumentaire;

@end

@interface NOVLivraisonView : UIView

@property(nonatomic, weak)NSString * argumentaire;

@property(nonatomic, weak)id<NOVLivraisonViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *travailLabel;

@property (weak, nonatomic) IBOutlet NOVTextField *coutTF;


@property (weak, nonatomic) IBOutlet UIImageView *argumentaireImage;


@property (weak, nonatomic) IBOutlet NOVTaggedButton  * argumentaireBtn;

- (IBAction)showArgumentaire:(id)sender;

@end
