//
//  NOVLivraisonView.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/7/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVLivraisonView.h"

@implementation NOVLivraisonView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)showArgumentaire:(id)sender {
    
    [self.delegate nOVLivraisonView:self showArgumentaire:self.argumentaire];
}
@end
