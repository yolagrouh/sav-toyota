#import "LivraisonViewController_ipad.h"
#import "NOVArgumentaireViewController_ipad.h"
#import "NOVTaggedButton.h"
#import "NOVLivraisonCell.h"
#import "AFNetworking.h"

@interface LivraisonViewController_ipad (){
    
   
    
    NSInteger objectIndex;
}

@property(nonatomic, strong)NSString * relanceDate;
@property(nonatomic, strong)NSString * relanceHeure;

@property(nonatomic, strong)NSMutableArray * tasks;

@property(nonatomic, strong)NOVPopUpViewController_ipad * nOVPopUpViewController;
@property (nonatomic, strong)NOVArgumentaireViewController_ipad * nOVArgumentaireViewController_ipad;


@end

@implementation LivraisonViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setDateFromNotif:) name:SET_DATE_NOTIFICATION object:nil];
        
        self.tasks = [NSMutableArray array];
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    // NSString * url = [NSString stringWithFormat:@"%@%@",SELECT_TASKS_WS, sharedData.timeStamp];
    
     NSString * url = [NSString stringWithFormat:@"%@%@",SELECT_TASKS_WS_2, sharedData.timeStamp];
   
    
    [self doGet:url requestID:9998];
    
    NSString * url1 = [NSString stringWithFormat:@"%@%@",SELECT_JOB_CARD_WS, sharedData.timeStamp];
    
   // [self doGet:url1 requestID:9999];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    if (requestID == 9998) {
        
        NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
        
        if([[response objectForKey:@"results"] count] > 0){
            
            sharedData.jobCard = [[[response objectForKey:@"results"] objectAtIndex:0] objectForKey:@"INCIDENT_NUMBER"];
            
        }
        
        [self.tasks addObjectsFromArray:[response objectForKey:@"results"]];
        
        [self.tasksTableView reloadData];
        
        [self setUpView];
        
        
        NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]] ;
        
        NSDictionary * dataToSave = [sharedData createArchivableObject];
        
        NSInteger index = -1;
        
        for (id data in savedRDV) {
            
            if ([[data objectForKey:@"TIME_STAMP"] isEqualToString:sharedData.timeStamp]) {
                
                index = [savedRDV indexOfObject:data];
                
                break;
                
            }
            
        }
        
        if (index >= 0) {
            
            [savedRDV replaceObjectAtIndex:index withObject:dataToSave];
            
        }
        
        [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];
        
        
    }else if (requestID == 9999) {
        
        if ([[response objectForKey:@"results"] count]> 0) {
            
            id object = [[response objectForKey:@"results"] objectAtIndex:0];
            
            self.snLabel.text           = [object objectForKey:@"SN"];
            self.codeClientLabel.text   = [object objectForKey:@"CODE_CLIENT"];
            self.typeClientLabel.text   = [object objectForKey:@"TYPE_CLIENT"];
            self.pbSummuryLabel.text    = [object objectForKey:@"PB_SUMMARY"];
            self.workshopLabel.text     = [object objectForKey:@"WORKSHOP"];
            self.saLabel.text           = [object objectForKey:@"SA"];
            self.kmLabel.text           = [object objectForKey:@"KM"];
            
        }
        
    }else{
        //   [Utils showAlertView:@"Dossier clôturé"];
        
        self.nOVPopUpViewController = [[NOVPopUpViewController_ipad alloc]initWithNibName:@"NOVPopUpViewController" bundle:nil];
        
        self.nOVPopUpViewController.delegate = self;
        
        self.nOVPopUpViewController.popUpModeInteraction = PopUpModeInteractionEnabled;
        
        self.nOVPopUpViewController.message = @"Dossier clôturé";
        
        [self.view addSubview:self.nOVPopUpViewController.view];
        
        [self.nOVPopUpViewController show];
        
        //   [savedRDV replaceObjectAtIndex:index withObject:[[NOVSharedData sharedNOVData] createArchivableObject]];
        
        
        NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
        
        NSString * dateRelance = [NSString stringWithFormat:@"%@ %@", self.relanceDate, self.relanceHeure];
        
        NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]];
        
        [savedRDV removeObjectAtIndex:objectIndex];
        
        [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];
        
        
        //Relance
        
        if ([dateRelance length] > 10) {
            
            NSDictionary * relanceDict = [sharedData createArchivableObject];
            
            NSMutableArray * relances = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"RELANCES_OBJECTS"]];
            
            [relances addObject:relanceDict];
            
            [Utils saveMutableObject:relances  key:@"RELANCES_OBJECTS"];
            
        }
        
    }
    

    
}


-(void)gotError:(NSError *)error requestId:(NSInteger)requestID{
    
    
    
}

-(void)setUpView{
    
    [super setUpView];
    
    self.otherWorksView.layer.cornerRadius = 5;
    
    self.otherWorksView.layer.borderColor = [UIColor colorWithRed:155.0/255 green:160.0/255 blue:165.0/255 alpha:1.0].CGColor;
    
    self.otherWorksView.layer.borderWidth = 1;
    
    
    CGFloat total = 0;
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    self.clientLabel.text = sharedData.nomClient;
    
    NSInteger index = 0;
    
    for (id travail in self.tasks) {
        
        total += [Utils getNumber :[travail objectForKey:@"PRIX_TASK"]];
        
       // if (![[travail objectForKey:@"PRIX"] isEqualToString:@"0"]) {
            
            NOVLivraisonView * tView = [[[NSBundle mainBundle] loadNibNamed:@"NOVLivraisonView" owner:nil options:nil] objectAtIndex:0];
            
            tView.delegate = self;
            
            tView.travailLabel.text = [travail objectForKey:@"OP_DESCRIPTION"];
            
            tView.coutTF.text    = [travail objectForKey:@"PRIX_TASK"];
            
            tView.translatesAutoresizingMaskIntoConstraints = NO;
            
            if ([[travail objectForKey:@"OP_DESCRIPTION"] caseInsensitiveCompare:@"Remplacement Batterie"] == NSOrderedSame){
                
                tView.argumentaire = @"Batterie";
                
                tView.argumentaireImage.image = [UIImage imageNamed:@"t_car_battery2-.png"];
                
            }else if ([Utils checkString:[travail objectForKey:@"TASK_DESCRIPTION"] contains:@"Pneu"]|| [Utils checkString:[travail objectForKey:@"TASK_DESCRIPTION"] contains:@"géometrie"]|| [Utils checkString:[travail objectForKey:@"TASK_DESCRIPTION"] contains:@"Equlibrage"]||[Utils checkString:[travail objectForKey:@"TASK_DESCRIPTION"] contains:@"pneus"]){
                
                tView.argumentaire = @"Entretien des pneus";
                
                tView.argumentaireImage.image = [UIImage imageNamed:@"t_pneu2.png"];
                
            }else if ([Utils checkString:[travail objectForKey:@"OP_DESCRIPTION"] contains:@"Travaux carrosserie"]){
                
                tView.argumentaireImage.image = [UIImage imageNamed:@"t_travaux carrosserie2.png"];
                
                tView.argumentaire = @"Travaux Carrosserie";
                
            }else if ([Utils checkString:[travail objectForKey:@"OP_DESCRIPTION"] contains:@"optiques"]){
                
                tView.argumentaireImage.image = [UIImage imageNamed:@"t_rénovation_optiques_icone2.png"];
                
                tView.argumentaire = @"Rénovation des optiques";
                
            }else if ([Utils checkString:[travail objectForKey:@"OP_DESCRIPTION"] contains:@"Essuie-glace"]){
                
                tView.argumentaireImage.image = [UIImage imageNamed:@"t_Essuis glace2.png"];
                
                tView.argumentaire = @"Balais d’essuie-glaces";
                
            }else if ([Utils checkString:[travail objectForKey:@"OP_DESCRIPTION"] contains:@"jantes"]){
                
                tView.argumentaireImage.image = [UIImage imageNamed:@"t_Icone_netoyage_jantes2.png"];
                
                tView.argumentaire = @"Nettoyage des jantes";
                
            }else {
                
                tView.argumentaireImage.image = [UIImage imageNamed:@"t_Requetes_client_2.png"];
                
                tView.argumentaire = @"Révisions Périodiques";
                
            }
            
            
            
            
            [self.travauxView addSubview:tView];
            
            [self.travauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                         attribute:NSLayoutAttributeWidth
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.travauxView
                                                                         attribute:NSLayoutAttributeWidth
                                                                        multiplier:1
                                                                          constant:521]];
            
            [self.travauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                         attribute:NSLayoutAttributeHeight
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.travauxView
                                                                         attribute:NSLayoutAttributeHeight
                                                                        multiplier:1
                                                                          constant:66]];
            
            [self.travauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                         attribute:NSLayoutAttributeLeft
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.travauxView
                                                                         attribute:NSLayoutAttributeLeft
                                                                        multiplier:1
                                                                          constant:1]];
            
            CGFloat top = ([[self.travauxView  subviews] count] -1) * 66;
            
            [self.travauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.travauxView
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1
                                                                          constant:top]];
            
            index ++;
        }
        
       
   // }
    
    self.travauxViewHeightConstraint.constant = index  * 66;
    
    self.containterViewHeightConstraint.constant = self.containterViewHeightConstraint.constant + index  * 66;
    

    //TAddiionnels
    
    NSArray * travauxAdd = [NSArray array] ;//  sharedData.travauxAdditionnels;
    
  //  NSInteger travailIndex = 0;
    
    for (id travail in travauxAdd) {
        
        
        if ([[travail objectForKey:@"CONFIMATION"] isEqualToString:@"Yes"]) {
            
           // total += [Utils getNumber :[travail objectForKey:@"PRIX"]];
            
            NOVLivraisonView * tView = [[[NSBundle mainBundle] loadNibNamed:@"NOVLivraisonView" owner:nil options:nil] objectAtIndex:0];
            
            tView.travailLabel.text = [travail objectForKey:@"TASK_DESCRIPTION"];
            
            tView.delegate = self;
            
            tView.coutTF.text    = [travail objectForKey:@"TASK_NAME"];
            
            tView.translatesAutoresizingMaskIntoConstraints = NO;
            
            tView.argumentaireImage.image = [UIImage imageNamed:@"t_Requetes_client_2.png"];
            
            tView.argumentaire = @"Révisions Périodiques";
            
            [self.travauxView addSubview:tView];
            
            [self.travauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                         attribute:NSLayoutAttributeWidth
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.travauxView
                                                                         attribute:NSLayoutAttributeWidth
                                                                        multiplier:1
                                                                          constant:521]];
            
            [self.travauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                         attribute:NSLayoutAttributeHeight
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.travauxView
                                                                         attribute:NSLayoutAttributeHeight
                                                                        multiplier:1
                                                                          constant:66]];
            
            [self.travauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                         attribute:NSLayoutAttributeLeft
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.travauxView
                                                                         attribute:NSLayoutAttributeLeft
                                                                        multiplier:1
                                                                          constant:1]];
            
            CGFloat top = ([[self.travauxView  subviews] count] -1) * 66;
            
            [self.travauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.travauxView
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1
                                                                          constant:top]];
            
        }
       
    }
    
    self.travauxViewHeightConstraint.constant = self.travauxViewHeightConstraint.constant  + [travauxAdd count]  * 66;
    
    self.containterViewHeightConstraint.constant = self.containterViewHeightConstraint.constant +  [travauxAdd count]  * 66;
    
    NSLog(@"View Height %f",self.containterViewHeightConstraint.constant);
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width,  self.containterViewHeightConstraint.constant - 160);
    
    self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f", total];
    
    
    //V2
    
    
    
}

#pragma mark --
#pragma mark Actions

- (IBAction)hideOtherTasksView:(id)sender {
    
        [UIView animateWithDuration:0.3 animations:^{
            
            
            self.otherTasksView.alpha = 0;
            
        } completion:^(BOOL finished) {
            
            [self.otherTasksView removeFromSuperview];
            
        }] ;
        
    
}

- (IBAction)showOtherTasks:(id)sender {
    
    self.otherTasksView.alpha = 0;
    
    [self.view addSubview:self.otherTasksView];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        
        self.otherTasksView.alpha = 1;
        
    }];
    
    
}

- (IBAction)valider:(id)sender {
    
    if (!self.isOnline) {
        
        [Utils showAlertView:@"Veuillez vous assurer que vous disposez d'une connexion internet avant de continuer."];
        
        return;
        
    }
    
    NSLog(@"Text %@", self.heureTF.text);

    BOOL isSelected = NO;
    
    if (self.btn24.isSelected) {
        
        isSelected = YES;
        
    }else if (self.btn48.isSelected) {
        
        isSelected = YES;
        
    }else if (self.btn72.isSelected) {
        
        isSelected = YES;
        
    }
    
    
    if ((self.btn24.isSelected || self.btn48.isSelected || self.btn72.isSelected )&&  [self.heureTF.text length] == 0) {
        
        [self showError:@"Veuillez renseigner l'heure du Suivi Clients"];
        
        return;
        
    }else if( !isSelected &&  [self.heureTF.text length] > 0){
        
        [self showError:@"Veuillez renseigner l'heure du Suivi Clients"];
        
        return;
        
        
    }
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    for (id travail in sharedData.tousLesTravaux) {
        
        if (![sharedData.travauxConfirmes containsObject:travail]) {
            
            [sharedData.travauxConfirmes addObject:travail];
            
        }
        
    }
    
    
    NSString * dateRelance = [NSString stringWithFormat:@"%@ %@", self.relanceDate, self.relanceHeure];
    
    sharedData.rappelSuivi = [NSMutableDictionary dictionaryWithObjectsAndKeys:dateRelance ,@"DATE", @"OUI",@"STATUS", nil];
    
    NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]];
    
    NSInteger index = 0;
 
    
    for (id rdv  in savedRDV) {
        
        if ([[rdv objectForKey:@"OPERATION_CODE"] isEqualToString:sharedData.operationCode]) {
            
            index = [savedRDV indexOfObject:rdv];
            
            break;
        }
        
    }
    
    objectIndex = index;
    
    NSDictionary * dict = [NSDictionary dictionaryWithObject:[sharedData createArchivableObject] forKey:@"data"];
    
  //  [self doPost:SAVE_URL params:dict requestID:0];
    self.nOVPopUpViewController = [[NOVPopUpViewController_ipad alloc]initWithNibName:@"NOVPopUpViewController" bundle:nil];
    
    self.nOVPopUpViewController.delegate = self;
    
    self.nOVPopUpViewController.popUpModeInteraction = PopUpModeInteractionEnabled;
    
    self.nOVPopUpViewController.message = @"Dossier clôturé";
    
    [self.view addSubview:self.nOVPopUpViewController.view];
    
    [self.nOVPopUpViewController show];
    
    //   [savedRDV replaceObjectAtIndex:index withObject:[[NOVSharedData sharedNOVData] createArchivableObject]];

    
    
    [savedRDV removeObjectAtIndex:objectIndex];
    
    [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];
    
    
    //Relance
    
    if ([dateRelance length] > 10) {
        
        NSDictionary * relanceDict = [sharedData createArchivableObject];
        
        NSMutableArray * relances = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"RELANCES_OBJECTS"]];
        
        [relances addObject:relanceDict];
        
        [Utils saveMutableObject:relances  key:@"RELANCES_OBJECTS"];
        
    }
    
}

- (IBAction)programmerRappel:(id)sender {
    
    NOVTaggedButton * button = (NOVTaggedButton *)sender;
    
    self.btn24.selected = self.btn48.selected = self.btn72.selected = NO;
    
    [button setSelected:YES];
    
    NSString * dateRelance =[Utils addHoursToToday:button.content];
    
    self.relanceDate = dateRelance;
    
}

- (IBAction)selectHeure:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_DATE_PICKER_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1],@"SENDER", nil]];
    
}

#pragma mark --
#pragma mark Argumentaire 

-(void)nOVLivraisonView:(NOVLivraisonView *)nOVLivraisonView showArgumentaire:(NSString *)argumentaire{
    
    self.nOVArgumentaireViewController_ipad = [[NOVArgumentaireViewController_ipad alloc]initWithNibName:@"NOVArgumentaireViewController" bundle:nil];
    
    self.nOVArgumentaireViewController_ipad.categorie = argumentaire;
    
    self.nOVArgumentaireViewController_ipad.mode = NOVArgumentaireModeNormal;
    
    [self.view addSubview:self.nOVArgumentaireViewController_ipad.view];
    
    [self.nOVArgumentaireViewController_ipad show];
    
}

#pragma mark --
#pragma mark Notification

-(void)setDateFromNotif:(NSNotification *)notification{
    
    NSDictionary * dict = [notification userInfo];
    
    NSDate * date =[dict objectForKey:@"selected_date"] ;
    
    self.heureTF.text = [Utils stringFromDate:date dateFormat:@"HH:mm" ];
    
    self.relanceHeure = [Utils stringFromDate:date dateFormat:@"HH:mm" ];
    
}

#pragma mark --
#pragma mark PopUp Delegate 

-(void)nOVPopUpViewController:(NOVPopUpViewController *)nOVPopUpViewController showNext:(BOOL)showNext{
    
    [self hide];
    
     [[NSNotificationCenter defaultCenter] postNotificationName:FINISH_VISIT_NOTIFICATION object:nil];
}


//V2

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.tasks count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 74;
    
}

-(UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"NOVLivraisonCell";
    
    NOVLivraisonCell *cell = (NOVLivraisonCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        
        cell = (NOVLivraisonCell *)[self getCellWithIdentifier:cellIdentifier];
        
    }
    
    id task = [self.tasks objectAtIndex:indexPath.row];
    
    cell.taskNameLabel.text = [task objectForKey:@"TASK_NAME"];
    
    cell.taskDescriptionLabel.text = [task objectForKey:@"TASK_DESCRIPTION"];
    
    return cell;
}

@end
