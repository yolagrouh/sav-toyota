//
//  NOVLivraisonCell.h
//  SAV
//
//  Created by Mohamed AHDIDOU on 7/7/15.
//  Copyright (c) 2015 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NOVLivraisonCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *taskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskDescriptionLabel;
@end
