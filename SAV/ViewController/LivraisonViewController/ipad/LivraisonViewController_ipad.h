//
//  LivraisonViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LivraisonViewController.h"
#import "NOVLivraisonView.h"
#import "NOVPopUpViewController_ipad.h"

@interface LivraisonViewController_ipad : LivraisonViewController<NOVPopUpViewControllerDelegate, NOVLivraisonViewDelegate, UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UIView *travauxView;

// Constraints

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *travauxViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containterViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NOVTextField *coutTotalTF;

@property (weak, nonatomic) IBOutlet NOVTextField *heureTF;


@property (weak, nonatomic) IBOutlet UIButton *btn24;

@property (weak, nonatomic) IBOutlet UIButton *btn48;

@property (weak, nonatomic) IBOutlet UIButton *btn72;


@property (weak, nonatomic) IBOutlet UILabel *clientLabel;

//V2
@property (weak, nonatomic) IBOutlet UITableView *tasksTableView;
@property (weak, nonatomic) IBOutlet UILabel *snLabel;
@property (weak, nonatomic) IBOutlet UILabel *codeClientLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeClientLabel;
@property (weak, nonatomic) IBOutlet UILabel *pbSummuryLabel;
@property (weak, nonatomic) IBOutlet UILabel *workshopLabel;
@property (weak, nonatomic) IBOutlet UILabel *saLabel;
@property (weak, nonatomic) IBOutlet UILabel *kmLabel;
@property (strong, nonatomic) IBOutlet UIView *otherTasksView;
@property (weak, nonatomic) IBOutlet UIView *otherWorksView;

- (IBAction)hideOtherTasksView:(id)sender;

- (IBAction)showOtherTasks:(id)sender;

- (IBAction)valider:(id)sender ;

- (IBAction)programmerRappel:(id)sender;

- (IBAction)selectHeure:(id)sender;

@end
