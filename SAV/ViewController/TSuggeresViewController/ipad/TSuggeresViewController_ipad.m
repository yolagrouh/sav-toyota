#import "TSuggeresViewController_ipad.h"
#import "TAdditionnelsViewController_ipad.h"
#import "NOVArgumentaireViewController_ipad.h"
#import "NOVSharedUSer.h"
#import "NOVPopUpViewController_ipad.h"
#import "AIDatePickerController.h"
#import "SSFlatDatePicker.h"
#import "NOVTaggedButton.h"

@interface TSuggeresViewController_ipad (){
    
    NSInteger travauxCount;
    
    CGSize initialSVSize;
    NSInteger numberOfRequests;
    NSInteger requestsCount;
    
    
    CGFloat initialTravauxViewHeightHeight;
    CGFloat initialSchemaEclateViewHeightHeight;
    CGFloat initialInfosClientViewHeightHeight;
    CGFloat initialContentViewHeightHeight;
    CGFloat initialRequetesClientvViewHeightHeight;
    
    BOOL didShowMoreTravaux;
}

@property (nonatomic, strong) IBOutlet SSFlatDatePicker *datePicker;

@property (nonatomic, strong)NOVMoreTravauxViewController_ipad * nOVMoreTravauxViewController;
@property (nonatomic, strong)NOVArgumentaireViewController_ipad * nOVArgumentaireViewController_ipad;
@property(nonatomic, strong)NOVPopUpViewController_ipad * nOVPopUpViewController;


@property(nonatomic, strong)NSMutableArray * travauxCarosserieObjects;

@property(nonatomic, strong)NSMutableArray * mappingObjects;

@property(nonatomic, strong)NSMutableArray * outTCarosserieObjects;

@end

@implementation TSuggeresViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.outTCarosserieObjects = [NSMutableArray array];
        
        self.mappingObjects = [NSMutableArray array];
        
        travauxCount = -1;
        
        numberOfRequests = 0;
        
        requestsCount = 0;
        
        didShowMoreTravaux = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setDateFromNotif:) name:SET_DATE_NOTIFICATION object:nil];
        
        self.travauxCarosserieObjects= [NSMutableArray array];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    
    NSString * timeStamp = [Utils getTimeStamp];
    
    sharedData.timeStamp = timeStamp;
    
    //Default Date
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    self.dateTF.text = [dateFormatter stringFromDate:[NSDate date]];
    
    [dateFormatter setDateFormat:@"HH:mm"];
    
    self.heureTF.text = [dateFormatter stringFromDate:[NSDate date]];

    
    sharedData.livraisonDate = self.dateTF.text;
    
    sharedData.livraisonHeure = self.heureTF.text;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    NOVSharedData * shareData = [NOVSharedData sharedNOVData];
    
    shareData.demandesClient = [NSString string];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setUpView{
    
    //Load Travaux Carosserie Objects
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Malle",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Feu AR droite",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Feu AR gauche",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Pneu AR Gauche",@"OP_CODE":@"PNE999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Pare brise AR",@"OP_CODE":@"TOL003"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Pare brise AV",@"OP_CODE":@"TOL003"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Pare brise AV",@"OP_CODE":@"TOL003"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Essuie-glace AV",@"OP_CODE":@"850071"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Essuie-glace AR",@"OP_CODE":@"850081"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Pare choc AV",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Capot",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Rétroviseur Extérieur Gauche",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Optique AV Gauche",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Optique AV Droite",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Rétroviseur Extérieur Droite",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Pneu AV Gauche",@"OP_CODE":@"PNE999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Pneu AR Droite",@"OP_CODE":@"PNE999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Toit",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Porte AR Droite",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Vitre AR Droite",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Porte AV Droite",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Vitre AV Droite",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Porte AR Gauche",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Porte AV Gauche",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Vitre AR Gauche",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Vitre AV Gauche",@"OP_CODE":@"B999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Aile AR Droite",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Aile AR Gauche",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Aile AV Droite",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Aile AV Gauche",@"OP_CODE":@"TOL999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Pneu AV Droite",@"OP_CODE":@"PNE999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Batterie",@"OP_CODE":@"190011"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Permutation des pneus",@"OP_CODE":@"GJ999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Equlibrage",@"OP_CODE":@"EQU001"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Outils",@"OP_CODE":@"GJ999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Roue de secours",@"OP_CODE":@"GJ999"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Contrôle géometrie",@"OP_CODE":@"PAR001"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Nettoyage des jantes",@"OP_CODE":@"LUSJANT"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Optique Gauche",@"OP_CODE":@"LUS1OP"}];
    
    [self.travauxCarosserieObjects addObject:@{@"OP_DESCRIPTION":@"Optique Droite",@"OP_CODE":@"LUS1OP"}];
    
    //Load Travaux Carosserie Objects
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.contentSV.frame.size.height + 40);
    
    initialSVSize                               = self.contentSV.contentSize;
    
    initialTravauxViewHeightHeight              = self.travauxViewHeightConstraint.constant;
    initialSchemaEclateViewHeightHeight         = self.schemaEclateViewHeightConstraint.constant;
    initialInfosClientViewHeightHeight          = self.infosClientViewHeightConstraint.constant;
    initialContentViewHeightHeight              = self.contentViewHeightConstraint.constant;
    initialRequetesClientvViewHeightHeight      = self.requetesClientvViewHeightConstraint.constant;
    
    [super setUpView];
    
    [self addShemaEclate];
    
    [self addRequetesClient];
    
    [self addAutresRequetesClient];
    
    
}

-(void)addRequetesClient{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    CGFloat cout = 0;
    
    for (id  travail in sharedData.selectedRequestsClient) {
        
        travauxCount ++;
        
        NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
        
        [sharedData.tousLesTravaux addObject:[NSDictionary dictionaryWithObjectsAndKeys:[travail objectForKey:@"MONTANT"] ,@"PRIX",[travail objectForKey:@"OP_DESCRIPTION"],@"TRAVAIL",@"OUI",@"STATUS", nil]];
        
        
        //[self insetMappedobject:[travail objectForKey:@"OP_DESCRIPTION"] prix:[travail objectForKey:@"MONTANT"]];
        
        cout += [Utils getNumber:[travail objectForKey:@"MONTANT"]];
        
        NOVTView * tView = [[[NSBundle mainBundle] loadNibNamed:@"NOVTView" owner:nil options:nil] objectAtIndex:0];
        
        
        if ( [Utils checkString: [travail objectForKey:@"OP_CODE"] contains:@"PMA"]) {
            
            tView.argumentaireBtn.content=  @"Révisions Périodiques";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_Requetes_client_2.png"];
            
            tView.argumentaireBtn.hidden = NO;
            
            tView.argumentaireImageView.hidden = NO;
            
            tView.argumentaireBtn.tag = [sharedData.selectedRequestsClient indexOfObject:travail];
            
        }
        
        tView.delegate = self;
        
        tView.checkBox.selected = YES;
        
        
        tView.tag = travauxCount;
        
        tView.travailLabel.text = [travail objectForKey:@"OP_DESCRIPTION"];
        
        tView.coutTF.content    = [travail objectForKey:@"OP_DESCRIPTION"];
        
        tView.coutTF.text       = [travail objectForKey:@"MONTANT"];
        
        tView.coutTF.enabled    = YES;
        
        tView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.requetesClientView addSubview:tView];
        
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeWidth
                                                                           multiplier:0
                                                                             constant:516]];
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeHeight
                                                                           multiplier:0
                                                                             constant:68]];
        
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeLeft
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeLeft
                                                                           multiplier:1
                                                                             constant:0]];
        
        CGFloat top = ([[self.requetesClientView  subviews] count] -1) * 68;
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeTop
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeTop
                                                                           multiplier:1
                                                                             constant:top]];
        
    }
    
    self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",cout];
    
    
    if ([sharedData.selectedRequestsClient count] <=1){
        
        self.requetesClientvViewHeightConstraint.constant = 100;
        
    }else if ([sharedData.selectedRequestsClient count] > 2) {
        
        self.requetesClientvViewHeightConstraint.constant =  [sharedData.selectedRequestsClient count]  * 68;
        
        self.travauxViewHeightConstraint.constant = self.travauxViewHeightConstraint.constant +  ([sharedData.selectedRequestsClient count] - 2)  * 68;
        
    }
    
    
    
    self.contentViewHeightConstraint.constant = self.contentViewHeightConstraint.constant + self.requetesClientvViewHeightConstraint.constant;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width,  self.contentViewHeightConstraint.constant - 160 * [sharedData.selectedRequestsClient count]);
    
}

-(void)addAutresRequetesClient{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    
    for (id  travail in sharedData.selectedOtherRequestsClient) {
        
        travauxCount ++;
        
        [sharedData.tousLesTravaux addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"0" ,@"PRIX",travail,@"TRAVAIL",@"NON",@"STATUS",nil]];
        
        //[self insetMappedobject:travail prix:@"0"];
        
        NOVTView * tView = [[[NSBundle mainBundle] loadNibNamed:@"NOVTView" owner:nil options:nil] objectAtIndex:0];
        
        tView.tag = travauxCount;
        
        tView.delegate = self;
        
        tView.travailLabel.text =  [sharedData getRequestDescription:travail];
        
        tView.coutTF.content    = travail;
        
        tView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.requetesClientView addSubview:tView];
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeWidth
                                                                           multiplier:0
                                                                             constant:516]];
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeHeight
                                                                           multiplier:0
                                                                             constant:68]];
        
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeLeft
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeLeft
                                                                           multiplier:1
                                                                             constant:0]];
        
        CGFloat top = ([[self.requetesClientView  subviews] count] -1) * 68;
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeTop
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeTop
                                                                           multiplier:1
                                                                             constant:top]];
        
    }
    
    self.requetesClientvViewHeightConstraint.constant = self.requetesClientvViewHeightConstraint.constant + [sharedData.selectedOtherRequestsClient count]  * 68 ;
    
    self.travauxViewHeightConstraint.constant = self.travauxViewHeightConstraint.constant +  [sharedData.selectedOtherRequestsClient count]  * 68 ;
    
    self.contentViewHeightConstraint.constant = self.contentViewHeightConstraint.constant + [sharedData.selectedOtherRequestsClient count]  * 68;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width,  self.contentViewHeightConstraint.constant- 160 );
    
}

-(void)addShemaEclate{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
//    if ([self.travaux containsObject:@"Travaux carrosserie"]) {
//        
//        [self.travaux removeObject:@"Travaux carrosserie"];
//        
//        [self.travaux addObject:@"Travaux carrosserie"];
//        
//    }

    
    for (NSString * travail in self.travaux) {
        
        travauxCount ++;
        
        [sharedData.tousLesTravaux addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"0" ,@"PRIX",travail,@"TRAVAIL",@"NON",@"STATUS", nil]];
        
        //[self insetMappedobject:travail prix:@"0"];
        
        NOVTView * tView = [[[NSBundle mainBundle] loadNibNamed:@"NOVTView" owner:nil options:nil] objectAtIndex:0];
        
        tView.tag = travauxCount;
        
        tView.delegate = self;
        
        tView.travailLabel.text = travail;
        
       
        if ([travail caseInsensitiveCompare:@"Travaux carrosserie"] == NSOrderedSame) {
            
            tView.expandImage.hidden = NO;
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 1;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Travaux Carrosserie";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_travaux carrosserie2.png"];
            
        }else if ([travail caseInsensitiveCompare:@"Remplacement Batterie"] == NSOrderedSame){
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 1;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Batterie";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_car_battery2-.png"];
            
        }else if ([Utils checkString:travail contains:@"Pneu"]||[Utils checkString:travail contains:@"géometrie"]||[Utils checkString:travail contains:@"Equlibrage"]||[Utils checkString:travail contains:@"Permutation des pneus"]){//[travail isEqualToString:@"Remplacement Batterie"]
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 2;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Entretien des pneus";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_pneu2.png"];
            
        }else if ([Utils checkString:travail contains:@"Essuie-glace"]){//[travail isEqualToString:@"Remplacement Batterie"]
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 2;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Balais d’essuie-glaces";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_Essuis glace2.png"];
            
        }else if ([Utils checkString:travail contains:@"optiques"]){//[travail isEqualToString:@"Remplacement Batterie"]
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 2;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Rénovation des optiques";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_rénovation_optiques_icone2.png"];
            
        }else if ([Utils checkString:travail contains:@"jantes"]){//[travail isEqualToString:@"Remplacement Batterie"]
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 2;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Nettoyage des jantes";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_Icone_netoyage_jantes2.png"];
            
        }
        
        tView.coutTF.content    = travail;
        
        tView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.schemaEclateTravauxView addSubview:tView];
        
        if ([self.mappingObjects count] > 0) {
            
            NSLog(@"Mapped Travail %@",[self getMappedPrice:travail]);
            
            if (![[self getMappedPrice:travail] isEqualToString:@"0"]&& ![[self getMappedPrice:travail] isEqualToString:@""]) {
                
                tView.coutTF.text = [self getMappedPrice:travail];
                
                tView.checkBox.selected = YES;
                
            }
            
            
        }
        
       
        
        [self.schemaEclateTravauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                                 attribute:NSLayoutAttributeWidth
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.schemaEclateTravauxView
                                                                                 attribute:NSLayoutAttributeWidth
                                                                                multiplier:0
                                                                                  constant:516]];
        
        [self.schemaEclateTravauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.schemaEclateTravauxView
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                multiplier:0
                                                                                  constant:68]];
        
        [self.schemaEclateTravauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                                 attribute:NSLayoutAttributeLeft
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.schemaEclateTravauxView
                                                                                 attribute:NSLayoutAttributeLeft
                                                                                multiplier:1
                                                                                  constant:0]];
        
        CGFloat top = ([[self.schemaEclateTravauxView  subviews] count] -1) * 68;
        
        [self.schemaEclateTravauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                                 attribute:NSLayoutAttributeTop
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.schemaEclateTravauxView
                                                                                 attribute:NSLayoutAttributeTop
                                                                                multiplier:1
                                                                                  constant:top]];
    }
    
    self.schemaEclateViewHeightConstraint.constant = [self.travaux count]  * 68;
    
    self.travauxViewHeightConstraint.constant = self.travauxViewHeightConstraint.constant +  [self.travaux count]  * 68;
    
    self.contentViewHeightConstraint.constant = self.contentViewHeightConstraint.constant + [self.travaux count]  * 68;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width,  self.contentViewHeightConstraint.constant - 160 );
    
}

#pragma mark --
 
#pragma mark Data

-(void)sendData{
    
//    NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]] ;
//    
//    if (!savedRDV) {
//        
//        savedRDV = [NSMutableArray array];
//    }
//    
//    [savedRDV addObject:[[NOVSharedData sharedNOVData] createArchivableObject]];
//    
//    [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    
    NOVSharedUSer * sharedUser = [NOVSharedUSer sharedNOVUser];
    
    numberOfRequests = [[self.requetesClientView subviews] count] + [[self.schemaEclateTravauxView subviews] count];
    
    NSInteger index = 0;
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    for (NOVTView * view in [self.requetesClientView subviews]) {
        
        NSString * taskName = [[sharedData getRequestCode:view.travailLabel.text] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        NSString * taskDescription = [[sharedData getRequestDescription:view.travailLabel.text] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString * statut ;
        
        view.checkBox.enabled ? (statut = @"Yes" ): (statut = @"No");
        
        NSString * requete = [NSString stringWithFormat:@"field_IPAD_NUMBER=%@&field_TASK_NAME=%@&field_TASK_DESCRIPTION=%@&field_TASK_TYPE=%@&field_CONFIMATION=%@&field_STATUS=New&field_MESSAGE=",sharedData.timeStamp,taskName, taskDescription, taskName,statut];
        
        
        requete = [requete stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
      //  [self doGet:[NSString stringWithFormat:@"%@%@",TASK_SAVE_WS, requete] requestID:[[self.requetesClientView subviews] indexOfObject:view]];
        
        [params setObject:sharedData.timeStamp  forKey:[NSString stringWithFormat:@"tasks[%d][IPAD_NUMBER]",index]];
        [params setObject:taskName              forKey:[NSString stringWithFormat:@"tasks[%d][TASK_NAME]",index]];
        [params setObject:taskDescription       forKey:[NSString stringWithFormat:@"tasks[%d][TASK_DESCRIPTION]",index]];
        [params setObject:taskName              forKey:[NSString stringWithFormat:@"tasks[%d][TASK_TYPE]",index]];
        [params setObject:statut                forKey:[NSString stringWithFormat:@"tasks[%d][CONFIMATION]",index]];
        [params setObject:@"New"                forKey:[NSString stringWithFormat:@"tasks[%d][STATUS]",index]];
        [params setObject:@""                   forKey:[NSString stringWithFormat:@"tasks[%d][MESSAGE]",index]];
        
        index ++;
        
    }
    
    for (NOVTView * view in [self.schemaEclateTravauxView subviews]) {
        
        
        
        if ([view.travailLabel.text isEqualToString:@"Travaux Carrosserie"]) {
            
            
                for (id travail in self.travauxCarosserie) {
                    
                    NSString * taskDescription = [travail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    NSString * taskName = [self getRequestCode:travail];
                    
                
                    NSString * statut ;
                    
                    view.checkBox.selected ? (statut = @"Yes" ): (statut = @"No");
                    
                    NSString * requete = [NSString stringWithFormat:@"field_IPAD_NUMBER=%@&field_TASK_NAME=%@&field_TASK_DESCRIPTION=%@&field_TASK_TYPE=%@&field_CONFIMATION=%@&field_STATUS=New&field_MESSAGE=",sharedData.timeStamp,taskName, taskDescription, taskName,statut];
                    
                    requete = [requete stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    
                   // [self doGet:[NSString stringWithFormat:@"%@%@",TASK_SAVE_WS, requete] requestID:[[self.requetesClientView subviews] indexOfObject:view]];
                    
                    [params setObject:sharedData.timeStamp  forKey:[NSString stringWithFormat:@"tasks[%d][IPAD_NUMBER]",index]];
                    [params setObject:taskName              forKey:[NSString stringWithFormat:@"tasks[%d][TASK_NAME]",index]];
                    [params setObject:taskDescription       forKey:[NSString stringWithFormat:@"tasks[%d][TASK_DESCRIPTION]",index]];
                    [params setObject:taskName              forKey:[NSString stringWithFormat:@"tasks[%d][TASK_TYPE]",index]];
                    [params setObject:statut                forKey:[NSString stringWithFormat:@"tasks[%d][CONFIMATION]",index]];
                    [params setObject:@"New"                forKey:[NSString stringWithFormat:@"tasks[%d][STATUS]",index]];
                    [params setObject:@""                   forKey:[NSString stringWithFormat:@"tasks[%d][MESSAGE]",index]];
                    
                    index++;
                    
                }
            
        }else{
            
            NSString * taskDescription = [view.travailLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSString * taskName = [[self getRequestCode:view.travailLabel.text]  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            
            if (![Utils checkString:taskDescription contains:@"Permutation" ]) {
                
                
                
                NSString * statut ;
                
                view.checkBox.selected ? (statut = @"Yes" ): (statut = @"No");
                
                NSString * requete = [NSString stringWithFormat:@"field_IPAD_NUMBER=%@&field_TASK_NAME=%@&field_TASK_DESCRIPTION=%@&field_TASK_TYPE=%@&field_CONFIMATION=%@&field_STATUS=New&field_MESSAGE=",sharedData.timeStamp,taskName, taskDescription, taskName,statut];
                
                NSLog(@"Requete %@", requete);
                
                
                requete = [requete stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                
               // [self doGet:[NSString stringWithFormat:@"%@%@",TASK_SAVE_WS, requete] requestID:[[self.requetesClientView subviews] indexOfObject:view]];
                
                [params setObject:sharedData.timeStamp  forKey:[NSString stringWithFormat:@"tasks[%d][IPAD_NUMBER]",index]];
                [params setObject:taskName              forKey:[NSString stringWithFormat:@"tasks[%d][TASK_NAME]",index]];
                [params setObject:taskDescription       forKey:[NSString stringWithFormat:@"tasks[%d][TASK_DESCRIPTION]",index]];
                [params setObject:taskName              forKey:[NSString stringWithFormat:@"tasks[%d][TASK_TYPE]",index]];
                [params setObject:statut                forKey:[NSString stringWithFormat:@"tasks[%d][CONFIMATION]",index]];
                [params setObject:@"New"                forKey:[NSString stringWithFormat:@"tasks[%d][STATUS]",index]];
                [params setObject:@""                   forKey:[NSString stringWithFormat:@"tasks[%d][MESSAGE]",index]];
                
                index++;
            }
            
            
        }
        
    }
    
    [self doPost:TASK_INSERT_WS params:params requestID:999888];
    
}

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];

    if (requestID == 99999) {
        
    }else if (requestID == 999888){
        
        [self createJobCard:nil];
        
        
        
        [self showValidation];
        
    }else if (requestID == 999777){
        
        NSLog(@"rifhreih");
    }
    
//    requestsCount ++;
//    
//    if (requestsCount == numberOfRequests) {
//        
//        
//        
//        [self showValidation];
//        
//    }
    
}

#pragma mark --
#pragma mark Actions

- (IBAction)imprimerConvention:(id)sender {
    
    if([self.heureTF.text length] == 0 || [self.dateTF.text length] == 0 ){
        
        
        [self showError:@"Veuillez renseigner l'heure du Suivi Clients"];
        
        return;
        
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:IMPRESSION_NOTIFICATION object:nil userInfo:nil];
    
}

- (IBAction)showCalendar:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_DATE_PICKER_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:[sender tag]],@"SENDER", nil]];
    
    //    NOVCalendarView * calendarView = [[[NSBundle mainBundle] loadNibNamed:@"NOVCalendarView" owner:nil options:nil] objectAtIndex:0];
    //
    //    switch ([sender tag]) {
    //        case 0:
    //
    //            calendarView.datePicker.datePickerMode = SSFlatDatePickerModeDate;
    //            calendarView.datePickerMode = SSFlatDatePickerModeDate;
    //
    //            break;
    //        case 1:
    //
    //            calendarView.datePicker.datePickerMode = SSFlatDatePickerModeTime;
    //            calendarView.datePickerMode = SSFlatDatePickerModeTime;
    //
    //            break;
    //
    //        default:
    //            break;
    //    }
    //
    //    calendarView.frame = CGRectMake(-calendarView.frame.size.width, 0, calendarView.frame.size.width, calendarView.frame.size.height);
    //
    //    calendarView.tag = [sender tag];
    //
    //    calendarView.delegate = self;
    //
    //    [self.clientView addSubview:calendarView];
    //
    //    [UIView animateWithDuration:0.3 animations:^{
    //
    //
    //        calendarView.frame = CGRectMake(0, 0, calendarView.frame.size.width, calendarView.frame.size.height);
    //
    //    } ];
    
}



- (IBAction)valider:(id)sender {
    
    if (!self.isOnline) {
        
        [Utils showAlertView:@"Veuillez vous assurer que vous disposez d'une connexion internet avant de continuer."];
        
        return;
        
    }
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    for (id travail in sharedData.tousLesTravaux) {
        
        if ([[travail objectForKey:@"STATUS"] isEqualToString:@"OUI"] && [[travail objectForKey:@"PRIX"] length] ==0) {
            
            [Utils showAlertView:[NSString stringWithFormat:@"Veuillez renseigner le prix du travail : %@",[travail objectForKey:@"TRAVAIL"]]];
            
            return;
            
        }
        
    }
    
    if ([self.dateTF.text length] == 0 || [self.heureTF.text length] ==0) {
        
        [Utils showAlertView:@"Veuillez renseigner l'heure du Suivi Clients."];
        
        return;
        
    }
    
    
    if (!self.isOnline) {
        
        [Utils showAlertView:@"Veuillez vous assurer que vous disposez d'une connexion internet avant de continuer."];
        
        return;
        
    }
    
    [self sendData];
    
}

- (IBAction)choisirOperation:(id)sender {
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    
    NSMutableArray * demandesClient = [NSMutableArray arrayWithArray:[sharedData.demandesClient componentsSeparatedByString:@","]];
    
    NOVTaggedButton * taggedButton = (NOVTaggedButton *)sender;
    
    taggedButton.selected = !taggedButton.selected;
    
    if (taggedButton.selected) {
        
        [demandesClient addObject:taggedButton.content];
        
    }else{
        
        [demandesClient removeObject:taggedButton.content];
    }
    
    if ([demandesClient count] >0) {
        if ([[demandesClient objectAtIndex:0] length] == 0) {
            
            
            [demandesClient removeObjectAtIndex:0];
        }
    }
    
    
    
    sharedData.demandesClient = [demandesClient componentsJoinedByString:@","];
    
}

#pragma mark --
#pragma mark TextField

-(void)nOVTView:(NOVTView *)nOVTView input:(NSString *)input previousValue:(NSString *)previousValue{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    //[self insetMappedobject:nOVTView.travailLabel.text prix:input];
    
    NSMutableDictionary * dict ;
    
    if ([sharedData.tousLesTravaux count] > 0) {
        for (id tr in sharedData.tousLesTravaux) {
            
            if ([[tr objectForKey:@"TRAVAIL"] caseInsensitiveCompare:nOVTView.coutTF.content] == NSOrderedSame) {
                
                dict = [NSMutableDictionary dictionaryWithDictionary:tr];
                
                [dict setObject:input forKey:@"PRIX"];
                
                [dict setObject:@"OUI" forKey:@"STATUS"];
                
                break;
                
            }
            
        }
        
        [sharedData.tousLesTravaux replaceObjectAtIndex:nOVTView.tag withObject:dict];
        
        CGFloat total = 0;
        
        for (id tr in sharedData.tousLesTravaux) {
            
            if ([[tr objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
                
                total += [Utils getNumber:[tr objectForKey:@"PRIX"]] ;
                
            }
            
        }
        
        
        self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",total];
    }
    
   
    
}

-(void)nOVTView:(NOVTView *)nOVTView cleared:(BOOL)cleared previousValue:(NSString *)value{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSMutableDictionary * dict ;
    
    //[self insetMappedobject:nOVTView.travailLabel.text prix:@"0"];
    
    if ([sharedData.tousLesTravaux count] > 0) {
        for (id travail in sharedData.tousLesTravaux) {
            
            if ([[travail objectForKey:@"TRAVAIL"] caseInsensitiveCompare:nOVTView.coutTF.content] == NSOrderedSame) {
                
                dict = [NSMutableDictionary dictionaryWithDictionary:travail];
                
                [dict setObject:@"NON" forKey:@"STATUS"];
                
                [dict setObject:@"0" forKey:@"PRIX"];
                
                break;
            }
            
        }
        
        [sharedData.tousLesTravaux replaceObjectAtIndex:nOVTView.tag withObject:dict];
        
        CGFloat total = 0;
        
        for (id tr in sharedData.tousLesTravaux) {
            
            if ([[tr objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
                
                total += [Utils getNumber:[tr objectForKey:@"PRIX"]] ;
                
            }
            
        }
        
        self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",total];
    }
    
   
    
}

-(void)nOVTView:(NOVTView *)nOVTView selected:(BOOL)selected travail:(NSString *)travail prix:(NSString * )prix{
    
    CGFloat cout = [Utils getNumber:prix];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    
    //[self insetMappedobject:nOVTView.travailLabel.text prix:prix];
    
    if (selected) {
        
        [sharedData.tousLesTravaux replaceObjectAtIndex:nOVTView.tag withObject:[NSDictionary dictionaryWithObjectsAndKeys:travail, @"TRAVAIL",prix,@"PRIX",@"OUI",@"STATUS", nil]];
        
        
        
    }else{
        
        
        NSDictionary * dict =  [NSDictionary dictionaryWithObjectsAndKeys:travail, @"TRAVAIL",prix,@"PRIX",@"NON",@"STATUS", nil];
        
        NSInteger index = -1;
        
        for (id tr in sharedData.tousLesTravaux) {
            
            if ([[tr objectForKey:@"TRAVAIL"] caseInsensitiveCompare:travail] ==  NSOrderedSame) {
                
                //[sharedData.tousLesTravaux removeObject:tr];
                
                index = [sharedData.tousLesTravaux indexOfObject:tr];
                
                break;
            }
            
        }
        
        [sharedData.tousLesTravaux replaceObjectAtIndex:index withObject:dict];
    }
    
    CGFloat total = 0;
    
    
    for (id tr in sharedData.tousLesTravaux) {
        
        if ([[tr objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
            
            total += [Utils getNumber:[tr objectForKey:@"PRIX"]] ;
            
        }
        
    }
    
    
    self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",total];
    
}

-(void)nOVTView:(NOVTView *)nOVTView showPopUp:(BOOL)showPopUp{
    
    if ([nOVTView.travailLabel.text caseInsensitiveCompare:@"Travaux carrosserie"] != NSOrderedSame) {
        
        return;
    }
    
    
    if (!didShowMoreTravaux) {
        
        self.nOVMoreTravauxViewController = [[NOVMoreTravauxViewController_ipad alloc]initWithNibName:@"NOVMoreTravauxViewController" bundle:nil];
        
        self.nOVMoreTravauxViewController.delegate = self;
        
        self.nOVMoreTravauxViewController.travaux = [NSArray arrayWithArray:self.travauxCarosserie];
        
        [self.view addSubview:self.nOVMoreTravauxViewController.view];
        
        [self.nOVMoreTravauxViewController show];
    }
    
    
}

-(void)nOVTView:(NOVTView *)nOVTView showArgumentairePopUp:(BOOL)showPopUp sender:(NOVTaggedButton *)sender{
    
    
    self.nOVArgumentaireViewController_ipad = [[NOVArgumentaireViewController_ipad alloc]initWithNibName:@"NOVArgumentaireViewController" bundle:nil];
    
    self.nOVArgumentaireViewController_ipad.categorie = sender.content;
    
    [self.view addSubview:self.nOVArgumentaireViewController_ipad.view];
    
    [self.nOVArgumentaireViewController_ipad show];
    
}

#pragma mark --
#pragma mark Calendar

-(void)nOVCalendarView:(NOVCalendarView *)nOVCalendarView date:(NSString *)date{
    
    self.contentSV.scrollEnabled = YES;
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    switch (nOVCalendarView.tag) {
        case 0:
            
            self.dateTF.text = date;
            
            sharedData.livraisonDate = date;
            
            break;
        case 1:
            
            self.heureTF.text = date;
            
            sharedData.livraisonHeure = date;
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark --
#pragma mark Validation

-(void)showNext{
    
    NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]] ;
    
    if (!savedRDV) {
        
        savedRDV = [NSMutableArray array];
    }
    
    [savedRDV addObject:[[NOVSharedData sharedNOVData] createArchivableObject]];
    
    [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];
    
    
    self.nOVPopUpViewController = [[NOVPopUpViewController_ipad alloc]initWithNibName:@"NOVPopUpViewController" bundle:nil];
    
    self.nOVPopUpViewController.delegate = self;
    
    self.nOVPopUpViewController.message = @"Ce dossier a été enregistré avec succès";
    
    self.nOVPopUpViewController.view.frame = CGRectMake(0, -self.nOVPopUpViewController.view.frame.size.height, self.nOVPopUpViewController.view.frame.size.width, self.nOVPopUpViewController.view.frame.size.height);
    
    [self.view addSubview:self.nOVPopUpViewController.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.nOVPopUpViewController.view.frame = CGRectMake(0, 0, self.nOVPopUpViewController.view.frame.size.width, self.nOVPopUpViewController.view.frame.size.height);
        
    }];
    
}

#pragma mark --
#pragma mark MoreTravaux Delegate

-(void)nOVMoreTravauxViewController:(NOVMoreTravauxViewController *)nOVMoreTravauxViewController selectedTravaux:(NSArray *)travaux{
    
    didShowMoreTravaux  = NO;
    travauxCount = -1;
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSMutableArray * tempArray2 = [NSMutableArray arrayWithArray:self.travaux];
    
    NSMutableArray * tempArray3 = [NSMutableArray arrayWithArray:self.travauxCarosserie];
    
    [sharedData.tousLesTravaux removeAllObjects];
    
    
    for (id travail in travaux) {
        
        if ([tempArray3 containsObject:travail]) {
            
            [self.travauxCarosserie removeObject:travail];
            
            [self.outTCarosserieObjects addObject:travail];
            
        }
    }
    
    for (id travail in travaux) {
        
        if ([tempArray2 containsObject:travail]) {
            
            [self.travaux removeObject:travail];
            
        }
    }
    
    //Add Selected Travaux
    
    
    //   NSIndexSet * indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange([self.travaux indexOfObject:@"Travaux carrosserie"]+1, [travaux count])];
    
    
    if ([self.travauxCarosserie count] ==0) {
        
        [self.travaux removeObject:@"Travaux carrosserie"];
        
    }
    
    for (id travail in self.outTCarosserieObjects) {
        
        for (id tTra in self.travaux) {
            
            if ([tTra isEqual:travail]) {
                
                
                [self.travaux removeObject:tTra];
                
                break;
                
            }
            
        }
        
    }
    
        if ([self.travaux containsObject:@"Travaux carrosserie"]) {
    
            [self.travaux removeObject:@"Travaux carrosserie"];
    
            [self.travaux addObject:@"Travaux carrosserie"];
    
        }
    
    [self.travaux addObjectsFromArray:self.outTCarosserieObjects];
    
    
    [self.mappingObjects removeAllObjects];
    
    for (NOVTView * view in [self.schemaEclateTravauxView subviews]) {
        
        if ([view.coutTF.text length]) {
            
            
            [self insetMappedobject:view.travailLabel.text prix:view.coutTF.text];
            
        }
        
        
    }
    
    for (UIView * view in [self.schemaEclateTravauxView subviews]) {
        
        [view removeFromSuperview];
        
    }
    
    for (UIView * view in [self.requetesClientView subviews]) {
        
        [view removeFromSuperview];
        
    }
    
    
    self.travauxViewHeightConstraint.constant               = initialTravauxViewHeightHeight;
    self.schemaEclateViewHeightConstraint.constant          = initialSchemaEclateViewHeightHeight;
    self.infosClientViewHeightConstraint.constant           = initialInfosClientViewHeightHeight;
    self.contentViewHeightConstraint.constant               = initialContentViewHeightHeight;
    self.requetesClientvViewHeightConstraint.constant       = initialRequetesClientvViewHeightHeight;
    
    self.contentSV.contentSize = initialSVSize;
    
   
    
    
    [self addShemaEclate];
    
    [self addRequetesClient];
    
    [self addAutresRequetesClient];
    
    
}

#pragma mark --
#pragma mark Notification
-(void)setDateFromNotif:(NSNotification *)notification{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSDictionary * dict = [notification userInfo];
    
    NSDate * date =[dict objectForKey:@"selected_date"] ;
    
    switch ([[dict objectForKey:@"SENDER"] intValue]) {
        case 0:
            
            if(![Utils isDatePast:[Utils stringFromDate:date dateFormat:@"yyyy-MM-dd"] format:@"yyyy-MM-dd"]){
                
                [Utils showAlertView:@"Veuillez selectionner une date posterieure à la date/ heure système"];
                
                return;
            }
            
            self.dateTF.text = [Utils stringFromDate:date dateFormat:@"yyyy-MM-dd"];
            
            sharedData.livraisonDate = self.dateTF.text;
            
            break;
            
        case 1:
            
            if ([self.dateTF.text length]  > 0) {
                
                if(![Utils isDatePast:[NSString stringWithFormat:@"%@ %@", self.dateTF.text ,[Utils stringFromDate:date dateFormat:@"HH:mm" ] ] format:@"yyyy-MM-dd HH:mm"]){
                    
                    [Utils showAlertView:@"Veuillez selectionner une date posterieure à la date/ heure système"];
                    
                    return;
                }
                
            }
            
            self.heureTF.text = [Utils stringFromDate:date dateFormat:@"HH:mm" ];
            
            sharedData.livraisonHeure = self.heureTF.text;
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark --
#pragma mark PopUp Delegate

-(void)nOVPopUpViewController:(NOVPopUpViewController *)nOVPopUpViewController showNext:(BOOL)showNext{
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FINISH_VISIT_NOTIFICATION object:nil];
    
}


-(NSString *)getRequestCode:(NSString *)requestDescription{
    
    NSString * code;
    
    BOOL found = NO;
    
    for (id requete in self.travauxCarosserieObjects) {
        
        if ([requestDescription rangeOfString:[requete objectForKey:@"OP_DESCRIPTION"] options:NSCaseInsensitiveSearch].location  != NSNotFound) {
            
            code = [requete objectForKey:@"OP_CODE"];
            
            found = YES;
            
            break;
            
        }
        
    }
    
    if (!found) {
        
        return  requestDescription;
        
    }
    
    return code;
    
}

-(void)insetMappedobject:(NSString * )travail prix:(NSString *)prix{
    
    NSInteger index = -1;
    
    BOOL found = NO;
    
    for (id object in self.mappingObjects) {
        
        for (NOVTView * tView in [self.schemaEclateTravauxView subviews]) {
            
            if ([tView.travailLabel.text caseInsensitiveCompare:[object objectForKey:@"TRAVAIL"]]==NSOrderedSame) {
                
                index = [self.mappingObjects indexOfObject:object];
                
                found  = YES;
                
                break;
                
            }
            
        }
        
        
        if (found) {
            break;
        }
        
    }
    
    
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:travail,@"TRAVAIL", prix, @"PRIX", nil];
    
    
    if (index > 0) {
        
        [self.mappingObjects replaceObjectAtIndex:index withObject:dict];
        
    }else{
            
            [self.mappingObjects addObject:dict];
        
    }
    
}


-(NSString *)getMappedPrice:(NSString *)travail{
    
    NSInteger index = -1;
    
    NSString * price = @"";
    
    for (id object in self.mappingObjects) {
            
            if ([travail caseInsensitiveCompare:[object objectForKey:@"TRAVAIL"]] == NSOrderedSame) {
                
                index = [self.mappingObjects indexOfObject:object];
                
                price = [object objectForKey:@"PRIX"];
                
                break;
            
        }
        
    }
    

    return price;
    
}



#pragma mark --Job Card

-(void)createJobCard:(id)client{
    
    /**
     
     ADRESSE = "RES  AL QODS 12";
     "CIN_RC" = "<null>";
     "CODE_CLIENT" = 257939;
     "DATE_MISE_EN_CIRCULATION" = "19-FEB-10";
     EMAIL = "<null>";
     IMMATRICULE = "32318-A-11";
     KATASHIKI = "ALA30L-AWFXYW";
     "MOD\U00c8LE_VEH" = "RAV 4";
     "NOM_CLIENT" = "MEKMOTEX SARL";
     SN = JTMBA33V30D020525;
     TEL = 2120661151922;
     "TYPE_CLIENT" = ORGANIZATION;
     VILLE = CASABLANCA;
     **/
    
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    
    NSString * summury = @"";
    for (id requete in sharedData.selectedRequestsClient) {
        
        if ([summury length] ==0) {
            
            summury = [requete objectForKey:@"OP_DESCRIPTION"];
        }else{
            
            summury  = [NSString stringWithFormat   :@"%@,%@",summury,[requete objectForKey:@"OP_DESCRIPTION"]];
            
        }
        
    }
    
    
    NOVSharedUSer * sharedUser = [NOVSharedUSer sharedNOVUser];
    
   // sharedUser.workshop     = @"CASA SERVICE CENTER";
   // sharedUser.name         = @"GANNOUNE, HAMID";
    
    
    NSString * params  = [NSString stringWithFormat:@"field_SN=%@&field_TYPE_CLIENT=%@&field_PB_SUMMARY=%@&field_WORKSHOP=%@&field_SA=%@&field_KM=%@&field_STATUS=New&field_MESSAGE=&field_IPAD_NUMBER=%@&field_CODE_CLIENT=%@", sharedData.sn,sharedData.typeClient,summury,sharedUser.workshop,sharedUser.name,sharedData.userKM,sharedData.timeStamp,sharedData.codeClient];
    
    params = [params stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self doGet:[NSString stringWithFormat:@"%@%@",JOB_CARD_INSERT_WS, params] requestID:999777];
    
}

#pragma mark --
#pragma mark Memory

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
