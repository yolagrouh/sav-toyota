//
//  TSuggeresViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVTView.h"
#import "TSuggeresViewController.h"
#import "NOVMoreTravauxViewController_ipad.h"
#import "NOVCalendarView.h"


@interface TSuggeresViewController_ipad : TSuggeresViewController<NOVTViewDelegate,NOVCalendarViewDelegate, NOVMoreTravauxViewControllerDelegate>

//Data

@property (strong, nonatomic)NSMutableArray * travaux;

@property (strong, nonatomic)NSMutableArray * travauxCarosserie;



//Outlets
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray * operations;


@property (weak, nonatomic) IBOutlet UIView *requetesClientView;

@property (weak, nonatomic) IBOutlet UIView *schemaEclateTravauxView;

@property (weak, nonatomic) IBOutlet NOVBlockView *tSuggeresView;


@property (weak, nonatomic) IBOutlet NOVTextField *coutTotalTF;


@property (weak, nonatomic) IBOutlet NOVBlockView *clientView;


@property (weak, nonatomic) IBOutlet UIButton *calendarBtn;

@property (weak, nonatomic) IBOutlet UIButton *heureBtn;


@property (weak, nonatomic) IBOutlet NOVTextField *dateTF;


@property (weak, nonatomic) IBOutlet NOVTextField *heureTF;

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *travauxViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schemaEclateViewHeightConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infosClientViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *requetesClientvViewHeightConstraint;


- (IBAction)imprimerConvention:(id)sender;


- (IBAction)valider:(id)sender;

- (IBAction)choisirOperation:(id)sender;

-(void)addShemaEclate;

-(void)addRequetesClient;

-(void)addAutresRequetesClient;

- (IBAction)showCalendar:(id)sender;

-(void)sendData;


@end
