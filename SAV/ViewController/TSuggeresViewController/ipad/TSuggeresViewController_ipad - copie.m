#import "TSuggeresViewController_ipad.h"
#import "TAdditionnelsViewController_ipad.h"
#import "NOVArgumentaireViewController_ipad.h"

#import "NOVPopUpViewController_ipad.h"
#import "AIDatePickerController.h"
#import "SSFlatDatePicker.h"
#import "NOVTaggedButton.h"

@interface TSuggeresViewController_ipad (){
    
    NSInteger travauxCount;
    
    CGSize initialSVSize;
    
    
    NSInteger numberOfRequests;
    
    NSInteger requestsCount;
    
    CGFloat initialTravauxViewHeightHeight;
    CGFloat initialSchemaEclateViewHeightHeight;
    CGFloat initialInfosClientViewHeightHeight;
    CGFloat initialContentViewHeightHeight;
    CGFloat initialRequetesClientvViewHeightHeight;
    
    BOOL didShowMoreTravaux;
}

@property (nonatomic, strong) IBOutlet SSFlatDatePicker *datePicker;

@property (nonatomic, strong)NOVMoreTravauxViewController_ipad * nOVMoreTravauxViewController;
@property (nonatomic, strong)NOVArgumentaireViewController_ipad * nOVArgumentaireViewController_ipad;
@property(nonatomic, strong)NOVPopUpViewController_ipad * nOVPopUpViewController;

@end

@implementation TSuggeresViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        travauxCount = -1;
        
        didShowMoreTravaux = NO;
        
        numberOfRequests = 0;
        
        requestsCount = 0;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setDateFromNotif:) name:SET_DATE_NOTIFICATION object:nil];
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setUpView{
    
    [self performSelector:@selector(addTravaux) withObject:nil afterDelay:0.3];
    
    
}

-(void)addTravaux{
    
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.contentSV.frame.size.height + 40);
    
    initialSVSize                               = self.contentSV.contentSize;
    
    initialTravauxViewHeightHeight              = self.travauxViewHeightConstraint.constant;
    initialSchemaEclateViewHeightHeight         = self.schemaEclateViewHeightConstraint.constant;
    initialInfosClientViewHeightHeight          = self.infosClientViewHeightConstraint.constant;
    initialContentViewHeightHeight              = self.contentViewHeightConstraint.constant;
    initialRequetesClientvViewHeightHeight      = self.requetesClientvViewHeightConstraint.constant;
    
    [super setUpView];
    
    [self addShemaEclate];
    
    [self addRequetesClient];
    
    [self addAutresRequetesClient];
    
}

-(void)addRequetesClient{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    CGFloat cout = 0;
    
    for (id  travail in sharedData.selectedRequestsClient) {
        
        travauxCount ++;
        
        NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
        
        [sharedData.tousLesTravaux addObject:[NSDictionary dictionaryWithObjectsAndKeys:[travail objectForKey:@"MONTANT"] ,@"PRIX",[travail objectForKey:@"OP_DESCRIPTION"],@"TRAVAIL",@"OUI",@"STATUS", nil]];
        
        cout += [Utils getNumber:[travail objectForKey:@"MONTANT"]];
        
        NOVTView * tView = [[[NSBundle mainBundle] loadNibNamed:@"NOVTView" owner:nil options:nil] objectAtIndex:0];
        
        tView.argumentaireBtn.argument  = @"Révisions Périodiques";
        
        tView.argumentaireBtn.hidden = NO;
        tView.argumentaireBtn.tag = 1;
        tView.argumentaireImageView.hidden = NO;
        tView.argumentaireImageView.image = [UIImage imageNamed:@"t_Requetes_client_2.png"];
        
        tView.delegate = self;
        
        tView.checkBox.enabled = NO;
        
        tView.tag = travauxCount;
        
        tView.travailLabel.text = [travail objectForKey:@"OP_DESCRIPTION"];
        
        tView.coutTF.content    = [travail objectForKey:@"OP_CODE"];
        
        tView.coutTF.text       = [travail objectForKey:@"MONTANT"];
        
        tView.coutTF.enabled    = NO;
        
        tView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.requetesClientView addSubview:tView];
        
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeWidth
                                                                           multiplier:0
                                                                             constant:516]];
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeHeight
                                                                           multiplier:0
                                                                             constant:68]];
        
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeLeft
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeLeft
                                                                           multiplier:0
                                                                             constant:0]];
        
        CGFloat top = ([[self.requetesClientView  subviews] count] -1) * 68;
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeTop
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeTop
                                                                           multiplier:0
                                                                             constant:top]];
        
    }
    
    self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",cout];
    
    
    if ([sharedData.selectedRequestsClient count] <=1){
        
        self.requetesClientvViewHeightConstraint.constant = 100;
        
    }else if ([sharedData.selectedRequestsClient count] > 2) {
        
        self.requetesClientvViewHeightConstraint.constant =  [sharedData.selectedRequestsClient count]  * 68;
        
        self.travauxViewHeightConstraint.constant = self.travauxViewHeightConstraint.constant +  ([sharedData.selectedRequestsClient count] - 2)  * 68;
        
    }
    
    
    
    self.contentViewHeightConstraint.constant = self.contentViewHeightConstraint.constant + self.requetesClientvViewHeightConstraint.constant;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width,  self.contentViewHeightConstraint.constant - 160 * [sharedData.selectedRequestsClient count]);
    
}

-(void)addAutresRequetesClient{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
     CGFloat cout = [Utils getNumber:self.coutTotalTF.text];
    
    for (id  travail in sharedData.selectedOtherRequestsClient) {
        
        travauxCount ++;
        
        cout += [Utils getNumber:[travail objectForKey:@"MONTANT"]];
        
        [sharedData.tousLesTravaux addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"0" ,@"PRIX",[travail objectForKey:@"OP_DESCRIPTION"],@"TRAVAIL",@"OUI",@"STATUS",nil]];
        
        NOVTView * tView = [[[NSBundle mainBundle] loadNibNamed:@"NOVTView" owner:nil options:nil] objectAtIndex:0];
        
        tView.tag = travauxCount;

        
        tView.checkBox.enabled = NO;
        
        tView.coutTF.enabled    = NO;
        
        tView.delegate = self;
        
        tView.travailLabel.text =  [travail objectForKey:@"OP_DESCRIPTION"];
        
        tView.coutTF.content    = [travail objectForKey:@"OP_CODE"];
        
        tView.coutTF.text       = [travail objectForKey:@"MONTANT"];
        
        tView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.requetesClientView addSubview:tView];
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeWidth
                                                                           multiplier:0
                                                                             constant:516]];
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeHeight
                                                                           multiplier:0
                                                                             constant:68]];
        
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeLeft
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeLeft
                                                                           multiplier:0
                                                                             constant:0]];
        
        CGFloat top = ([[self.requetesClientView  subviews] count] -1) * 68;
        
        [self.requetesClientView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                            attribute:NSLayoutAttributeTop
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.requetesClientView
                                                                            attribute:NSLayoutAttributeTop
                                                                           multiplier:0
                                                                             constant:top]];
        
    }
    
    self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",cout];
    
    self.requetesClientvViewHeightConstraint.constant = self.requetesClientvViewHeightConstraint.constant + [sharedData.selectedOtherRequestsClient count]  * 68 ;
    
    self.travauxViewHeightConstraint.constant = self.travauxViewHeightConstraint.constant +  [sharedData.selectedOtherRequestsClient count]  * 68 ;
    
    self.contentViewHeightConstraint.constant = self.contentViewHeightConstraint.constant + [sharedData.selectedOtherRequestsClient count]  * 68;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width,  self.contentViewHeightConstraint.constant- 160 );
    
}

-(void)addShemaEclate{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    if ([self.travaux containsObject:@"Travaux carrosserie"]) {
        
        [self.travaux removeObject:@"Travaux carrosserie"];
        
        [self.travaux addObject:@"Travaux carrosserie"];
        
    }
    
    for (NSString * travail in self.travaux) {
        
        travauxCount ++;
        
        [sharedData.tousLesTravaux addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"0" ,@"PRIX",travail,@"TRAVAIL",@"NON",@"STATUS", nil]];
        
        NOVTView * tView = [[[NSBundle mainBundle] loadNibNamed:@"NOVTView" owner:nil options:nil] objectAtIndex:0];
        
        tView.tag = travauxCount;
        
        tView.delegate = self;
        
        tView.travailLabel.text = travail;
        
        if ([travail caseInsensitiveCompare:@"Travaux carrosserie"] == NSOrderedSame) {
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 0;
            tView.argumentaireImageView.hidden = NO;
            tView.expandImage.hidden = NO;
            tView.argumentaireBtn.argument = @"Travaux Carrosserie";
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_travaux carrosserie2.png"];
            
            
            
        }else if ([travail caseInsensitiveCompare:@"Remplacement Batterie"] == NSOrderedSame){
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 1;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Batterie";
            tView.argumentaireBtn.argument = @"Batterie";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_car_battery2-.png"];
            
        }else if ([Utils checkString:travail contains:@"Remplacement Pneu"]){//[travail isEqualToString:@"Remplacement Batterie"]
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 2;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Pneu";
            tView.argumentaireBtn.argument = @"Entretien des pneus";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_pneu2.png"];
            
        }else if ([Utils checkString:travail contains:@"jantes"]){//[travail isEqualToString:@"Remplacement Batterie"]
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 2;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Nettoyage des jantes";
            tView.argumentaireBtn.argument = @"Nettoyage des jantes";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_Icone_netoyage_jantes2.png"];
            
        }else if ([Utils checkString:travail contains:@"Feu"]){//[travail isEqualToString:@"Remplacement Batterie"]
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 2;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Rénovation des optiques";
            tView.argumentaireBtn.argument = @"Rénovation des optiques";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_rénovation_optiques_icone2.png"];
            
        }else if ([Utils checkString:travail contains:@"Essuie-glace"]){//[travail isEqualToString:@"Remplacement Batterie"]
            
            tView.argumentaireBtn.hidden = NO;
            tView.argumentaireBtn.tag = 2;
            tView.argumentaireImageView.hidden = NO;
            tView.argumentaireBtn.content = @"Balais d’essuie-glaces";
            tView.argumentaireBtn.argument = @"Balais d’essuie-glaces";
            
            tView.argumentaireImageView.image = [UIImage imageNamed:@"t_Essuis glace2.png"];
            
        }
        
        tView.coutTF.content    = travail;
        
        tView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.schemaEclateTravauxView addSubview:tView];
        
        [self.schemaEclateTravauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                                 attribute:NSLayoutAttributeWidth
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.schemaEclateTravauxView
                                                                                 attribute:NSLayoutAttributeWidth
                                                                                multiplier:0
                                                                                  constant:516]];
        
        [self.schemaEclateTravauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.schemaEclateTravauxView
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                multiplier:0
                                                                                  constant:68]];
        
        [self.schemaEclateTravauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                                 attribute:NSLayoutAttributeLeft
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.schemaEclateTravauxView
                                                                                 attribute:NSLayoutAttributeLeft
                                                                                multiplier:0
                                                                                  constant:0]];
        
        CGFloat top = ([[self.schemaEclateTravauxView  subviews] count] -1) * 68;
        
        [self.schemaEclateTravauxView addConstraint:[NSLayoutConstraint constraintWithItem:tView
                                                                                 attribute:NSLayoutAttributeTop
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.schemaEclateTravauxView
                                                                                 attribute:NSLayoutAttributeTop
                                                                                multiplier:0
                                                                                  constant:top]];
    }
    
    self.schemaEclateViewHeightConstraint.constant = [self.travaux count]  * 68;
    
    self.travauxViewHeightConstraint.constant = self.travauxViewHeightConstraint.constant +  [self.travaux count]  * 68;
    
    self.contentViewHeightConstraint.constant = self.contentViewHeightConstraint.constant + [self.travaux count]  * 68;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width,  self.contentViewHeightConstraint.constant - 160 );
    
}

#pragma mark --
#pragma mark Data

-(void)sendData{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    
    numberOfRequests = [[self.requetesClientView subviews] count] + [[self.schemaEclateTravauxView subviews] count];
    
    for (NOVTView * view in [self.requetesClientView subviews]) {
        
        NSString * taskName = [view.travailLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        
        NSString * taskDescription = [view.coutTF.content stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString * statut ;
        
        view.checkBox.enabled ? (statut = @"yes" ): (statut = @"no");
        
        NSString * requete = [NSString stringWithFormat:@"%@field_IPAD_NUMBER=%@&field_TASK_NAME=%@&field_TASK_DESCRIPTION=%@&field_TASK_TYPE=sdsd&field_CONFIMATION=Reparation&field_STATUS=%@&field_MESSAGE=message",TASK_SAVE_WS,sharedData.timeStamp,taskName, taskDescription, statut];
        
        
        [self doGet:requete requestID:[[self.requetesClientView subviews] indexOfObject:view]];
        
    }
    
    for (NOVTView * view in [self.schemaEclateTravauxView subviews]) {
        
        NSString * taskName = [view.travailLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        NSString * taskDescription = [view.coutTF.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString * statut ;
        
        view.checkBox.selected ? (statut = @"OUI" ): (statut = @"NON");
        
        NSString * requete = [NSString stringWithFormat:@"%@field_IPAD_NUMBER=%@&field_TASK_NAME=%@&field_TASK_DESCRIPTION=%@&field_TASK_TYPE=sdsd&field_CONFIMATION=Reparation&field_STATUS=%@&field_MESSAGE=message",TASK_SAVE_WS,sharedData.timeStamp,taskName, taskDescription, statut];
        
        
        [self doGet:requete requestID:[[self.requetesClientView subviews] indexOfObject:view]];
        
    }
    
}

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    requestsCount ++;
    
    if (requestsCount == numberOfRequests) {
        
        
        
        [self showValidation];
        
    }
    
}

#pragma mark --
#pragma mark Actions

- (IBAction)imprimerConvention:(id)sender {
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    for (id travail in sharedData.tousLesTravaux) {
        
        if ([[travail objectForKey:@"STATUS"] isEqualToString:@"OUI"] && [[travail objectForKey:@"PRIX"] length] ==0) {
            
            [Utils showAlertView:[NSString stringWithFormat:@"Veuillez renseigner le prix du travail : %@",[travail objectForKey:@"TRAVAIL"]]];
            
            return;
            
        }
        
    }
    
    if([self.heureTF.text length] == 0 || [self.dateTF.text length] == 0 ){
        
        
        [self showError:@"Veuillez spécifier la date et l'heure de livraison"];
        
        return;
        
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:IMPRESSION_NOTIFICATION object:nil userInfo:nil];
    
}

- (IBAction)showCalendar:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_DATE_PICKER_NOTIFICATION object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:[sender tag]],@"SENDER", nil]];
    
    //    NOVCalendarView * calendarView = [[[NSBundle mainBundle] loadNibNamed:@"NOVCalendarView" owner:nil options:nil] objectAtIndex:0];
    //
    //    switch ([sender tag]) {
    //        case 0:
    //
    //            calendarView.datePicker.datePickerMode = SSFlatDatePickerModeDate;
    //            calendarView.datePickerMode = SSFlatDatePickerModeDate;
    //
    //            break;
    //        case 1:
    //
    //            calendarView.datePicker.datePickerMode = SSFlatDatePickerModeTime;
    //            calendarView.datePickerMode = SSFlatDatePickerModeTime;
    //
    //            break;
    //
    //        default:
    //            break;
    //    }
    //
    //    calendarView.frame = CGRectMake(-calendarView.frame.size.width, 0, calendarView.frame.size.width, calendarView.frame.size.height);
    //
    //    calendarView.tag = [sender tag];
    //
    //    calendarView.delegate = self;
    //
    //    [self.clientView addSubview:calendarView];
    //
    //    [UIView animateWithDuration:0.3 animations:^{
    //
    //
    //        calendarView.frame = CGRectMake(0, 0, calendarView.frame.size.width, calendarView.frame.size.height);
    //
    //    } ];
    
}



- (IBAction)valider:(id)sender {
    
    if (!self.isOnline) {
        
        [Utils showAlertView:@"Veuillez vous assurer que vous disposez d'une connexion internet avant de continuer."];
        
        return;
        
    }
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    for (id travail in sharedData.tousLesTravaux) {
        
        if ([[travail objectForKey:@"STATUS"] isEqualToString:@"OUI"] && [[travail objectForKey:@"PRIX"] length] ==0) {
            
            [Utils showAlertView:[NSString stringWithFormat:@"Veuillez renseigner le prix du travail : %@",[travail objectForKey:@"TRAVAIL"]]];
            
            return;
            
        }
        
    }
    
    if ([self.dateTF.text length] == 0 || [self.heureTF.text length] ==0) {
        
        [Utils showAlertView:@"Veuillez spécifier la date promise de livraison."];
        
        return;
        
    }
    
    [self sendData];
}

- (IBAction)choisirOperation:(id)sender {
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    
    NSMutableArray * demandesClient = [NSMutableArray arrayWithArray:[sharedData.demandesClient componentsSeparatedByString:@","]];
    
    NOVTaggedButton * taggedButton = (NOVTaggedButton *)sender;
    
    taggedButton.selected = !taggedButton.selected;
    
    if (taggedButton.selected) {
        
        [demandesClient addObject:taggedButton.content];
        
    }else{
        
        [demandesClient removeObject:taggedButton.content];
    }
    
    if ([demandesClient count] >0) {
        if ([[demandesClient objectAtIndex:0] length] == 0) {
            
            
            [demandesClient removeObjectAtIndex:0];
        }
    }
    
    
    
    sharedData.demandesClient = [demandesClient componentsJoinedByString:@","];
    
}

#pragma mark --
#pragma mark TextField

-(void)nOVTView:(NOVTView *)nOVTView input:(NSString *)input previousValue:(NSString *)previousValue{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSMutableDictionary * dict ;
    
    for (id tr in sharedData.tousLesTravaux) {
        
        if ([[tr objectForKey:@"TRAVAIL"] caseInsensitiveCompare:nOVTView.coutTF.content] == NSOrderedSame) {
            
            dict = [NSMutableDictionary dictionaryWithDictionary:tr];
            
            [dict setObject:input forKey:@"PRIX"];
            
            [dict setObject:@"OUI" forKey:@"STATUS"];
            
            break;
            
        }
        
    }
    
    [sharedData.tousLesTravaux replaceObjectAtIndex:nOVTView.tag withObject:dict];
    
    CGFloat total = 0;
    
    for (id tr in sharedData.tousLesTravaux) {
        
        if ([[tr objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
            
            total += [Utils getNumber:[tr objectForKey:@"PRIX"]] ;
            
        }
        
    }
    
    
    self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",total];
    
}

-(void)nOVTView:(NOVTView *)nOVTView cleared:(BOOL)cleared previousValue:(NSString *)value{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSMutableDictionary * dict ;
    
    for (id travail in sharedData.tousLesTravaux) {
        
        if ([[travail objectForKey:@"TRAVAIL"] caseInsensitiveCompare:nOVTView.coutTF.content] == NSOrderedSame) {
            
            dict = [NSMutableDictionary dictionaryWithDictionary:travail];
            
            [dict setObject:@"NON" forKey:@"STATUS"];
            
            [dict setObject:@"0" forKey:@"PRIX"];
            
            break;
        }
        
    }
    
    [sharedData.tousLesTravaux replaceObjectAtIndex:nOVTView.tag withObject:dict];
    
    CGFloat total = 0;
    
    for (id tr in sharedData.tousLesTravaux) {
        
        if ([[tr objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
            
            total += [Utils getNumber:[tr objectForKey:@"PRIX"]] ;
            
        }
        
    }
    
    self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",total];
    
}

-(void)nOVTView:(NOVTView *)nOVTView selected:(BOOL)selected travail:(NSString *)travail prix:(NSString * )prix{
    
    CGFloat cout = [Utils getNumber:prix];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    if (selected) {
        
        [sharedData.tousLesTravaux replaceObjectAtIndex:nOVTView.tag withObject:[NSDictionary dictionaryWithObjectsAndKeys:travail, @"TRAVAIL",prix,@"PRIX",@"OUI",@"STATUS", nil]];
        
        
    }else{
        
        
        NSDictionary * dict =  [NSDictionary dictionaryWithObjectsAndKeys:travail, @"TRAVAIL",prix,@"PRIX",@"NON",@"STATUS", nil];
        
        NSInteger index = -1;
        
        for (id tr in sharedData.tousLesTravaux) {
            
            if ([[tr objectForKey:@"TRAVAIL"] caseInsensitiveCompare:travail] ==  NSOrderedSame) {
                
                //[sharedData.tousLesTravaux removeObject:tr];
                
                index = [sharedData.tousLesTravaux indexOfObject:tr];
                
                break;
            }
            
        }
        
        [sharedData.tousLesTravaux replaceObjectAtIndex:index withObject:dict];
    }
    
    CGFloat total = 0;
    
    
    for (id tr in sharedData.tousLesTravaux) {
        
        if ([[tr objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
            
            total += [Utils getNumber:[tr objectForKey:@"PRIX"]] ;
            
        }
        
    }
    
    
    self.coutTotalTF.text = [NSString stringWithFormat:@"%.2f",total];
    
}

-(void)nOVTView:(NOVTView *)nOVTView showPopUp:(BOOL)showPopUp{
    
    if ([nOVTView.travailLabel.text caseInsensitiveCompare:@"Travaux carrosserie"] != NSOrderedSame) {
        
        return;
    }
    
    
    if (!didShowMoreTravaux) {
        
        self.nOVMoreTravauxViewController = [[NOVMoreTravauxViewController_ipad alloc]initWithNibName:@"NOVMoreTravauxViewController" bundle:nil];
        
        self.nOVMoreTravauxViewController.delegate = self;
        
        self.nOVMoreTravauxViewController.travaux = [NSArray arrayWithArray:self.travauxCarosserie];
        
        [self.view addSubview:self.nOVMoreTravauxViewController.view];
        
        [self.nOVMoreTravauxViewController show];
    }
    
    
}

-(void)nOVTView:(NOVTView *)nOVTView showArgumentairePopUp:(BOOL)showPopUp sender:(NOVArgumentaireBtn *)sender{
    

    self.nOVArgumentaireViewController_ipad = [[NOVArgumentaireViewController_ipad alloc]initWithNibName:@"NOVArgumentaireViewController" bundle:nil];
    
    self.nOVArgumentaireViewController_ipad.categorie = sender.argument;
    
    [self.view addSubview:self.nOVArgumentaireViewController_ipad.view];
    
    [self.nOVArgumentaireViewController_ipad show];
      
}

#pragma mark --
#pragma mark Calendar

-(void)nOVCalendarView:(NOVCalendarView *)nOVCalendarView date:(NSString *)date{
    
    self.contentSV.scrollEnabled = YES;
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    switch (nOVCalendarView.tag) {
        case 0:
            
            self.dateTF.text = date;
            
            sharedData.livraisonDate = date;
            
            break;
        case 1:
            
            self.heureTF.text = date;
            
            sharedData.livraisonHeure = date;
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark --
#pragma mark Validation

-(void)showNext{
    
    NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]] ;
    
    if (!savedRDV) {
        
        savedRDV = [NSMutableArray array];
    }
    
    [savedRDV addObject:[[NOVSharedData sharedNOVData] createArchivableObject]];
    
    [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];
    
    
    self.nOVPopUpViewController = [[NOVPopUpViewController_ipad alloc]initWithNibName:@"NOVPopUpViewController" bundle:nil];
    
    self.nOVPopUpViewController.delegate = self;
    
    self.nOVPopUpViewController.message = @"Ce dossier a été enregistré avec succès";
    
    self.nOVPopUpViewController.view.frame = CGRectMake(0, -self.nOVPopUpViewController.view.frame.size.height, self.nOVPopUpViewController.view.frame.size.width, self.nOVPopUpViewController.view.frame.size.height);
    
    [self.view addSubview:self.nOVPopUpViewController.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.nOVPopUpViewController.view.frame = CGRectMake(0, 0, self.nOVPopUpViewController.view.frame.size.width, self.nOVPopUpViewController.view.frame.size.height);
        
    }];
    
}

#pragma mark --
#pragma mark MoreTravaux Delegate

-(void)nOVMoreTravauxViewController:(NOVMoreTravauxViewController *)nOVMoreTravauxViewController selectedTravaux:(NSArray *)travaux{
    
    didShowMoreTravaux  = NO;
    travauxCount = -1;
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSMutableArray * tempArray2 = [NSMutableArray arrayWithArray:self.travaux];
    
    NSMutableArray * tempArray3 = [NSMutableArray arrayWithArray:self.travauxCarosserie];
    
    [sharedData.tousLesTravaux removeAllObjects];
    
    
    for (id travail in travaux) {
        
        if ([tempArray3 containsObject:travail]) {
            
            [self.travauxCarosserie removeObject:travail];
            
        }
    }
    
    for (id travail in travaux) {
        
        if ([tempArray2 containsObject:travail]) {
            
            [self.travaux removeObject:travail];
            
        }
    }
    
    //Add Selected Travaux
    
    
//   NSIndexSet * indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange([self.travaux indexOfObject:@"Travaux carrosserie"]+1, [travaux count])];

    
    if ([self.travauxCarosserie count] ==0) {
        
        [self.travaux removeObject:@"Travaux carrosserie"];
        
    }
    
    [self.travaux addObjectsFromArray:travaux];
    
    for (UIView * view in [self.schemaEclateTravauxView subviews]) {
        
        [view removeFromSuperview];
        
    }
    
    for (UIView * view in [self.requetesClientView subviews]) {
        
        [view removeFromSuperview];
        
    }
    
    
    self.travauxViewHeightConstraint.constant               = initialTravauxViewHeightHeight;
    self.schemaEclateViewHeightConstraint.constant          = initialSchemaEclateViewHeightHeight;
    self.infosClientViewHeightConstraint.constant           = initialInfosClientViewHeightHeight;
    self.contentViewHeightConstraint.constant               = initialContentViewHeightHeight;
    self.requetesClientvViewHeightConstraint.constant       = initialRequetesClientvViewHeightHeight;
    
    self.contentSV.contentSize = initialSVSize;
    
    [self addShemaEclate];
    
    [self addRequetesClient];
    
    [self addAutresRequetesClient];
    
    
}

#pragma mark --
#pragma mark Notification
-(void)setDateFromNotif:(NSNotification *)notification{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSDictionary * dict = [notification userInfo];
    
    NSDate * date =[dict objectForKey:@"selected_date"] ;
    
    switch ([[dict objectForKey:@"SENDER"] intValue]) {
        case 0:
            
            if(![Utils isDatePast:[Utils stringFromDate:date dateFormat:@"yyyy-MM-dd"] format:@"yyyy-MM-dd"]){
                
                [Utils showAlertView:@"Veuillez selectionner une date posterieure à la date/ heure système"];
                
                return;
            }
            
            self.dateTF.text = [Utils stringFromDate:date dateFormat:@"yyyy-MM-dd" ];
            
            sharedData.livraisonDate = self.dateTF.text;
            
            break;
            
        case 1:
            
            if ([self.dateTF.text length]  > 0) {
                
                if(![Utils isDatePast:[NSString stringWithFormat:@"%@ %@", self.dateTF.text ,[Utils stringFromDate:date dateFormat:@"HH:mm" ] ] format:@"yyyy-MM-dd HH:mm"]){
                    
                    [Utils showAlertView:@"Veuillez selectionner une date posterieure à la date/ heure système"];
                    
                    return;
                }
                
            }
            
            self.heureTF.text = [Utils stringFromDate:date dateFormat:@"HH:mm" ];
            
            sharedData.livraisonHeure = self.heureTF.text;
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark --
#pragma mark PopUp Delegate

-(void)nOVPopUpViewController:(NOVPopUpViewController *)nOVPopUpViewController showNext:(BOOL)showNext{
    

    [[NSNotificationCenter defaultCenter] postNotificationName:FINISH_VISIT_NOTIFICATION object:nil];
    
}


#pragma mark --
#pragma mark Memory

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
