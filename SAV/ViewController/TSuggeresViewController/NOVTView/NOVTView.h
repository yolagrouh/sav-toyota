//
//  NOVTView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/5/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVTextField.h"
#import "NOVArgumentaireBtn.h"

@class NOVTView;

@protocol NOVTViewDelegate <NSObject>

@required

-(void)nOVTView:(NOVTView * )nOVTView input:(NSString *)input previousValue:(NSString *)previousValue;

-(void)nOVTView:(NOVTView * )nOVTView cleared:(BOOL)cleared previousValue:(NSString *)value;

-(void)nOVTView:(NOVTView * )nOVTView selected:(BOOL)selected travail:(NSString *)travail prix:(NSString *)prix;

-(void)nOVTView:(NOVTView * )nOVTView showPopUp:(BOOL)showPopUp ;

-(void)nOVTView:(NOVTView * )nOVTView showArgumentairePopUp:(BOOL)showPopUp sender:(UIButton  *)sender ;

@end

@interface NOVTView : UIView<UITextFieldDelegate>

@property(nonatomic, weak)id<NOVTViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet NOVTextField *coutTF;

@property (weak, nonatomic) IBOutlet UIImageView *expandImage;


@property (weak, nonatomic) IBOutlet UILabel *travailLabel;

@property (weak, nonatomic) IBOutlet UIButton *checkBox;

@property(nonatomic, strong) NSString * currentValue;

@property (weak, nonatomic) IBOutlet UIImageView *argumentaireImageView;
@property (weak, nonatomic) IBOutlet NOVArgumentaireBtn *argumentaireBtn;

- (IBAction)selectionnerTravail:(id)sender;

- (IBAction)showPopUp:(id)sender;

- (IBAction)showArgumentaire:(id)sender;


@end
