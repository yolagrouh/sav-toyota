//
//  NOVTView.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/5/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVTView.h"
#import "Utils.h"


@implementation NOVTView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (![Utils isNumber:string]) {
        
        return NO;
    }   
    
    if ([textField.text length] == 0 && [string isEqualToString:@"."])  {
        
        return  NO;
    }
    
    if ([textField.text length] > 0 && [Utils checkString:textField.text  contains:@"."] && [string isEqualToString:@"."])  {
        
        return  NO;
    }
    
    return  YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if ([textField.text length]>0) {
        
        self.checkBox.selected = YES;
        
        [self.delegate nOVTView:self input:textField.text previousValue:self.currentValue];
        
        self.currentValue= textField.text;
        
    }else{
        
        self.checkBox.selected = NO;
        
        [self.delegate nOVTView:self cleared:YES previousValue:self.currentValue ];
        
    }
    
}

- (IBAction)selectionnerTravail:(id)sender {
    
    UIButton * button = (UIButton * )sender;
    
    button.selected = !button.selected;
    
    [self.delegate nOVTView:self selected:button.selected  travail:self.coutTF.content prix:self.coutTF.text];
    
}

- (IBAction)showPopUp:(id)sender {
    
    [self.delegate nOVTView:self showPopUp:YES];
}

- (IBAction)showArgumentaire:(id)sender {
    
    [self.delegate nOVTView:self showArgumentairePopUp:YES sender:sender];
}
@end
