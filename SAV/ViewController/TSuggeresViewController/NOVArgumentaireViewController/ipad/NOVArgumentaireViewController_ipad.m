#import "NOVArgumentaireViewController_ipad.h"

@interface NOVArgumentaireViewController_ipad (){
    
    
    BOOL didRead;
}

@property(nonatomic, strong)NOVPopUpViewController_ipad * nOVPopUpViewController;
@property(nonatomic, strong)NSMutableArray * arguments;

@end

@implementation NOVArgumentaireViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        didRead = YES;
        
        self.mode = NOVArgumentaireModeWait;
        
        self.shouldAddGradient = NO;
        
        self.arguments = [NSMutableArray array];
    }
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    
    [self doGet:[NSString stringWithFormat:@"%@%@",ARGUMENTAIRE_WS, [self.categorie stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] requestID:0];
    
}

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    
    if ([response objectForKey:@"result"]) {
        
        [self.arguments addObjectsFromArray:[response objectForKey:@"result"]];
        
        if ([self.arguments count] > 0) {
            
        [self addArgument];
            
        }
        

    }
    
    
}

-(void)addArgument{
    
    [self.switchControl removeAllSegments];
    
    for (id argument in self.arguments) {
        
        if (![[argument objectForKey:@"sou_categorie"]  isKindOfClass:[NSNull class]]) {
            
            
            
            [self.switchControl insertSegmentWithTitle:[argument objectForKey:@"sou_categorie"] atIndex:[self.arguments indexOfObject:argument] animated:YES];
        }else{
            
            [self.switchControl insertSegmentWithTitle:@"Sous catégorie" atIndex:[self.arguments indexOfObject:argument] animated:YES];
        }
        
        
    }
    
    
    self.switchControl.selectedSegmentIndex = 0;
    
    [self switchContent:nil];
}


-(void)canPass{
    
    didRead = YES;
    
}


-(void)setUpView{
    
    
    [super setUpView];
    
    [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(canPass) userInfo:nil repeats:NO];
    
//    NSString *pdf = [[NSBundle mainBundle] pathForResource:@"GARANTIE" ofType:@"pdf"];
//    
//    NSURL *targetURL = [NSURL fileURLWithPath:pdf];
//    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
//    [self.webView loadRequest:request];
    
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)fermer:(id)sender {
    
    if (self.mode == NOVArgumentaireModeWait) {
        
        if (!didRead ) {
            
            
            return;
        }
        
        //        self.nOVPopUpViewController = [[NOVPopUpViewController_ipad alloc]initWithNibName:@"NOVPopUpViewController" bundle:nil];
        //
        //        self.nOVPopUpViewController.delegate = self;
        //
        //        self.nOVPopUpViewController.message = @"Avez-vous expliqué au client la nature des travaux effectués sur son véhicule?";
        //
        //        self.nOVPopUpViewController.view.frame = CGRectMake(0, -self.nOVPopUpViewController.view.frame.size.height, self.nOVPopUpViewController.view.frame.size.width, self.nOVPopUpViewController.view.frame.size.height);
        //
        //        [self.view addSubview:self.nOVPopUpViewController.view];
        //
        //        [UIView animateWithDuration:0.3 animations:^{
        //
        //            self.nOVPopUpViewController.view.frame = CGRectMake(0, 0, self.nOVPopUpViewController.view.frame.size.width, self.nOVPopUpViewController.view.frame.size.height);
        //
        //        }];
        
    }
    
    NSString *pdf = [[NSBundle mainBundle] pathForResource:@"GARANTIE" ofType:@"pdf"];
    
    NSURL *targetURL = [NSURL fileURLWithPath:pdf];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    [self.webView loadRequest:request];
    
    [super fermer:sender];
}

-(void)nOVPopUpViewController:(NOVPopUpViewController *)nOVPopUpViewController showNext:(BOOL)showNext{
    
    
    [self canPass];
    
}

- (IBAction)switchContent:(id)sender {
    
    NSInteger index = self.switchControl.selectedSegmentIndex ;
    
    NSDictionary * argumentaire = [self.arguments objectAtIndex:index];
    
    BOOL isPDF = [[argumentaire objectForKey:@"pdf"] boolValue];
    
    
    NSString * content = [argumentaire objectForKey:@"content"];
    
    if (!isPDF) {
        
        
        
        [self.webView loadHTMLString:content baseURL:nil];
        
    }else{
        
        
        NSURL *targetURL = [NSURL URLWithString:content];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        
        [self.webView loadRequest:request];
    }
    
    
    //NSString *pdf = [[NSBundle mainBundle] pathForResource:@"GARANTIE" ofType:@"pdf"];
    
    //    NSURL *targetURL = [NSURL fileURLWithPath:pdf];
    //    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    //
    // [self.webView loadRequest:request];
    
    //    if (self.switchControl.selectedSegmentIndex == 0) {
    //
    //        NSString *pdf = [[NSBundle mainBundle] pathForResource:@"GARANTIE" ofType:@"pdf"];
    //
    //        NSURL *targetURL = [NSURL fileURLWithPath:pdf];
    //        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    //
    //        [self.webView loadRequest:request];
    //
    //    }else{
    //
    //
    //        NSString *pdf = [[NSBundle mainBundle] pathForResource:@"toyota_arg_1" ofType:@"mp4"];
    //
    //        NSURL *targetURL = [NSURL fileURLWithPath:pdf];
    //        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    //        
    //        [self.webView loadRequest:request];
    //        
    //    }
    
    
}

#pragma mark --
#pragma mark WebView

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    [Utils log:@"ARGUMENTAIRE" message:[error description]];
 //   [SVProgressHUD showWithStatus:FAILED_MESSAGE];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
     // [SVProgressHUD showWithStatus:SUCCESS_MESSAGE];
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    
   //   [SVProgressHUD showWithStatus:LOADING_MESSAGE];
    
}

@end
