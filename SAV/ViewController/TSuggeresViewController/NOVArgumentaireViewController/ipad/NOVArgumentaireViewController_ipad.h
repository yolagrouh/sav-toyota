//
//  NOVArgumentaireViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVArgumentaireViewController.h"
#import "NOVPopUpViewController_ipad.h"

typedef enum : NSUInteger {
    NOVArgumentaireModeWait,
    NOVArgumentaireModeNormal,
} NOVArgumentaireMode;

@interface NOVArgumentaireViewController_ipad : NOVArgumentaireViewController<NOVPopUpViewControllerDelegate,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property(nonatomic, assign)NOVArgumentaireMode mode;

@property(nonatomic, strong)NSString * categorie;

@property (weak, nonatomic) IBOutlet UISegmentedControl *switchControl;

- (IBAction)switchContent:(id)sender;


@end
