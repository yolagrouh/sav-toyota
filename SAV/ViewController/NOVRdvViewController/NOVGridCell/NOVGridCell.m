//
//  NOVGridCell.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/29/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVGridCell.h"

@implementation NOVGridCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)afficherHistorique:(id)sender {
    
    [self.delegate nOVGridCell:self showHistorique:self.tag];
}
@end
