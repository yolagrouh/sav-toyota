//
//  NOVGridCell.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/29/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "TMQuiltView.h"

@class NOVGridCell;

@protocol NOVGridCellDelegate <NSObject>

@required

-(void)nOVGridCell:(NOVGridCell *)nOVGridCell showHistorique:(NSInteger)index;


@end

@interface NOVGridCell : TMQuiltViewCell

@property(nonatomic, weak)id<NOVGridCellDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *nomLabel;

@property (weak, nonatomic) IBOutlet UILabel *immatriculeLabel;

@property (weak, nonatomic) IBOutlet UILabel *travauxLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIButton *historiqueBtn;

@property (weak, nonatomic) IBOutlet UILabel *telProprietaire;
@property (weak, nonatomic) IBOutlet UILabel *contactName;

@property (weak, nonatomic) IBOutlet UILabel *contactTel;

- (IBAction)afficherHistorique:(id)sender;

@end
