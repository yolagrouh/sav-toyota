//
//  NOVGridCell.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/29/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "TMQuiltView.h"
#import "NOVTextField.h"

@class NOVGridRelanceCell;

@protocol NOVGridRelanceCellDelegate <NSObject>

@required

-(void)nOVGridRelanceCell:(NOVGridRelanceCell *)nOVGridRelanceCell status:(BOOL)status;

-(void)nOVGridRelanceCell:(NOVGridRelanceCell *)nOVGridRelanceCell showOptions:(BOOL)show;

-(void)nOVGridRelanceCell:(NOVGridRelanceCell *)nOVGridRelanceCell envoyer:(NSString *)justif;

@end

@interface NOVGridRelanceCell : TMQuiltViewCell

@property (weak, nonatomic)id<NOVGridRelanceCellDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *nomLabel;

@property (weak, nonatomic) IBOutlet UILabel *immatriculeLabel;

@property (weak, nonatomic) IBOutlet UILabel *contactLabel;

@property (weak, nonatomic) IBOutlet UILabel *contact;

@property (weak, nonatomic) IBOutlet UILabel *travauxLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelValue;

@property (weak, nonatomic) IBOutlet UILabel *modelLabel;

@property (weak, nonatomic) IBOutlet UILabel *tel;

@property (weak, nonatomic) IBOutlet UILabel *telLabel;

@property (weak, nonatomic) IBOutlet UILabel *telProprioValue;

@property (weak, nonatomic) IBOutlet UILabel *telProprioLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UITextField *raisonTF;

@property (weak, nonatomic) IBOutlet UIView *relanceKoView;

- (IBAction)relanceOK:(id)sender;

- (IBAction)relanceKO:(id)sender;

- (IBAction)envoyerRelanceKO:(id)sender;

- (IBAction)showRelanceKOOptions:(id)sender;

@end
