//
//  NOVGridCell.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/29/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVGridRelanceCell.h"
#import "Utils.h"

@implementation NOVGridRelanceCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)relanceOK:(id)sender {
    
    [self.delegate nOVGridRelanceCell:self status:YES];
    
}

- (IBAction)relanceKO:(id)sender {
    
    NSLog(@"View Origine %f %f", self.frame.size.height, self.relanceKoView.frame.size.height + self.relanceKoView.frame.origin.y);

    [UIView animateWithDuration:0.3  animations:^{
        
        self.relanceKoView.frame = CGRectMake(0, self.relanceKoView.frame.origin.y, self.relanceKoView.frame.size.width, self.relanceKoView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self.delegate nOVGridRelanceCell:self status:NO];
        
    }];
    
}

- (IBAction)envoyerRelanceKO:(id)sender {
    
    if ([self.raisonTF.text length] == 0) {
        
        [Utils showAlertView:@"Veuillez renseigner la raison"];
        
        return;
        
    }
    
    [self.delegate nOVGridRelanceCell:self envoyer:self.raisonTF.text];
   
    
}

- (IBAction)showRelanceKOOptions:(id)sender {
    
    [UIView animateWithDuration:0.3  animations:^{
        
       // self.relanceKoView.frame = CGRectMake(self.relanceKoView.frame.size.width, self.relanceKoView.frame.origin.y, self.relanceKoView.frame.size.width, self.relanceKoView.frame.size.height);
        
    }];
    
    [self.delegate nOVGridRelanceCell:self showOptions:YES];
    
}
@end
