#import "NOVRelanceOptionsViewController_ipad.h"
#import "NOVRelanceRaisonCell.h"

@interface NOVRelanceOptionsViewController_ipad ()

@property(nonatomic, strong)NSMutableArray * selectedRaisons;

@property(nonatomic, assign)NSInteger selectedIndex;

@end

@implementation NOVRelanceOptionsViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        self.raisons = [NSMutableArray arrayWithObjects:@"Le prix de la prestation",@"Le temps d’exécution de la prestation",@"Le client a pris rendez-vous", nil];
        
        self.selectedRaisons = [NSMutableArray array];
        
        self.selectedIndex = -1;
        
        self.selectionMode = PopUpModeSingle;
        
        self.shouldAddGradient = NO;
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)setUpView{
    
    [super setUpView];
    
    self.autreTF.hidden = ![self.raisons containsObject:@"Autre"];

}

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.raisons count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
    
}

-(UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"NOVRelanceRaisonCell";
    
    NOVRelanceRaisonCell *cell = (NOVRelanceRaisonCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        
        cell = (NOVRelanceRaisonCell *)[self getCellWithIdentifier:cellIdentifier];
        
    }
    
    NSString * raison = [self.raisons objectAtIndex:indexPath.row];
    
    cell.raisonLabel.text = raison;
    
    if (self.selectionMode == PopUpModeMultiple) {
        
        cell.iconeImage.hidden = ![self.selectedRaisons containsObject:[self.raisons objectAtIndex:indexPath.row]];
        
    }else{
        
        cell.iconeImage.hidden = !(self.selectedIndex == indexPath.row);
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.selectionMode == PopUpModeMultiple) {
        
        if (![self.selectedRaisons containsObject:[self.raisons objectAtIndex:indexPath.row]]) {
            
            if (![[self.raisons objectAtIndex:indexPath.row] isEqualToString:@"Autre"]) {
                
                [self.selectedRaisons addObject:[self.raisons objectAtIndex:indexPath.row]];
            }
            
            
        }else{
            
            [self.selectedRaisons removeObject:[self.raisons objectAtIndex:indexPath.row]];
            
        }
        
    }else{
        
        self.selectedIndex = indexPath.row;
        
    }
    
    if ([self.raisons containsObject:@"Autre"]) {
        
        if (indexPath.row == [self.raisons count]-1) {
            
            [self.autreTF becomeFirstResponder];
            
            
        }else{
            
            [self.autreTF resignFirstResponder];
            
        }
        
    }
    
    
    
    [tableView reloadData];
    
}

- (IBAction)valider:(id)sender {
    
//    if ([self.raisons containsObject:@"Autre"]) {
//        
//        if ([self.autreTF.text length] == 0) {
//            
//            [self showError:@"Veuillez remplire le champs Autre"];
//            
//            return;
//            
//        }
//        
//    }
    
    if (self.selectionMode == PopUpModeSingle) {
        
        if (self.selectedIndex == -1) {
            
            [self showError:@"Veuillez sélectionner la raison de non satisfaction du client"];
            
            return;
            
        }
    }else{
        
        
        if ([self.selectedRaisons count] == 0) {
            
            [self showError:@"Veuillez sélectionner les raisons de non satisfaction du client"];
            
            return;
            
        }
        
    }
    
    
    [super valider:sender];
    
    if (self.selectionMode == PopUpModeSingle) {
        
        [self.delegate nOVRelanceOptionsViewController:self selected:[self.raisons objectAtIndex:self.selectedIndex]];
        
    }else{
        
        NSString * rai = [self.selectedRaisons componentsJoinedByString:@","];
        
        if ([self.raisons containsObject:@"Autre"]) {
            
            if ([rai length] ==0) {
                
                rai = self.autreTF.text;
            }else{
                
                rai = [NSString stringWithFormat:@"%@,%@", rai, self.autreTF.text];
                
            }
        }
        
        [self.delegate nOVRelanceOptionsViewController:self selected:rai];
        
    }
}

- (IBAction)annuler:(id)sender {
    
    [super annuler:sender];
    
    [self.delegate nOVRelanceOptionsViewController:self selected:@""];
}
@end
