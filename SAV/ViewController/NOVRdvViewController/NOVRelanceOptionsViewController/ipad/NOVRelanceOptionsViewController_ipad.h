//
//  NOVRelanceOptionsViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVRelanceOptionsViewController.h"

typedef enum : NSUInteger {
    PopUpModeSingle,
    PopUpModeMultiple,
} PopUpMode;

@interface NOVRelanceOptionsViewController_ipad : NOVRelanceOptionsViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak)id<NOVRelanceOptionsViewControllerDelegate>delegate;

@property(nonatomic, assign)PopUpMode  selectionMode;

@property(nonatomic, strong)NSMutableArray * raisons;

@property (weak, nonatomic) IBOutlet NOVTextField *autreTF;



@end
