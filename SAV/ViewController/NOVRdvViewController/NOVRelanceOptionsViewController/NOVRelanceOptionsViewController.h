//
//  NOVRelanceOptionsViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class NOVRelanceOptionsViewController;

@protocol NOVRelanceOptionsViewControllerDelegate <NSObject>

@required

-(void)nOVRelanceOptionsViewController:(NOVRelanceOptionsViewController *)nOVRelanceOptionsViewController selected:(NSString *)raison;

@end

@interface NOVRelanceOptionsViewController : NOVParentPopUpViewController

@property(nonatomic, assign)NSInteger owner;


@end
