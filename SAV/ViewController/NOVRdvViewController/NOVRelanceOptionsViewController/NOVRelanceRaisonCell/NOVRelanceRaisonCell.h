//
//  NOVMoreTravauxCell.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/12/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "BaseCell.h"

@interface NOVRelanceRaisonCell : BaseCell

@property (weak, nonatomic) IBOutlet UILabel *raisonLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iconeImage;

@end
