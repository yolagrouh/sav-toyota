//
//  NOVRdvViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVRdvViewController.h"
#import "NOVGridRelanceCell.h"
#import "NOVRelanceOptionsViewController_ipad.h"

@interface NOVRdvViewController_ipad : NOVRdvViewController<TMQuiltViewDataSource, TMQuiltViewDelegate,NOVGridRelanceCellDelegate, NOVRelanceOptionsViewControllerDelegate>

@property (weak, nonatomic) IBOutlet NOVButton *confirmerBtn;

@property (nonatomic, assign) CGFloat lastContentOffset;

@property (nonatomic, strong) NSMutableArray * rdvs;
@property (nonatomic, strong) NSMutableArray * relances;

@end
