#import "NOVRdvViewController_ipad.h"
#import "NOVGridCell.h"
#import "NOVSharedUser.h"

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;

typedef enum GrideMode {
    GrideModeRdv,
    GrideModeRelances
} GrideMode;

@interface NOVRdvViewController_ipad (){
    
    
    NSArray * _imageNames;
    
    GrideMode gridMode;
    
    NSInteger activeRelanceCell;
    
    NSString * raisonInsatisfaction;
    
    TMQuiltViewController * gridController;
    
    NSMutableArray * filteredArray;
}

@property(nonatomic, strong) NOVRelanceOptionsViewController_ipad * nOVRelanceOptionsViewController;

@end

@implementation NOVRdvViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        gridMode = GrideModeRdv;
        
        activeRelanceCell = -1;
        
        self.rdvs       = [NSMutableArray array];
        
        self.relances   = [NSMutableArray array];
        
        filteredArray   = [NSMutableArray array];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRDV:) name:RELOAD_RDV_NOTIFICATION object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRelances:) name:RELOAD_RELANCES_NOTIFICATION object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recherche:) name:RECHERCHE_NOTIFICATION object:nil];
        
        
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    
}

-(void)setUpView{
    
    [super setUpView];
    
    NOVSharedUSer * shareUser = [NOVSharedUSer sharedNOVUser];
    
    [self doGet:[NSString stringWithFormat:@"%@%@",RDV_WS, shareUser.login] requestID:0];
    
    gridController = [[TMQuiltViewController alloc]init];
    
    gridController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    gridController.quiltView.delegate = self;
    
    gridController.quiltView.dataSource = self;
    
    gridController.quiltView.backgroundColor =[UIColor clearColor];
    
    [self.view addSubview:gridController.view];
    
    [gridController.quiltView reloadData];
    
    [self.view bringSubviewToFront:self.confirmerBtn];
    
}

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    [self.rdvs addObjectsFromArray:[response objectForKey:@"rdvs"]];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    sharedData.rendezVous = [NSMutableArray arrayWithArray:[response objectForKey:@"rdvs"]];
    
    [filteredArray removeAllObjects];
    
    [filteredArray addObjectsFromArray:self.rdvs];
    
    [gridController.quiltView reloadData];
    
}

#pragma mark - QuiltViewControllerDataSource

- (NSInteger)quiltViewNumberOfCells:(TMQuiltView *)TMQuiltView {
    
    return [filteredArray count];
}

- (TMQuiltViewCell *)quiltView:(TMQuiltView *)quiltView cellAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (gridMode == GrideModeRdv) {
        
        NOVGridCell * gridCell = (NOVGridCell *)[quiltView dequeueReusableCellWithReuseIdentifier:@"NOVGridCell"];
        
        if (!gridCell) {
            
            gridCell = [[[NSBundle mainBundle]loadNibNamed:@"NOVGridCell" owner:nil options:nil]objectAtIndex:0];
        }
        
        NSDictionary * rdv = [filteredArray objectAtIndex:indexPath.row];
        
        if (![[rdv objectForKey:@"NOM_CLIENT"] isKindOfClass:[NSNull class]]) {
            
            gridCell.nomLabel.text =[rdv objectForKey:@"NOM_CLIENT"];
            
        }else{
            
            gridCell.nomLabel.text =@"";
        }
        
        if (![[rdv objectForKey:@"IMMATRICULE"] isKindOfClass:[NSNull class]]) {
            
            gridCell.immatriculeLabel.text =[rdv objectForKey:@"IMMATRICULE"];
            
        }else{
            
            gridCell.immatriculeLabel.text =@"";
        }
        
        if (![[rdv objectForKey:@"OPERATION_CODE"] isKindOfClass:[NSNull class]]) {
            
            gridCell.travauxLabel.text =[rdv objectForKey:@"OPERATION_CODE"];
            
        }else{
            
            gridCell.travauxLabel.text =@"";
        }
        
        if (![[rdv objectForKey:@"DATE_RDV"] isKindOfClass:[NSNull class]]) {
            
            gridCell.dateLabel.text =[rdv objectForKey:@"DATE_RDV"];
            
        }else{
            
            gridCell.dateLabel.text =@"";
        }
        
        
        if (![[rdv objectForKey:@"TEL"] isKindOfClass:[NSNull class]]) {
            
            gridCell.telProprietaire.text =[rdv objectForKey:@"TEL"];
            
        }else{
            
            gridCell.telProprietaire.text =@"";
        }
        
        return gridCell;
        
        
    }else{
        
        NOVGridRelanceCell * gridCell  = (NOVGridRelanceCell *)[quiltView dequeueReusableCellWithReuseIdentifier:@"NOVGridRelanceCell"];
        
        if (!gridCell) {
            
            gridCell = [[[NSBundle mainBundle]loadNibNamed:@"NOVGridRelanceCell" owner:nil options:nil]objectAtIndex:0];
        }
        
        gridCell.delegate = self;
        
        gridCell.tag = indexPath.row;
        
        if (activeRelanceCell != indexPath.row) {
            
            gridCell.relanceKoView.frame = CGRectMake(gridCell.relanceKoView.frame.size.width,gridCell.relanceKoView.frame.origin.y, gridCell.relanceKoView.frame.size.width, gridCell.relanceKoView.frame.size.height);
            
            //            [UIView animateWithDuration:0.3 animations:^{
            //
            //                gridCell.relanceKoView.frame = CGRectMake(gridCell.relanceKoView.frame.size.width,gridCell.relanceKoView.frame.origin.y, gridCell.relanceKoView.frame.size.width, gridCell.relanceKoView.frame.size.height);
            //
            //            }];
        }else{
            
            gridCell.raisonTF.text = raisonInsatisfaction;
            
            gridCell.relanceKoView.frame = CGRectMake( 0 ,gridCell.relanceKoView.frame.origin.y, gridCell.relanceKoView.frame.size.width, gridCell.relanceKoView.frame.size.height);
            
        }
        
        NSDictionary * rdv = [filteredArray objectAtIndex:indexPath.row];
        
        if (![[rdv objectForKey:@"NOM_CLIENT"] isKindOfClass:[NSNull class]]) {
            
            gridCell.nomLabel.text =[rdv objectForKey:@"NOM_CLIENT"];
            
            
        }else{
            
            gridCell.nomLabel.text =@"";
        }
        
        if (![[rdv objectForKey:@"IMMATRICULE"] isKindOfClass:[NSNull class]]) {
            
            gridCell.immatriculeLabel.text =[rdv objectForKey:@"IMMATRICULE"];
            
            
        }else{
            
            gridCell.immatriculeLabel.text =@"";
        }
        
         if (![[rdv objectForKey:@"CONTACT_NOM"] isKindOfClass:[NSNull class]]) {
             
             if ([[rdv objectForKey:@"CONTACT_NOM"] length] > 0) {
                 
                 gridCell.contactLabel.hidden = NO;
                 gridCell.contact.hidden = NO;
                 gridCell.contact.text =[rdv objectForKey:@"CONTACT_NOM"];
                 
             }else{
                 
                 gridCell.contactLabel.hidden = YES;
                 gridCell.contact.hidden = YES;
                 
             }
             
         }else{
             
             gridCell.contact.text =@"";
             gridCell.contactLabel.hidden = YES;
             gridCell.contact.hidden = YES;
         }
        
        
        
        if (![[rdv objectForKey:@"CONTACT_TEL"] isKindOfClass:[NSNull class]]) {
            
            if ([[rdv objectForKey:@"CONTACT_TEL"] length] > 0) {
                
                gridCell.tel.hidden = NO;
                gridCell.telLabel.hidden = NO;
                gridCell.tel.text =[rdv objectForKey:@"CONTACT_TEL"];
                
            }else{
                
                gridCell.tel.hidden = YES;
                gridCell.telLabel.hidden = YES;
                
            }
            
        }else{
            
            gridCell.tel.text =@"";
            gridCell.tel.hidden = YES;
            gridCell.telLabel.hidden = YES;
        }
        
        if (![[rdv objectForKey:@"TEL"] isKindOfClass:[NSNull class]]) {
            
            if ([[rdv objectForKey:@"TEL"] length] > 0) {
                
                gridCell.telProprioLabel.hidden = NO;
                gridCell.telProprioValue.hidden = NO;
                gridCell.telProprioValue.text =[rdv objectForKey:@"TEL"];
                
            }else{
                
                gridCell.telProprioLabel.hidden = YES;
                gridCell.telProprioValue.hidden = YES;
                
            }
            
        }else{
            
            gridCell.telProprioLabel.text =@"";
            gridCell.telProprioValue.hidden = YES;
            gridCell.telProprioValue.hidden = YES;
        }
        
        if (![[rdv objectForKey:@"MODELE_VEH"] isKindOfClass:[NSNull class]]) {
            
            if ([[rdv objectForKey:@"MODELE_VEH"] length] > 0) {
                
                gridCell.modelLabel.hidden = NO;
                gridCell.modelValue.hidden = NO;
                gridCell.modelValue.text =[rdv objectForKey:@"MODELE_VEH"];
                
            }else{
                
                gridCell.modelLabel.hidden = YES;
                gridCell.modelValue.hidden = YES;
                
            }
            
        }else{
            
            gridCell.modelLabel.text =@"";
            gridCell.modelValue.hidden = YES;
            gridCell.modelValue.hidden = YES;
        }
        
        NSString * travaux = @"";
        
        for (id requeteClient in [rdv objectForKey:@"SELECTED_REQUESTES"]) {
            
            travaux = [NSString stringWithFormat:@"%@,%@",travaux, [requeteClient objectForKey:@"OP_DESCRIPTION"]];
            
        }
        
        if ([travaux length] > 0) {
            
            travaux = [travaux substringFromIndex:1];
        }
        
        gridCell.travauxLabel.text      = travaux;
        
        gridCell.dateLabel.text         = [[rdv objectForKey:@"RAPPEL_SUIVI"] objectForKey:@"DATE"];
        
        return gridCell;
    }
    
    return nil;
}


-(void)quiltView:(TMQuiltView *)quiltView didSelectCellAtIndexPath:(NSIndexPath *)indexPath{
    
    if (gridMode == GrideModeRdv) {
        
        id rdv = [self.rdvs objectAtIndex:indexPath.row];
        
        NSDictionary * userInfo = [NSDictionary dictionaryWithObject:[rdv objectForKey:@"SN"] forKey:@"SN"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_VIN_POP_UP_WITH_OBJECT object:nil userInfo:userInfo];
        
    }
    
}

#pragma mark - TMQuiltViewDelegate

- (NSInteger)quiltViewNumberOfColumns:(TMQuiltView *)quiltView {
    
    return 3;
}

- (CGFloat)quiltView:(TMQuiltView *)quiltView heightForCellAtIndexPath:(NSIndexPath *)indexPath {
    
    return 344;
}

#pragma marl --
#pragma mark ScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection;
    
    if (self.lastContentOffset > scrollView.contentOffset.y){
        
        scrollDirection = ScrollDirectionUp;
        [UIView animateWithDuration:0.3 animations:^{
            
            self.confirmerBtn.frame =  CGRectMake(294, self.view.frame.size.height + 50, self.confirmerBtn.frame.size.width, self.confirmerBtn.frame.size.height);
            
        }];
        
    }else if (self.lastContentOffset < scrollView.contentOffset.y){
        
        scrollDirection = ScrollDirectionDown;
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
    
}



- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    
    if (bottomEdge >= scrollView.contentSize.height) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.confirmerBtn.frame =  CGRectMake(294, 751, self.confirmerBtn.frame.size.width, self.confirmerBtn.frame.size.height);
            
        }];
    }
}

#pragma mark --
#pragma mark GridRelanceCell Delegate

-(void)nOVGridRelanceCell:(NOVGridRelanceCell *)nOVGridRelanceCell status:(BOOL)status{
    
    if (status == NO) {
        
        return;
    }
    
    NSMutableArray * relances = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"RELANCES_OBJECTS"]];
    
    [relances removeObjectAtIndex:nOVGridRelanceCell.tag];
    
    [Utils saveMutableObject:relances  key:@"RELANCES_OBJECTS"];
    
    [filteredArray removeObjectAtIndex:nOVGridRelanceCell.tag];
    
    [gridController.quiltView reloadData];
    
}

-(void)nOVGridRelanceCell:(NOVGridRelanceCell *)nOVGridRelanceCell showOptions:(BOOL)show{
    
    activeRelanceCell = nOVGridRelanceCell.tag;
    
    self.nOVRelanceOptionsViewController = [[NOVRelanceOptionsViewController_ipad alloc] initWithNibName:@"NOVRelanceOptionsViewController" bundle:nil];
    
    self.nOVRelanceOptionsViewController.delegate = self;
    
    self.nOVRelanceOptionsViewController.selectionMode = PopUpModeMultiple;
    
    [self.view addSubview:self.nOVRelanceOptionsViewController.view];
   
    [self.nOVRelanceOptionsViewController show];
    
    
}

-(void)nOVGridRelanceCell:(NOVGridRelanceCell *)nOVGridRelanceCell envoyer:(NSString *)justif{
    
    NSMutableArray * relances = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"RELANCES_OBJECTS"]];
    
    [relances removeObjectAtIndex:nOVGridRelanceCell.tag];
    
    [Utils saveMutableObject:relances  key:@"RELANCES_OBJECTS"];
    
    [filteredArray removeObjectAtIndex:nOVGridRelanceCell.tag];
    
    [gridController.quiltView reloadData];
    
}

#pragma mark --
#pragma mark Reload Notification

-(void)showRDV:(NSNotification *)notification{
    
    gridMode = GrideModeRdv;
    
    [filteredArray removeAllObjects];
    
    [filteredArray addObjectsFromArray:self.rdvs];
    
    [gridController.quiltView reloadData];
}

-(void)showRelances:(NSNotification *)notification{
    
    //Relances
    
    raisonInsatisfaction = nil;
    
    activeRelanceCell = -1;
    
    [self.relances removeAllObjects];
    
    [self.relances addObjectsFromArray:[Utils getSavedMutableObject:@"RELANCES_OBJECTS"]];
    
    [filteredArray removeAllObjects];
    
    [filteredArray addObjectsFromArray:self.relances];
    
    gridMode = GrideModeRelances;
    
    [gridController.quiltView reloadData];
    
}

#pragma mark --
#pragma mark Recherche

-(void)recherche:(NSNotification *)notification{
    
    [filteredArray removeAllObjects];
    
    NSDictionary * userInfo = [notification userInfo];
    
    NSString * recherche = [userInfo objectForKey:@"RECHERCHE"];
    
    switch (gridMode) {
        case GrideModeRdv:
            
            for (id rdv in self.rdvs) {
                
                if ([Utils checkString:[rdv objectForKey:@"SN"] contains:recherche]) {
                    
                    [filteredArray addObject:rdv];
                    
                }
                
            }
            
            break;
            
        case GrideModeRelances:
            
            for (id rdv in self.rdvs) {
                
                if ([Utils checkString:[rdv objectForKey:@"SN"] contains:recherche]) {
                    
                    [filteredArray addObject:rdv];
                    
                }
                
            }
            
            break;
            
        default:
            break;
    }
    
    [gridController.quiltView reloadData];
    
}

#pragma mark --
#pragma mark Raison Options Delegate

-(void)nOVRelanceOptionsViewController:(NOVRelanceOptionsViewController *)nOVRelanceOptionsViewController selected:(NSString *)raison{
    
    raisonInsatisfaction = raison;
    
    NSMutableDictionary * relance = [self.relances objectAtIndex:activeRelanceCell];
    
    [relance setObject:raison forKey:@"RELANCE_REFUS_RAISON"];
    
    [gridController.quiltView reloadData];
    
}

@end
