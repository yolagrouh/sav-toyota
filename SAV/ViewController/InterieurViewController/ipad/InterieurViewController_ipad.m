#import "InterieurViewController_ipad.h"
#import "ExterieurViewController_ipad.h"
#import "NOVAutreObjetView.h"
#import "ALPValidator.h"
#import "NOVSharedUSer.h"

@interface InterieurViewController_ipad (){

    NSInteger autreObjetsInterieursCount;
    
    BOOL canGo;
    
}

@property(nonatomic, strong)NSString * selectedObjectsClient;

@end

@implementation InterieurViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        self.selectedObjectsClient = [NSString string];
        
        autreObjetsInterieursCount = 0;
        
        canGo = NO;
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    NOVSharedData * shareData = [NOVSharedData sharedNOVData];
    
    shareData.objectsInterieur = [NSString string];
    
}

-(void)setUpView{
    
    [super setUpView];
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.containerView.frame.size.height);
}


-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    
    
}

#pragma mark --
#pragma mark Actions

- (IBAction)valider:(id)sender {
    
//    if (!self.isOnline) {
//        
//        [Utils showAlertView:@"Veuillez vous assurer que vous disposez d'une connexion internet avant de continuer."];
//        
//        return;
//        
//    }
    
    
    ALPValidator *validator = [ALPValidator validatorWithType:ALPValidatorTypeNumeric];
    
    [validator validate:self.kilometrageTF.text];
    
    if ([self.kilometrageTF.text length] == 0) {
        
        [self showError:@"Veuillez spécifier le kilométrage"];
        
        return;
    }
    
    
    NOVSharedData * shareData = [NOVSharedData sharedNOVData];
    
    if ([Utils getNumber:self.kilometrageTF.text] <=[Utils getNumber: shareData.previousKM]) {
        
        [Utils showAlertView:[NSString stringWithFormat:@"Veuillez saisir un kilométrage supérieur à %@",shareData.previousKM]];
        
        return;
    }
    
    
    
    BOOL isCarburantSelected = NO;
    
    for (UISwitch * carburantSwitch in self.niveauxCarburant) {
        
        if (carburantSwitch.isOn) {
            
            isCarburantSelected = YES;
            
            break;
            
        }
        
    }
    
    if (!isCarburantSelected) {
        
        [self showError:@"Veuillez spécifier le niveau du carburant"];
        
        return;
    }
    
    
    [self showValidation];

}

- (IBAction)choisirNiveau:(id)sender {
    
    for (UISwitch * niveauSwitch in self.niveauxCarburant) {
        
        if ([niveauSwitch isEqual:sender]) {
            
            //niveauSwitch.on = !niveauSwitch.on;
            
        }else{
            
            [niveauSwitch setOn:NO animated:YES];
        }
        
    }
    
}

- (IBAction)choisirObject:(id)sender {
    
    NOVTaggedButton * taggedButton = (NOVTaggedButton *)sender;
    
    taggedButton.selected = !taggedButton.selected;
    
    
    NOVSharedData * shareData = [NOVSharedData sharedNOVData];
    
    NSMutableArray * objects = [NSMutableArray arrayWithArray:[shareData.objectsInterieur componentsSeparatedByString:@","]];
    
    if (taggedButton.isSelected) {
        
        [objects addObject:taggedButton.content];
      
    }else{
        
        [objects removeObject:taggedButton.content];
    }
    
    shareData.objectsInterieur = [objects componentsJoinedByString:@","];
}


- (IBAction)choisirAutreObject:(id)sender {
    
    [(UIButton *)sender setSelected:![sender isSelected]];
    
    self.autresElementContainer.hidden = !self.autresElementContainer.hidden;
    self.addAutresElementsBtn.hidden = !self.addAutresElementsBtn.hidden;
    
    if (!((UIButton *)sender).isSelected) {
        
        
        autreObjetsInterieursCount = 0;
        self.addAutresElementsBtn.enabled = YES;
    
        for (NOVAutreObjetView * autreObjetView in [self.autresElementContainer subviews]) {
            
            NSMutableArray * tempsArray = [[self.selectedObjectsClient componentsSeparatedByString:@","] mutableCopy];
            
            NSLog(@"Text : %@",autreObjetView.field.text);
            
            if ([tempsArray containsObject:autreObjetView.field.text]) {
                
                [tempsArray removeObject:autreObjetView.field.text];
            }
            
            [tempsArray count] == 0 ? self.selectedObjectsClient = @"": ( self.selectedObjectsClient = [tempsArray componentsJoinedByString:@","]);
            
            [autreObjetView removeFromSuperview];
           
        }
        
        self.containerViewHeightConstraint.constant -= self.autresObjetsHeightConstraint.constant-48;
        
        self.autresObjetsHeightConstraint.constant = 48;
        
        self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.containerViewHeightConstraint.constant +20);
        
    }else{
        
        [self addAutresElements:nil];
        
    }
    
}

- (IBAction)choisirObjectValeur:(id)sender {// Activer Elements Valeur
    
    [(UIButton *)sender setSelected:![sender isSelected]];
    
    self.objectsValeurContainer.hidden = !self.objectsValeurContainer.hidden;
    
    self.addObjectValeurBtn.hidden = !self.addObjectValeurBtn.hidden;
    
    if (!((UIButton *)sender).isSelected) {
        
        for (UITextField * textField in [self.objectsValeurContainer subviews]) {
            
            NSMutableArray * tempsArray = [[self.selectedObjectsClient componentsSeparatedByString:@","] mutableCopy];
            
            NSLog(@"Text : %@",textField.text);
            
            if ([tempsArray containsObject:textField.text]) {
                
                [tempsArray removeObject:textField.text];
            }
            
            [tempsArray count] == 0 ? self.selectedObjectsClient = @"": ( self.selectedObjectsClient = [tempsArray componentsJoinedByString:@","]);
            
            if (textField != self.firstObjectValeurTF) {
                
                [textField removeFromSuperview];
            }
            
        }
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.firstAutreTF.text = nil;
            
            self.containerViewHeightConstraint.constant -= self.objetsValeursHeightConstraint.constant-48;
            
            self.objetsValeursHeightConstraint.constant = 48;
            
            self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.containerViewHeightConstraint.constant +20);
            
        }];
        
    }
}

- (IBAction)addObjectValeurElements:(id)sender { // Add Elements Valeur TextFields
    
    UIButton * autreBtn = [self.objectsInterieur objectAtIndex:[self.objectsInterieur count] -2];
    
    if (!autreBtn.isSelected) {
        
        return;
    }
    
    NOVTextField * textField = [[NOVTextField alloc]initWithFrame:CGRectZero];
    
    textField.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.objectsValeurContainer addSubview:textField];
    
    [self.objectsValeurContainer addConstraint:[NSLayoutConstraint constraintWithItem:textField
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.objectsValeurContainer
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:0
                                                           constant:321]];
    
    [self.objectsValeurContainer addConstraint:[NSLayoutConstraint constraintWithItem:textField
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.objectsValeurContainer
                                                                            attribute:NSLayoutAttributeHeight
                                                                           multiplier:0
                                                                             constant:48]];
    
    [self.objectsValeurContainer addConstraint:[NSLayoutConstraint constraintWithItem:textField
                                                                            attribute:NSLayoutAttributeLeft
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.objectsValeurContainer
                                                                            attribute:NSLayoutAttributeLeft
                                                                           multiplier:0
                                                                             constant:0]];
    
    CGFloat top = ([[self.objectsValeurContainer  subviews] count] -1) * 58;
    
    [self.objectsValeurContainer addConstraint:[NSLayoutConstraint constraintWithItem:textField
                                                                           attribute:NSLayoutAttributeTop
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.objectsValeurContainer
                                                                           attribute:NSLayoutAttributeTop
                                                                          multiplier:0
                                                                            constant:top]];
    
    [textField becomeFirstResponder];
    
    [textField setUpField];
    
    textField.delegate = self;
    
    textField.placeholder = @"SAISIR LE TEXTE";
    
    textField.font = [UIFont fontWithName:@"Arial" size:20];
    
    self.objetsValeursHeightConstraint.constant  += 58;
    
    self.containerViewHeightConstraint.constant  += 58;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.containerViewHeightConstraint.constant +20);
    
}

- (IBAction)addAutresElements:(id)sender {
    
    UIButton * autreBtn = [self.objectsInterieur lastObject];
    
    if (!autreBtn.isSelected) {
        
        return;
    }
    
    NOVAutreObjetView * autreView = [[[NSBundle mainBundle] loadNibNamed:@"NOVAutreObjetView" owner:nil options:nil] objectAtIndex:0];
    
    [autreView.field becomeFirstResponder];
    
    autreView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.autresElementContainer addSubview:autreView];
    
    [self.autresElementContainer addConstraint:[NSLayoutConstraint constraintWithItem:autreView
                                                                            attribute:NSLayoutAttributeWidth
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.autresElementContainer
                                                                            attribute:NSLayoutAttributeWidth
                                                                           multiplier:0
                                                                             constant:321]];
    
    [self.autresElementContainer addConstraint:[NSLayoutConstraint constraintWithItem:autreView
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.autresElementContainer
                                                                            attribute:NSLayoutAttributeHeight
                                                                           multiplier:0
                                                                             constant:48]];
    
    [self.autresElementContainer addConstraint:[NSLayoutConstraint constraintWithItem:autreView
                                                                            attribute:NSLayoutAttributeLeft
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.autresElementContainer
                                                                            attribute:NSLayoutAttributeLeft
                                                                           multiplier:1
                                                                             constant:0]];
    
    CGFloat top = ([[self.autresElementContainer  subviews] count] -1) * 58;
    
    [self.autresElementContainer addConstraint:[NSLayoutConstraint constraintWithItem:autreView
                                                                            attribute:NSLayoutAttributeTop
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.autresElementContainer
                                                                            attribute:NSLayoutAttributeTop
                                                                           multiplier:1
                                                                             constant:top]];
    
    
    autreView.field.delegate = self;
    
    autreView.field.placeholder = @"Saisir le texte";
    
    autreView.field.font = [UIFont fontWithName:@"Arial" size:20];
    
    self.autresObjetsHeightConstraint.constant  += 58;
    
    self.containerViewHeightConstraint.constant  += 58;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.containerViewHeightConstraint.constant+ 20);
    
    if (autreObjetsInterieursCount == 2) {
        
        
        
        self.addObjectsBtn.enabled = NO;
        
        self.addObjectsValeurBtn.enabled = NO;
        
    }
    
}

#pragma mark --
#pragma mark TextField

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.tag == 11) { // Kilometrage
        
        if (![Utils isNumber:string]) {
            
            return NO;
            
        }
        if ([textField.text length] == 0 && [string isEqualToString:@"."])  {
            
            return  NO;
        }
        
        if ([textField.text length] > 0 && [Utils checkString:textField.text  contains:@"."] && [string isEqualToString:@"."])  {
            
            return  NO;
        }
    }
    
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [super textFieldDidBeginEditing:textField];
    
    if ([textField.text length] == 0) {
        
        return;
    }
    
    if ([self.selectedObjectsClient length] == 0) {
        
        return;
        
    }
    
    NSMutableArray * tempsArray = [[self.selectedObjectsClient componentsSeparatedByString:@","] mutableCopy];
    
    if ([tempsArray containsObject:textField.text]) {
        
        [tempsArray removeObject:textField.text];
    }
    
    [tempsArray count] == 0 ? self.selectedObjectsClient = @"": ( self.selectedObjectsClient = [tempsArray componentsJoinedByString:@","]);
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [super textFieldDidEndEditing:textField];
    
    if (textField.tag == 11) {//KM don't
        
        NOVSharedData * shareData = [NOVSharedData sharedNOVData];
        
        if ([Utils getNumber:textField.text] <=[Utils getNumber:shareData.previousKM]) {
            
            [Utils showAlertView:[NSString stringWithFormat:@"Veuillez saisir un kilométrage supérieur à %@",shareData.previousKM]];
            
        }else{
            
            canGo = YES;
            
            NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
            
            sharedData.userKM    = self.kilometrageTF.text;
            
        }
        
        return;
    }
    
    if ([textField.text length] > 0) {
        
        autreObjetsInterieursCount ++;
        
        NOVSharedData * shareData = [NOVSharedData sharedNOVData];
        
        NSMutableArray * objects = [NSMutableArray arrayWithArray:[shareData.objectsInterieur componentsSeparatedByString:@","]];
        
       
        
        if (![objects containsObject:textField.text]) {
            
            [objects addObject:textField.text];
            
        }
        
        
        
        shareData.objectsInterieur = [objects componentsJoinedByString:@","];
        
        if ([self.selectedObjectsClient length] == 0) {
            
            self.selectedObjectsClient = textField.text;
            
            return;
        }
        
        self.selectedObjectsClient = [NSString stringWithFormat:@"%@,%@",self.selectedObjectsClient, textField.text];
        
        if (autreObjetsInterieursCount  == 3) {
            
            self.addObjectsBtn.enabled = NO;
            
            self.addObjectsValeurBtn.enabled = NO;
            
           // [Utils showAlertView:@"Vous ne pouvez pas ajouter plus que 3 objets"];
            
            return;
            
        }
    }
    
    
    
}

-(IBAction)onEditingDidBegin:(NSNotification *) notif{
    
 //  [self.contentSV scrollRectToVisible:[self.contentSV convertRect:self.autresElementContainer.frame fromView:self.containerView] animated:YES];
    
}
-(IBAction)onEditingDidEnd:(NSNotification *) notif{
    
    
}

#pragma mark --
#pragma mark Validation

-(void)showNext{
    
    [super showNext];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    sharedData.kilometrage = self.kilometrageTF.text;
    
    if ([(UISwitch *)[self.niveauxCarburant objectAtIndex:0] isOn]) {
        
        sharedData.niveauCarburant = @"0-1/4";
        
    }else if ([(UISwitch *)[self.niveauxCarburant objectAtIndex:1] isOn]) {
        
        sharedData.niveauCarburant = @"1/4-1/2";
        
    }else if ([(UISwitch *)[self.niveauxCarburant objectAtIndex:2] isOn]) {
        
        sharedData.niveauCarburant = @"1/2-3/4";
        
    }else if ([(UISwitch *)[self.niveauxCarburant objectAtIndex:3] isOn]) {
        
        sharedData.niveauCarburant = @"3/4-1";
        
    }
    
    
    ExterieurViewController_ipad * exterieurViewController = [[ExterieurViewController_ipad alloc]initWithNibName:@"ExterieurViewController" bundle:nil];
    
    [self.navigationController pushViewController:exterieurViewController animated:YES];
    
}


@end
