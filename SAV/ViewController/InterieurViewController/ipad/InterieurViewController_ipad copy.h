//
//  InterieurViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterieurViewController.h"

@interface InterieurViewController_ipad : InterieurViewController<UITextFieldDelegate>

//Data

@property (strong, nonatomic) IBOutletCollection(UISwitch) NSArray * niveauxCarburant;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray * objectsInterieur;

//Outlets

@property (weak, nonatomic) IBOutlet UIView * objectsValeurContainer;

@property (weak, nonatomic) IBOutlet UIView * autresElementContainer;

@property (weak, nonatomic) IBOutlet NOVBlockView *containerView;

@property (weak, nonatomic) IBOutlet NOVButton *validerBtn;

@property (weak, nonatomic) IBOutlet UIButton *addAutresElementsBtn;

@property (weak, nonatomic) IBOutlet UIButton *addObjectValeurBtn;

@property (weak, nonatomic) IBOutlet NOVTextField *kilometrageTF;

@property (weak, nonatomic) IBOutlet NOVTextField *firstObjectValeurTF;

@property (weak, nonatomic) IBOutlet NOVTextField *firstAutreTF;

@property (weak, nonatomic) IBOutlet UILabel *autreLabel;



//Actions

- (IBAction)valider:(id)sender;

- (IBAction)choisirNiveau:(id)sender;

- (IBAction)choisirObject:(id)sender;

- (IBAction)choisirAutreObject:(id)sender;

- (IBAction)choisirObjectValeur:(id)sender;

- (IBAction)addObjectValeurElements:(id)sender;

- (IBAction)addAutresElements:(id)sender;


@end
