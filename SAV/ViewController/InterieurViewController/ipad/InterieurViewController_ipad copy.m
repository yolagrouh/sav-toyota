#import "InterieurViewController_ipad.h"
#import "ExterieurViewController_ipad.h"
#import "ALPValidator.h"

@interface InterieurViewController_ipad (){
    
    
    CGRect initialObjectContainerFrame;
    CGRect initialAutreLabelFrame;
    CGRect initialObjectBtnFrame;
    CGRect initialAddObjectBtnFrame;
    
    
    CGRect initialObjectValeurContainerFrame;
    CGRect initialAddObjectValeurBtnFrame;
    
    
    CGRect initialValiderBtnFrame;
}

@property(nonatomic, strong)NSString * selectedObjectsClient;

@end

@implementation InterieurViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        self.selectedObjectsClient = [NSString string];
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)setUpView{
    
    [super setUpView];
    
    
    initialObjectContainerFrame         = self.autresElementContainer.frame;
    initialAutreLabelFrame              = self.autreLabel.frame;
    initialObjectBtnFrame               = [[self.objectsInterieur lastObject] frame];
    initialAddObjectBtnFrame            = self.addAutresElementsBtn.frame;
    
    
    initialObjectValeurContainerFrame   = self.objectsValeurContainer.frame;
    initialAddObjectValeurBtnFrame      = self.addObjectValeurBtn.frame;
    
    initialValiderBtnFrame              = self.validerBtn.frame;
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.containerView.frame.size.height);
}

#pragma mark --
#pragma mark Actions

- (IBAction)valider:(id)sender {
    
    ALPValidator *validator = [ALPValidator validatorWithType:ALPValidatorTypeNumeric];
    
    [validator validate:self.kilometrageTF.text];
    
    if ([self.kilometrageTF.text length] == 0) {
        
        [self showError:@"Veuillez spécifier le kilométrage"];
        
        return;
    }
    
    BOOL isCarburantSelected = NO;
    
    for (UISwitch * carburantSwitch in self.niveauxCarburant) {
        
        if (carburantSwitch.isOn) {
            
            isCarburantSelected = YES;
            
            break;
            
        }
        
    }
    
    if (!isCarburantSelected) {
        
        [self showError:@"Veuillez spécifier le niveau du carburant"];
        
        return;
    }
    
    ExterieurViewController_ipad * exterieurViewController = [[ExterieurViewController_ipad alloc]initWithNibName:@"ExterieurViewController" bundle:nil];
    
    [self.navigationController pushViewController:exterieurViewController animated:YES];
}

- (IBAction)choisirNiveau:(id)sender {
    
    for (UISwitch * niveauSwitch in self.niveauxCarburant) {
        
        if ([niveauSwitch isEqual:sender]) {
            
            //niveauSwitch.on = !niveauSwitch.on;
            
        }else{
            
            [niveauSwitch setOn:NO animated:YES];
        }
        
    }
    
}

- (IBAction)choisirObject:(id)sender {
    
    [(UIButton *)sender setSelected:![sender isSelected]];
    
    
    
}


- (IBAction)choisirAutreObject:(id)sender {
    
    [(UIButton *)sender setSelected:![sender isSelected]];
    
    self.autresElementContainer.hidden = !self.autresElementContainer.hidden;
    self.addAutresElementsBtn.hidden = !self.addAutresElementsBtn.hidden;
    
    if (!((UIButton *)sender).isSelected) {
        
        //Autres
        self.autresElementContainer.frame   = CGRectMake(initialObjectContainerFrame.origin.x, self.objectsValeurContainer.frame.origin.y + self.objectsValeurContainer.frame.size.height + 15, initialObjectContainerFrame.size.width, self.autresElementContainer.frame.size.height);
        
        self.addAutresElementsBtn.frame     = initialAddObjectBtnFrame;
        
        self.autreLabel.frame               = initialAutreLabelFrame;
        
        self.validerBtn.frame               = CGRectMake(initialValiderBtnFrame.origin.x, self.autresElementContainer.frame.origin.y + self.autresElementContainer.frame.size.height + 25, initialValiderBtnFrame.size.width, initialValiderBtnFrame.size.height);
        
        UIButton * autreCheckElementBtn = [self.objectsInterieur lastObject];
        
        autreCheckElementBtn.frame      = initialObjectBtnFrame;
        
        
        for (UITextField * textField in [self.autresElementContainer subviews]) {
            
            NSMutableArray * tempsArray = [[self.selectedObjectsClient componentsSeparatedByString:@","] mutableCopy];
            
            NSLog(@"Text : %@",textField.text);
            
            if ([tempsArray containsObject:textField.text]) {
                
                [tempsArray removeObject:textField.text];
            }
            
            [tempsArray count] == 0 ? self.selectedObjectsClient = @"": ( self.selectedObjectsClient = [tempsArray componentsJoinedByString:@","]);
            
            if (textField != self.firstAutreTF) {
                
                [textField removeFromSuperview];
            }
            
        }
        
        self.firstAutreTF.text = nil;
        
    }
    
}

- (IBAction)choisirObjectValeur:(id)sender {// Activer Elements Valeur
    
    [(UIButton *)sender setSelected:![sender isSelected]];
    
    self.objectsValeurContainer.hidden = !self.objectsValeurContainer.hidden;
    self.addObjectValeurBtn.hidden = !self.addObjectValeurBtn.hidden;
    
    
    if (!((UIButton *)sender).isSelected) {
        
        self.objectsValeurContainer.frame   = initialObjectValeurContainerFrame;
        self.addObjectValeurBtn.frame       = initialAddObjectValeurBtnFrame;
        
        //Autres
        self.autresElementContainer.frame   = CGRectMake(initialObjectContainerFrame.origin.x, initialObjectContainerFrame.origin.y, initialObjectContainerFrame.size.width, self.autresElementContainer.frame.size.height);
        
        self.addAutresElementsBtn.frame     = initialAddObjectBtnFrame;
        
        self.autreLabel.frame               = initialAutreLabelFrame;
        
        self.validerBtn.frame               = CGRectMake(initialValiderBtnFrame.origin.x, self.autresElementContainer.frame.origin.y + self.autresElementContainer.frame.size.height + 25, initialValiderBtnFrame.size.width, initialValiderBtnFrame.size.height);
        
        UIButton * autreCheckElementBtn = [self.objectsInterieur lastObject];
        
        autreCheckElementBtn.frame      = initialObjectBtnFrame;
        

        for (UITextField * textField in [self.objectsValeurContainer subviews]) {
            
            NSMutableArray * tempsArray = [[self.selectedObjectsClient componentsSeparatedByString:@","] mutableCopy];
            
            NSLog(@"Text : %@",textField.text);
            
            if ([tempsArray containsObject:textField.text]) {
                
                [tempsArray removeObject:textField.text];
            }
            
            [tempsArray count] == 0 ? self.selectedObjectsClient = @"": ( self.selectedObjectsClient = [tempsArray componentsJoinedByString:@","]);
            
            if (textField != self.firstObjectValeurTF) {
                
                [textField removeFromSuperview];
            }
            
        }
        
        self.firstAutreTF.text = nil;
        
    }
}

- (IBAction)addObjectValeurElements:(id)sender { // Add Elements Valeur TextFields
    
    UIButton * autreBtn = [self.objectsInterieur objectAtIndex:[self.objectsInterieur count] -1];
    
    if (!autreBtn.isSelected) {
        
        return;
    }
    
    UIView * lastTF = [[self.objectsValeurContainer subviews] lastObject];
    
    CGRect frame = lastTF.frame;
    
    NOVTextField * textField = [[NOVTextField alloc]initWithFrame:CGRectMake(self.objectsValeurContainer.frame.size.width, frame.origin.y + frame.size.height + 10, frame.size.width, frame.size.height)];
    
    textField.delegate = self;
    
    textField.placeholder = @"SAISIR LE TEXTE";
    
    textField.font = [UIFont fontWithName:@"Arial" size:20];
    
    [self.objectsValeurContainer addSubview:textField];
    
    [textField becomeFirstResponder];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.objectsValeurContainer.frame = CGRectMake(self.objectsValeurContainer.frame.origin.x, self.objectsValeurContainer.frame.origin.y, self.objectsValeurContainer.frame.size.width, self.objectsValeurContainer.frame.size.height + 58);
        
        self.validerBtn.frame = CGRectMake(self.validerBtn.frame.origin.x, self.validerBtn.frame.origin.y + 58, self.validerBtn.frame.size.width, self.validerBtn.frame.size.height );
        
        self.containerView.frame = CGRectMake(self.containerView.frame.origin.x, self.containerView.frame.origin.y, self.containerView.frame.size.width, self.containerView.frame.size.height + 58);
        
        self.autresElementContainer.frame = CGRectMake(self.autresElementContainer.frame.origin.x, self.autresElementContainer.frame.origin.y + 58, self.autresElementContainer.frame.size.width, self.autresElementContainer.frame.size.height );
        
        autreBtn.frame = CGRectMake(autreBtn.frame.origin.x, autreBtn.frame.origin.y + 58, autreBtn.frame.size.width, autreBtn.frame.size.height );
        
        self.autreLabel.frame = CGRectMake(self.autreLabel.frame.origin.x, self.autreLabel.frame.origin.y + 58, self.autreLabel.frame.size.width, self.autreLabel.frame.size.height );
        
        self.addObjectValeurBtn.frame = CGRectMake(self.addObjectValeurBtn.frame.origin.x, self.addObjectValeurBtn.frame.origin.y + 58, self.addObjectValeurBtn.frame.size.width, self.addObjectValeurBtn.frame.size.height );
        
        self.addAutresElementsBtn.frame = CGRectMake(self.addAutresElementsBtn.frame.origin.x, self.addAutresElementsBtn.frame.origin.y + 58, self.addAutresElementsBtn.frame.size.width, self.addAutresElementsBtn.frame.size.height );
        
        textField.frame =  CGRectMake(0, frame.origin.y + frame.size.height + 10, frame.size.width, frame.size.height);
        
    } completion:^(BOOL finished) {
        
        
    }];
    
    
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.containerView.frame.size.height + 20);
    
}

- (IBAction)addAutresElements:(id)sender {
    
    UIButton * autreBtn = [self.objectsInterieur lastObject];
    
    if (!autreBtn.isSelected) {
        
        return;
    }
    
    UIView * lastTF = [[self.autresElementContainer subviews] lastObject];
    
    CGRect frame = lastTF.frame;
    
    NOVTextField * textField = [[NOVTextField alloc]initWithFrame:CGRectMake(self.autresElementContainer.frame.size.width, frame.origin.y + frame.size.height + 10, frame.size.width, frame.size.height)];
    
    textField.delegate = self;
    
    textField.placeholder = @"SAISIR LE TEXTE";
    
    textField.font = [UIFont fontWithName:@"Arial" size:20];
    
    [self.autresElementContainer addSubview:textField];
    
    [textField becomeFirstResponder];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.autresElementContainer.frame = CGRectMake(self.autresElementContainer.frame.origin.x, self.autresElementContainer.frame.origin.y, self.autresElementContainer.frame.size.width, self.autresElementContainer.frame.size.height + 58);
        
        self.validerBtn.frame = CGRectMake(self.validerBtn.frame.origin.x, self.validerBtn.frame.origin.y + 58, self.validerBtn.frame.size.width, self.validerBtn.frame.size.height );
        
        self.containerView.frame = CGRectMake(self.containerView.frame.origin.x, self.containerView.frame.origin.y, self.containerView.frame.size.width, self.containerView.frame.size.height + 58);
        
        self.addAutresElementsBtn.frame = CGRectMake(self.addAutresElementsBtn.frame.origin.x, self.addAutresElementsBtn.frame.origin.y + 58, self.addAutresElementsBtn.frame.size.width, self.addAutresElementsBtn.frame.size.height );
        
        textField.frame =  CGRectMake(0, frame.origin.y + frame.size.height + 10, frame.size.width, frame.size.height);
        
    } completion:^(BOOL finished) {
        
        
    }];
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.containerView.frame.size.height + 20);
    
}

#pragma mark --
#pragma mark TextField


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if ([textField.text length] == 0) {
        
        return;
    }
    
    if ([self.selectedObjectsClient length] == 0) {
        
        return;
        
    }
    
    NSMutableArray * tempsArray = [[self.selectedObjectsClient componentsSeparatedByString:@","] mutableCopy];
    
    if ([tempsArray containsObject:textField.text]) {
        
        [tempsArray removeObject:textField.text];
    }
    
    [tempsArray count] == 0 ? self.selectedObjectsClient = @"": ( self.selectedObjectsClient = [tempsArray componentsJoinedByString:@","]);
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    if ([self.selectedObjectsClient length] == 0) {
        
        self.selectedObjectsClient = textField.text;
        
        return;
    }
    
    self.selectedObjectsClient = [NSString stringWithFormat:@"%@,%@",self.selectedObjectsClient, textField.text];
    
}

-(IBAction)onEditingDidBegin:(NSNotification *) notif{
    
 //  [self.contentSV scrollRectToVisible:[self.contentSV convertRect:self.autresElementContainer.frame fromView:self.containerView] animated:YES];
    
}
-(IBAction)onEditingDidEnd:(NSNotification *) notif{
    
    
}
@end
