//
//  NOVAutreObjetView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/27/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NOVAutreObjetView : UIView


@property (weak, nonatomic) IBOutlet UITextField *field;

@end
