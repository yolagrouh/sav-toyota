//
//  NOVTPDFView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/9/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NOVTPDFView : UIView

@property (weak, nonatomic) IBOutlet UITextField *prixLabel;

@property (weak, nonatomic) IBOutlet UILabel *travailLabel;

@end
