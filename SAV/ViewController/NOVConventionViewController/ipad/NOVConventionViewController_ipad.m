#import "NOVConventionViewController_ipad.h"
#import "NOVSharedData.h"
#import "NOVOPDFView.h"
#import "NOVTPDFView.h"
#import "ScrollViewToPDF.h"
#import "NOVSharedUSer.h"

#define BORDER_COLOR    [UIColor colorWithRed:178.0/255 green:191.0/255 blue:210.0/255 alpha:1]

@interface NOVConventionViewController_ipad ()

@property(nonatomic, strong)UIPrintInteractionController *printController;

@property(nonatomic, strong)NSData * pdfData;

-(void)tourVehicule;
-(void)travaux;
-(void)demandesClient;

@end

@implementation NOVConventionViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	// Custom initialization
        
        self.conventionMode = ConventionModeWrite;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    [self performSelector:@selector(createPDF) withObject:nil afterDelay:1];
    
    NOVSharedData * sharedDate  = [NOVSharedData sharedNOVData];
    
    self.etatVoitureLabel.hidden = !sharedDate.didControlCarosserie;
	// Do any additional setup after loading the view.
    
    [self getInvoice];

}

-(void)getInvoice{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    if([sharedData.jobCard length] > 0){
        
        return;
    }
    // NSString * url = [NSString stringWithFormat:@"%@%@",SELECT_TASKS_WS, sharedData.timeStamp];
    
    NSString * url = [NSString stringWithFormat:@"%@%@",SELECT_TASKS_WS_2, sharedData.timeStamp];
    
    [self doGet:url requestID:0];

}

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    [super updateView:response requestId:requestID];
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    if([[response objectForKey:@"results"] count] > 0){
        
        sharedData.jobCard = [[[response objectForKey:@"results"] objectAtIndex:0] objectForKey:@"INCIDENT_NUMBER"];
        
    }
    NSMutableArray * savedRDV = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:@"SAVED_DATA"]] ;
    
    NSDictionary * dataToSave = [sharedData createArchivableObject];
    
    NSInteger index = -1;
    
    for (id data in savedRDV) {
        
        if ([[data objectForKey:@"TIME_STAMP"] isEqualToString:sharedData.timeStamp]) {
            
            index = [savedRDV indexOfObject:data];
            
            break;
            
        }
        
    }
    
    if (index >= 0) {
        
        [savedRDV replaceObjectAtIndex:index withObject:dataToSave];
        
    }
    
    [Utils saveMutableObject:savedRDV  key:@"SAVED_DATA"];
    
    if([sharedData.jobCard length] > 0){
        
        self.jobCardLabel.text = [NSString stringWithFormat:@"%@ \n%@",sharedData.jobCard,sharedData.timeStamp];
    }
}

-(void)setUpView{
    
    [super setUpView];
    
    [Utils addBorder:self.conventionServiceHeaderView borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.accordClientHeaderView borderColor:BORDER_COLOR width:1];
    
    [Utils addBorder:self.schemaEclateImageView borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.accordClientContainer borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.travauxContainer borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.objetsTrouvesView borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.infosView borderColor:BORDER_COLOR width:1];
    
    //TF
    
    [Utils addBorder:self.nomTF borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.telTF borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.modelTV borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.vinTF borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.matriculeTF borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.kilometrageTF borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.adresseTV borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.anneeTF borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.niveauCarburantTF borderColor:BORDER_COLOR width:1];
    [Utils addBorder:self.clientsInfoView borderColor:BORDER_COLOR width:1];
    
    
    switch (self.conventionMode) {
        case ConventionModeRead:
            
            self.annulerBTn.hidden  = YES;
            //self.imprimerBtn.hidden = YES;
            self.fermerBTn.hidden   = NO;
            
            self.bookmarkBtn.hidden = YES;
            
            break;
        case ConventionModeWrite:
            
            self.annulerBTn.hidden  = NO;
            //self.imprimerBtn.hidden = NO;
            self.fermerBTn.hidden   = YES;
            
            break;
        default:
            break;
    }
    
    self.contentSV.contentSize = CGSizeMake(self.contentSV.frame.size.width, self.contentViewHeightConstraint.constant);
    
    NOVSharedData * sharedData          = [NOVSharedData sharedNOVData];
    
    NOVSharedUSer * sharedUSer = [NOVSharedUSer sharedNOVUser];
    
    self.dateLabel.text                 = [Utils stringFromDate:[NSDate date] dateFormat:@"dd/MM/yyyy HH:mm"];
    
    self.saLabel.text                   =  [NSString stringWithFormat:@"SA : %@", sharedUSer.name ];
    
    self.nomTF.text                     = sharedData.nomClient;
    
    self.conventionNomLabel.text        = sharedData.nomClient;
    
    if (![sharedData.telClient isKindOfClass:[NSNull class]]) {
        
        self.telTF.text                     = sharedData.telClient;
    }
    
    if (![sharedData.adresseClient isKindOfClass:[NSNull class]]) {
        
        self.adresseTV.text                 = sharedData.adresseClient;
    }
    if (![sharedData.modelVehicule isKindOfClass:[NSNull class]]) {
        
        self.modelTV.text                   = sharedData.modelVehicule;
    }
    
    if (![sharedData.immatriculation isKindOfClass:[NSNull class]]) {
        
        self.matriculeTF.text               = sharedData.immatriculation;
    }
    
    if (![sharedData.dateMiseEnCirculation isKindOfClass:[NSNull class]]) {
        
        
        self.anneeTF.text                   = sharedData.dateMiseEnCirculation;
    }
    if (![sharedData.sn isKindOfClass:[NSNull class]]) {
        
        
        self.vinTF.text                     = sharedData.sn;
    }
    if (![sharedData.kilometrage isKindOfClass:[NSNull class]]) {
        
        
        self.kilometrageTF.text             = sharedData.kilometrage;
    }
    if (![sharedData.niveauCarburant isKindOfClass:[NSNull class]]) {
        
        
        self.niveauCarburantTF.text         = sharedData.niveauCarburant;
    }
    
    
    self.livraisonDateTF.text           = [NSString stringWithFormat:@"%@ %@", sharedData.livraisonDate, sharedData.livraisonHeure];

    
    self.schemaEclateImageView.image    = [[UIImage alloc] initWithCGImage: sharedData.schema.CGImage
                                                                     scale: 1.0
                                                               orientation: UIImageOrientationLeft];
    
    [self tourVehicule];
    
    [self travaux];
    
    [self demandesClient];
    
    //JobCard
    
    if([sharedData.jobCard length] > 0){
    
        self.jobCardLabel.text = [NSString stringWithFormat:@"%@ \n%@",sharedData.jobCard,sharedData.timeStamp];
    }
    
}

#pragma mark --
#pragma mark Tour Véhicule 

-(void)tourVehicule{
    
    NSString * allumeCigare = @"Allume cigare";
    NSString * cendrier     = @"Cendrier";
    NSString * cleUSB       = @"Clé USB";
    NSString * tapis        = @"Tapis";
    
    
    NOVSharedData * sharedData          = [NOVSharedData sharedNOVData];
    
    NSMutableArray *objets =  [NSMutableArray arrayWithArray: [sharedData.objectsInterieur componentsSeparatedByString:@","]];
    
    self.allumeCigareBtn.selected    = [objets containsObject:allumeCigare];
    self.cendrierBtn.selected        = [objets containsObject:cendrier];
    self.usbBtn.selected             = [objets containsObject:cleUSB];
    self.tapisBtn.selected           = [objets containsObject:tapis];
    
    [objets removeObjectAtIndex:0];
    [objets removeObject:allumeCigare];
    [objets removeObject:cendrier];
    [objets removeObject:cleUSB];
    [objets removeObject:tapis];
    
    for (NSString * object in objets) {
        
        NSInteger index = [objets indexOfObject:object];
        
        NOVOPDFView * opdfView = [[[NSBundle mainBundle] loadNibNamed:@"NOVOPDFView" owner:nil options:nil] objectAtIndex:0];
        
        opdfView.translatesAutoresizingMaskIntoConstraints = NO;
        
        opdfView.valueTF.text = object;
        
        [Utils addBorder:opdfView.valueTF borderColor:BORDER_COLOR width:1];
        
        [self.autresObjectsView addSubview:opdfView];
        
        [self.autresObjectsView addConstraint:[NSLayoutConstraint constraintWithItem:opdfView
                                                                                 attribute:NSLayoutAttributeWidth
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.autresObjectsView
                                                                                 attribute:NSLayoutAttributeWidth
                                                                                multiplier:1
                                                                                  constant:115]];
        
        [self.autresObjectsView addConstraint:[NSLayoutConstraint constraintWithItem:opdfView
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.autresObjectsView
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                multiplier:1
                                                                                  constant:25]];
        
        [self.autresObjectsView addConstraint:[NSLayoutConstraint constraintWithItem:opdfView
                                                                                 attribute:NSLayoutAttributeLeft
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.autresObjectsView
                                                                                 attribute:NSLayoutAttributeLeft
                                                                                multiplier:1
                                                                                  constant:115 * index ]];
        
        CGFloat top = [objets indexOfObject:object] * 35;
        
        [self.autresObjectsView addConstraint:[NSLayoutConstraint constraintWithItem:opdfView
                                                                                 attribute:NSLayoutAttributeTop
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.autresObjectsView
                                                                                 attribute:NSLayoutAttributeTop
                                                                                multiplier:1
                                                                                  constant:1]];
    }
    
//    self.autresObjectsViewHeightConstraint.constant = [objets count] * 35;
//    
//    self.objectTrouveresViewHeightConstraint.constant =self.objectTrouveresViewHeightConstraint.constant  +[ objets count] * 35;
//    
//    self.contentViewHeightConstraint.constant       = self.contentViewHeightConstraint.constant + self.objectTrouveresViewHeightConstraint.constant;
//    
//    self.contentSV.contentSize = CGSizeMake(self.contentSV.contentSize.width , self.contentViewHeightConstraint.constant);
}

-(void)travaux{
    
    
    NOVSharedData * sharedData          = [NOVSharedData sharedNOVData];
    
    NSMutableArray * travaux =  [NSMutableArray arrayWithArray:sharedData.tousLesTravaux];
    
    NSMutableArray * travauxOUI =  [NSMutableArray array];
    
    for (id travail in travaux) {
        
        if ([[travail objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
            
            [travauxOUI addObject:travail];
            
        }
        
    }
    
    NSMutableArray * travauxLeft = [NSMutableArray array];
    
    NSMutableArray * travauxRight = [NSMutableArray array];
    
    if ([travauxOUI count] % 2 == 0) {
        
        [travauxLeft addObjectsFromArray:[travauxOUI subarrayWithRange:NSMakeRange(0,[travauxOUI count]/2)]];
        
        [travauxRight addObjectsFromArray:[travauxOUI subarrayWithRange:NSMakeRange([travauxOUI count]/2 ,[travauxOUI count]/2)]];
        
    }else{
        if ([travauxOUI count] > 1) {
            
            [travauxLeft addObjectsFromArray:[travauxOUI subarrayWithRange:NSMakeRange(0,[travauxOUI count]/2 +1)]];
            
            [travauxRight addObjectsFromArray:[travauxOUI subarrayWithRange:NSMakeRange([travauxOUI count]/2+1 ,[travauxOUI count]/2)]];
        }else{
            
            [travauxLeft addObjectsFromArray:travauxOUI];
        }
      
        
    }

    CGFloat total = 0;
    
    NSInteger index = -1;
    
    for (id  travail in  travauxLeft) {
        
        if ([[travail objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
            
            index ++;
            
            CGFloat cout = [Utils getNumber:[travail objectForKey:@"PRIX"]] ;
            
            total = total + cout ;
            
            
            NOVTPDFView * tPDFView = [[[NSBundle mainBundle] loadNibNamed:@"NOVTPDFView" owner:nil options:nil] objectAtIndex:0];
            
            tPDFView.translatesAutoresizingMaskIntoConstraints = NO;
            
            tPDFView.travailLabel.text = [sharedData getRequestDescription:[travail objectForKey:@"TRAVAIL"]];
            
            tPDFView.prixLabel.text = [travail objectForKey:@"PRIX"];
            
            [Utils addBorder:tPDFView.prixLabel borderColor:BORDER_COLOR width:1];
            
            [self.tView1 addSubview:tPDFView];
            
            [self.tView1 addConstraint:[NSLayoutConstraint constraintWithItem:tPDFView
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.tView1
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                 multiplier:1
                                                                                   constant:352]];
            
            [self.tView1 addConstraint:[NSLayoutConstraint constraintWithItem:tPDFView
                                                                                  attribute:NSLayoutAttributeHeight
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.tView1
                                                                                  attribute:NSLayoutAttributeHeight
                                                                                 multiplier:1
                                                                                   constant:27]];
            
            [self.tView1 addConstraint:[NSLayoutConstraint constraintWithItem:tPDFView
                                                                                  attribute:NSLayoutAttributeLeft
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.tView1
                                                                                  attribute:NSLayoutAttributeLeft
                                                                                 multiplier:1
                                                                                   constant:1]];
            
            CGFloat top = index * 27;
            
            [self.tView1 addConstraint:[NSLayoutConstraint constraintWithItem:tPDFView
                                                                                  attribute:NSLayoutAttributeTop
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.tView1
                                                                                  attribute:NSLayoutAttributeTop
                                                                                 multiplier:1
                                                                                   constant:top]];

            
        }
    }
    
    NSInteger index2 = -1;
    
    for (id  travail in  travauxRight) {
        
        if ([[travail objectForKey:@"STATUS"] isEqualToString:@"OUI"]) {
            
            index2 ++;
            
            CGFloat cout = [Utils getNumber:[travail objectForKey:@"PRIX"]] ;
            
            total = total + cout ;
            
            
            NOVTPDFView * tPDFView = [[[NSBundle mainBundle] loadNibNamed:@"NOVTPDFView" owner:nil options:nil] objectAtIndex:0];
            
            tPDFView.translatesAutoresizingMaskIntoConstraints = NO;
            
            tPDFView.travailLabel.text =  [sharedData getRequestDescription:[travail objectForKey:@"TRAVAIL"]];
            
            tPDFView.prixLabel.text = [travail objectForKey:@"PRIX"];
            
            [Utils addBorder:tPDFView.prixLabel borderColor:BORDER_COLOR width:1];
            
            [self.tView2 addSubview:tPDFView];
            
            [self.tView2 addConstraint:[NSLayoutConstraint constraintWithItem:tPDFView
                                                                    attribute:NSLayoutAttributeWidth
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.tView2
                                                                    attribute:NSLayoutAttributeWidth
                                                                   multiplier:1
                                                                     constant:352]];
            
            [self.tView2 addConstraint:[NSLayoutConstraint constraintWithItem:tPDFView
                                                                    attribute:NSLayoutAttributeHeight
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.tView2
                                                                    attribute:NSLayoutAttributeHeight
                                                                   multiplier:1
                                                                     constant:27]];
            
            [self.tView2 addConstraint:[NSLayoutConstraint constraintWithItem:tPDFView
                                                                    attribute:NSLayoutAttributeLeft
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.tView2
                                                                    attribute:NSLayoutAttributeLeft
                                                                   multiplier:1
                                                                     constant:1]];
            
            CGFloat top = index2 * 27;
            
            [self.tView2 addConstraint:[NSLayoutConstraint constraintWithItem:tPDFView
                                                                    attribute:NSLayoutAttributeTop
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.tView2
                                                                    attribute:NSLayoutAttributeTop
                                                                   multiplier:1
                                                                     constant:top]];
            
            
        }
    }
    
    self.tView1HeighConstraint.constant = ( index +1)  * 27;
    
    self.tView2HeighConstraint.constant = ( index2 +1)  * 27;

    self.travauxViewHeightConstraint.constant  = self.travauxViewHeightConstraint.constant + self.tView1HeighConstraint.constant;
    
    self.contentViewHeightConstraint.constant       = self.contentViewHeightConstraint.constant +  self.tView1HeighConstraint.constant;
    
   // self.contentSV.contentSize = CGSizeMake(self.contentSV.contentSize.width , self.contentViewHeightConstraint.constant );
    
    NSLog(@"ContentSize %f %f ",self.contentSV.contentSize.width,self.contentSV.contentSize.height);
    
    self.montantEstimatifTF.text = [NSString stringWithFormat:@"%.2f",total ];
}

-(void)demandesClient{
    
    NSString *  operation1 = @"Client en attente";
    NSString *  operation2 = @"Garder les piéces remplacées";
    NSString *  operation3 = @"Passage aspirateur";
    NSString *  operation4 = @"Lavage extérieur";
    
    NOVSharedData * shareData = [NOVSharedData sharedNOVData];
    
    NSArray * demandesClient = [shareData.demandesClient componentsSeparatedByString:@","];
    
    self.clientAttenteBtn.selected   = [demandesClient containsObject:operation1];
    self.piecesBtn.selected          = [demandesClient containsObject:operation2];
    self.lavageIntBtn.selected       = [demandesClient containsObject:operation3];
    self.lavageExtBtn.selected       = [demandesClient containsObject:operation4];
    
}

- (UIImage *) renderScrollViewToImage
{
    UIImage* image = nil;
    
    UIGraphicsBeginImageContext(self.contentSV.contentSize);
    {
        CGPoint savedContentOffset = self.contentSV.contentOffset;
        CGRect savedFrame = self.contentSV.frame;
        
        self.contentSV.contentOffset = CGPointZero;
        self.contentSV.frame = CGRectMake(0, 0, self.contentSV.contentSize.width, self.contentSV.contentSize.height);
        
        [self.contentSV.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        self.contentSV.contentOffset = savedContentOffset;
        self.contentSV.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    if (image != nil) {
        [UIImagePNGRepresentation(image) writeToFile: @"/tmp/test.png" atomically: YES];
       // system("open /tmp/test.png");
    }
    
    return image;
}

-(void)createPDF{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    NSString * conventionName = [NSString stringWithFormat:@"convention_%@_%@.pdf",sharedData.sn, [Utils stringFromDate:[NSDate date] dateFormat:@"dd-MM-yyyy HH-mm"]];
    
    
   //
    
    NSData *pdfData = [ScrollViewToPDF pdfDataOfScrollView:self.contentSV];
    NSString *tmpDirectory = [Utils applicationDocumentsDirectory];
    NSString *path = [tmpDirectory stringByAppendingPathComponent:conventionName];
    [pdfData writeToFile:path atomically:NO];
    
    sharedData.pdfURL = conventionName;
    
    self.pdfData = pdfData;

}

- (IBAction)imprimer:(id)sender {
    
    Class printControllerClass = NSClassFromString(@"UIPrintInteractionController");
    
    if (printControllerClass) {
        
        self.printController = [printControllerClass sharedPrintController];
        
        self.printController.printingItem = self.pdfData;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            if (!completed && error) NSLog(@"Print error: %@", error);
            
        };
        
        [self.printController presentFromRect:self.imprimerBtn.frame inView:self.view
                                animated:YES completionHandler:completionHandler];
    }
    
    //[Utils createPDFfromUIView:self.contentSV saveToDocumentsWithFileName:@"Convention"];
}

- (IBAction)bookmark:(id)sender {
    
    [Utils showAlertView:@"Ce dossier a été ajouté aux favoris avec succès"];
    
    [sender setEnabled:NO];
    
    NSDictionary * dict = [[NOVSharedData sharedNOVData] createArchivableObject];
    
    NSMutableArray * array = [NSMutableArray arrayWithArray:[Utils getSavedMutableObject:BOOKMARK]];
    
    BOOL found = NO;
    
    for (id dossier in array) {
        
        
        if ([[dossier objectForKey:@"SN"] isEqualToString:[dict objectForKey:@"SN"]]) {
            
            found = YES;
            
            break;
            
        }
        
    }
    
    if (!found) {
        
        [array addObject:dict];
        
        [Utils saveMutableObject:array key:BOOKMARK];
    }
    
}

#pragma mark --
#pragma mark ScrollView 

- (UIImage *)screenShotScrollView:(UIScrollView *)scrollView {

    [scrollView setContentOffset:CGPointZero animated:NO];

    [scrollView setShowsHorizontalScrollIndicator:NO];
    [scrollView setShowsVerticalScrollIndicator:NO];
    UIImage *image = nil;
    UIGraphicsBeginImageContextWithOptions(scrollView.contentSize, YES, 1.0);
    {
        CGPoint savedContentOffset = scrollView.contentOffset;
        CGRect savedFrame = scrollView.frame;
        
        scrollView.contentOffset = CGPointZero;
        scrollView.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height);
        
        [scrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        scrollView.contentOffset = savedContentOffset;
        scrollView.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    if (image != nil){
        return image;
    }
    return nil;
}


#pragma mark --
#pragma mark Devide 

void DMArrayDivide(NSArray **slices, NSArray *inArray, NSUInteger parts) {
    if (parts == 0 || parts == 1) {
        *slices = inArray;
        return;
    }
    NSMutableArray *arrays = [NSMutableArray arrayWithCapacity:parts];
    for (int i = 0; i < parts; i++) {
        arrays[i] = @[].mutableCopy;
    }
    long int idx = 0;
    for (id object in inArray) {
        [arrays[idx] addObject:object];
        idx = (idx + 1) % parts;
    }
    *slices = arrays;
}
@end
