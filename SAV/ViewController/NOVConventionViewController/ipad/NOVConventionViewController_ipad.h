//
//  NOVConventionViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVConventionViewController.h"

typedef enum : NSUInteger {
    ConventionModeRead,
    ConventionModeWrite,
} ConventionMode;

@interface NOVConventionViewController_ipad : NOVConventionViewController

@property(nonatomic, assign)ConventionMode conventionMode;

@property (weak, nonatomic) IBOutlet NOVButton *imprimerBtn;
@property (weak, nonatomic) IBOutlet NOVButton *annulerBTn;
@property (weak, nonatomic) IBOutlet NOVButton *fermerBTn;


@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UILabel *saLabel;

@property (weak, nonatomic) IBOutlet UILabel *etatVoitureLabel;

@property (weak, nonatomic) IBOutlet UITextField *nomTF;
@property (weak, nonatomic) IBOutlet UITextField *telTF;
@property (weak, nonatomic) IBOutlet UITextView *adresseTV;
@property (weak, nonatomic) IBOutlet UITextField *modelTV;
@property (weak, nonatomic) IBOutlet UITextField *matriculeTF;
@property (weak, nonatomic) IBOutlet UITextField *vinTF;
@property (weak, nonatomic) IBOutlet UITextField *kilometrageTF;
@property (weak, nonatomic) IBOutlet UITextField *niveauCarburantTF;

@property (weak, nonatomic) IBOutlet UILabel *conventionNomLabel;


@property (weak, nonatomic) IBOutlet UITextField *anneeTF;
@property (weak, nonatomic) IBOutlet UIImageView *schemaEclateImageView;

@property (weak, nonatomic) IBOutlet UITextField *montantEstimatifTF;
@property (weak, nonatomic) IBOutlet UITextField *livraisonDateTF;

//Views

@property (weak, nonatomic) IBOutlet UIView *allumeCigareView;
@property (weak, nonatomic) IBOutlet UIButton *allumeCigareBtn;

@property (weak, nonatomic) IBOutlet UIView *cendrierView;
@property (weak, nonatomic) IBOutlet UIButton *cendrierBtn;

@property (weak, nonatomic) IBOutlet UIView *usbView;
@property (weak, nonatomic) IBOutlet UIButton *usbBtn;

@property (weak, nonatomic) IBOutlet UIView *tapisView;
@property (weak, nonatomic) IBOutlet UIButton *tapisBtn;

//
@property (weak, nonatomic) IBOutlet UILabel *jobCardLabel;

@property (weak, nonatomic) IBOutlet UIView *travauxContainer;

@property (weak, nonatomic) IBOutlet UIView *accordClientContainer;


@property (weak, nonatomic) IBOutlet UIView *autresObjectsView;

@property (weak, nonatomic) IBOutlet UIView * tView1;
@property (weak, nonatomic) IBOutlet UIView * tView2;

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *objetsTrouvesView;

@property (weak, nonatomic) IBOutlet UIView *infosView;


@property (weak, nonatomic) IBOutlet UIView *accordClientHeaderView;



@property (weak, nonatomic) IBOutlet UIView *conventionServiceHeaderView;
@property (weak, nonatomic) IBOutlet UIButton *bookmarkBtn;
@property (weak, nonatomic) IBOutlet UIView *clientsInfoView;

//Demandes Client

@property (weak, nonatomic) IBOutlet UIView *clientAttenteView;
@property (weak, nonatomic) IBOutlet UIButton *clientAttenteBtn;
@property (weak, nonatomic) IBOutlet UIView *piecesView;
@property (weak, nonatomic) IBOutlet UIButton *piecesBtn;
@property (weak, nonatomic) IBOutlet UIView *lavageIntView;
@property (weak, nonatomic) IBOutlet UIButton *lavageIntBtn;
@property (weak, nonatomic) IBOutlet UIView *lavageExtView;
@property (weak, nonatomic) IBOutlet UIButton *lavageExtBtn;


//Constraints

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tView1HeighConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tView2HeighConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *travauxViewHeightConstraint;




- (IBAction)imprimer:(id)sender;


- (IBAction)annuler:(id)sender;

- (IBAction)bookmark:(id)sender;

-(void)createPDF;

void DMArrayDivide(NSArray **slices, NSArray *inArray, NSUInteger parts) ;

@end
