//
//  NOVTowOptionsView.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/5/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVTowOptionsView.h"

@implementation NOVTowOptionsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.optionSwitch.layer.cornerRadius = self.optionSwitch.frame.size.height/2;
        
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)valider:(id)sender {
    
    [self.delegate nOVTowOptionsView:self sender:self.sender selectedOption:self.optionSwitch.isOn];
    
    [self removeFromSuperview];
    
}
@end
