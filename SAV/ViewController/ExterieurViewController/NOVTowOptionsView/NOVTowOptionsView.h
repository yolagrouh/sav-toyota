//
//  NOVTowOptionsView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/5/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVTaggedButton.h"

@class NOVTowOptionsView;

@protocol NOVTowOptionsViewDelegate <NSObject>

@required

-(void)nOVTowOptionsView:(NOVTowOptionsView *) nOVTowOptionsView  sender:(NOVTaggedButton * )sender selectedOption:(BOOL) option;

@end

@interface NOVTowOptionsView : UIView

@property (weak, nonatomic) id<NOVTowOptionsViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UISwitch *optionSwitch;

@property (weak, nonatomic) NOVTaggedButton * sender;



- (IBAction)valider:(id)sender;

@end
