#import "ExterieurViewController_ipad.h"
#import "TSuggeresViewController_ipad.h"

#define CAROSSERIE_IMAGE_TAG            1000
#define OTHER_IMAGE_TAG                 1001

#define PNEU_AD_IMAGE_TAG               1002
#define PNEU_AG_IMAGE_TAG               1003
#define PNEU_DD_IMAGE_TAG               1004
#define PNEU_DG_IMAGE_TAG               1005


#define BATTERIE_IMAGE_TAG              1006

@interface ExterieurViewController_ipad (){
    
    NSInteger okOptionsCounter;
    
}

@property(nonatomic, strong)NSMutableArray * reparations;

@property(nonatomic, strong)NSMutableArray * travauxCarosserie;

@property(nonatomic, strong)NSMutableArray * carosseriesImages;


@end

@implementation ExterieurViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        self.reparations = [NSMutableArray array];
        
        okOptionsCounter = 0;
        
        self.travauxCarosserie = [NSMutableArray array];
        
        self.carosseriesImages = [NSMutableArray array];
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)setUpView{
    
    [super setUpView];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(-65, -60);
    
    transform = CGAffineTransformRotate(transform, - 3.14/2);
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    sharedData.didControlCarosserie = NO;
    
    
   // self.etatVoitureLabel.transform = transform;
    
}

#pragma mark --
#pragma mark Actions

- (IBAction)valider:(id)sender {
    
    
    [self.view resignFirstResponder];
    
//    if ([self.batterieTF.text length] == 0 || [self.roueADTF.text length] == 0 ||  [self.roueAGTF.text length] == 0 ||  [self.roueDDTF.text length] == 0 || [self.roueDGTF.text length] == 0 || okOptionsCounter < 16) {
//        
//       [self showError:@"Veuillez renseigner tous les champs avant de continuer"];
//        
//        return;
//        
//    }
    
    
    for (NOVTaggedButton * taggedBtn in self.importantElements) {
        
        if (!taggedBtn.isActivated) {
            

        }
        
    }
    
    [self showValidation];
    
}

- (IBAction)selectRoueAG:(id)sender{

    if (self.roueAGTF.hidden == YES) {
        
        self.roueAGTF.hidden = NO;
        
    }else{
        
        self.roueAGTF.hidden = YES;
        self.roueAGTF.text = nil;
        
    }

}
- (IBAction)selectRoueAD:(id)sender{
    
    
    if (self.roueADTF.hidden == YES) {
        
        self.roueADTF.hidden = NO;
        
    }else{
        
        self.roueADTF.hidden = YES;
        self.roueADTF.text = nil;
        
    }
    
}
- (IBAction)selectRoueDG:(id)sender{
    
    if (self.roueDGTF.hidden == YES) {
        
        self.roueDGTF.hidden = NO;
        
    }else{
        
        self.roueDGTF.hidden = YES;
        self.roueDGTF.text = nil;
        
    }
}
- (IBAction)selectRoueDD:(id)sender{
    
    if (self.roueDDTF.hidden == YES) {
        
        self.roueDDTF.hidden = NO;
        
    }else{
        
        self.roueDDTF.hidden = YES;
        self.roueDDTF.text = nil;
        
    }
    
}


- (IBAction)selectBatterie:(id)sender{
    
    if (self.batterieTF.hidden == YES) {
        
        self.batterieTF.hidden = NO;
        
    }else{
        
        self.batterieTF.hidden = YES;
        self.batterieTF.text = nil;
        
    }
    
    
}

- (IBAction)showMetalOptions:(id)sender forEvent:(UIEvent*)event{
    
    UIView *button = (UIView *)sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    CGPoint location = [touch locationInView:button];
    
    id value = [sender valueForKey:@"content"];
    
    NOVOptionsView * optionsView = [[[NSBundle mainBundle]loadNibNamed:@"NOVOptionsView" owner:nil options:nil]objectAtIndex:0];
    
    optionsView.tag = CAROSSERIE_IMAGE_TAG;
    
    optionsView.frame = CGRectMake([sender center].x - optionsView.frame.size.width/2, [sender center].y - optionsView.frame.size.height/2, optionsView.frame.size.width, optionsView.frame.size.height);
    
    optionsView.delegate = self;
    
    optionsView.touchLocation =location;
    
    optionsView.sender = sender;
    
    [self.view addSubview:optionsView];
    
}

- (IBAction)showOptiquesOptions:(id)sender forEvent:(UIEvent*)event{
    
    UIView *button = (UIView *)sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    CGPoint location = [touch locationInView:button];
    
    id value = [sender valueForKey:@"content"];
    
    NOVOptionsView * optionsView = [[[NSBundle mainBundle]loadNibNamed:@"NOVOptionsViewOptiques" owner:nil options:nil]objectAtIndex:0];
    
    optionsView.tag = OTHER_IMAGE_TAG;
    
    optionsView.frame = CGRectMake([sender center].x - optionsView.frame.size.width/2, [sender center].y - optionsView.frame.size.height/2, optionsView.frame.size.width, optionsView.frame.size.height);
    
    optionsView.delegate = self;
    
    optionsView.touchLocation =location;
    
    optionsView.sender = sender;
    
    [self.view addSubview:optionsView];
    
}

- (IBAction)showMetalOptions:(id)sender{
    
    id value = [sender valueForKey:@"content"];
    
    NOVOptionsView * optionsView = [[[NSBundle mainBundle]loadNibNamed:@"NOVOptionsView" owner:nil options:nil]objectAtIndex:0];
    
    optionsView.frame = CGRectMake([sender center].x - optionsView.frame.size.width/2, [sender center].y - optionsView.frame.size.height/2, optionsView.frame.size.width, optionsView.frame.size.height);
    
    optionsView.delegate = self;
    
    optionsView.sender = sender;
    
    [self.view addSubview:optionsView];
    
}

- (IBAction)showVitreOptions:(id)sender{
    
    NOVTowOptionsView * optionsView = [[[NSBundle mainBundle]loadNibNamed:@"NOVTowOptionsView" owner:nil options:nil]objectAtIndex:0];
    
    optionsView.frame = CGRectMake([sender center].x - optionsView.frame.size.width/2, [sender center].y - optionsView.frame.size.height/2, optionsView.frame.size.width, optionsView.frame.size.height);
    
    optionsView.delegate = self;
    
    optionsView.sender = sender;
    
    optionsView.optionSwitch.layer.cornerRadius = optionsView.optionSwitch.frame.size.height/2;
    
    [self.view addSubview:optionsView];
    
    
}



- (IBAction)selectOutils:(id)sender{
    
    NOVTaggedButton * taggedBtn = (NOVTaggedButton *)sender;
    
    taggedBtn.selected = !taggedBtn.selected;
    
    taggedBtn.isActivated = !taggedBtn.isActivated;
}

- (IBAction)selectAutresReparations:(id)sender{
    
    
    NOVTaggedButton * button = (NOVTaggedButton *)sender;
    
    button.selected = !button.selected;
    
    button.isActivated = !button.isActivated;
    
    if (![self.reparations containsObject:button.content]) {
        
        
        [self.reparations addObject:button.content];
    }else{
        
        [self.reparations removeObject:button.content];
        
    }

}

- (IBAction)etatCarosserie:(id)sender {
    
    UIButton * button = (UIButton *)sender;
    
    button.selected = !button.selected;
    
    for (UIButton * element in self.carosserieElements) {
        
        element.enabled = !button.isSelected;
        
    }
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    sharedData.didControlCarosserie = [sender isSelected];
    
}

#pragma mark --
#pragma mark Options Delegate

-(void)nOVOptionsView:(NOVOptionsView *)nOVOptionsView sender:(NOVTaggedButton *)sender selectedOption:(NSInteger)option touchLocation:(CGPoint)touchLocation{
    
    NSLog(@"Content %@", sender.content);
    
    NSString * typeTravail;
    
    sender.isActivated = YES;
    
    //sender.enabled = NO;
    
    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithFrame:CGRectMake(touchLocation.x + [sender frame].origin.x - 35/2,touchLocation.y + [sender frame].origin.y - 35/2, 35, 35)];
    
    optionImage.delegate = self;
    
    optionImage.tag = nOVOptionsView.tag;
    
    switch (option) {
        case 0:
            
            typeTravail = @"Réparation";
            
            optionImage.image = [UIImage imageNamed:@"Boutons_123_03.png"];
            
            break;
        case 1:
            
            typeTravail = @"Réparation";
            
            optionImage.image = [UIImage imageNamed:@"Boutons_123_06.png"];
            
            break;
        case 2:
            
            typeTravail = @"Remplacement";
            
            optionImage.image = [UIImage imageNamed:@"button_ko.png"];
            
            break;
            
        default:
            break;
    }
    
    
    if ([typeTravail isEqualToString:@"Réparation"] && ![self.travauxCarosserie containsObject:[NSString stringWithFormat:@"%@ %@",typeTravail , sender.content]]) {
        
        NSString * content= [NSString stringWithFormat:@"%@ %@",typeTravail , sender.content];
        
        optionImage.content = content;
        
        if (![Utils checkString:sender.content contains:@"Optique AV"]) {
            
            [self.travauxCarosserie addObject:content];
            
            
            if (![self.reparations containsObject:@"Travaux carrosserie"]) {
                
                [self.reparations addObject:@"Travaux carrosserie"];
            }
            
        }else{
            
             [self.reparations addObject:[NSString stringWithFormat:@"Rénovation %@", sender.content]];
            
        }
        
        
    }
    
    
    [self.schemaView addSubview:optionImage];
    
}

-(void)nOVTowOptionsView:(NOVTowOptionsView *)nOVTowOptionsView sender:(NOVTaggedButton *)sender selectedOption:(BOOL)option{
    
    okOptionsCounter ++;

    sender.isActivated = YES;
    
    sender.enabled = NO;
    
    if (!option) {
        
        NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"Part3_03.jpg"]];
        
        optionImage.frame = CGRectMake([sender center].x - 35/2, [sender center].y - 35/2, 35, 35);
        
        optionImage.delegate = self;
        
        optionImage.tag = OTHER_IMAGE_TAG;
        
        optionImage.sender = sender;
        
        [self.schemaView addSubview:optionImage];
        
       // [self.reparations addObject:[NSString stringWithFormat:@"Remplacement %@",sender.content]];
        
        return;
    }
    
    
    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"button_ko.png"]];
    
    optionImage.frame = CGRectMake([sender center].x - 35/2, [sender center].y - 35/2, 35, 35);
    
    optionImage.delegate = self;
    
    optionImage.tag = OTHER_IMAGE_TAG;
    
    optionImage.sender = sender;
    
    [self.schemaView addSubview:optionImage];
    
    [self.schemaView bringSubviewToFront:optionImage];
    
    if ([sender.content isEqualToString:@"Essuie-glace AV"] ||[sender.content isEqualToString:@"Essuie-glace AR"] ) {
        
         [self.reparations addObject:[NSString stringWithFormat:@"Remplacement %@",sender.content]];
        
    }
    
   
    
}

#pragma mark --
#pragma mark TextField Delegate 

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (![Utils isNumber:string ]) {
        
        return NO;
        
    }
    
    if ([textField.text length] == 0 && [string isEqualToString:@"."])  {
        
        return  NO;
    }
    
    if ([textField.text length] > 0 && [Utils checkString:textField.text  contains:@"."] && [string isEqualToString:@"."])  {
        
        return  NO;
    }
    
    
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [super textFieldDidEndEditing:textField];
    
    if ([textField.text length] == 0) {
        
        return;
    }
    
    if (textField.tag == 0 || textField.tag == 1 ||textField.tag == 2 || textField.tag == 3 ) {
        
        if ([Utils  getNumber:textField.text] >= 5 || [Utils  getNumber:textField.text] <=1 ) {
            
            [Utils showAlertView:@"La valeur du champs pneu doit être comprise entre 1 et 5"];
            
            textField.text = nil;
            
            return;
            
        }
    }else if (textField.tag == 4){
        
        if ([Utils  getNumber:textField.text] < 0 || [Utils  getNumber:textField.text]> 100 ) {
            
            [Utils showAlertView:@"La valeur du champs batterie doit être comprise entre 0 et 100"];
            
            textField.text = nil;
            
            return;
            
        }
    }
    
    
    NSString * element;
    
    float fieldValue = [Utils getNumber:textField.text] ;

    if (textField.tag == 0 || textField.tag == 1 ||textField.tag == 2 || textField.tag == 3 ) {
        
       
        
        switch (textField.tag) {
            case 0:
                
                 [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", self.roueADBtn.content]];
                
                break;
            case 1:
                
                [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", self.roueAGBtn.content]];
                
                break;
            case 2:
                
                [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", self.roueDDBtn.content]];
                
                break;
            case 3:
                
                [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", self.roueDGBtn.content]];
                
                break;
            default:
                break;
        }
        
        if (fieldValue > [Utils getNumber:@"2"]) {
            
            switch (textField.tag) {
                    
                case 0 :
                {
                    
                    for (UIView * imageView in [self.schemaView subviews]) {
                        
                        
                        if (imageView.tag == PNEU_AD_IMAGE_TAG) {
                            
                            [imageView removeFromSuperview];
                            
                            break;
                            
                        }
                    }
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"Part3_03.jpg"]];
                    
                    optionImage.tag = PNEU_AD_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.roueADBtn;
                    
                    optionImage.frame = CGRectMake([self.roueADBtn center].x - 35/2, [self.roueADBtn center].y - 35/2, 35, 35);
                    
                    [self.view addSubview:optionImage];
                    
                    self.roueADBtn.isActivated = YES;
                    
                }
                    break;
                case 1 :
                {
                    
                    for (UIView * imageView in [self.schemaView subviews]) {
                        
                        
                        if (imageView.tag == PNEU_AG_IMAGE_TAG) {
                            
                            [imageView removeFromSuperview];
                            
                            break;
                            
                        }
                    }
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"Part3_03.jpg"]];
                    
                    optionImage.tag = PNEU_AG_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.roueAGBtn;
                    
                    optionImage.frame = CGRectMake([self.roueAGBtn center].x - 35/2, [self.roueAGBtn center].y - 35/2, 35, 35);
                    
                    [self.schemaView addSubview:optionImage];
                    
                    self.roueAGBtn.isActivated = YES;
                    
                }
                    break;
                    
                case 2 :
                {
                    
                    for (UIView * imageView in [self.schemaView subviews]) {
                        
                        
                        if (imageView.tag == PNEU_DD_IMAGE_TAG) {
                            
                            [imageView removeFromSuperview];
                            
                            break;
                            
                        }
                    }
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"Part3_03.jpg"]];
                    
                    optionImage.tag = PNEU_DD_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.roueDDBtn;
                    
                    optionImage.frame = CGRectMake([self.roueDDBtn center].x - 35/2, [self.roueDDBtn center].y - 35/2, 35, 35);
                    
                    [self.schemaView addSubview:optionImage];
                    
                    self.roueDDBtn.isActivated = YES;
                    
                }
                    break;
                    
                    
                case 3 :
                {
                    
                    for (UIView * imageView in [self.schemaView subviews]) {
                        
                        
                        if (imageView.tag == PNEU_DG_IMAGE_TAG) {
                            
                            [imageView removeFromSuperview];
                            
                            break;
                            
                        }
                    }
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"Part3_03.jpg"]];
                    
                    optionImage.tag = PNEU_DG_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.roueDGBtn;
                    
                    optionImage.frame = CGRectMake([self.roueDGBtn center].x - 35/2, [self.roueDGBtn center].y - 35/2, 35, 35);
                    
                    [self.schemaView addSubview:optionImage];
                    
                    self.roueDGBtn.isActivated = YES;
                    
                }
                    break;
                case 4:
                {
                    
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"Part3_03.jpg"]];
                    
                    optionImage.tag = BATTERIE_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.batterieBtn;
                    
                    optionImage.frame = CGRectMake([self.batterieBtn center].x - 35/2, [self.batterieBtn center].y - 35/2, 35, 35);
                    
                    [self.schemaView addSubview:optionImage];
                    
                    self.batterieBtn.isActivated = YES;
                }
                    
                    break;
                    
                default:
                    break;
            }
            
        }else if (fieldValue >  [Utils getNumber:@"1.6"] && fieldValue <= [Utils getNumber:@"2"]){
            
                switch (textField.tag) {
                        
                    case 0 :
                    {
                        
                        for (UIView * imageView in [self.schemaView subviews]) {
                            
                            
                            if (imageView.tag == PNEU_AD_IMAGE_TAG) {
                                
                                [imageView removeFromSuperview];
                                
                                break;
                                
                            }
                        }
                        
                        NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"pneu_warning.jpg"]];
                        
                        optionImage.tag = PNEU_AD_IMAGE_TAG;
                        
                        optionImage.delegate = self;
                        
                        optionImage.sender = self.roueADBtn;
                        
                        optionImage.frame = CGRectMake([self.roueADBtn center].x - 35/2, [self.roueADBtn center].y - 35/2, 35, 35);
                        
                        [self.view addSubview:optionImage];
                        
                        self.roueADBtn.isActivated = YES;
                        
                    }
                        break;
                    case 1 :
                    {
                        
                        for (UIView * imageView in [self.schemaView subviews]) {
                            
                            
                            if (imageView.tag == PNEU_AG_IMAGE_TAG) {
                                
                                [imageView removeFromSuperview];
                                
                                break;
                                
                            }
                        }
                        
                        NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"pneu_warning.jpg"]];
                        
                        optionImage.tag = PNEU_AG_IMAGE_TAG;
                        
                        optionImage.delegate = self;
                        
                        optionImage.sender = self.roueAGBtn;
                        
                        optionImage.frame = CGRectMake([self.roueAGBtn center].x - 35/2, [self.roueAGBtn center].y - 35/2, 35, 35);
                        
                        [self.schemaView addSubview:optionImage];
                        
                        self.roueAGBtn.isActivated = YES;
                        
                    }
                        break;
                        
                    case 2 :
                    {
                        
                        for (UIView * imageView in [self.schemaView subviews]) {
                            
                            
                            if (imageView.tag == PNEU_DD_IMAGE_TAG) {
                                
                                [imageView removeFromSuperview];
                                
                                break;
                                
                            }
                        }
                        
                        NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"pneu_warning.jpg"]];
                        
                        optionImage.tag = PNEU_DD_IMAGE_TAG;
                        
                        optionImage.delegate = self;
                        
                        optionImage.sender = self.roueDDBtn;
                        
                        optionImage.frame = CGRectMake([self.roueDDBtn center].x - 35/2, [self.roueDDBtn center].y - 35/2, 35, 35);
                        
                        [self.schemaView addSubview:optionImage];
                        
                        self.roueDDBtn.isActivated = YES;
                        
                    }
                        break;
                        
                        
                    case 3 :
                    {
                        
                        for (UIView * imageView in [self.schemaView subviews]) {
                            
                            
                            if (imageView.tag == PNEU_DG_IMAGE_TAG) {
                                
                                [imageView removeFromSuperview];
                                
                                break;
                                
                            }
                        }
                        
                        NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"pneu_warning.jpg"]];
                        
                        optionImage.tag = PNEU_DG_IMAGE_TAG;
                        
                        optionImage.delegate = self;
                        
                        optionImage.sender = self.roueDGBtn;
                        
                        optionImage.frame = CGRectMake([self.roueDGBtn center].x - 35/2, [self.roueDGBtn center].y - 35/2, 35, 35);
                        
                        [self.schemaView addSubview:optionImage];
                        
                        self.roueDGBtn.isActivated = YES;
                        
                    }
                        break;
                    case 4:
                    {
                        
                        NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"pneu_warning.jpg"]];
                        
                        optionImage.tag = BATTERIE_IMAGE_TAG;
                        
                        optionImage.delegate = self;
                        
                        optionImage.sender = self.batterieBtn;
                        
                        optionImage.frame = CGRectMake([self.batterieBtn center].x - 35/2, [self.batterieBtn center].y - 35/2, 35, 35);
                        
                        [self.schemaView addSubview:optionImage];
                        
                        self.batterieBtn.isActivated = YES;
                    }
                        
                        
                        break;
                        
                    default:
                        break;
                }
            
            
        }else if (fieldValue <= [Utils getNumber:@"1.6"]){
            
            switch (textField.tag) {
                    
                case 0 :
                {
                    
                    for (UIView * imageView in [self.schemaView subviews]) {
                        
                        
                        if (imageView.tag == PNEU_AD_IMAGE_TAG) {
                            
                            [imageView removeFromSuperview];
                            
                            break;
                            
                        }
                    }
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"button_ko.png"]];
                    
                    optionImage.tag = PNEU_AD_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.roueADBtn;
                    
                    optionImage.frame = CGRectMake([self.roueADBtn center].x - 35/2, [self.roueADBtn center].y - 35/2, 35, 35);
                    
                    [self.view addSubview:optionImage];
                    
                    element = self.roueADBtn.content;
                    
                    self.roueADBtn.isActivated = YES;
                    
                }
                    break;
                case 1 :
                {
                    
                    for (UIView * imageView in [self.schemaView subviews]) {
                        
                        
                        if (imageView.tag == PNEU_AG_IMAGE_TAG) {
                            
                            [imageView removeFromSuperview];
                            
                            break;
                            
                        }
                    }
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"button_ko.png"]];
                    
                    optionImage.tag = PNEU_AG_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.roueAGBtn;
                    
                    optionImage.frame = CGRectMake([self.roueAGBtn center].x - 35/2, [self.roueAGBtn center].y - 35/2, 35, 35);
                    
                    [self.schemaView addSubview:optionImage];
                    
                    element = self.roueAGBtn.content;
                    
                    self.roueAGBtn.isActivated = YES;
                    
                }
                    break;
                    
                case 2 :
                {
                    
                    for (UIView * imageView in [self.schemaView subviews]) {
                        
                        
                        if (imageView.tag == PNEU_DD_IMAGE_TAG) {
                            
                            [imageView removeFromSuperview];
                            
                            break;
                            
                        }
                    }
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"button_ko.png"]];
                    
                    optionImage.tag = PNEU_DD_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.roueDDBtn;
                    
                    optionImage.frame = CGRectMake([self.roueDDBtn center].x - 35/2, [self.roueDDBtn center].y - 35/2, 35, 35);
                    
                    [self.schemaView addSubview:optionImage];
                    
                    element = self.roueDDBtn.content;
                    
                    self.roueDDBtn.isActivated = YES;
                    
                }
                    break;
                    
                    
                case 3 :
                {
                    
                    for (UIView * imageView in [self.schemaView subviews]) {
                        
                        
                        if (imageView.tag == PNEU_DG_IMAGE_TAG) {
                            
                            [imageView removeFromSuperview];
                            
                            break;
                            
                        }
                    }
                    
                    NOVCarosserieImageView * optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"button_ko.png"]];
                    
                    optionImage.tag = PNEU_DG_IMAGE_TAG;
                    
                    optionImage.delegate = self;
                    
                    optionImage.sender = self.roueDGBtn;
                    
                    optionImage.frame = CGRectMake([self.roueDGBtn center].x - 35/2, [self.roueDGBtn center].y - 35/2, 35, 35);
                    
                    [self.schemaView addSubview:optionImage];
                    
                    element = self.roueDGBtn.content;
                    
                    self.roueDGBtn.isActivated = YES;
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }

    }else if (textField.tag == 4){
        
        NOVCarosserieImageView * optionImage;
        
        for (UIView * imageView in [self.schemaView subviews]) {
            
            
            if (imageView.tag == BATTERIE_IMAGE_TAG) {
                
                [imageView removeFromSuperview];
                
                break;
                
            }
        }
        
        if(fieldValue  < 60 ){
            
            optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"button_ko.png"]];
            
            element = self.batterieBtn.content;
            
            self.batterieBtn.isActivated = YES;
            
        }else if (fieldValue >= 60 &&  fieldValue <= 80){
            
            [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", self.batterieBtn.content]];
            
            optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"batterie_warning.jpg"]];
            
        }else if (fieldValue > 80 ){
            
            optionImage =  [[NOVCarosserieImageView alloc]initWithImage:[UIImage imageNamed:@"Part3_03.jpg"]];
            
            [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", self.batterieBtn.content]];
        }
        
        
        optionImage.frame = CGRectMake([self.batterieBtn center].x - 35/2, [self.batterieBtn center].y - 35/2, 35, 35);
        
        optionImage.delegate = self;
        
        optionImage.tag      = BATTERIE_IMAGE_TAG;
        
        optionImage.sender = self.batterieBtn;
        
        [self.schemaView addSubview:optionImage];
        
        self.batterieBtn.isActivated = YES;
    }

    
    if ([element length] > 0)  {
        
        if ([self.reparations containsObject:[NSString stringWithFormat:@"Remplacement %@",element]]) {
            
            return;
        }
        
        [self.reparations addObject:[NSString stringWithFormat:@"Remplacement %@",element]];
        
    }
    
    
}

#pragma mark --
#pragma mark Validation

-(void)showNext{
    
    NOVSharedData * sharedData = [NOVSharedData sharedNOVData];
    
    sharedData.schema  = [Utils imageWithView:self.schemaView];
    
    TSuggeresViewController_ipad * tSuggeresViewController = [[TSuggeresViewController_ipad alloc]initWithNibName:@"TSuggeresViewController" bundle:nil];
    
    tSuggeresViewController.travaux = [NSMutableArray arrayWithArray:self.reparations];
    
    tSuggeresViewController.travauxCarosserie = [NSMutableArray arrayWithArray:self.travauxCarosserie];
    
    [self.navigationController pushViewController:tSuggeresViewController animated:YES];
    
}

#pragma mark --
#pragma mark CarosserieImages

-(void)nOVCarosserieImageView:(NOVCarosserieImageView *)nOVCarosserieImageView remove:(NSString *)content{
    
    switch (nOVCarosserieImageView.tag) {
        case CAROSSERIE_IMAGE_TAG:
            
            [self.travauxCarosserie removeObject:content];
            
            if ([self.travauxCarosserie count] == 0) {
                
                [self.reparations removeObject:@"Travaux carrosserie"];
                
            }
            
            break;
            
        case OTHER_IMAGE_TAG:
            
            nOVCarosserieImageView.sender.isActivated = NO;
            
            nOVCarosserieImageView.sender.enabled = YES;
            
            [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", nOVCarosserieImageView.sender.content]];
            
            break;
        case PNEU_AD_IMAGE_TAG : case PNEU_AG_IMAGE_TAG: case PNEU_DD_IMAGE_TAG: case PNEU_DG_IMAGE_TAG:
            
            nOVCarosserieImageView.sender.isActivated = NO;
            
            nOVCarosserieImageView.sender.enabled = YES;
            
            [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", nOVCarosserieImageView.sender.content]];
            
            if ([nOVCarosserieImageView.sender isEqual:self.roueADBtn]) {
                
                [self.roueADTF becomeFirstResponder];
                
                self.roueADTF.text = nil;
                
            }else if ([nOVCarosserieImageView.sender isEqual:self.roueAGBtn]) {
                
                [self.roueAGTF becomeFirstResponder];
                
                self.roueAGTF.text = nil;
                
            }else if ([nOVCarosserieImageView.sender isEqual:self.roueDDBtn]) {
                
                [self.roueDDTF becomeFirstResponder];
                
                self.roueDDTF.text = nil;
                
            }else if ([nOVCarosserieImageView.sender isEqual:self.roueDGBtn]) {
                
                [self.roueDGTF becomeFirstResponder];
                
                self.roueDGTF.text = nil;
                
            }
            
            break;
        case BATTERIE_IMAGE_TAG:
            
            nOVCarosserieImageView.sender.isActivated = NO;
            
            nOVCarosserieImageView.sender.enabled = YES;
            
            [self.reparations removeObject:[NSString stringWithFormat:@"Remplacement %@", nOVCarosserieImageView.sender.content]];
            
            [self.batterieTF becomeFirstResponder];
            
            break;
            
        default:
            break;
    }
    
}

@end
