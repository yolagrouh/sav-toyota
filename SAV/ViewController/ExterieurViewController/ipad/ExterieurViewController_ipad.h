//
//  ExterieurViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExterieurViewController.h"
#import "NOVOptionsView.h"
#import "NOVTowOptionsView.h"
#import "NOVTaggedButton.h"
#import "NOVCarosserieImageView.h"

@interface ExterieurViewController_ipad : ExterieurViewController<NOVOptionsViewDelegate, NOVTowOptionsViewDelegate, UITextFieldDelegate, NOVCarosserieImageViewDelegate>

//Outlets

//elemts voiture

@property (strong, nonatomic) IBOutletCollection(NOVTaggedButton) NSArray *carosserieElements;

@property (strong, nonatomic) IBOutletCollection(NOVTaggedButton) NSArray * importantElements;


@property (weak, nonatomic) IBOutlet UILabel *etatVoitureLabel;


@property (weak, nonatomic) IBOutlet UITextField *roueADTF;
@property (weak, nonatomic) IBOutlet UITextField *roueAGTF;
@property (weak, nonatomic) IBOutlet UITextField *roueDDTF;
@property (weak, nonatomic) IBOutlet UITextField *roueDGTF;


@property (weak, nonatomic) IBOutlet NOVTaggedButton *roueADBtn;
@property (weak, nonatomic) IBOutlet NOVTaggedButton *roueAGBtn;
@property (weak, nonatomic) IBOutlet NOVTaggedButton *roueDDBtn;
@property (weak, nonatomic) IBOutlet NOVTaggedButton *roueDGBtn;



@property (weak, nonatomic) IBOutlet UITextField *batterieTF;
@property (weak, nonatomic) IBOutlet NOVTaggedButton *batterieBtn;


@property (weak, nonatomic) IBOutlet NOVTaggedButton *permutationBtn;
@property (weak, nonatomic) IBOutlet NOVTaggedButton *equilibrageBtn;
@property (weak, nonatomic) IBOutlet NOVTaggedButton *geometrieBtn;

@property (weak, nonatomic) IBOutlet NOVTaggedButton *outilsBtn;
@property (weak, nonatomic) IBOutlet NOVTaggedButton    *roueSecoursBtn;


@property (weak, nonatomic) IBOutlet UIView *schemaView;

- (IBAction)valider:(id)sender;

- (IBAction)showMetalOptions:(id)sender forEvent:(UIEvent*)event;

- (IBAction)showOptiquesOptions:(id)sender forEvent:(UIEvent*)event;

- (IBAction)showMetalOptions:(id)sender;

- (IBAction)showVitreOptions:(id)sender;

- (IBAction)selectOutils:(id)sender;

- (IBAction)selectAutresReparations:(id)sender;

- (IBAction)etatCarosserie:(id)sender;


- (IBAction)selectRoueAG:(id)sender;
- (IBAction)selectRoueAD:(id)sender;
- (IBAction)selectRoueDG:(id)sender;
- (IBAction)selectRoueDD:(id)sender;
- (IBAction)selectBatterie:(id)sender;

@end
