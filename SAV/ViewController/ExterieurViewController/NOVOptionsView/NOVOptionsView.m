//
//  NOVOptionsView.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/4/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVOptionsView.h"

@implementation NOVOptionsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(IBAction)selectOption:(id)sender{
    
    
    [self.delegate nOVOptionsView:self sender:self.sender selectedOption:[sender tag] touchLocation:self.touchLocation];
    
    [self removeFromSuperview];
    
}

- (IBAction)annuler:(id)sender {
    
    [self removeFromSuperview];
    
}

@end
