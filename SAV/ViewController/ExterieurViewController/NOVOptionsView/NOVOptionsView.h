//
//  NOVOptionsView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/4/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVTaggedButton.h"

@class NOVOptionsView;

@protocol NOVOptionsViewDelegate <NSObject>

@required

-(void)nOVOptionsView:(NOVOptionsView *)nOVOptionsView sender:(NOVTaggedButton *)sender selectedOption:(NSInteger)option touchLocation:(CGPoint)touchLocation;




@end

@interface NOVOptionsView : UIView

@property(nonatomic, assign) CGPoint touchLocation;

@property(nonatomic, weak)id<NOVOptionsViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIButton *option1;

@property (weak, nonatomic) IBOutlet UIButton *option2;

@property (weak, nonatomic) IBOutlet UIButton *option3;

@property (weak, nonatomic) NOVTaggedButton *sender;

-(IBAction)selectOption:(id)sender;

- (IBAction)annuler:(id)sender;
@end
