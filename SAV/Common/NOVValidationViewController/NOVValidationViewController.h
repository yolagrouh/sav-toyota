//
//  NOVValidationViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class NOVValidationViewController;

@protocol NOVValidationViewControllerDelegate <NSObject>

@required
-(void)nOVValidationViewController:(NOVValidationViewController *)nOVValidationViewController continuer:(BOOL)continuer;


@end

@interface NOVValidationViewController : NOVParentPopUpViewController

@end
