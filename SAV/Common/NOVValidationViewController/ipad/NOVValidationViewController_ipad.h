//
//  NOVValidationViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVValidationViewController.h"



@interface NOVValidationViewController_ipad : NOVValidationViewController


@property(nonatomic, weak)id<NOVValidationViewControllerDelegate>delegate;

@property (strong, nonatomic)  NSString *okBtnTitle;
@property (strong, nonatomic)  NSString *cancelBtnTitle;
@property (strong, nonatomic)  NSString *validationTitleString;

@property (weak, nonatomic) IBOutlet NOVButton *okBtn;
@property (weak, nonatomic) IBOutlet NOVButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UILabel *validationTitle;

- (IBAction)valider:(id)sender;

- (IBAction)annuler:(id)sender;

@end
