#import "NOVValidationViewController_ipad.h"

@interface NOVValidationViewController_ipad ()

@end

@implementation NOVValidationViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
        
        self.shouldAddGradient = NO;
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    if (self.okBtnTitle) {
        
        [self.okBtn setTitle:self.okBtnTitle forState:UIControlStateNormal];
    }
    
    if (self.cancelBtnTitle) {
        
        [self.cancelBtn setTitle:self.cancelBtnTitle forState:UIControlStateNormal];
    }
    
    if (self.validationTitleString) {
        
        
        self.validationTitle.text = self.validationTitleString;
        
        
    }
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)valider:(id)sender {
    
    [super valider:sender];
    
    [self.delegate nOVValidationViewController:self continuer:YES];
}

- (IBAction)annuler:(id)sender {
    
    [super annuler:sender];
    
    [self.delegate nOVValidationViewController:self continuer:NO];
    
}
@end
