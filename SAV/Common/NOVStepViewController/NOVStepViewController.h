//
//  NOVStepViewController.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/11/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVGradientViewController.h"
#import "NOVValidationViewController_ipad.h"

@interface NOVStepViewController : NOVGradientViewController<NOVValidationViewControllerDelegate>

-(void)showValidation;

-(void)showValidation:(NSString *)okBtnTitle cancelBtn:(NSString *)cancelBtnTitle title:(NSString *)title;

-(void)showNext;

-(void)showCancel;

@end
