//
//  NOVStepViewController.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/11/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVStepViewController.h"

@interface NOVStepViewController ()

@property(nonatomic, strong)NOVValidationViewController_ipad * validationViewController;

@end

@implementation NOVStepViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void)showValidation{
    
    self.validationViewController = [[NOVValidationViewController_ipad alloc]initWithNibName:@"NOVValidationViewController" bundle:nil];
    
    self.validationViewController.delegate = self;
    
    [self.view addSubview:self.validationViewController.view];
    
    [self.validationViewController show];
    
}


-(void)showValidation:(NSString *)okBtnTitle cancelBtn:(NSString *)cancelBtnTitle title:(NSString *)title{
    
    self.validationViewController = [[NOVValidationViewController_ipad alloc]initWithNibName:@"NOVValidationViewController" bundle:nil];
    
    self.validationViewController.okBtnTitle = okBtnTitle;
    
    self.validationViewController.cancelBtnTitle = cancelBtnTitle;
    
    self.validationViewController.validationTitleString= title;
    
    self.validationViewController.delegate = self;
    
    [self.view addSubview:self.validationViewController.view];
    
    [self.validationViewController show];
    
}

-(void)showCancel{
    
    
    
}

-(void)showNext{
    
    
    
}

#pragma mark --
#pragma mark Validation

-(void)nOVValidationViewController:(NOVValidationViewController *)nOVValidationViewController continuer:(BOOL)continuer{
    
    if (!continuer) {
        
        [self showCancel];
        
        return;
    }
    
    [self showNext];
    
}

@end
