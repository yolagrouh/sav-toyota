//
//  NOVCarosserieImageView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/25/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVTaggedButton.h"

@class NOVCarosserieImageView;

@protocol NOVCarosserieImageViewDelegate <NSObject>


@required
-(void)nOVCarosserieImageView:(NOVCarosserieImageView *)nOVCarosserieImageView remove:(NSString *)content;


@end

@interface NOVCarosserieImageView : UIImageView

@property(nonatomic, weak)id<NOVCarosserieImageViewDelegate>delegate;

@property(nonatomic, strong)NSString * content;

@property(nonatomic, strong)NOVTaggedButton * sender;

-(void)deleteImageView;

@end
