//
//  NOVCarosserieImageView.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/25/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVCarosserieImageView.h"

@implementation NOVCarosserieImageView

-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteImageView)];
        
        tapGesture.numberOfTapsRequired = 2;
        
        self.userInteractionEnabled = YES;
        
        [self addGestureRecognizer:tapGesture];
        
    }
    
    return self;
    
}

-(id)initWithImage:(UIImage *)image{
    
    self = [super initWithImage:image];
    
    if (self) {
        
        UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteImageView)];
        
        tapGesture.numberOfTapsRequired = 2;
        
        self.userInteractionEnabled = YES;
        
        [self addGestureRecognizer:tapGesture];
        
    }
    
    return self;
}


-(void)deleteImageView{
    
    [self removeFromSuperview];
    
    [self.delegate nOVCarosserieImageView:self remove:self.content];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
