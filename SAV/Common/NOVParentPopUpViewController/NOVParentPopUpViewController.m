//
//  NOVParentPopUpViewController.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/27/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVParentPopUpViewController.h"


@interface NOVParentPopUpViewController ()

@end

@implementation NOVParentPopUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)fermer:(id)sender{
    
    [self hide];
    
}


-(IBAction)valider:(id)sender{
    
    [self hide];

}

-(IBAction)annuler:(id)sender{
    
    [self hide];
    
}
#pragma mark --
#pragma mark ViewTransition

-(void)show{
    
    CGRect contentViewFrame = self.contentView.frame ;
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    
    self.view.alpha = 0;
    
    self.contentView.frame = CGRectMake( (self.view.frame.size.width - self.contentView.frame.size.width)/2, - self.contentView.frame.size.height, self.contentView.frame.size.width, self.contentView.frame.size.height);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        
        self.view.alpha = 1;
        
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.2 animations:^{
            
            
            self.contentView.frame = contentViewFrame;
            
        }] ;
        
    }];
    
}

-(void)hide{
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.contentView.frame = CGRectMake( self.contentView.frame.origin.x, - self.contentView.frame.size.height, self.contentView.frame.size.width, self.contentView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            
            self.view.alpha = 0;
            
            
        } completion:^(BOOL finished) {
            
            [self.view removeFromSuperview];
            
        }];
        
    }];
    
}


@end
