//
//  NOVParentPopUpViewController.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/27/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVGradientViewController.h"

static BOOL isPopUpVisible = NO;

@interface NOVParentPopUpViewController : NOVGradientViewController

@property(nonatomic, weak)IBOutlet UIView * contentView;


-(void)show;

-(void)hide;

-(IBAction)fermer:(id)sender;

-(IBAction)valider:(id)sender;

-(IBAction)annuler:(id)sender;

@end
