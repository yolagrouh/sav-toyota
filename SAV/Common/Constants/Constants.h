//
//  Constants.h
//  Adria
//
//  Created by MOHAMED AHDIDOU on 3/5/14.
//  Copyright (c) 2014 MOHAMED AHDIDOU. All rights reserved.
//

#ifndef Adria_Constants_h
#define Adria_Constants_h

/****************************************************** URLS **************************************************/

//JT111TJ8007016360
//J210LG-GMDF

#define BASE_URL                                @"http://private-65ecf-sav.apiary-mock.com/"
#define VIN_WS                                  @"http://196.12.250.247/toyota_middelware_ws/public/search.php?sn="
#define RDV_WS                                  @"http://196.12.250.247/toyota_sav_ws/hp_rdv_semaine.php?login="
#define RAPPEL_WS                               @"http://196.12.250.247/toyota_sav_ws/rappel.php?SN="
#define PRIX_URL                                @"http://thenextclic.net/toyota/sav/sav/toyota/ws/prix/liste?KATASHIKI="
#define HISTORIQUE_WS                           @"http://196.12.250.247/toyota_sav_ws/historique.php?where_SN="

#define TOUS_TRAVAUX_WS                         @"http://196.12.250.247/toyota_sav_ws/travaux-2-en-1.php?katashiki="
#define TRAVAUX_AUTRE_WS                        @"http://196.12.250.247/toyota_sav_ws/travaux-v2.php?view=tdm_travaux_autres&katashiki="

#define TRAVAUX_ADDITIONELS_WS                  @"http://196.12.250.247/toyota_sav_ws/travaux-v2.php?view=tdm_travaux_autres_add&katashiki="

#define SAVE_URL                                @"http://thenextclic.net/toyota/sav/sav/toyota/ws/interventions/create"

#define ARGUMENTAIRE_WS                         @"http://thenextclic.net/toyota/sav/ws/argumentaire/list?categorie="

#define TASK_SAVE_WS                            @"http://196.12.250.247/toyota_sav_ws/tasks_insert.php?"

#define JOB_CARD_INSERT_WS                      @"http://196.12.250.247/toyota_sav_ws/job_card_insert.php?"

#define SA_LIST                                 @"http://thenextclic.net/toyota/sav/ws/commercial/list"

#define PWD_RESET_WS                            @"http://thenextclic.net/toyota/sav/ws/commercial/set_password?email="


#define RECOMMENDATION_WS                       @"http://196.12.250.247/toyota_sav_ws/recommandation-select.php?where_INCIDENT_ID="

#define USER_SAVE_WS                            @"http://196.12.250.247/toyota_sav_ws/customer-insert.php?"

#define TASK_INSERT_WS                          @"http://196.12.250.247/toyota_sav_ws/tasks-insert-post.php"


//V2

#define SELECT_JOB_CARD_WS                      @"http://196.12.250.247/toyota_middelware_ws/public/select-job-cards.php?where_IPAD_NUMBER="

//#define SELECT_TASKS_WS                         @"http://196.12.250.247/toyota_middelware_ws/public/select-tasks.php?where_IPAD_NUMBER="

#define SELECT_TASKS_WS                         @"http://196.12.250.247/toyota_middelware_ws/public/select-table.php?table=TDM_INVOICES&where_IPAD_NUMBER="

#define SELECT_TASKS_WS_2                         @"http://196.12.250.247/toyota_middelware_ws/public/select-table.php?table=tdm_sr_history&where_NUM_IPAD="

/****************************************************** CONSTANTES **************************************************/

#define LOADING_MESSAGE                         @"Chargement..."

#define SUCCESS_MESSAGE                         @"Chargement effectué"

#define FAILED_MESSAGE                          @"Erreur lors du chargement"


#define BOOKMARK                                @"BOOKMARK"


/****************************************************** Notifications **************************************************/

#define SHOW_RELANCES_VIEW                      @"SHOW_RELANCES_VIEW"

#define FINISH_VISIT_NOTIFICATION               @"FINISH_VISIT_NOTIFICATION"

#define IMPRESSION_NOTIFICATION                 @"IMPRESSION_NOTIFICATION"

#define SHOW_DATE_PICKER_NOTIFICATION           @"SHOW_DATE_PICKER_NOTIFICATION"

#define SET_DATE_NOTIFICATION                   @"SET_DATE_NOTIFICATION"

#define RELOAD_RDV_NOTIFICATION                 @"RELOAD_RDV_NOTIFICATION"

#define RELOAD_RELANCES_NOTIFICATION            @"RELOAD_RELANCES_NOTIFICATION"

#define RECHERCHE_NOTIFICATION                  @"RECHERCHE_NOTIFICATION"

#define SHOW_VIN_POP_UP_WITH_OBJECT             @"SHOW_VIN_POP_UP_WITH_OBJECT"

#define SHOW_FAVORIS                            @"SHOW_FAVORIS"

#define SHOW_TADDITIONNELS_NOTIFICATION         @"SHOW_TADDITIONNELS_NOTIFICATION"

#endif