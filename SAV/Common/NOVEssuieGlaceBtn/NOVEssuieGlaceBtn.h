//
//  NOVEssuieGlaceBtn.h
//  SAV
//
//  Created by MacBook Pro on 10/03/2015.
//  Copyright (c) 2015 NOVINCIA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NOVEssuieGlaceBtn : UIButton

@property(nonatomic, strong)NSString * content;

@property(nonatomic, assign)BOOL isActivated;

@end
