//
//  NOVTextField.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/23/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVTextField.h"

@implementation NOVTextField

-(id)init{
    
    self = [super init];
    if (self) {
        
        [self setUpField];
        
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

       [self setUpField];
        
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self setUpField];
    }
    
    return self;
}

- (void)setUpField {
    
    UIView * loginTFPaddingView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, self.frame.size.height)];
    
    self.leftView = loginTFPaddingView ;
    
    self.leftViewMode = UITextFieldViewModeAlways;
    
    self.layer.cornerRadius = self.frame.size.height/2;
    
    self.layer.borderColor = [UIColor colorWithRed:155.0/255 green:160.0/255 blue:165.0/255 alpha:1.0].CGColor;
    
    self.layer.borderWidth = 1;
    
    self.textColor = [UIColor colorWithRed:33.0/255 green:43.0/255 blue:50.0/255 alpha:1];
    
    self.backgroundColor = [UIColor whiteColor];
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
