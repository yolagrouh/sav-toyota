//
//  NOVTextField.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/23/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//


@interface NOVTextField : UITextField

@property(nonatomic, strong)NSString * content;

- (void)setUpField;

@end
