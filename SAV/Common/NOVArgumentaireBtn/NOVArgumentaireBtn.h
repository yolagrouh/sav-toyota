//
//  NOVArgumentaireBtn.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 12/31/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVTaggedButton.h"

@interface NOVArgumentaireBtn : NOVTaggedButton

@property(nonatomic, strong)NSString * argument;

@end
