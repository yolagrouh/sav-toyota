//
//  NOVSharedData.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/27/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NOVSharedData : NSObject


#define CODE_CLIENT                                     @"CODE_CLIENT"
#define TYPE_CLIENT                                     @"TYPE_CLIENT"
#define NOM_CLIENT                                      @"NOM_CLIENT"
#define TEL                                             @"TEL "
#define ADRESSE                                         @"ADRESSE"
#define EMAIL                                           @"EMAIL"
#define CIN_RC                                          @"CIN_RC"
#define SN                                              @"SN"
#define MODELE_VEH                                      @"MODELE_VEH"
#define IMMATRICULE                                     @"IMMATRICULE"
#define DATE_MISE_EN_CIRCULATION                        @"DATE_MISE_EN_CIRCULATION"
#define KATASHIKI                                       @"KATASHIKI"
#define WORKSHOP                                        @"WORKSHOP"
#define SA                                              @"SA"
#define OPERATION_CODE                                  @"OPERATION_CODE"
#define DATE_RDV                                        @"DATE_RDV"
#define REQUETES_CLIENT                                 @"REQUETES_CLIENT"
#define CONTACT_NOM                                     @"CONTACT_NOM"
#define CONTACT_TEL                                     @"CONTACT_TEL"
#define KILOMETRAGE                                     @"KILOMETRAGE"
#define NEW_KILOMETRAGE                                 @"NEW_KILOMETRAGE"
#define NIVEAU_CARBURANT                                @"NIVEAU_CARBURANT"
#define OBJECTS_INTERIEUR                               @"OBJECTS_INTERIEUR"
#define DEMANDES_CLIENT                                 @"DEMANDES_CLIENT"

#define TIME_STAMP                                      @"TIME_STAMP"
#define JOB_CARD                                        @"JOB_CARD"


#define TRAVAUX_CONFIRMES
#define TRAVAUX_CONFIRMES_TRAVAIL                       @"TRAVAIL"
#define TRAVAUX_CONFIRMES_PRIX                          @"PRIX"


#define TRAVAUX_ADDITIONNELS                            @"TRAVAUX_ADDITIONNELS"
#define TRAVAUX_ADDITIONNELS_TRAVAIL                    @"TITRE DU TRAVAIL"
#define TRAVAUX_ADDITIONNELS_STATUS                     @"TRAVAUX_ADDITIONNELS_STATUS"
#define TRAVAUX_ADDITIONNELS_RAISON1                    @"RAISON1"
#define TRAVAUX_ADDITIONNELS_RAISON2                    @"RAISON2"
#define TRAVAUX_ADDITIONNELS_PRIX                       @"PRIX"


#define RAPPEL_SUIVI                                    @"RAPPEL_SUIVI"
#define RAPPEL_SUIVI_STATUS                             @"RAPPEL_SUIVI_STATUS"
#define RAPPEL_SUIVI_STATUS_DATE                        @"RAPPEL_SUIVI_STATUS_DATE"

//Controls
@property(nonatomic, strong)NSString                * previousKM;

@property(nonatomic, strong)NSString                * userKM;

//Data
@property(nonatomic, strong)NSMutableArray          * rendezVous;

@property(nonatomic, assign)BOOL                    doneLoading;

@property(nonatomic, strong)NSString                * codeClient;
@property(nonatomic, strong)NSString                * typeClient;
@property(nonatomic, strong)NSString                * nomClient;
@property(nonatomic, strong)NSString                * telClient;
@property(nonatomic, strong)NSString                * adresseClient;
@property(nonatomic, strong)NSString                * emailClient;
@property(nonatomic, strong)NSString                * cinRCClient;
@property(nonatomic, strong)NSString                * sn;//Serial Number ou VIN
@property(nonatomic, strong)NSString                * modelVehicule;
@property(nonatomic, strong)NSString                * immatriculation;
@property(nonatomic, strong)NSString                * dateMiseEnCirculation;
@property(nonatomic, strong)NSString                * katashiki;
@property(nonatomic, strong)NSString                * workshop;
@property(nonatomic, strong)NSString                * sa;
@property(nonatomic, strong)NSString                * operationCode;
@property(nonatomic, strong)NSString                * dateRDV;
@property(nonatomic, strong)NSString                * requetesClient;
@property(nonatomic, strong)NSString                * contactNom;
@property(nonatomic, strong)NSString                * contactTel;
@property(nonatomic, strong)NSString                * kilometrage;
@property(nonatomic, strong)NSString                * niveauCarburant;
@property(nonatomic, strong)NSString                * objectsInterieur;
@property(nonatomic, strong)NSString                * demandesClient;
@property(nonatomic, strong)NSString                * livraisonDate;
@property(nonatomic, strong)NSString                * livraisonHeure;
@property(nonatomic, strong)NSMutableArray          * travauxConfirmes;
@property(nonatomic, strong)NSMutableArray          * travauxAdditionnels;
@property(nonatomic, strong)NSMutableDictionary     * rappelSuivi;

@property(nonatomic, strong)NSString                * timeStamp;
@property(nonatomic, strong)NSString                * jobCard;




@property(nonatomic, strong)NSMutableArray         * selectedRequestsClient;
@property(nonatomic, strong)NSMutableArray         * selectedOtherRequestsClient;

@property(nonatomic, strong)NSMutableArray         * requestsList;


@property(nonatomic, strong)NSMutableArray         * tousLesTravaux;

@property(nonatomic, strong)UIImage                * schema;//Image Schema eclaté

@property(nonatomic, strong)UIImage                * pricesImage;//Image Schema eclaté

@property(nonatomic, strong)NSString               * pdfURL;//Image Schema eclaté

@property(nonatomic, assign)BOOL                   didControlCarosserie;

+(id)sharedNOVData;

-(void)reset;

-(NSDictionary *)createArchivableObject;

-(void)setData:(NSDictionary * )data;

-(NSString *)getRequestDescription:(NSString *)requestCode;

-(NSString *)getRequestCode:(NSString *)requestDescription;
@end
