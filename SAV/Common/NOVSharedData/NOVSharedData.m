//
//  NOVSharedData.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/27/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVSharedData.h"

@implementation NOVSharedData

+ (id)sharedNOVData {
    
    static NOVSharedData * sharedNOVData = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedNOVData = [[self alloc] init];
        sharedNOVData.doneLoading = NO;
        sharedNOVData.travauxConfirmes              = [NSMutableArray array];
        sharedNOVData.travauxAdditionnels           = [NSMutableArray array];
        sharedNOVData.rappelSuivi                   = [NSMutableDictionary dictionary];
        sharedNOVData.selectedRequestsClient        = [NSMutableArray array];
        sharedNOVData.selectedOtherRequestsClient   = [NSMutableArray array];
        
        
        sharedNOVData.rendezVous   = [NSMutableArray array];
        
        sharedNOVData.tousLesTravaux                = [NSMutableArray array];
        
        sharedNOVData.jobCard = [NSString string];
    });
    
    return sharedNOVData;
}


-(void)reset{
    
    self.codeClient                                         = [NSString string];
    self.typeClient                                         = [NSString string];
    self.nomClient                                          = [NSString string];
    self.telClient                                          = [NSString string];
    self.adresseClient                                      = [NSString string];
    self.emailClient                                        = [NSString string];
    self.cinRCClient                                        = [NSString string];
    self.sn                                                 = [NSString string];//Serial Number ou VIN
    self.modelVehicule                                      = [NSString string];
    self.immatriculation                                    = [NSString string];
    self.dateMiseEnCirculation                              = [NSString string];
    self.katashiki                                          = [NSString string];
    self.workshop                                           = [NSString string];
    self.sa                                                 = [NSString string];
    self.operationCode                                      = [NSString string];
    self.dateRDV                                            = [NSString string];
    self.requetesClient                                     = [NSString string];
    self.contactNom                                         = [NSString string];
    self.contactTel                                         = [NSString string];
    self.kilometrage                                        = [NSString string];
    self.userKM                                             = [NSString string];
    self.niveauCarburant                                    = [NSString string];
    self.objectsInterieur                                   = [NSString string];
    self.demandesClient                                     = [NSString string];
    self.pdfURL                                             = [NSString string];
    
    self.timeStamp                                          = [NSString string];
    self.jobCard                                            = [NSString string];
    
    self.livraisonDate                                      = @"";
    self.livraisonHeure                                     = @"";
    
    [self.travauxConfirmes removeAllObjects];
    [self.travauxAdditionnels removeAllObjects];
    [self.rappelSuivi removeAllObjects];
    
    
    [self.selectedRequestsClient removeAllObjects];
    [self.selectedOtherRequestsClient removeAllObjects];
    
    
    [self.tousLesTravaux removeAllObjects];
    
    self.schema                                             = nil;
    
}

-(NSDictionary *)createArchivableObject{
    
    NSMutableDictionary * data = [NSMutableDictionary dictionary];
    
    [data setObject:self.codeClient                     forKey:@"CODE_CLIENT"];
    [data setObject:self.typeClient                     forKey:@"TYPE_CLIENT"];
    [data setObject:self.nomClient                      forKey:@"NOM_CLIENT"];
    [data setObject:self.telClient                      forKey:@"TEL"];
    [data setObject:self.adresseClient                  forKey:@"ADRESSE"];
    [data setObject:self.emailClient                    forKey:@"EMAIL"];
    [data setObject:self.cinRCClient                    forKey:@"CIN_RC"];
    [data setObject:self.sn                             forKey:@"SN"];
    [data setObject:self.modelVehicule                  forKey:@"MODELE_VEH"];
    [data setObject:self.immatriculation                forKey:@"IMMATRICULE"];
    [data setObject:self.dateMiseEnCirculation          forKey:@"DATE_MISE_EN_CIRCULATION"];
    [data setObject:self.katashiki                      forKey:@"KATASHIKI"];
    [data setObject:self.workshop                       forKey:@"WORKSHOP"];
    [data setObject:self.sa                             forKey:@"SA"];
    [data setObject:self.operationCode                  forKey:@"OPERATION_CODE"];
    [data setObject:self.dateRDV                        forKey:@"DATE_RDV"];
    [data setObject:self.requetesClient                 forKey:@"REQUETES"];
    [data setObject:self.contactNom                     forKey:@"CONTACT_NOM"];
    [data setObject:self.contactTel                     forKey:@"CONTACT_TEL"];
    [data setObject:self.kilometrage                    forKey:@"KILOMETRAGE"];
    [data setObject:self.userKM                          forKey:@"NEW_KILOMETRAGE"];
    [data setObject:self.niveauCarburant                forKey:@"NIVEAU_CARBURANT"];
    [data setObject:self.objectsInterieur               forKey:@"OBJECTS_INTERIEUR"];
    [data setObject:self.demandesClient                 forKey:@"DEMANDES_CLIENT"];
    [data setObject:self.travauxConfirmes               forKey:@"TRAVAUX_CONFIRMES"];
    [data setObject:self.travauxAdditionnels            forKey:@"TRAVAUX_ADDITIONNELS"];
    [data setObject:self.rappelSuivi                    forKey:@"RAPPEL_SUIVI"];
    [data setObject:self.selectedRequestsClient         forKey:@"SELECTED_REQUESTES"];
    [data setObject:self.selectedOtherRequestsClient    forKey:@"SELECTED_OTHER_REQUESTES"];
    
    
    [data setObject:self.tousLesTravaux                 forKey:@"ALL_WORKS"];
    [data setObject:self.livraisonDate                  forKey:@"DATE_LIVRAISON"];
    [data setObject:self.livraisonHeure                 forKey:@"HEURE_LIVRAISON"];
    [data setObject:self.schema                         forKey:@"SCHEMA_IMAGE"];
    [data setObject:self.pdfURL                         forKey:@"CONVENTION_URL"];
    
    [data setObject:self.timeStamp                         forKey:@"TIME_STAMP"];
    [data setObject:self.jobCard                         forKey:@"JOB_CARD"];
    
    
    return data;
    
}


-(void)setData:(NSDictionary * )data{
    
    self.codeClient                     = [data objectForKey:@"CODE_CLIENT"];
    self.typeClient                     = [data objectForKey:@"TYPE_CLIENT"];
    self.nomClient                      = [data objectForKey:@"NOM_CLIENT"];
    self.telClient                      = [data objectForKey:@"TEL"];
    self.adresseClient                  = [data objectForKey:@"ADRESSE"];
    self.emailClient                    = [data objectForKey:@"EMAIL"];
    self.cinRCClient                    = [data objectForKey:@"CIN_RC"];
    self.sn                             = [data objectForKey:@"SN"];
    self.modelVehicule                  = [data objectForKey:@"MODELE_VEH"];
    self.immatriculation                = [data objectForKey:@"IMMATRICULE"];
    self.dateMiseEnCirculation          = [data objectForKey:@"DATE_MISE_EN_CIRCULATION"];
    self.katashiki                      = [data objectForKey:@"KATASHIKI"];
    self.workshop                       = [data objectForKey:@"WORKSHOP"];
    self.sa                             = [data objectForKey:@"SA"];
    self.operationCode                  = [data objectForKey:@"OPERATION_CODE"];
    self.dateRDV                        = [data objectForKey:@"DATE_RDV"];
    self.requetesClient                 = [data objectForKey:@"REQUETES"];
    self.contactNom                     = [data objectForKey:@"CONTACT_NOM"];
    self.contactTel                     = [data objectForKey:@"CONTACT_TEL"];
    self.kilometrage                    = [data objectForKey:@"KILOMETRAGE"];
    self.userKM                         = [data objectForKey:@"NEW_KILOMETRAGE"];
    
    self.niveauCarburant                = [data objectForKey:@"NIVEAU_CARBURANT"];
    self.objectsInterieur               = [data objectForKey:@"OBJECTS_INTERIEUR"];
    self.demandesClient                 = [data objectForKey:@"DEMANDES_CLIENT"];
    
    
    self.livraisonDate                  = [data objectForKey:@"DATE_LIVRAISON"];
    self.livraisonHeure                 = [data objectForKey:@"HEURE_LIVRAISON"];
    
    
    self.schema                         = [data objectForKey:@"SCHEMA_IMAGE"];
    self.pdfURL                         = [data objectForKey:@"CONVENTION_URL"];
    
    self.travauxConfirmes               = [NSMutableArray arrayWithArray:[data objectForKey:@"TRAVAUX_CONFIRMES"]];
    self.travauxAdditionnels            = [NSMutableArray arrayWithArray:[data objectForKey:@"TRAVAUX_ADDITIONNELS"]];
    self.rappelSuivi                    = [NSMutableDictionary dictionaryWithDictionary:[data objectForKey:@"RAPPEL_SUIVI"]];
    self.selectedRequestsClient         = [NSMutableArray arrayWithArray:[data objectForKey:@"SELECTED_REQUESTES"]];
    self.selectedOtherRequestsClient    = [NSMutableArray arrayWithArray:[data objectForKey:@"SELECTED_OTHER_REQUESTES"]];
  
    self.tousLesTravaux                 = [NSMutableArray arrayWithArray:[data objectForKey:@"ALL_WORKS"]];
    
    self.timeStamp                      = [data objectForKey:@"TIME_STAMP"];
    
    if([data objectForKey:@"JOB_CARD"]){
        
        self.jobCard                        = [data objectForKey:@"JOB_CARD"];
    }else{
        
        self.jobCard                        = [NSString string];
    }
    
    
}


-(NSString *)getRequestDescription:(NSString *)requestCode{
    
    NSLog(@"Looking for request %@", requestCode);
    
    NSString * description;
    
    BOOL found = NO;
    
    for (id requete in self.requestsList) {
        
        if ([[requete objectForKey:@"OP_CODE"] caseInsensitiveCompare:requestCode]  == NSOrderedSame) {
            
            description = [requete objectForKey:@"OP_DESCRIPTION"];
            
            found = YES;
            
            break;
            
        }
        
    }
    
    if (!found) {
        
        return  requestCode;
        
    }
    
    return description;
    
}

-(NSString *)getRequestCode:(NSString *)requestDescription{
    
    NSString * code;
    
    BOOL found = NO;
    
    for (id requete in self.requestsList) {
        
        if ([[requete objectForKey:@"OP_DESCRIPTION"] caseInsensitiveCompare:requestDescription]  == NSOrderedSame) {
            
            code = [requete objectForKey:@"OP_CODE"];
            
            found = YES;
            
            break;
            
        }
        
    }
    
    if (!found) {
        
        return  requestDescription;
        
    }
    
    return code;
    
}

@end
