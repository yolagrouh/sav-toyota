//
//  NOVMoreTravauxCell.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/12/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "BaseCell.h"

@interface NOVMoreTravauxCell : BaseCell

@property (weak, nonatomic) IBOutlet UILabel *travailLabel;

@property (weak, nonatomic) IBOutlet UIImageView *cellIndicator;

@end
