//
//  NOVMoreTravauxViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class NOVMoreTravauxViewController;

@protocol NOVMoreTravauxViewControllerDelegate <NSObject>

@required
-(void)nOVMoreTravauxViewController:(NOVMoreTravauxViewController *)nOVMoreTravauxViewController selectedTravaux:(NSArray *) travaux;

@end

@interface NOVMoreTravauxViewController : NOVParentPopUpViewController

@end
