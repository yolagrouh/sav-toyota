//
//  NOVMoreTravauxViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVMoreTravauxViewController.h"

@interface NOVMoreTravauxViewController_ipad : NOVMoreTravauxViewController<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong)NSArray * travaux;

@property(nonatomic, strong)NSMutableArray * selectedTravaux;

@property(nonatomic, weak)id<NOVMoreTravauxViewControllerDelegate>delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
