#import "NOVMoreTravauxViewController_ipad.h"
#import "NOVMoreTravauxCell.h"

@interface NOVMoreTravauxViewController_ipad ()

@end

@implementation NOVMoreTravauxViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {

        self.selectedTravaux = [NSMutableArray array];
        
        self.shouldAddGradient = NO;
        
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

#pragma mark --
#pragma mark TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.travaux count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
    
}

-(UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"NOVMoreTravauxCell";
    
    NOVMoreTravauxCell *cell = (NOVMoreTravauxCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        
        cell = (NOVMoreTravauxCell *)[self getCellWithIdentifier:cellIdentifier];
        
    }
    
    NSString * travail = [self.travaux objectAtIndex:indexPath.row];

    cell.travailLabel.text = travail;
    
    
    cell.cellIndicator.hidden = ![self.selectedTravaux containsObject:travail];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (![self.selectedTravaux containsObject:[self.travaux objectAtIndex:indexPath.row]] ) {
        
        [self.selectedTravaux addObject:[self.travaux objectAtIndex:indexPath.row]];
        
    }else{
        
        [self.selectedTravaux removeObject:[self.travaux objectAtIndex:indexPath.row]];
        
    }

    [self.tableView reloadData];
    
}


- (IBAction)fermer:(id)sender {
    
    [super fermer:sender];
    
    [self.delegate nOVMoreTravauxViewController:self selectedTravaux:self.selectedTravaux];
    
}
@end
