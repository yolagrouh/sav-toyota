//
//  NetworkHandler.h
//  Adria
//
//  Created by MOHAMED AHDIDOU on 3/5/14.
//  Copyright (c) 2014 MOHAMED AHDIDOU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetworkHandler;

@protocol NetworkHandlerDelegate <NSObject>

@required

-(void)networkStared:(NetworkHandler *)networkHandler;

-(void)networkFinished:(id)response networkHandler:(NetworkHandler *)networkHandler;

-(void)networkFailed:(NSError *)error networkHandler:(NetworkHandler *)networkHandler;


@end

@interface NetworkHandler : NSObject

@property(weak)id<NetworkHandlerDelegate>delegate;

@property(assign) NSInteger requestID;


-(void)startPostRequest:(NSString *)url params:(NSDictionary *)params;

-(void)startGetRequest:(NSString *)url;

-(void)startUploadRequest:(NSString *)url params:(NSDictionary *)params data:(id)data;

-(void)cancelAllRequests;

@end
