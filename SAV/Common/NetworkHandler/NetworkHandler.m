//
//  NetworkHandler.m
//  Adria
//
//  Created by MOHAMED AHDIDOU on 3/5/14.
//  Copyright (c) 2014 MOHAMED AHDIDOU. All rights reserved.
//

#import "NetworkHandler.h"
#import "Utils.h"
#import "AFNetworking.h"


@implementation NetworkHandler


-(void)startGetRequest:(NSString *)url{
    
    [self.delegate networkStared:self];
    
    NSLog(@"GET URL : %@", url);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id JSON) {
       
        NSLog(@"JSON: %@", JSON);
        
        [Utils log:@"NetworkHandler Success " message:[JSON description]];
        
        [self.delegate networkFinished:JSON networkHandler:self];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [Utils log:@"NetworkHandler Failed" message:[error description]];
        
        [self.delegate networkFailed:error networkHandler:self];
        
    }];
    
}


-(void)startPostRequest:(NSString *)url params:(NSDictionary *)params{
    
    [self.delegate networkStared:self];
    
    NSLog(@"POST URL : %@", url);
    
    NSURL *aUrl = [NSURL URLWithString:url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    
    [request setHTTPMethod:@"POST"];
    
    NSString *postString = @"";
    
    for (id key in [params allKeys]) {
        
        id  tmpValue = [params objectForKey:key] ;
        
        if ([tmpValue isKindOfClass:[NSString class]]) {
            
          tmpValue =   [tmpValue stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
        }
        
        if ([postString length] ==0) {
            
            postString = [NSString stringWithFormat:@"%@=%@",key, tmpValue];
            
        }else{
         
            postString = [NSString stringWithFormat:@"%@&%@=%@",postString, key,tmpValue];
            
        }
    }
    
//    postString = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
//                                                                                                 NULL,
//                                                                                                 (__bridge CFStringRef)postString,
//                                                                                                 NULL, 
//                                                                                                 (CFStringRef)@"!*'\"();:@+$,/?%#[]% ",
//                                                                                                 kCFStringEncodingUTF8);
    
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        [Utils log:@"NetworkHandler Success " message:[JSON description]];
        
        [self.delegate networkFinished:JSON networkHandler:self];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       
        [Utils log:@"NetworkHandler Failed" message:operation.responseString];
        
        
        [self.delegate networkFailed:error networkHandler:self];

        
    }];
   
    
}

-(void)startUploadRequest:(NSString *)url params:(NSDictionary *)params data:(id)data{
    
    [self.delegate networkStared:self];
    
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://server.url"]];
//    
//    NSData *imageData = UIImageJPEGRepresentation(data, 0.5);
//    
//    AFHTTPRequestOperation *op = [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        
//        [formData appendPartWithFileData:imageData name:@"pieceJointe" fileName:@"photo.jpg" mimeType:@"image/png"];
//        
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
//        
//        [self.delegate networkFinished:responseObject networkHandler:self] ;
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
//        
//        [self.delegate networkFailed:error networkHandler:self] ;
//        
//    }];
//    
//    [op start];
    
}


-(void)cancelAllRequests{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager.operationQueue cancelAllOperations];
    
    
}


@end
