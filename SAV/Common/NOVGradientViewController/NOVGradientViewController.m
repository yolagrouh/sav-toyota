//
//  NOVGradientViewController.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/23/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVGradientViewController.h"

@interface NOVGradientViewController ()

@end

@implementation NOVGradientViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        self.shouldAddGradient = YES;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark --
#pragma mark SetUp

-(void)setUpView{
    
    [super setUpView];
    
    //Gradient
    
    if (self.shouldAddGradient) {
        
        
        
        UIColor * startColor = [UIColor colorWithRed:33.0/255 green:43.0/255 blue:50.0/255 alpha:1];
        
        UIColor * endColor = [UIColor colorWithRed:223.0/255 green:223.0/255 blue:223.0/255 alpha:1];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        
        gradient.frame = self.view.bounds;
        
        gradient.colors = [NSArray arrayWithObjects:(id)[startColor CGColor], (id)[endColor CGColor], nil];
        
        [self.view.layer insertSublayer:gradient atIndex:0];
        
    }
    
}

@end
