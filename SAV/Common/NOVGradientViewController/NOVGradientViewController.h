//
//  NOVGradientViewController.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/23/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "BaseViewController.h"

@interface NOVGradientViewController : BaseViewController

@property(nonatomic, assign)BOOL shouldAddGradient;

@end
