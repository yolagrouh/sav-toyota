//
//  NOVSuggestionsTF.m
//  SAV
//
//  Created by MacBook Pro on 12/03/2015.
//  Copyright (c) 2015 NOVINCIA. All rights reserved.
//

#import "NOVSuggestionsTF.h"

@implementation NOVSuggestionsTF

-(id)init{
    
    self = [super init];
    if (self) {
        
        [self setUpField];
        
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUpField];
        
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self setUpField];
    }
    
    return self;
}

- (void)setUpField {
    
    UIView * loginTFPaddingView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, self.frame.size.height)];
    
    self.leftView = loginTFPaddingView ;
    
    self.leftViewMode = UITextFieldViewModeAlways;
    
    self.layer.cornerRadius = self.frame.size.height/2;
    
    self.layer.borderColor = [UIColor colorWithRed:155.0/255 green:160.0/255 blue:165.0/255 alpha:1.0].CGColor;
    
    self.layer.borderWidth = 1;
    
    self.textColor = [UIColor colorWithRed:33.0/255 green:43.0/255 blue:50.0/255 alpha:1];
    
    self.backgroundColor = [UIColor whiteColor];
    //Actions Button
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.actionButton.frame = self.bounds;
    
    
    [self.actionButton addTarget:self action:@selector(showOptions:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.actionButton];
    
}


-(void)handleTap{
    
    
    
}

- (IBAction)showOptions:(id)sender {
    
    self.nOVOptionsViewController = [[NOVOptionsViewController_ipad alloc] initWithNibName:@"NOVOptionsViewController" bundle:nil ];
    
    self.nOVOptionsViewController.suggestions = self.suggestions;
    
    self.nOVOptionsViewController.suggestionObjects = self.suggestionsObjects;
    
    self.nOVOptionsViewController.delegate = self;
    
    [[[[[self superview] superview] superview] superview] addSubview:self.nOVOptionsViewController.view];
    
    [self.nOVOptionsViewController show];
}

-(void)nOVOptionsViewController:(NOVOptionsViewController *)nOVOptionsViewController selected:(NSString *)option{
    
    self.text = option;
    [self becomeFirstResponder];
    [self resignFirstResponder];
    
    
}


@end
