//
//  NOVSuggestionsTF.h
//  SAV
//
//  Created by MacBook Pro on 12/03/2015.
//  Copyright (c) 2015 NOVINCIA. All rights reserved.
//


#import "NOVTextField.h"

#import "NOVOptionsViewController_ipad.h"

@interface NOVSuggestionsTF : NOVTextField<NOVOptViewControllerDelegate>


@property(nonatomic, strong)UIButton * actionButton;

@property(nonatomic, strong)NSMutableArray * suggestions;

@property(nonatomic, strong)NSMutableArray * suggestionsObjects;

@property(nonatomic, strong) NOVOptionsViewController_ipad * nOVOptionsViewController;

@end
