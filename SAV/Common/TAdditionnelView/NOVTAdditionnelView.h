//
//  NOVTAdditionnelView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/6/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVTextField.h"
#import "NOVOptionsViewController_ipad.h"

#define COLLAPSED_HEIGHT    58
#define EXPANDED_HEIGHT     167

@class NOVTAdditionnelView;

@protocol NOVTAdditionnelViewDelegate <NSObject, UITextFieldDelegate>

@required
-(void)nOVTAdditionnelView:(NOVTAdditionnelView *)nOVTAdditionnelView travail:(NSString *)travail prix:(NSString *)prix;

-(void)nOVTAdditionnelView:(NOVTAdditionnelView *)nOVTAdditionnelView choose:(BOOL)choose;

-(void)nOVTAdditionnelView:(NOVTAdditionnelView *)nOVTAdditionnelView showRaison:(UIButton *)sender;

@end

@interface NOVTAdditionnelView : UIView<UITextFieldDelegate, NOVOptViewControllerDelegate>

@property(nonatomic, strong)NOVOptionsViewController_ipad * nOVOptionsViewController;

@property(nonatomic, assign)BOOL isCollappsed;

@property(nonatomic, weak)id<NOVTAdditionnelViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet NOVTextField *travailLabel;
@property (weak, nonatomic) IBOutlet NOVTextField *coutLabel;

@property (weak, nonatomic) IBOutlet UITextView *raisonsTV;


@property (weak, nonatomic) IBOutlet UISwitch *selectSwitch;

@property(nonatomic, strong)NSMutableArray * suggestions;

@property(nonatomic, strong)NSMutableArray * suggestionsObjects;

@property(nonatomic, strong)NSString * opCode;

- (IBAction)showOptions:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *travailBtn;

@property(nonatomic, strong)NSString  * travail;
@property(nonatomic, strong)NSString  * prix;

- (IBAction)selecteTravail:(id)sender;

- (IBAction)showRaison:(id)sender;

-(void)resign;


@end
