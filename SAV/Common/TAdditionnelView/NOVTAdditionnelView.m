//
//  NOVTAdditionnelView.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/6/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVTAdditionnelView.h"
#import "Utils.h"


@implementation NOVTAdditionnelView

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        self.selectSwitch.layer.cornerRadius = self.selectSwitch.frame.size.height/2;
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.selectSwitch.layer.cornerRadius = self.selectSwitch.frame.size.height/2;
    }
    return self;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.tag == 1) {
        
        if (![Utils isNumber:string]) {
            
            return NO;
        }
        
        if ([textField.text length] == 0 && [string isEqualToString:@"."])  {
            
            return  NO;
        }
        
        if ([textField.text length] > 0 && [Utils checkString:textField.text  contains:@"."] && [string isEqualToString:@"."])  {
            
            return  NO;
        }
        
    }
    
    if (textField.tag == 1 && [self.travailLabel.text length] > 0) {
        
        
        self.selectSwitch.enabled = YES;
        
    }
    
    return  YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{

    
    switch (textField.tag) {
        case 0:
            
            self.travail = textField.text;
            
            break;
        case 1:
            
            self.prix = textField.text;
            
            break;
        default:
            break;
    }
    
    if ([self.prix length] >0 && [self.travail length] > 0) {
        
        [self.delegate nOVTAdditionnelView:self travail:self.travail prix:self.prix];
        
        self.selectSwitch.enabled = YES;
        
    }else{
        
       // self.selectSwitch.enabled = NO;
    
    }
    
}

- (IBAction)selecteTravail:(id)sender {
    
    [self.coutLabel resignFirstResponder];
    
    [self.travailLabel resignFirstResponder];
  
    
    [UIView animateWithDuration:0.3 animations:^{
        
        if (self.selectSwitch.isOn) {
            
            self.isCollappsed = YES;
            
            
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, EXPANDED_HEIGHT);
            
        }else{
            
            self.isCollappsed = NO;
            
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,COLLAPSED_HEIGHT );
        }

        
    }];
    
    
    
    [self.delegate nOVTAdditionnelView:self choose:!self.isCollappsed];

}

- (IBAction)showRaison:(id)sender {
    
    [self.delegate nOVTAdditionnelView:self showRaison:sender];
    
}

-(void)resign{
    
    [self.travailLabel resignFirstResponder];
    
    [self.coutLabel resignFirstResponder];
    
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if ( CGRectContainsPoint(CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.width+100), point) )
        return YES;
    
    return [super pointInside:point withEvent:event];
}

- (IBAction)showOptions:(id)sender {
    
    self.nOVOptionsViewController = [[NOVOptionsViewController_ipad alloc] initWithNibName:@"NOVOptionsViewController" bundle:nil ];

    self.nOVOptionsViewController.suggestions = self.suggestions;
    
    self.nOVOptionsViewController.suggestionObjects = self.suggestionsObjects;
    
    self.nOVOptionsViewController.delegate = self;
    
    [[[[[self superview] superview] superview] superview] addSubview:self.nOVOptionsViewController.view];
    
    [self.nOVOptionsViewController show];
}


-(void)nOVOptionsViewController:(NOVOptionsViewController *)nOVOptionsViewController selected:(NSString *)option{
    
    self.travail = option;
    
    self.travailLabel.text = option;
    
    self.coutLabel.text = [self getMontant:option];
    
    self.opCode = [self getCode:option];
    
    self.selectSwitch.enabled = YES;
    
   // self.travailBtn.enabled = NO;

    //self.travailLabel.enabled = NO;
    
    [self.delegate nOVTAdditionnelView:self travail:option prix:self.coutLabel.text];
    
    
}

-(NSString *)getMontant:(NSString *)requestDescription{
    
    NSString * code = @"";
    
    BOOL found = NO;
    
    for (id requete in self.suggestionsObjects) {
        
        if ([requestDescription rangeOfString:[requete objectForKey:@"OP_DESCRIPTION"] options:NSCaseInsensitiveSearch].location  != NSNotFound) {
            
            code = [requete objectForKey:@"PRIX"];
            
            found = YES;
            
            break;
            
        }
        
    }
    
    return code;
    
}


-(NSString *)getCode:(NSString *)requestDescription{
    
    NSString * code= @"";
    
    BOOL found = NO;
    
    for (id requete in self.suggestionsObjects) {
        
        if ([requestDescription rangeOfString:[requete objectForKey:@"OP_DESCRIPTION"] options:NSCaseInsensitiveSearch].location  != NSNotFound) {
            
            code = [requete objectForKey:@"NAME"];
            
            found = YES;
            
            break;
            
        }
        
    }
    
    return code;
    
}

@end
