//
//  NOVPopUpViewController.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVParentPopUpViewController.h"

@class NOVPopUpViewController;

@protocol NOVPopUpViewControllerDelegate <NSObject>

@required

-(void)nOVPopUpViewController:(NOVPopUpViewController *)nOVPopUpViewController showNext:(BOOL)showNext;

@end

@interface NOVPopUpViewController : NOVParentPopUpViewController

@end
