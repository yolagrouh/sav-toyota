#import "NOVPopUpViewController_ipad.h"

@interface NOVPopUpViewController_ipad ()

@end

@implementation NOVPopUpViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	// Custom initialization
        
        self.popUpModeInteraction = PopUpModeInteractionDisabled;
        
        self.time = 4;
        
        self.shouldAddGradient = NO;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
   
    self.imageInfo.hidden = !self.showImage;
    
    self.messageLabel.text = self.message;
    
    if (self.popUpModeInteraction == PopUpModeInteractionDisabled) {
        
        [NSTimer scheduledTimerWithTimeInterval:self.time target:self selector:@selector(femer:) userInfo:nil repeats:NO];
        
        self.closeBtn.hidden = YES;
        
    }else{
        
        self.closeBtn.hidden = NO;
        
    }
    
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)femer:(id)sender {
    
    
    [super fermer:sender];
    
    [self.delegate nOVPopUpViewController:self showNext:YES];

}
@end
