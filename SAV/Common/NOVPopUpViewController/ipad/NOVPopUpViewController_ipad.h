//
//  NOVPopUpViewController_ipad.h
//  
//
//  Created by AHDIDOU.
//  Copyright (c) 2013 AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVPopUpViewController.h"

typedef enum : NSUInteger {
    PopUpModeInteractionEnabled,
    PopUpModeInteractionDisabled,
}  PopUpModeInteraction;

@interface NOVPopUpViewController_ipad : NOVPopUpViewController

@property(nonatomic, weak)id<NOVPopUpViewControllerDelegate>delegate;

@property(nonatomic, assign) PopUpModeInteraction  popUpModeInteraction;

@property (strong, nonatomic)NSString * message;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet NOVButton *closeBtn;

@property(weak,nonatomic) IBOutlet UIImageView * imageInfo;

@property(nonatomic,assign) BOOL showImage;

@property(nonatomic, assign)NSInteger time;

- (IBAction)femer:(id)sender;

@end
