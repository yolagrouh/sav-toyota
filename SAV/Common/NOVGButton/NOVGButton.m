//
//  NOVGButton.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/24/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVGButton.h"
#import "FXLabel.h"

@implementation NOVGButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    FXLabel * titleLabel = (FXLabel *)[self titleLabel];
    
    //demonstrate gradient fill
    titleLabel.gradientStartColor = [UIColor redColor];
    titleLabel.gradientEndColor = [UIColor blackColor];
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
