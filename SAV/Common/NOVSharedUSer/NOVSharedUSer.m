//
//  NOVSharedUSer.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/7/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVSharedUSer.h"

@implementation NOVSharedUSer

+ (id)sharedNOVUser {
    
    static NOVSharedUSer * sharedUser = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedUser = [[self alloc] init];
    });
    
    return sharedUser;
}


@end
