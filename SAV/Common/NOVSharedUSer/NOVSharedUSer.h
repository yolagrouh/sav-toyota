//
//  NOVSharedUSer.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/7/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NOVSharedUSer : NSObject

@property(nonatomic, strong)NSString * name;
@property(nonatomic, strong)NSString * login;
@property(nonatomic, strong)NSString * workshop;

+(id)sharedNOVUser;

@end
