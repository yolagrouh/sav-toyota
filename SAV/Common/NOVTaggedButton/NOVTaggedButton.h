//
//  NOVTaggedButton.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/5/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "OBShapedButton.h"

@interface NOVTaggedButton : OBShapedButton

@property(nonatomic, strong)NSString * content;

@property(nonatomic, assign)BOOL isActivated;

@end
