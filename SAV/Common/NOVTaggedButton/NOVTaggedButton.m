//
//  NOVTaggedButton.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/5/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVTaggedButton.h"

@implementation NOVTaggedButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// This method will get called for each attribute you define.
-(void) setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"content"]) {
        
        self.content = value;
        
    }
}

@end
