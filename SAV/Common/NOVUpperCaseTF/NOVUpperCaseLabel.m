//
//  NOVUpperCaseTF.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 12/5/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVUpperCaseLabel.h"

@implementation NOVUpperCaseLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)setText:(NSString *)text{
    
    [super setText:[text capitalizedString]];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
