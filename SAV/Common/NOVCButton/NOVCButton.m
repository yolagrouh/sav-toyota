//
//  NOVCButton.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 10/23/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVCButton.h"

@implementation NOVCButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
    }
    return self;
}

-(void)select{
    
    [self setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
}

-(void)setSelected:(BOOL)selected{
    
    if (selected) {
        
        [self setImage:[UIImage imageNamed:@"check_btn_selected"] forState:UIControlStateNormal];
        
    }else{
        
        [self setImage:[UIImage imageNamed:@"check_btn_unselected"] forState:UIControlStateNormal];
        
    }
    
}

@end
