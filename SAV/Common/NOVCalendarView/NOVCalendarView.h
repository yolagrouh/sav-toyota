//
//  NOVCalendarView.h
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/9/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NOVBlockView.h"
#import "SSFlatDatePicker.h"

@class NOVCalendarView;

@protocol NOVCalendarViewDelegate <NSObject>

@required

-(void)nOVCalendarView:(NOVCalendarView *)nOVCalendarView date:(NSString *)date;

@end

@interface NOVCalendarView : NOVBlockView

@property (weak, nonatomic)id<NOVCalendarViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet SSFlatDatePicker *datePicker;

@property(assign)SSFlatDatePickerMode datePickerMode;

@property(strong, nonatomic)NSDate * selectedDate;

- (IBAction)dateChanged:(id)sender;

- (IBAction)valider:(id)sender;

@end
