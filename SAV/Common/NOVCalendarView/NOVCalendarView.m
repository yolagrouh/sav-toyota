//
//  NOVCalendarView.m
//  SAV
//
//  Created by MOHAMED AHDIDOU on 11/9/14.
//  Copyright (c) 2014 NOVINCIA. All rights reserved.
//

#import "NOVCalendarView.h"

#import "Utils.h"

@implementation NOVCalendarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (IBAction)dateChanged:(id)sender{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *settingComponents;
    
    self.datePicker.datePickerMode = self.datePickerMode;
    
    if (self.datePickerMode == SSFlatDatePickerModeTime) {
        
       settingComponents =  [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSTimeZoneCalendarUnit fromDate:self.datePicker.date];
        
    }else{
        
        settingComponents =  [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:self.datePicker.date];
        
    }
    
    
    
    
    self.selectedDate = [calendar dateFromComponents:settingComponents];
}

- (IBAction)valider:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.size.height, self.frame.size.width, self.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
        
        if (self.datePickerMode == SSFlatDatePickerModeTime) {
            
            [self.delegate nOVCalendarView:self date:[Utils stringFromDate:self.selectedDate dateFormat:@"hh:mm"]];
            
        }else{
            
            [self.delegate nOVCalendarView:self date:[Utils stringFromDate:self.selectedDate dateFormat:@"dd-MM-yyyy"]];
        }
        
    }];
    
}

@end
