//
//  BaseViewController.m
//  Adria
//
//  Created by MOHAMED AHDIDOU on 3/5/14.
//  Copyright (c) 2014 MOHAMED AHDIDOU. All rights reserved.
//

#import "BaseViewController.h"
#import "AFNetworking.h"

@interface BaseViewController ()

@property(nonatomic, assign)NSInteger requestsCount;

@property(nonatomic, assign)MBProgressHUD  * progressHud;

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    if ([Utils isIPad]) {
        
        nibNameOrNil  = [NSString stringWithFormat:@"%@_ipad",nibNameOrNil];
        
    }else if ([Utils isIPhone]) {
        
        nibNameOrNil  = [NSString stringWithFormat:@"%@_iphone",nibNameOrNil];
        
    }else if ([Utils isIPhone5]) {
        
        nibNameOrNil  = [NSString stringWithFormat:@"%@_iphone5",nibNameOrNil];
        
    }
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        self.shouldHandleKeyBoard = YES;
        
        self.displayTableFooter = YES;
        
        self.page = 1;

        self.requestsCount = 0;
        
        self.offset = 100;
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
        switch (status) {
                
            case AFNetworkReachabilityStatusNotReachable:
                
                self.isOnline = NO;
                
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                
                self.isOnline = YES;
                
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                
                self.isOnline = YES;
                
                break;
                
            case AFNetworkReachabilityStatusUnknown:
                
                self.isOnline = NO;
                
                break;
            default:
                break;
        }
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [self setUpView];
    
    self.view.clipsToBounds = YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(onEditingDidBegin:)
//                                                 name:UIKeyboardDidShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(onEditingDidEnd:)
//                                                 name:UIKeyboardDidHideNotification
//                                               object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    @try{
        
        [self removeObserver:self forKeyPath:UIKeyboardDidHideNotification];
        
        [self removeObserver:self forKeyPath:UIKeyboardDidShowNotification];
        
    }@catch(id anException){
        
        
    }

}

#pragma mark --
#pragma mark Actions 

-(IBAction)showVirtualKeyBoard:(id)sender{
    
    
}

-(IBAction)popToBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)showSideMenu:(id)sender{
    
//    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
//        
//        
//    }];

    [self.delegate showSideMenu];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark HandleEmpty 

-(void)showEmpty{
 
    self.emptyView.hidden = NO;
    
}

-(IBAction)retry:(id)sender{
    
    self.emptyView.hidden = YES;
    
}

#pragma mark --
#pragma mark View

-(void)updateView:(id)response requestId:(NSInteger)requestID{
    
    //TODO update view data
    
    self.requestsCount --;
    
    if (self.requestsCount == 0) {
        
        [self.progressHud hide:YES];
        
    }
    
}

-(void)gotError:(NSError *)error requestId:(NSInteger)requestID{
    
    self.requestsCount --;
    
    if (self.requestsCount == 0) {
        
        [self.progressHud hide:YES];
        
    }
    
    [SVProgressHUD showErrorWithStatus: FAILED_MESSAGE];
    
}

#pragma mark --
#pragma mark Network Handling

-(void)doGet:(NSString *)url requestID:(NSInteger)requesID {
    
    NetworkHandler * networkHandler = [[NetworkHandler alloc]init];
    
    networkHandler.delegate = self;
    
    networkHandler.requestID = requesID;
    
    //[networkHandler startGetRequest:[NSString stringWithFormat:@"%@%@",BASE_URL, url]];
    [networkHandler startGetRequest:url];
    
    self.requestsCount ++;
    
}

-(void)doPost:(NSString *)url params:(NSDictionary *)params requestID:(NSInteger)requesID{
    
    NetworkHandler * networkHandler = [[NetworkHandler alloc]init];
    
    networkHandler.delegate = self;
    
    networkHandler.requestID = requesID;
    
    //[networkHandler startPostRequest:[NSString stringWithFormat:@"%@%@",BASE_URL, url] params:params];
    
    [networkHandler startPostRequest:url params:params];
    
    self.requestsCount ++;
    
}

-(void)doUpload:(NSString *)url params:(NSDictionary *)params data:(id)data requestID:(NSInteger)requesID{
    
    NetworkHandler * networkHandler = [[NetworkHandler alloc]init];
    
    networkHandler.delegate = self;
    
    networkHandler.requestID = requesID;
    
    //[networkHandler startUploadRequest:[NSString stringWithFormat:@"%@%@",BASE_URL, url] params:params data:data];
    [networkHandler startUploadRequest:url params:params data:data];
}


-(void)cancelAllRequests{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager.operationQueue cancelAllOperations];
    
    NSLog(@"Requests Count %d", [manager.operationQueue operationCount]);
}

#pragma mark --
#pragma mark NetworkHandler Delegate


-(void)networkStared:(NetworkHandler *)networkHandler{
    
   //[SVProgressHUD showWithStatus:LOADING_MESSAGE];
   
    if (self.requestsCount == 0) {
        
        self.progressHud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        self.progressHud.labelText = @"Chargement en cours...";
    }
    
    
}

-(void)networkFinished:(id)response networkHandler:(NetworkHandler *)networkHandler{
    
    //[SVProgressHUD showSuccessWithStatus:SUCCESS_MESSAGE];
    
    [self updateView:response requestId:networkHandler.requestID];
    
}
-(void)networkFailed:(NSError *)error networkHandler:(NetworkHandler *)networkHandler{
    
    [self gotError:error requestId:networkHandler.requestID];
    
}


#pragma mark --
#pragma mark TF Delegate

-(IBAction)onEditingDidBegin:(NSNotification *) notif
{
    
    if (!self.shouldHandleKeyBoard) {
        
        return;
        
    }
    
    if (self.view.frame.origin.y == 0) {
        
        [UIView beginAnimations:nil context:nil];
        
        [UIView setAnimationDuration:0.3];
        
        CGAffineTransform transform = CGAffineTransformConcat([[self view] transform], CGAffineTransformMakeTranslation(0,-self.offset));
        
        [[self view] setTransform:transform];
        
        [UIView commitAnimations];
    }
    
	
}

-(IBAction)onEditingDidEnd:(NSNotification *) notif
{
    
    
    if (self.view.frame.origin.y == -self.offset) {
        
        [UIView beginAnimations:nil context:nil];
        
        [UIView setAnimationDuration:0.3];
        
        CGAffineTransform transform = CGAffineTransformConcat([[self view] transform], CGAffineTransformMakeTranslation(0,self.offset));
        
        [[self view] setTransform:transform];
        
        [UIView commitAnimations];
    }
	
}
#pragma mark --
#pragma mark TextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (!self.shouldHandleKeyBoard) {
        
        return;
        
    }
    
    if (self.view.frame.origin.y == 0) {
        
        CGFloat textFieldY = textField.frame.origin.y;
        
        CGFloat offset;
        
        if (textFieldY > self.view.frame.size.height/2) {
            
            offset = textFieldY - self.view.frame.size.height/2;
            
            self.offset =  offset;
            
            [UIView beginAnimations:nil context:nil];
            
            [UIView setAnimationDuration:0.3];
            
            CGAffineTransform transform = CGAffineTransformConcat([[self view] transform], CGAffineTransformMakeTranslation(0,-offset));
            
            [[self view] setTransform:transform];
            
            [UIView commitAnimations];

        }
        
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    if (self.view.frame.origin.y == -self.offset) {
        
        [UIView beginAnimations:nil context:nil];
        
        [UIView setAnimationDuration:0.3];
        
        CGAffineTransform transform = CGAffineTransformConcat([[self view] transform], CGAffineTransformMakeTranslation(0,self.offset));
        
        [[self view] setTransform:transform];
        
        [UIView commitAnimations];
    }
    
}

#pragma mark --
#pragma mark TableView


#pragma mark --
#pragma mark Other

-(UITableViewCell * )getCellWithIdentifier:(NSString *)identifier{
    
    return  [[[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil] objectAtIndex:0];
}

-(void)showAlerte:(NSString *)message{
    
    ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.view
                                                        style:ALAlertBannerStyleSuccess
                                                     position:ALAlertBannerPositionTop
                                                        title:@"TOYOTA SAV"
                                                     subtitle:message];
    
    [banner show];
    
}
-(void)showError:(NSString *)message{
    
    ALAlertBanner *banner = [ALAlertBanner alertBannerForView:self.view
                                                        style:ALAlertBannerStyleFailure
                                                     position:ALAlertBannerPositionTop
                                                        title:@"TOYOTA SAV"
                                                     subtitle:message];
    
    [banner show];
    
}


#pragma mark --
#pragma mark Disconnecte

-(IBAction)disconnecte:(id)sender{
    
    //[self popToBack:sender];
    
    UIAlertView * alerte = [[UIAlertView alloc] initWithTitle:nil message:@"Etes-vous sûrs de vouloir vous déconnecter de l'application ?" delegate:self cancelButtonTitle:@"Continuer" otherButtonTitles:@"Non", nil];
    
    [alerte show];
    
    
}


#pragma mark --
#pragma mark setupView

-(void)setUpView{
    
    self.contentSV.backgroundColor = [UIColor clearColor];
    
    
}

@end
