//
//  BaseViewController.h
//  Adria
//
//  Created by MOHAMED AHDIDOU on 3/5/14.
//  Copyright (c) 2014 MOHAMED AHDIDOU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHandler.h"
#import "SVProgressHUD.h"
#import "NOVSharedData.h"
#import "ALAlertBanner.h"
#import "MBProgressHUD.h"
#import "ESFlatButton.h"
#import "NOVBlockView.h"
#import "NOVTextField.h"
#import "NOVCButton.h"
#import "NOVButton.h"
#import "Constants.h"
#import "FXLabel.h"
#import "Utils.h"

@protocol SideMenuDelegate <NSObject>

@required
-(void)showSideMenu;

@end

#define STEP_UNSELECTED_COLOR    [UIColor colorWithRed:32.0/255 green:125.0/255 blue:227.0/255 alpha:1];

@interface BaseViewController : UIViewController<NetworkHandlerDelegate, UITextFieldDelegate, UIAlertViewDelegate>

@property(nonatomic, assign)BOOL isOnline;


@property (weak, nonatomic) id<SideMenuDelegate>delegate;

@property (assign, nonatomic) NSInteger offset;

@property(nonatomic, assign)BOOL shouldHandleKeyBoard;

@property(nonatomic, weak)IBOutlet UIView * emptyView;

@property(nonatomic, weak)IBOutlet UILabel * emptyViewMessage;

@property(nonatomic, weak)IBOutlet UIScrollView * contentSV;

@property(nonatomic, assign)BOOL displayTableFooter;

@property(nonatomic, assign)NSInteger page;

-(void)updateView:(id)response requestId:(NSInteger)requestID;

-(void)gotError:(NSError *)error requestId:(NSInteger)requestID;

-(void)doGet:(NSString *)url requestID:(NSInteger)requesID ;

-(void)doPost:(NSString *)url params:(NSDictionary *)params requestID:(NSInteger)requesID;

-(void)doUpload:(NSString *)url params:(NSDictionary *)params data:(id)data requestID:(NSInteger)requesID;

-(void)cancelAllRequests;

-(IBAction)onEditingDidBegin:(NSNotification *) notif;

-(IBAction)onEditingDidEnd:(NSNotification *) notif;

-(IBAction)showSideMenu:(id)sender;

-(IBAction)showVirtualKeyBoard:(id)sender;

-(IBAction)popToBack:(id)sender;

-(IBAction)disconnecte:(id)sender;

-(UITableViewCell * )getCellWithIdentifier:(NSString *)identifier;

-(void)showAlerte:(NSString *)message;

-(void)showError:(NSString *)message;

-(void)showEmpty;

-(IBAction)retry:(id)sender;

-(void)setUpView;


@end
