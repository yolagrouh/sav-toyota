//
//  Utils.h
//  Adria
//
//  Created by MOHAMED AHDIDOU on 3/5/14.
//  Copyright (c) 2014 MOHAMED AHDIDOU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMPageControl.h"

#define SHOULD_LOG_EVENTS YES

@interface Utils : NSObject

+(void)log:(NSString *)title message:(NSString *)message;

+(BOOL)isIPad;

+(BOOL)isIPhone;

+(BOOL)isIPhone5;

+(NSString *)addHoursToToday:(NSString *)hours;

+(BOOL)isDatePast:(NSString *)date format:(NSString *)format;

+(BOOL)isDateFuture:(NSString *)date;

+(void) showAlertView:(NSString *)message;

+(BOOL)isCharacterNumber:(NSString *)character;

+(BOOL)isNumber:(NSString *)character;

+(BOOL)isAlpha:(NSString *)character;

+(BOOL)isAlphaNumerique:(NSString *)character;

+(BOOL)isValideEmail:(NSString *)email;

+(BOOL)isValidePhone:(NSString *)phone;

+(NSString*) removeHTMLTags:(NSString*) html;

+(void)setUpPageControl:(SMPageControl *)pageControl pages:(NSInteger)pages;

+(void)removeSubviews:(UIView *)parentView;

+(float)getNumber:(NSString *)number;

+ (UIImage *) imageWithView:(UIView *)view;

+ (void) saveObject:(id) object key:(NSString *)key;

+ (id) getSavedObject:(NSString *)key;

+ (void) saveMutableObject:(id) object key:(NSString *)key;

+ (id) getSavedMutableObject:(NSString *)key;

+(NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat;

+(NSDate *)dateFromString:(NSString *)date dateFormat:(NSString *)dateFormat;

+(UIImage *)rotateImage:(UIImage*) src  orientation:(UIImageOrientation) orientation;

+(void)createPDFfromUIView:(UIScrollView*)aView saveToDocumentsWithFileName:(NSString*)aFilename;

+(void)addBorder:(UIView *)view borderColor:(UIColor * )borderColor width:(CGFloat)width;

+(void)addGradient:(UIView *)view startColor:(UIColor *)startColor endColor:(UIColor *)endColor;

+(BOOL)checkString:(NSString *)originalString  contains :(NSString *)subString;

+ (NSString *) applicationDocumentsDirectory;

+(BOOL)isOnline;

+(NSString *)getTimeStamp;
@end
