//
//  Utils.m
//  Adria
//
//  Created by MOHAMED AHDIDOU on 3/5/14.
//  Copyright (c) 2014 MOHAMED AHDIDOU. All rights reserved.
//

#import "Utils.h"
#import "AFNetworking.h"

static inline double radians (double degrees) {return degrees * M_PI/180;}

@implementation Utils

+(void)log:(NSString *)title message:(NSString *)message{
    
    if (SHOULD_LOG_EVENTS) {
        
        NSLog(@"----------------------------------------------------------------");
        
        NSLog(@"%@ : %@",title, message);
        
        NSLog(@"----------------------------------------------------------------");
        
    }
    
}

+(BOOL)isIPad{
    
    return [[UIDevice currentDevice]userInterfaceIdiom] ==  UIUserInterfaceIdiomPad;
    
}

+(BOOL)isIPhone{
    
    return [[UIScreen mainScreen] bounds].size.height < 568.0f;
    
}

+(BOOL)isIPhone5{
    
    return [[UIScreen mainScreen] bounds].size.height == 568.0f;
    
}

+(NSString *)addHoursToToday:(NSString *)hours{
    
    NSNumber * hoursNumber = [NSNumber numberWithFloat:[Utils  getNumber:hours]];
    NSDate * now = [NSDate date];
    NSTimeInterval seconds = [hours intValue] * 60 * 60;
    NSDate *dateEightHoursAhead = [now dateByAddingTimeInterval:seconds];
    
    return [Utils stringFromDate:dateEightHoursAhead dateFormat:@"dd-MM-yyyy"];
    
}

+(BOOL)isDatePast:(NSString *)date format:(NSString *)format{
    
    BOOL inPast = YES;
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:format];
    NSDate* enteredDate = [df dateFromString:date];
    NSDate * today = [df dateFromString:[df stringFromDate:[NSDate date]]];
    
    int daysToAdd = 1;
    
    NSComparisonResult result = [enteredDate compare:today];
    switch (result)
    {
        case NSOrderedAscending:
            
            inPast = NO;
            break;
        case NSOrderedDescending:
            
            inPast = YES;
            break;
        case NSOrderedSame:
            inPast = YES;
            break;
        default:
            break;
    }
    
    return inPast;
}


+(BOOL)isDateFuture:(NSString *)date{
    
    BOOL inPast = YES;
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy"];
    NSDate* enteredDate = [df dateFromString:date];
    NSDate * today = [NSDate date];
    NSComparisonResult result = [enteredDate compare:today];
    switch (result)
    {
        case NSOrderedAscending:
            
            inPast = YES;
            break;
        case NSOrderedDescending:
            
            inPast = NO;
            break;
        case NSOrderedSame:
            inPast = YES;
            break;
        default:
            break;
    }
    
    return inPast;
}

+(BOOL)isCharacterNumber:(NSString *)character{
    
    BOOL isNumber = YES;
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    
    for (int i = 0; i < [character length]; i++)
    {
        unichar c = [character characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            isNumber =  NO;
        }
    }
    
    return isNumber;
}

+(BOOL)isNumber:(NSString *)character{
    
    BOOL isNumber = YES;
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@".0123456789"];
    
    for (int i = 0; i < [character length]; i++)
    {
        unichar c = [character characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            isNumber =  NO;
        }
    }
    
    return isNumber;
}

+(BOOL)isAlpha:(NSString *)character{
    
    BOOL isNumber = YES;
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "];
    
    for (int i = 0; i < [character length]; i++)
    {
        unichar c = [character characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            isNumber =  NO;
        }
    }
    
    return isNumber;
}

+(BOOL)isAlphaNumerique:(NSString *)character{
    
    BOOL isNumber = YES;
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"];
    
    for (int i = 0; i < [character length]; i++)
    {
        unichar c = [character characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            isNumber =  NO;
        }
    }
    
    return isNumber;
}


+(void) showAlertView:(NSString *)message{
    
       [[[UIAlertView alloc]initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    
}

+(BOOL)isValideEmail:(NSString *)email{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}

+(BOOL)isValidePhone:(NSString *)phone{
    
    NSString *phoneRegex = @"^((05)|(06))[0-9]{8}$";
    
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phone];
    
}

+(NSString*) removeHTMLTags:(NSString*) html{
    
    NSMutableString *returnString = [NSMutableString string];
    NSScanner *scanner = [NSScanner scannerWithString:html];
    while(![scanner isAtEnd]) {
        NSString *content=@"";
        [scanner scanUpToString:@"<" intoString:&content];
        NSString *tmp = @"";
        [scanner scanUpToString:@">" intoString:&tmp];
        if([tmp hasPrefix:@"<br"])[returnString appendString:@""];
        if([tmp hasPrefix:@"<p"])[returnString appendString:@"\n"];
        if([tmp hasPrefix:@"<div"])[returnString appendString:@"\n"];
        if([tmp hasPrefix:@"<p"])[returnString appendString:@"\n"];
        [scanner scanString:@">" intoString:nil];
        [returnString appendString:content];
    }
    return returnString;
}

+(float)getNumber:(NSString *)number{
    
    if (number == nil) {
        
        number = @"0";
        
    }
    
    NSLocale * locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    
    number = [number stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    NSDecimalNumber * decimalNumber = [NSDecimalNumber decimalNumberWithString:number locale:locale] ;
    
    
    NSString * temp = [NSNumberFormatter localizedStringFromNumber:decimalNumber numberStyle:NSNumberFormatterDecimalStyle];
    
   if (decimalNumber == [NSDecimalNumber notANumber]) {
        
        return 0;
    }
    
    CGFloat f = [decimalNumber floatValue];
    
    f  = roundf( f*100.0)/100.0;
    
    NSString * test = [NSString stringWithFormat:@"%.1f", f];
    
    float testFloat = [test floatValue];
    
    testFloat = testFloat - 0.00000002;
    
   return  f;
}


+(BOOL)checkString:(NSString *)originalString  contains :(NSString *)subString{
    BOOL found = NO;
    if ([originalString rangeOfString:subString options:NSWidthInsensitiveSearch].location != NSNotFound) {
        found = YES;
        
    }
    return found;
}

#pragma mark --
#pragma mark Page Control

+(void)setUpPageControl:(SMPageControl *)pageControl pages:(NSInteger)pages{
    
    pageControl.numberOfPages = pages;
    pageControl.indicatorMargin = -15;
    pageControl.userInteractionEnabled = NO;
    pageControl.pageIndicatorImage = [UIImage imageNamed:@"bcp_slide_inactif"];
    pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"bcp_slide_actif"];
   // [pageControl sizeToFit];
    
}

#pragma mark --
#pragma mark UIView

+(void)removeSubviews:(UIView *)parentView{
    
    
    for (UIView * view in [parentView subviews]) {
        
        [view removeFromSuperview];
        
    }
    
}

+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

#pragma mark --
#pragma mark Prefs 

+(void) saveObject:(id) object key:(NSString *)key{
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:object forKey:key];
    
    [defaults synchronize];
    
}

+ (id) getSavedObject:(NSString *)key{
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
   return  [defaults objectForKey:key];
    
    
}

+ (void) saveMutableObject:(id) object key:(NSString *)key{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:object];
    
    [userDefaults setObject:data forKey:key];
    
    [userDefaults synchronize];
}

+ (id) getSavedMutableObject:(NSString *)key{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData * data = [userDefaults objectForKey:key];
    
    NSArray * array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    
    return array;
}

#pragma mark-- 
#pragma mark Date 

+(NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat{
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:dateFormat];
  
    return [df stringFromDate:date];
    
}

+(NSDate *)dateFromString:(NSString *)date dateFormat:(NSString *)dateFormat{
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:dateFormat];
    
    return [df dateFromString:date];
    
}

#pragma mark--
#pragma mark ImageRotate

+(UIImage *)rotateImage:(UIImage*) src  orientation:(UIImageOrientation) orientation
{
    UIGraphicsBeginImageContext(src.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, radians(90));
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, radians(-90));
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, radians(90));
    }
    
    [src drawAtPoint:CGPointMake(0, 0)];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}
#pragma mark --
#pragma mark PDF 

+(void)createPDFfromUIView:(UIScrollView *)aView saveToDocumentsWithFileName:(NSString*)aFilename
{
    // Creates a mutable data object for updating with binary data, like a byte array
    NSMutableData *pdfData = [NSMutableData data];
    
    // Get Scrollview size
    CGRect scrollSize = CGRectMake(aView.frame.origin.x,aView.frame.origin.y,aView.contentSize.width,aView.contentSize.height);
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, scrollSize, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    
    [aView.layer renderInContext:pdfContext];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    // Retrieves the document directories from the iOS device
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    NSLog(@"documentDirectoryFileName: %@",documentDirectoryFilename);
}

#pragma mark --
#pragma mark Colors 

+(void)addBorder:(UIView *)view borderColor:(UIColor * )borderColor width:(CGFloat)width{
    
    view.layer.borderColor = borderColor.CGColor;
    view.layer.borderWidth = width;
}

+(void)addGradient:(UIView *)view startColor:(UIColor *)startColor endColor:(UIColor *)endColor{
    
    
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    
    gradient.frame = view.bounds;
    
    gradient.colors = [NSArray arrayWithObjects:(id)[startColor CGColor], (id)[endColor CGColor], nil];
    
    [view.layer insertSublayer:gradient atIndex:0];
    
}

//Document directory
+ (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}


#pragma mark --
#pragma mark Network 

+ (BOOL)isOnline{
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

+(NSString *)getTimeStamp{
    
    
    
    return  [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
    
}

@end
